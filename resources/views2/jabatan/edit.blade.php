@extends('layouts.master')

@section('title')
<title>Edit Biodata</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
   <div class="page-content">
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                  <h4 class="mb-sm-0 font-size-18">Edit Jabatan</h4>
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('jabatan.index') }}">Jabatan</a></li>
                        <li class="breadcrumb-item active">Edit Jabatan</li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <!-- end page title -->
         <div class="row">
            <div class="col-lg-12">
               <form method="POST" action="/jabatan/update/{{ $detail->id }}">
                  @csrf
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-lg-8">
                              <label class="form-label">Site</label>
                              <select name="site" id="site" class="form-control {{ $errors->has('site') ? 'is-invalid':'' }}" required>
                                 <option value="">--Choose--</option>
                                 @foreach($site as $row)
                                 <option value="{{ $row->id }}" @if($row->id == $detail->id_site) selected @endif>{{ $row->site_name }}</option>
                                 @endforeach
                              </select>
                              <p class="text-danger">{{ $errors->first('site') }}</p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-8">
                              <label class="form-label">Position Name</label>
                              <input class="form-control {{ $errors->has('position_name') ? 'is-invalid':'' }}" type="text" name="position_name" id="position_name" value="{{ $detail->position_name }}" required>
                              <p class="text-danger">{{ $errors->first('position_name') }}</p>
                           </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <label class="form-label">Contract</label>
                                <input class="form-control {{ $errors->has('contract') ? 'is-invalid':'' }}" type="text" name="contract" id="contract" required>
                                <p class="text-danger">{{ $errors->first('contract') }}</p>
                            </div>
                        </div>

			            <div class="row">
                           <div class="col-lg-8">
                              <label class="form-label">Status</label>
                              <select name="flag" id="flag" class="form-control {{ $errors->has('flag') ? 'is-invalid':'' }}" required>
                                 <option value="1" @if($detail->flag == 1) selected @endif>Active</option>
                                 <option value="0" @if($detail->flag == 0) selected @endif>Non Active</option>
                              </select>
                              <p class="text-danger">{{ $errors->first('flag') }}</p>
                           </div>
                        </div>
                        <div>
                           <button type="submit" class="btn btn-primary">Submit</button>
                           <a href="{{ route('jabatan.index') }}" class="btn btn-danger">Cancel</a>
                        </div>
                     </div>
                  </div>
            </div>
            </form>
         </div>
      </div>
      <!-- end row -->
   </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
</div>
<!-- end main content-->
<!-- /.control-sidebar -->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/jabatan/edit.js') }}"></script>
@endsection
