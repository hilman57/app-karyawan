@extends('layouts.master')

@section('title')
    <title>User Roles</title>
@endsection

@section('content')
<!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">List Role</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Utilitas</a></li>
                                        <li class="breadcrumb-item active">Role</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    @include ('partials.messages')
                                    <div class="col-md-6">
                                        <h4 class="card-title">User Role</h4><br>
                                        <form role="form" action="{{ route('user.set_role', $user->uuid) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="PUT">
                                            @include ('partials.messages')
                                            <div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <td>:</td>
                                                        <td>{{ $user->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email</th>
                                                        <td>:</td>
                                                        <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Role</th>
                                                        <td>:</td>
                                                        <td>
                                                            @foreach ($roles as $row)
                                                            <input type="radio" name="role"
                                                                {{ $roleName == $row ? 'checked':'' }}
                                                                value="{{ $row }}"> {{  $row }} <br>
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                </thead>
                                            </table>
                                            </div>
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-success">
                                                    Set Role
                                                </button>
                                                <a href="{{ route('user.index') }}" class="btn btn-primary">Kembali</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->

                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
        </div>
        <!-- end main content-->
@endsection
