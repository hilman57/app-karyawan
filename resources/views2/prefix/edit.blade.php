@extends('layouts.master')

@section('title')
   <title>Edit Biodata</title>
@endsection

@section('content')
<!-- ============================================================== -->
         <!-- Start right Content here -->
         <!-- ============================================================== -->
         <div class="main-content">

         <div class="page-content"> 
               <div class="container-fluid">

                  <!-- start page title -->
                  <div class="row">
                     <div class="col-12">
                           <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                              <h4 class="mb-sm-0 font-size-18">Edit prefix</h4>

                              <div class="page-title-right">
                                 <ol class="breadcrumb m-0">
                                       {{-- <li class="breadcrumb-item"><a href="javascript: void(0);">Utilitas</a></li> --}}
                                       <li class="breadcrumb-item"><a href="{{ route('prefix.index') }}">prefix</a></li>
                                       <li class="breadcrumb-item active">Edit Biodata</li>
                                 </ol>
                              </div>
                           </div>
                     </div>
                  </div>
                  <!-- end page title -->
                  <div class="row">
                     <div class="col-lg-12">
                        {{-- <form method="POST" action="{{ url('biodata/update')  }}"> --}}
                        <form method="POST" action="{{ route('prefix.update')  }}">
                            @foreach ($menu as $detail)                          
                              @csrf                             
                              <input type="hidden" id="userId" value="{{ $detail->id }}">
                              <input type="hidden" name="id"  value="{{ $detail->id }}">
                              <div class="card">
                                 <div class="card-body">
                                          
                                          <div class="col-lg-8">
                                             <div class="row">
                                                <div class="col">
                                                   <label class="form-label">Site</label>
                                                   <input class="form-control {{ $errors->has('site_id') ? 'is-invalid':'' }}" type="text" name="site_id"  value="{{ $detail->site_id }}" required>
                                                   <p class="text-danger">{{ $errors->first('prefix') }}</p>
                                                   
                                                </div>                                                                                                  
                                             </div>                                                   
                                          </div>
                                          <div class="col-lg-8">
                                             <div class="row">
                                                <div class="col">
                                                   <label class="form-label">Jabatan</label>
                                                   <input class="form-control {{ $errors->has('jabatan_id') ? 'is-invalid':'' }}" type="text" name="jabatan_id"  value="{{ $detail->jabatan_id }}" required>
                                                   <p class="text-danger">{{ $errors->first('prefix') }}</p>
                                                   
                                                </div>                                                                                                  
                                             </div>                                                   
                                          </div>
                                          <div class="col-lg-8">
                                             <div class="row">
                                                <div class="col">
                                                   <label class="form-label">Start From</label>
                                                   <input class="form-control {{ $errors->has('start_from') ? 'is-invalid':'' }}" type="text" name="start_from"  value="{{ $detail->start_from }}" required>
                                                   <p class="text-danger">{{ $errors->first('prefix') }}</p>
                                                   
                                                </div>                                                                                                  
                                             </div>                                                   
                                          </div>
                                          <div class="col-lg-8">
                                             <div class="row">
                                                <div class="col">
                                                   <label class="form-label">Last</label>
                                                   <input class="form-control {{ $errors->has('last') ? 'is-invalid':'' }}" type="text" name="last"  value="{{ $detail->last }}" required>
                                                   <p class="text-danger">{{ $errors->first('prefix') }}</p>
                                                   
                                                </div>                                                                                                  
                                             </div>                                                   
                                          </div>
                                          
                                          <button type="submit" class="btn btn-primary">Submit</button>
                                          <a href="{{ route('prefix.index') }}" class="btn btn-danger">Cancel</a>
                                       </div>
                                       @endforeach
                                 </div>
                              </div>
                           </form>
                     </div>
                  </div>
                  <!-- end row -->
               </div> <!-- container-fluid -->
         </div>
         <!-- End Page-content -->
      </div>
      <!-- end main content-->
<!-- /.control-sidebar -->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/prefix/edit.js') }}"></script>
@endsection