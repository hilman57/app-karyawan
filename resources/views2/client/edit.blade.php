@extends('layouts.master')
@section('title')
<title>Edit Client</title>
@endsection
@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

  <div class="page-content">
    <div class="container-fluid">
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Edit site</h4>

            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="/client-master">Client</a></li>
                <li class="breadcrumb-item active">Edit</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <!-- end page title -->
      <div class="row">
        <div class="col-lg-12">
          <form method="POST" action="/client-master/update/{{ $detail->id }}">
            @csrf
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6">
                    <label class="form-label">Client</label>
                    <input class="form-control {{ $errors->has('client') ? 'is-invalid':'' }}" type="text" name="client" id="client" value="{{ $detail->client }}" required>
                    <p class="text-danger">{{ $errors->first('client') }}</p>
                  </div>
                  <div class="col-lg-6">
                    <label class="form-label">Client Name</label>
                    <input class="form-control {{ $errors->has('clientname') ? 'is-invalid':'' }}" type="text" name="clientname" id="clientname" value="{{ $detail->clientname }}" required>
                    <p class="text-danger">{{ $errors->first('clientname') }}</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <label class="form-label">No Kontrak</label>
                    <input class="form-control {{ $errors->has('no_kontrak') ? 'is-invalid':'' }}" type="text" name="no_kontrak" id="no_kontrak" value="{{ $detail->no_kontrak }}" required>
                    <p class="text-danger">{{ $errors->first('no_kontrak') }}</p>
                  </div>
                  <div class="col-lg-6">
                    <label class="form-label">Last PKWT Number</label>
                    <input class="form-control {{ $errors->has('last_number_pkwt') ? 'is-invalid':'' }}" type="number" name="last_number_pkwt" id="last_number_pkwt" value="{{ $detail->last_number_pkwt }}" required>
                    <p class="text-danger">{{ $errors->first('last_number_pkwt') }}</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <label class="form-label">Valid From</label>
                    <input class="form-control {{ $errors->has('validfrom') ? 'is-invalid':'' }}" type="date" name="validfrom" id="validfrom" value="{{ $detail->validfrom }}" required>
                    <p class="text-danger">{{ $errors->first('validfrom') }}</p>
                  </div>
                  <div class="col-lg-6">
                    <label class="form-label">Valid Until</label>
                    <input class="form-control {{ $errors->has('validuntil') ? 'is-invalid':'' }}" type="date" name="validuntil" id="validuntil" value="{{ $detail->validuntil }}" required>
                    <p class="text-danger">{{ $errors->first('validuntil') }}</p>
                  </div>
                </div>
                <div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="/client-master" class="btn btn-danger">Cancel</a>
                </div>
              </div>
            </div>
        </div>
        </form>
      </div>
    </div>
    <!-- end row -->
  </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
</div>
<!-- end main content-->
<!-- /.control-sidebar -->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/site/edit.js') }}"></script>
@endsection