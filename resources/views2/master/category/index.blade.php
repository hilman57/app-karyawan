@extends('layouts.master')

@section('title')
<title>Data Kategori</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">List Category</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Master</a></li>
                                <li class="breadcrumb-item active">List Category</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">
                            <ul class="nav nav-tabs nav-tabs-custom mb-3" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active mb-2" data-bs-toggle="tab" href="#tab-company" id="showCompany" role="tab">
                                        Company
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-2" data-bs-toggle="tab" href="#tab-helpdesk" id="showHelpdesk" role="tab">
                                        Helpdesk
                                    </a>
                                </li>
                            </ul>

                            @include ('partials.messages')
                            <div class="tab-content">
                                <div id="tab-company" class="tab-pane active" role="tabpanel">
                                    <div class="d-flex">
                                        <div class="col-lg-4 p-1 pe-3">
                                            <div style="background-color: #F8F8FB;" class="card h-100">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Add Company Category</h5>
                                                    <form id="formInputCompany">
                                                        <input type="hidden" id="routeGetDataCompanyCategory" value="{{ route('category.company.get-data-category') }}">
                                                        <input type="hidden" id="routeSaveDataCompanyCategory" value="{{ route('category.company.save') }}">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div>
                                                                        <label class="form-label">Name</label>
                                                                        <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="companyName" required>
                                                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div>
                                                                        <label class="form-label">Description</label>
                                                                        <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" rows="3" id="companyDesc"></textarea>
                                                                        <p class="text-danger">{{ $errors->first('description') }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col">
                                                                <button type="submit" class="btn btn-primary" id="btnSaveCompany"><i class="fas fa-save me-2"></i>Submit</button>
                                                                <button type="button" class="btn btn-light" id="btnClearCompany">Clear</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="col-lg-4 d-flex  align-items-center">
                                                        <h5 class="card-title mb-0">List Data</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-end">
                                                        <div class="search-box me-2 d-inline-block">
                                                            <div class="position-relative">
                                                                <input style="background-color: #eff2f7; border: none;" type="text" id="inputSeacrhCompany" class="form-control" placeholder="Search...">
                                                                <i class="bx bx-search-alt search-icon"></i>
                                                            </div>
                                                        </div>
                                                        <button id="btnSearchCompany" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row" style="padding-top: 10px;" id="notifCompany">
                                            </div>
                                            <table id="table-company" class="table  dt-responsive nowrap w-100 tabcontent ">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Deskripsi</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>

                                <div class="tab-pane tab-helpdesk" id="suspended" role="tabpanel">
                                    <div class="d-flex">

                                        <div class="col-lg-4 p-1 pe-3">
                                            <div style="background-color: #F8F8FB;" class="card h-100">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Add Help Desk Category</h5>
                                                    <form id="formInputHelpdesk">
                                                        <input type="hidden" id="routeGetDataHelpdeskCategory" value="{{ route('category.helpdesk.get-data-category') }}">
                                                        <input type="hidden" id="routeSaveDataHelpdeskCategory" value="{{ route('category.helpdesk.save') }}">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div>
                                                                        <label class="form-label">Name</label>
                                                                        <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="helpdeskName" required>
                                                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary" id="btnSaveHelpdesk">Submit</button>
                                                            <button type="button" class="btn btn-light" id="btnClearHelpdesk">Clear</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="col-lg-4 d-flex  align-items-center">
                                                        <h5 class="card-title mb-0">List Data</h5>
                                                    </div>
                                                    <div class="d-flex justify-content-end">
                                                        <div class="search-box me-2 d-inline-block">
                                                            <div class="position-relative">
                                                                <input style="background-color: #eff2f7; border: none;" type="text" id="inputSeacrhHelpdesk" class="form-control" placeholder="Search...">
                                                                <i class="bx bx-search-alt search-icon"></i>
                                                            </div>
                                                        </div>
                                                        <button id="btnSearchHelpdesk" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row" style="padding-top: 10px;" id="notifHelpdesk">
                                            </div>
                                            <table id="table-helpdesk" class="table  table-bordered dt-responsive nowrap w-100 tabcontent">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/category/index.js') }}"></script>
@endsection