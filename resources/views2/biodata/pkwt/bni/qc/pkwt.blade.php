<!DOCTYPE html>
<html>

<head>
  <title>PERJANJIAN KERJA WAKTU TERTENTU</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 9px;
    }
  </style>
</head>

<body>
  <img src="{{ public_path('backend/assets/img_hrd/header.png') }}" alt="header" width="100%">
  <br /><br />
  <div class="container" align="jusitfy">
    <center>
      <h5>PERJANJIAN KERJA WAKTU TERTENTU <br /> <small>No. {{ $param['last_number_pkwt'] }}/{{ $param['singkatan'] }}-BNI/PKWT/{{ $param['romans'][date('m')] }}/{{ date('Y') }}</small></h5>
    </center>
    <div class="m-2">
      <p style="margin-bottom: -2px;"><u>PIHAK PERTAMA</u></p>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-2">Nama</div>
        <div class="col-1">:</div>
        <div class="col-8">Herman Julianto</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-2">Jabatan</div>
        <div class="col-1">:</div>
        <div class="col-8">Direktur HRD</div>
      </div>
      <br />
      <p>Dalam hal ini bertindak untuk dan atas nama PT. ASEANINDO NETWORKS SOLUTIONS, berkedudukan di Gedung Setiabudi Building II Lt.6,JL. HR. Rasuna Said Kav.62 Jakarta 12920.</p>
      <p style="margin-bottom: -2px;"><u>PIHAK KEDUA</u></p>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-2">Nama</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->nama_lengkap }}</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-2">Tempat dan tanggal lahir</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->tempat }}, {{ $param['tgl_lahir'] }}</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-2">Alamat</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->alamat }}</div>
      </div>
      <br />
      <p>Dalam hal ini bertindak untuk dan atas nama sendiri, selanjutnya dalam perjanjian ini disebut sebagai PIHAK KEDUA.</p>
      <p>PIHAK PERTAMA dan PIHAK KEDUA (selanjutnya secara bersama – sama “PARA PIHAK”), SETUJU untuk mengikatkan diri pada Perjanjian ini.</p>
      <p>PARA PIHAK telah mengetahui dan sepakat :</p>
      <div class="row">
        <div class="col-1">a. </div>
        <div class="col-11">
          Bahwa PIHAK PERTAMA terikat atas Perjanjian Kerjasama Pemborong Pekerjaan Penjualan & Administrasi dengan PT Bank Negara Indonesia (Persero) TBK, (selanjutnya disebut dengan “Perusahaan”) SPK Nomor {{ $param['user']->contract }}, tertanggal {{ $param['date_pkwt'] }}
        </div>
      </div>
      <div class="row">
        <div class="col-1">b. </div>
        <div class="col-11">
          Bahwa untuk melaksanakan perjanjian tersebut dalam huruf (a) diatas, PIHAK PERTAMA membuat Perjaanjian Kerja Waktu Tertentu dengan PIHAK KEDUA yang mana PIHAK KEDUA setuju dan sepakat akan bekerja untuk PIHAK PERTAMA dan atau Perusahaan.
        </div>
      </div>
      <div class="row">
        <div class="col-1">c. </div>
        <div class="col-11">
          Bahwa walaupun Perjanjian memiliki jangka waktu berlaku sebagaimana ditetapkan dibawah , Perjanjian ini akan berakhir dengan sendirinya jika Perjanjian Kerjasama Pemborong Pekerjaan Penjualan & Administrasi dengan PT Bank Negara Indonesia (Persero) TBK, (selanjutnya disebut dengan “Perusahaan”) SPK Nomor {{ $param['user']->contract }}, tertanggal {{ $param['date_pkwt'] }} antara PIHAK PERTAMA dan Perusahaan berakhir , tanpa ada kewajiban dari PARA PIHAK untuk memberikan ganti kerugian dalam bentuk apapun kepada lainnya.
        </div>
      </div>
      <p>PARA PIHAK sepakat untuk melaksanakan Perjanjian ini dengan ketentuan – ketentuan sebagai berikut :</p>
      <!-- Point 1 -->
      <div class="row">
        <div class="col-12">1. </div>
        <!-- <div class="col-11"><u>JUDUL</u></div> -->
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-2"><strong>Posisi/Jabatan</strong></div>
        <div class="col-9">
          {{ $param['user']->position_name }} yang diperbantukan untuk menjual salah satu produk BANK NEGARA INDONESIA
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-2"><strong>Periode Waktu</strong></div>
        <div class="col-9">
          {{ $param['date_pkwt'] }} s/d {{ $param['valid_until'] }}
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-2"><strong>Tunjangan</strong></div>
        <div class="col-9">
          Rp. {{ number_format($param['user']->gaji) }}
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-2"><strong>Jam Kerja</strong></div>
        <div class="col-9">
          Senin sampai dengan Jumat : jam 08.00 – 17.00 WIB <br />
          Hari Sabtu : Jam 08.00 – 13.00 WIB <br />
          atau mengikuti peraturan yang disesuaikan dengan jadwal kerja yang berlaku di tempat Pihak Kedua diperbantukan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-2"><strong>Tunjangan THR</strong></div>
        <div class="col-9">
          Apabila PIHAK KEDUA telah bekerja selama 1 (satu) bulan atau lebih, PIHAK KEDUA akan menerima Tunjangan Hari Raya (THR) dengan jumlah yang setara dengan 1 (satu) bulan upah bruto atau perhitungan dilakukan secara prorata atau proposional, yang akan dibayarkan 2 (dua) minggu sebelum jatuh tempo hari raya keagamaan setiap tahunnya. Sesuai dengan PERMENAKER No. 06/Men/2016, ketentuan ini tidak berlaku bagi pekerja yang hubungan kerja untuk waktu tertentu-nya berakhir sebelum tanggal jatuh tempo hari raya keagamaan tersebut. Hari raya keagamaan yang dimaksud dalam ayat ini disepakati adalah Hari Raya Iedul Fitri.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-2"><strong>BPJS</strong></div>
        <div class="col-9">
          Sesuai dengan Keputusan Menteri Tenaga Kerja (MENAKER SE. 1 /MEN/III/2014), program JKK, JKM, JHT = 6,24%.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-2"><strong>Lembur</strong></div>
        <div class="col-9">
          Kerja lembur adalah kerja yang dilakukan melebihi jam kerja dan hanya dilakukan atas perintah pihak klien dimana Pihak Pertama diperbantukan.
        </div>
      </div>
      <!-- Point 2 -->
      <div class="row">
        <div class="col-1">2. </div>
        <div class="col-11"><u>TUGAS DAN KEWAJIBAN</u></div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Memenuhi target penjualan yang telah ditentukan oleh BNI.
        </div>
      </div>
      <br /><br /><br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Pihak Kedua berkewajiban menjaga kerahasiaan Pihak Pertama dan Pihak BNI terhadap kerugian dan atau tuntutan yang timbul akibat penyalahgunaan Trade Secret, Patent, Copy Right atau pelanggaran hak-hak kepemilikan dari informasi dan atau bahan-bahan yang diberikan oleh nasabah atau customer atau pengguna jasa.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Pihak Kedua akan dievaluasi pekerjaannya pada 3 bulan pertama sejak tanggal mulai bekerja.
        </div>
      </div>
      <!-- Point 3 -->
      <div class="row">
        <div class="col-1">3. </div>
        <div class="col-11"><u>LARANGAN DAN SANKSI</u></div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Surat Peringatan (SP) dikeluarkan dengan berdasarkan kepada bobot dari pada permasalahan/ pelanggaran yang terjadi.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Untuk setiap SP yang dikeluarkan, apabila PIHAK KEDUA dalam bulan berikutnya atau pada kesempatan berikutnya dapat memperbaiki kesalahannya maka semua SP yang telah dikeluarkan dan diberlakukan bagi pihak Kedua tersebut akan gugur. Jangka waktu evaluasi untuk gugurnya SP tersebut adalah selama 6 bulan berjalan, dan dengan sendirinya surat peringatan akan gugur saat memasuki bulan ke 7.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Sanksi Fraud Case : Pemberian SP tergantung dari bobot kesalahan atau terminasi tanpa ada ganti kerugian dari Pihak Pertama. Pihak Kedua akan dikenakan uang penggantian sebesar kerugian timbul yang akan dipotong dari gaji Pihak Kedua, apabila Pihak Kedua mengundurkan diri dari pekerjaannya serta terindikasi adanya Fraud Case dan apabila Pihak Kedua dikenakan terminasi akibat adanya Fraud Case, maka dengan ini Pihak Kedua memberikan izin kepada Pihak Pertama untuk melakukan penahanan terhadap komisi, maupun gaji Pihak Kedua dalam jangka waktu tertentu.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Tingkatan SP 1 menjadi SP 2 atau SP 3 dapat dikeluarkan dalam jangka waktu penentuan target (dalam 1 minggu dan/atau 2 minggu), selama tidak lebih dari 6 bulan sejak dikeluarkannya SP 1.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Apabila Pihak Kedua menggunakan fasilitas kantor untuk kepentingan pribadi (Personal Call) diluar aturan yang telah diberlakukan diatas maka akan dikenakan SP.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-10">
          SP akan diberikan apabila melanggar Peraturan Perusahaan.
        </div>
      </div>
      <!-- Point 4 -->
      <div class="row">
        <div class="col-1">4. </div>
        <div class="col-11"><u>KETERLAMBATAN:</u></div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Apabila keterlambatan terjadi tiga kali dalam kurun waktu satu bulan tanpa alasan yang dapat diterima maka akan dikenakan SP 1.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Apabila keterlambatan terjadi hingga 5 kali dalam dua bulan tanpa alasan yang dapat diterima maka akan dikenakan SP 2.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Apabila masih terjadi keterlambatan lebih dari 5 kali dalam kurun waktu dua bulan tanpa alasan yang dapat diterima maka akan dikenakan SP 3.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Bila terjadi absent tanpa alasan yang jelas Pihak kedua dianggap mangkir, kecuali dalam hal Pihak Kedua sakit yang disertai dengan Surat Keterangan Dokter (SKD) yang benar dan sah dan diserahkah kepada Perusahaaan pada hari pertama Pihak Kedua kembali bekerja. Surat Keterangan tersebut wajib dilampirkan bersamaan dengan pengiriman absensi kerja Pihak Pertama. Jika terjadi pada bulan kedua maka akan diberikan Surat Peringatan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Dan apabila Pihak Kedua tidak masuk kerja wajib memberitahukan via telpon, sebelum pukul 10.00
        </div>
      </div>
      <!-- Point 5 -->
      <div class="row">
        <div class="col-1">5. </div>
        <div class="col-11">
          <u>TIDAK HADIR KERJA</u>
          <p>Apabila Pihak Kedua melakukan absent, yaitu meninggalkan kantor tanpa ijin dari Manager/atasan yang bersangkutan dengan alasan yang tidak dapat diterima maka akan dikenakan SP dengan ketentuan:</p>
          <p align="center">
            <span>
              satu kali mangkir SP 1 <br />
              dua kali mangkir SP 2 <br />
              tiga kali mangkir SP 3
            </span>
          </p>
        </div>
      </div>
      <!-- Point 6 -->
      <div class="row">
        <div class="col-1">6. </div>
        <div class="col-11">
          <u>CUTI</u><br />
          Diatur sesuai dengan ketentuan Peraturan Perusahaan dan Peraturan Ketenagakerjaan.
        </div>
      </div>
      <!-- Point 7 -->
      <div class="row">
        <div class="col-1">7. </div>
        <div class="col-11"><u>7. KETENTUAN LAIN LAIN :</u></div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Pihak Kedua wajib memberitahukan kepada Pihak Pertama apabila ada perubahan alamat tempat tinggal.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Apabila Pihak Kedua dapat memenuhi ketentuan-ketentuan yang ditentukan oleh Klien Pihak Pertama maka perjanjian kerja ini tetap sesuai dengan waktu yang ditentukan pada poin 1 (satu) di atas, akan tetapi jika Pihak Kedua tidak dapat memenuhi ketentuan-ketentuan yang ditentukan oleh klien Pihak Pertama, maka Pihak Pertama dapat membatalkan PKWT ini.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Apabila dikemudian hari ada terjadi pemberhentian kerjasama (MoU) antara Pihak Pertama dan klien Pihak Pertama dan sementara itu Pihak Kedua masih memiliki tenggang waktu dalam perjanjian kerja tersebut maka segala bentuk perjanjian yang telah disepakati antara Pihak Pertama dan Pihak Kedua akan secara otomatis batal (tidak berlaku).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Pihak Kedua mengerti dan menyetujui jika terjadinya pemberhentian kerja sama antara Pihak Pertama dan klien Pihak Pertama, maka Pihak Kedua secara otomatis sudah tidak menjadi karyawan kontrak Pihak Pertama.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Apabila Pihak Kedua berkeinginan untuk mengundurkan diri maka diharuskan membuat surat pengunduran diri selambat-lambatnya 1 (satu) minggu sebelum tanggal terakhir efektif bekerja, dan selanjutnya Pihak Kedua berhak mendapatkan Surat Referensi 1 (satu) bulan dari tanggal efektif bekerja (resign).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-10">
          Bila diperlukan, Pihak Kedua bersedia dan sanggup untuk melakukan pekerjaan diluar jam kerja yang telah ditetapkan atau pada hari libur atas permintaan terlebih dahulu dari klien Pihak Pertama. Dalam hal ini Pihak Pertama akan membantu proses pembayaran uang lembur sesuai dengan peraturan yang berlaku, yang akan dibayarkan 1 (satu) bulan ke depan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-10">
          Pihak Kedua memahami dengan seksama atas target yang diinginkan oleh klien Pihak Pertama baik secara lisan maupun secara tertulis dimana ketentuan dan rincian target tersebut akan diberikan langsung oleh klien Pihak Pertama.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">h. </div>
        <div class="col-10">
          Bilamana ketentuan target yang telah ditentukan oleh klien Pihak Pertama tidak dapat dipenuhi oleh Pihak Kedua, maka ketentuan sanksi yang berlaku sesuai dengan yang tertera pada PKWT ini.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-10">
          Apabila memang Pihak Kedua masih diperlukan oleh klien Pihak Pertama, maka Perjanjian Kerja Waktu Tertentu ini akan diperpanjang sesuai dengan kebutuhan, dan akan diatur dalam lembar tambahan dari Surat Perjanjian ini yang kekuatan hukumnya sama dengan lembar utamanya.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">j. </div>
        <div class="col-10">
          Apabila Pihak Kedua menerima sanksi berupa Surat Peringatan Terakhir (SP 3) maka Pihak Kedua dengan sadar dan menyatakan melepaskan haknya terhadap semua hal yang timbul dari hubungan kerja yang ada seperti pesangon, uang jasa atau ganti rugi dalam bentuk apapun dari Pihak Pertama.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">k. </div>
        <div class="col-10">
          Surat Peringatan Terakhir (SP 3) dapat diberikan langsung kepada Pihak Kedua sesuai dengan bobot pelanggaran dan/atau kesalahan yang dilakukan oleh Pihak Kedua berdasarkan penilaian dan/atau pertimbangan yang diambil oleh Pihak Pertama.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">l. </div>
        <div class="col-10">
          Hal-hal lain yang belum diberlakukan dalam PKWT ini, akan diberlakukan pada Peraturan Perusahaan.
        </div>
      </div>
      <p>
        Pihak Kedua menyadari bahwa Perjanjian Kerja Waktu Tertentu (PKWT) ini akan berakhir dengan sendirinya pada tanggal yang telah tertulis diatas tanpa menerima uang pesangon dan pembayaran lainnya dari Pihak Pertama. Perjanjian Kerja Waktu Tertentu ini (PKWT) ini disetujui oleh kedua belah pihak, dan ditanda tangani pada hari ini tanggal : {{ $param['date_pkwt'] }}
      </p>
      <div class="row">
        <div class="col-4">
          <div align="center">
            <strong>Pihak Kedua</strong>
          </div>
          <p>Saya telah membaca, mengerti dan setuju dengan isi perjanjian ini dan dengan ini pula saya menyatakan bahwa saya tidak akan melakukan tuntutan dalam bentuk apapun terhadap perjanjian setelah berakhir PKWT ini.</p>
        </div>
        <div class="col-2"></div>
        <div class="col-3">
          <div align="center">
            <strong>Pihak Pertama</strong>
          </div>
          <p>PT. ASEANINDO NETWORKS SOLUTIONS</p>
        </div>
        <div class="col-3"></div>
      </div>
      <div class="row">
        <div class="col-4" align="center">
          <br /><br />
          <!-- <div class="box">
          Materai 10.000
        </div> -->
          <p>[Materai 10.000]</p>
          <br /><br />
          <p>(________________________) <br /> {{ $param['user']->nama_lengkap }}</p>
        </div>
        <div class="col-2"></div>
        <div class="col-3" align="center">
          <img src="{{ public_path('backend/assets/img_hrd/cap_ans.png') }}" alt="cap_ans" style="width: 150px; height: 75px; object-fit: cover;">
          <p style="margin-top: 8px;">
            <strong>(Herman Julianto)</strong> <br />
            Direktur HRD
          </p>
        </div>
        <div class="col-3"></div>
      </div>
    </div>
  </div>
</body>

</html>