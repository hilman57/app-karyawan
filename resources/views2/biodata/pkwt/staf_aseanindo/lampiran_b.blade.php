<!DOCTYPE html>
<html lang="en">

<head>
  <title>LAMPIRAN B</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container" align="justify">
    <center>
      <h5>
        <u><strong>LAMPIRAN B</strong></u>
        <p><strong>SURAT PERNYATAAN <br /> KERAHASIAAN PERBANKAN</strong></p>
      </h5>
    </center>
    <div class="container">
      <p>Saya yang bertanda tangan dibawah ini</p>
      <div class="row">
        <div class="col-1">Nama</div>
        <div class="col-11">: {{ $param['user']->nama_lengkap }}</div>
      </div>
      <div class="row">
        <div class="col-1">Alamat</div>
        <div class="col-11">: {{ $param['user']->alamat }}</div>
      </div>
      <br />
      <p>Sehubungan dengan tugas yang diberikan oleh <strong>PT. Aseanindo Network Solutions</strong> kepada saya dan dikarenakan pemberian tugas ini akan memberikan peluang secara langsung maupun tidak langsung untuk mengetahui informasi yang dianggap rahasia baik menurut Undang-undang Perbankan yang berlaku di Republik Indonesia secara umum ataupun yang dianggap rahasia oleh Klien atau afiliasinya seperti para customer , klien serta para vendor, yang sekiranya dapat memberikan suatu keuntungan bagi pihak lainnya, namun tidak terbatas pada rencana-rencana usaha atau metode serta strategi pemasaran, baik dalam masa penugasan saya maupun setelah masa penugasan berakhir, maka dengan ini saya menyatakan hal-hal sebagai berikut:</p>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">Setuju untuk tidak memberitahukan atau menggunakan informasi rahasia tersebut, dan dengan segera akan mengembalikan kepada klien semua dokumen dan bahan-bahan lainnya baik berupa soft copy maupun hard copy yang merupakan milik klien atau afiliasinya berkaitan dengan hal tersebut diatas.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Akan menjaga kerahasiaan informasi dimaksud baik selama masa saya menjalankan tugas ataupun sampai kapan waktupun juga, dan dalam saya mempunyai intreprestasi yang berbeda mengenai kerahasiaan informasi dimaksud maka saya dengan ini akan mengacu kepada intreprestasi yang diberikan oleh klien.</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">Selama masa penugasan saya dalam memberikan pelayanan kepada klien saya akan memberitahukan dengan segera dan menyerahkan kepada klien kepentingan saya terhadap setiap ciptaan, pengembangan atau penemuan yang dibuat atau dilakukan oleh saya, baik secara sendiri atau bersama-sama dengan pihak lain, yang timbul sehubungan dengan penugasan saya tersebut.</div>
      </div>
      <div class="row">
        <div class="col-1">4.</div>
        <div class="col-11">Membantu klien selama jangka waktu penugasan saya dan di kemudian hari sehubungan dengan proses hukum yang berkaitan dengan ciptaan tersebut, pengembangan atau penemuan serta untuk memperoleh perlindungan hukum mengenai hal tersebut.</div>
      </div>
      <p>Demikianlah Surat Pernyataan ini saya buat dengan sebenar-benarnya dalam keadaan sadar dan tidak ada pemaksaan dari pihak manapun juga, Pernyataan dapat dipergunakan dimana perlu dan akan menjadi bagian yang tidak terpisahkan dari Surat Perjanjian Kerja Waktu Tertentu yang telah saya tandatangani dan akan saya jadikan pegangan dalam menjalankan tugas-tugas saya sebagai {{ $param['user']->position_name }}</p>
      <p>Ditandatangani di Jakarta</p>
      <p>Tanggal, {{ date('d-m-Y') }}</p>
      <br /><br /><br /><br />
      <p>Tanda Tangan <br /> Nama: {{ $param['user']->nama_lengkap }}</p>
    </div>
  </div>
</body>

</html>