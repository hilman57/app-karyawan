<!DOCTYPE html>
<html lang="en">

<head>
  <title>PERJANJIAN KERAHASIAAN KARYAWAN</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    body {
      font-size: 11px;
    }

    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }
  </style>
</head>

<body>
  <div class="container" align="justify">
    <center>
      <h5>
        <u><strong>PERJANJIAN KERAHASIAAN KARYAWAN</strong></u>
      </h5>
    </center>
    <div>
      <p>Saya {{ $param['user']->nama_lengkap }} pemegang Kartu Tanda Penduduk No. : {{ $param['user']->ktp }} <br /> memahami bahwa sebagai karyawan dari <strong>PT ASEANINDO NETWORKS SOLUTIONS</strong> dalam melaksanakan tugasnya untuk memberikan Jasa, Saya akan menerima akses untuk atau menerima informasi rahasia mengenai Aseanindo dan Kliennya dan Saya dengan ini menerima dan menyetujui hal-hal sebagai berikut:</p>
      <p>Saya tidak akan, baik selama atau setelah masa kerja Saya dengan Aseanindo, menyebarluaskan kepada siapapun, atau menggunakan setiap rahasia dagang atau ciptaan, pengembangan atau penemuan atau setiap informasi rahasia mengenai kegiatan operasional, kesepakatan, transaksi, hubungan, bisnis atau keuangan Aseanindo atau Kliennya. <strong>Setiap rahasia dagang atau ciptaan, pengembangan atau penemuan selama saya bekerja di Aseanindo menjadi hak milik Aseanindo</strong>.</p>
      <p>Saya tidak akan menyebarluaskan informasi mengenai Aseanindo dan Kliennya kepada pihak ketiga tanpa persetujuan dari Aseanindo dan Kliennya secara tertulis dimana kewajiban melaksanakan hal tersebut sesuai dengan ketentuan hukum yang berlaku.</p>
      <p>Saya tidak akan menggandakan setiap dokumen atau informasi yang berhubungan dengan Aseanindo, bisnis Aseanindo, Klien atau bisnis dari Klien untuk dipergunakan sendiri dan berkewajiban untuk setiap saat apabila diminta oleh Aseanindo, menghancurkan (sebagaimana dibuktikan dengan suatu pernyataan tertulis) atau mengembalikan segera kepada Aseanindo setiap dan seluruh informasi yang Saya miliki, atau Saya kuasai.</p>
      <p>Saya berkewajiban mengikuti instruksi, kebiasaan dan/atau kebijakan yang dikeluarkan oleh Aseanindo dari waktu ke waktu (termasuk namun tidak terbatas untuk menjaga meja kerja Saya, di manapun berada, bersih dari segala dokumen dan barang-barang lainnya yang berkaitan dengan rahasia dagang atau setiap informasi rahasia mengenai kegiatan operasional, kesepakatan, transaksi, hubungan, bisnis atau keuangan Aseanindo atau Klientnya selama Saya tidak berada di meja kerja Saya tersebut) untuk tujuan menjalankan tugas-tugas Saya kepada Perusahaan dan setuju bahwa Saya akan menjaga segala tindakan Saya agar Perusahaan tidak melanggar tugas dan kewajiban berdasarkan ketentuan Perjanjian ini.</p>
      <p>Saya mengerti bahwa setiap pelanggaran atas Perjanjian Kerahasiaan ini akan berakibat pada diajukannya tindakan hukum terhadap Saya.</p>
      <p>Perjanjian Kerahasiaan Karyawan ini tunduk dan diatur pada ketentuan hukum negara Republik Indonesia.</p>
      <div class="row">
        <div class="col-7">
          <br /><br />
          <p>[materai Rp 10.000]</p>
          <p>___________________ <br /> Nama: {{ $param['user']->nama_lengkap }}</p>
          <p><u>{{ date('d-m-Y') }}</u> <br /> Tanggal</p>
        </div>
        <div class="col-5">
          <img src="{{ public_path('backend/assets/img_hrd/cap_ans.png') }}" alt="cap_ans" style="width: 150px; height: 75px; object-fit: cover; margin-left: -20px;">
          <p>___________________ <br /> <strong>Saksi: Herman Julianto</strong></p>
          <p><u>{{ date('d-m-Y') }}</u> <br /> Tanggal</p>
        </div>
      </div>
    </div>
  </div>
</body>

</html>