<!DOCTYPE html>
<html>

<head>
  <title>PERJANJIAN KERJA WAKTU TERTENTU</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 9px;
    }
  </style>
</head>

<body>
  <img src="{{ public_path('backend/assets/img_hrd/header.png') }}" alt="header" width="100%">
  <br /><br />
  <div class="container" align="jusitfy">
    <center>
      <h5>PERJANJIAN KERJA WAKTU TERTENTU <br /> <small>{{ $param['last_number_pkwt'] }}/PKWT-AKS/{{ date('Y') }}</small></h5>
    </center>
    <div class="m-2">
      <p align="jusitfy">
        Perjanjian Kerja Waktu Tertentu berdasarkan jangka waktu ini (selanjutnya disebut “PERJANJIAN”) dibuat dan ditandatangani pada {{ $param['date_pkwt'] }} oleh dan diantara:
      </p>
      <div class="row">
        <div class="col-3">Nama Perusahaan</div>
        <div class="col-1">:</div>
        <div class="col-8">PT. Aseanindo Kapita Solusi</div>
      </div>
      <div class="row">
        <div class="col-3">Alamat</div>
        <div class="col-1">:</div>
        <div class="col-8">Gd. Setiabudi II Lt.6 Ruang 601 JL. HR Rasuna Said Kav. 62 – Kuningan Jakarta</div>
      </div>
      <p align="jusitfy">
        PT. Aseanindo Kapita Solusi adalah perseroan terbatas yang didirikan menurut hukum Indonesia yang salah satu jenis usahanya adalah penyedia jasa pekerja, selanjutnya disebut sebagai PIHAK PERTAMA:
      </p>
      <div class="row">
        <div class="col-3">Nama</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->nama_lengkap }}</div>
      </div>
      <div class="row">
        <div class="col-3">Jenis Kelamin</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->jenis_kelamin == 'P' ? 'Perempuan' : 'Laki-laki' }}</div>
      </div>
      <div class="row">
        <div class="col-3">Tempat dan tanggal lahir</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->tempat }}, {{ $param['tgl_lahir'] }}</div>
      </div>
      <div class="row">
        <div class="col-3">Alamat tempat tinggal</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->alamat }}</div>
      </div>
      <p align="jusitfy">Dalam hal ini bertindak untuk dan atas nama sendiri, selanjutnya dalam perjanjian ini disebut sebagai PIHAK KEDUA</p>
      <p align="jusitfy">PIHAK PERTAMA dan PIHAK KEDUA (selanjutnya secara bersama-sama disebut “PARA PIHAK”), SETUJU untuk mengikatkan diri pada Perjanjian ini.</p>
      <p align="jusitfy">PARA PIHAK telah mengetahui dan sepakat:</p>
      <div class="row">
        <div class="col-1">a.</div>
        <div class="col-11">bahwa PIHAK PERTAMA terikat dalam Perjanjian Pemborongan Pekerjaan Penagihan hutang dengan The Hongkong and Shanghai Banking Corporation Limited, Indonesia (selanjutnya disebut dengan “Perusahaan”) <strong>No {{ $param['user']->contract }} tertanggal {{ $param['valid_from'] }}</strong>.</div>
      </div>
      <div class="row">
        <div class="col-1">b.</div>
        <div class="col-11">bahwa untuk melaksanakan perjanjian tersebut dalam huruf (a) di atas, PIHAK PERTAMA membuat Perjanjian Kerja Waktu Tertentu dengan PIHAK KEDUA yang mana PIHAK KEDUA setuju dan sepakat akan bekerja untuk PIHAK PERTAMA dan atau Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">c.</div>
        <div class="col-11">bahwa walaupun Perjanjian memiliki jangka waktu berlaku sebagaimana ditetapkan di bawah ini, Perjanjian ini akan berakhir dengan sendirinya jika Perjanjian Pemborongan Pekerjaan Penagihan hutang antara PIHAK PERTAMA dan Perusahaan berakhir, tanpa ada kewajiban dari PARA PIHAK untuk memberikan ganti kerugian dalam bentuk apapun kepada pihak lainnya.</div>
      </div>
      <p align="jusitfy">PARA PIHAK sepakat untuk melaksanakan Perjanjian ini dengan ketentuan-ketentuan sebagai berikut:</p>
      <center>Pasal 1</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">Perjanjian ini dibuat terhitung mulai tanggal {{ $param['date_pkwt'] }} dan berakhir demi hukum tanggal {{ $param['valid_until'] }} tanpa ada kewajiban dari PIHAK PERTAMA untuk memberikan ganti kerugian dalam bentuk apapun</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Dengan persetujuan tertulis dari PARA PIHAK, kesepakatan kerja yang diatur dalam Perjanjian ini dapat diperpanjang sesuai dengan ketentuan yang berlaku.</div>
      </div>
      <center>Pasal 2</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">PIHAK PERTAMA mempekerjakan PIHAK KEDUA dengan status Karyawan Kontrak PIHAK PERTAMA sebagai {{ $param['user']->position_name }} dan pada saat Perjanjian ini ditandatangani, PIHAK KEDUA ditempatkan di kantor Perusahaan yang beralamat di Setiabudi Building Lt.6 Ruang 602A, Jl. HR Rasuna Said Kav.62 - Jakarta.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Bila diperlukan, Perusahaan dapat menempatkan PIHAK KEDUA pada tugas lain sesuai dengan kemampuannya.</div>
      </div>
      <center>Pasal 3</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">PIHAK KEDUA akan menerima upah kotor (bruto) sebesar Rp {{ number_format($param['user']->gaji) }}, <strong>[{{ $param['salary'] }}]</strong> per bulan yang dibayarkan dengan cara transfer ke Rekening Bank atas nama PIHAK KEDUA, dan Pajak Penghasilan dibayar oleh PIHAK KEDUA sesuai dengan ketentuan yang berlaku.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Untuk jangka waktu kerja kurang dari sebulan, PIHAK KEDUA akan menerima upah prorata atau proposional dari jumlah tersebut diatas.</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">Apabila PIHAK KEDUA tidak masuk bekerja tanpa alasan yang sah atau mangkir, PIHAK PERTAMA akan memotong upah PIHAK KEDUA untuk setiap harinya tersebut, kecuali dalam hal PIHAK KEDUA sakit yang disertai dengan Surat Keterangan Dokter (SKD) yang benar dan sah dan diserahkan kepada Perusahaan pada hari pertama PIHAK KEDUA kembali bekerja. Surat Keterangan tersebut wajib dilampirkan bersamaan dengan pengiriman absensi kerja ke PIHAK PERTAMA.</div>
      </div>
      <div class="row">
        <div class="col-1">4.</div>
        <div class="col-11">PIHAK KEDUA wajib mencatatkan absensi kerja, dan setiap perintah dari Perusahaan untuk bekerja Lembur. PIHAK KEDUA harus meminta persetujuan dari Perusahaan atas kelebihan jam kerja tersebut serta mengirimkan Rekapitulasi Absensi dan Lembur tersebut paling lambat tanggal 05 ( lima ) setiap bulannya.</div>
      </div>
      <div class="row">
        <div class="col-1">5.</div>
        <div class="col-11">Apabila PIHAK KEDUA mangkir selama 5 (lima) hari kerja atau lebih berturut-turut tanpa keterangan tertulis yang dilengkapi dengan bukti yang sah dan telah dipanggil 2 (dua) kali secara tertulis yang dikirim ke alamat sesuai catatan yang disampaikan pada PIHAK PERTAMA, dapat diputus hubungan kerjanya karena dikualifikasikan sebagai mengundurkan diri.</div>
      </div>
      <div class="row">
        <div class="col-1">6.</div>
        <div class="col-11">Semua pembayaran hanya dilakukan berdasarkan Form Absensi dan Lembur yang asli dan telah ditandatangani oleh Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">7.</div>
        <div class="col-11">Pembayaran upah dilaksanakan tanggal 25 setiap bulannya.</div>
      </div>
      <div class="row">
        <div class="col-1">8.</div>
        <div class="col-11">Bila diperlukan, PIHAK KEDUA bersedia dan sanggup untuk melakukan pekerjaan diluar jam kerja yang telah ditetapkan atau pada hari libur atas perintah dari pihak Perusahaan. Dalam hal ini, PIHAK PERTAMA akan membayar uang lembur sesuai dengan peraturan yang berlaku, sebagai berikut:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a.</div>
        <div class="col-10">Dasar Upah Lembur adalah Upah per Jam (UpJ).</div>
      </div>
      <br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b.</div>
        <div class="col-10">Upah per Jam adalah Upah Sebulan dibagi dengan angka 173 (seratus tujuh puluh tiga).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c.</div>
        <div class="col-10">Dasar perhitungan lembur sebagai berikut:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-11">
          <table border="1">
            <tr>
              <th colspan="2">HARI KERJA</th>
              <th colspan="2">HARI LIBUR</th>
            </tr>
            <tr>
              <td>
                Jam ke-1 <br />
                Jam ke-2 dan seterusnya
              </td>
              <td>
                1,5 X UpJ <br />
                2 X UpJ
              </td>
              <td>
                Jam ke – 1 s/d Jam ke - 8 <br />
                Jam ke – 9 <br />
                Jam ke – 10 dan seterusnya
              </td>
              <td>
                2 X UpJ <br />
                3 X UpJ <br />
                4 X UpJ
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-1">9.</div>
        <div class="col-11">Apabila PIHAK KEDUA telah bekerja selama 1 (satu) bulan atau lebih, PIHAK KEDUA akan menerima Tunjangan Hari Raya (THR) dengan jumlah yang setara dengan 1 (satu) bulan upah bruto atau perhitungan dilakukan secara prorata atau proposional, yang akan dibayarkan 2 (dua) minggu sebelum jatuh tempo hari raya keagamaan setiap tahunnya. Sesuai dengan PERMENAKER No. 06 TAHUN 2016 ketentuan ini tidak berlaku bagi pekerja yang hubungan kerja untuk waktu tertentu-nya berakhir sebelum tanggal jatuh tempo hari raya keagamaan tersebut. Hari raya keagamaan yang dimaksud dalam ayat ini disepakati adalah Hari Raya Iedul Fitri.</div>
      </div>
      <div class="row">
        <div class="col-1">10.</div>
        <div class="col-11">PIHAK KEDUA menyetujui bahwa pengisian absensi yang salah/tidak lengkap akan memperlambat waktu pembayaran upah.</div>
      </div>
      <center>Pasal 4</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">Hari Kerja adalah 5 (lima) hari atau 6 (enam) hari kerja seminggu, dengan ketentuan 8 (delapan) jam atau 7 (tujuh) jam per hari dan 40 (empat puluh) jam per minggu.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Jam Kerja normal adalah dari jam 08.00 sampai dengan jam 17.00, dengan Jam Istirahat normal adalah dari jam 12.00 sampai dengan jam 13.00.</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">PIHAK PERTAMA dan atau Perusahaan dapat merubah dan menentukan jadwal jam kerja, jam istirahat dan jam kerja bergilir (shift) setiap saat sesuai kebutuhan operasional Perusahaan, sepanjang tidak bertentangan dengan peraturan perundangan.</div>
      </div>
      <center>Pasal 5</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">PIHAK KEDUA wajib (i) - menjaga nama baik dan reputasi Perusahaan; (ii) – mematuhi dan melaksanakan pekerjaannya dengan penuh tanggung jawab sesuai dengan prosedur dan aturan yang berlaku; dan (iii) – mentaati dan melaksanakan setiap kebijakan dan atau peraturan di Perusahaan berikut seluruh ketentuan yang berlaku termasuk tetapi tidak terbatas pada tata tertib dan prosedur kerja yang berlaku di Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Walaupun ditempatkan di Perusahaan, PIHAK KEDUA bukan merupakan wakil dari Perusahaan dan oleh karenanya, tidak dapat bertindak untuk dan atau atas nama Perusahaan dalam kondisi dan dengan alasan apapun juga. Lebih lanjut, PIHAK KEDUA dilarang menggunakan setiap dan segala perlengkapan dan atau properti dan atau dokumen yang mengandung nama dan atau merek dan atau logo Perusahaan kecuali untuk menjalankan kewajiban dan tugasnya berkenaan dengan penempatan PIHAK KEDUA di Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">PIHAK PERTAMA dapat mengambil tindakan disiplin termasuk tetapi tidak terbatas pada pelanggaran tata tertib dan disiplin kerja, hasil Evaluasi Kerja Tertulis PIHAK KEDUA, dalam bentuk Surat Peringatan ke I (pertama), Surat Peringatan ke II (kedua), Surat Peringatan ke III (ketiga)/terakhir, Skorsing dan Pemutusan Hubungan Kerja.</div>
      </div>
      <div class="row">
        <div class="col-1">4.</div>
        <div class="col-11">Surat Peringatan seperti tersebut pada ayat 3 diatas dapat diberikan oleh PIHAK PERTAMA kepada PIHAK KEDUA secara tidak urut, didasarkan atas penilaian besar kecilnya kesalahan PIHAK KEDUA yang diatur dalam Ketentuan Operasional yang terinci dalam Lampiran Perjanjian ini.</div>
      </div>
      <div class="row">
        <div class="col-1">5.</div>
        <div class="col-11">Ketentuan mengenai syarat-syarat kerja, hak dan kewajiban PARA PIHAK serta Evaluasi Kerja Tertulis PIHAK KEDUA yang belum diatur dalam Perjanjian ini, diatur dalam Ketentuan Operasional yang terinci dalam Lampiran Perjanjian ini.</div>
      </div>
      <div class="row">
        <div class="col-1">6.</div>
        <div class="col-11">Pada saat perjanjian kerja ini tidak dilanjutkan, PIHAK KEDUA wajib mengembalikan seluruh barang milik Perusahaan yang dipercayakan kepada PIHAK KEDUA, yang dipergunakan untuk kepentingan Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">7.</div>
        <div class="col-11">PIHAK KEDUA wajib memberitahukan PIHAK PERTAMA apabila ada perubahan alamat tempat tinggal maupun bila ada kerabat PIHAK KEDUA yang melamar kerja ke Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">8.</div>
        <div class="col-11">Tanpa persetujuan tertulis sebelumnya dari PIHAK PERTAMA, PIHAK KEDUA tidak diperkenankan untuk membuat perjanjian kerja dengan pihak lain dalam bentuk apapun. PIHAK KEDUA menyatakan mengundurkan diri jika ternyata, selama Perjanjian ini berlaku, PIHAK KEDUA memiliki hubungan kerja rangkap dengan pihak di luar PIHAK PERTAMA.</div>
      </div>
      <div class="row">
        <div class="col-1">9.</div>
        <div class="col-11">Dalam hal terjadi pergantian perusahaan penyedia jasa pekerja dan PIHAK KEDUA menerima tawaran perusahaan penyedia jasa pekerja yang baru untuk bekerja pada jabatan dan/atau jenis pekerjaan yang sama, PARA PIHAK sepakat bahwa perhitungan hak cuti tahunan dan/atau tunjangan hari raya pada periode tahun berjalan serta catatan hubungan kerja PIHAK KEDUA termasuk tetapi tidak terbatas pada catatan hasil evaluasi kerja, surat peringatan dan pelaksanaan Ketentuan Operasional Kerja pada PIHAK PERTAMA tetap berlaku dan/atau dilanjutkan di perusahaan penyedia jasa pekerja yang baru.</div>
      </div>
      <br/><br/><br/>
      <center>Pasal 6</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">PIHAK KEDUA berhak atas cuti tahunan selama 12 (dua belas) hari setelah bekerja selama 12 (dua belas) bulan secara terus-menerus.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Pelaksanaan cuti tahunan sebagaimana dimaksud dalam ayat (1) akan diatur kemudian dan disesuaikan dengan Peraturan Perusahaan PIHAK PERTAMA dan atau peraturan yang berlaku di Perusahaan sesuai dengan peraturan ketenagakerjaan yang berlaku.</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">Sesuai peraturan perundangan yang berlaku, PIHAK KEDUA berhak mendapatkan ijin meninggalkan pekerjaan dengan mendapat upah penuh untuk hal-hal sebagai berikut:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a.</div>
        <div class="col-8">PIHAK KEDUA menikah</div>
        <div class="col-2">3 hari kerja</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b.</div>
        <div class="col-8">PIHAK KEDUA menikahkan anaknya</div>
        <div class="col-2">2 hari kerja</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c.</div>
        <div class="col-8">Khitanan/Sunatan atau Pembaptisan anak PIHAK KEDUA</div>
        <div class="col-2">2 hari kerja</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d.</div>
        <div class="col-8">Isteri PIHAK KEDUA melahirkan atau keguguran kandungan</div>
        <div class="col-2">2 hari kerja</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e.</div>
        <div class="col-8">Kematian Suami / isteri, Orang tua / Mertua atau Anak / Menantu PIHAK KEDUA</div>
        <div class="col-2">2 hari kerja</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f.</div>
        <div class="col-8">Kematian keluarga PIHAK KEDUA yang tinggal serumah</div>
        <div class="col-2">1 hari kerja</div>
      </div>
      <center>Pasal 7</center>
      <p align="jusitfy">PIHAK PERTAMA akan mengikutsertakan PIHAK KEDUA pada program BPJS atau asuransi lainnya sesuai dengan ketentuan yang berlaku dan karenanya biaya pengobatan/kesehatan PIHAK KEDUA disediakan sejalan dengan program tersebut.</p>
      <center>Pasal 8</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">PIHAK KEDUA yang akan mengundurkan diri harus mengajukan permohonan pengunduran diri secara tertulis selambat-lambatnya 30 (tiga puluh) hari sebelum tanggal pengunduran diri dan tetap melaksanakan kewajibannya sampai tanggal tersebut.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">PIHAK PERTAMA akan membayar upah sisa waktu kontrak sekaligus apabila PIHAK PERTAMA mengakhiri kesepakatan kerja berdasarkan Perjanjian ini sebelum waktunya sebagaimana diatur dalam Pasal 1 di atas berakhir.</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">Dalam hal Pemutusan Hubungan Kerja dalam Perjanjian ini, PARA PIHAK sepakat melaksanakan ketentuan Pasal 151 dan 152 Undang-undang Ketenagakerjaan No. 13 Tahun 2003, yang mana Pemutusan Hubungan Kerja harus mendapatkan Penetapan Lembaga Penyelesaian Perselisihan Hubungan Industrial, atau ketentuan Pasal 3 Undang-undang Penyelesaian Perselisihan Hubungan Industrial No. 2 Tahun 2004.</div>
      </div>
      <div class="row">
        <div class="col-1">4.</div>
        <div class="col-11">Dengan berakhirnya kesepakatan kerja berdasarkan Perjanjian ini maka PIHAK KEDUA dengan ini melepaskan haknya terhadap semua hal yang timbul dari hubungan kerja yang ada, termasuk tetapi tidak terbatas seperti uang pesangon, uang jasa atau ganti rugi dalam bentuk apapun dari PIHAK PERTAMA.</div>
      </div>
      <div class="row">
        <div class="col-1">5.</div>
        <div class="col-11">Tidak terpenuhinya ketentuan ayat (1) di atas, dan atau adanya dugaan indikasi tindakan penipuan atau kejahatan lainnya, dan atau tindakan lain yang dapat merugikan PIHAK PERTAMA dan atau Perusahaan dan atau nasabah Perusahaan, PIHAK KEDUA setuju dan memberikan kuasa dan wewenang penuh kepada PIHAK PERTAMA untuk menahan sementara semua hak yang belum diberikan kepada PIHAK KEDUA berdasarkan Perjanjian ini.</div>
      </div>
      <center>Pasal 9</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">Semua informasi dan atau bahan-bahan yang di dapat dari pekerjaan yang dilakukan berdasarkan Perjanjian ini baik yang sudah selesai atau masih dalam proses penyelesaian adalah merupakan milik dari PIHAK PERTAMA dan atau Perusahaan. Untuk segala keperluan termasuk tetapi tidak terbatas penggandaan, modifikasi atau pengungkapan dari informasi dan atau bahan-bahan tersebut harus mendapatkan persetujuan tertulis dari PIHAK PERTAMA dan atau Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">PIHAK KEDUA berjanji untuk melindungi PIHAK PERTAMA dan atau Perusahaan dari dan terhadap kerugian atau kewajiban yang timbul dari tuntutan apapun akibat dari penyalahgunaan ‘trade secret’, ‘patent’, ‘copyright’ atau pelanggaran hak-hak kepemilikan dari informasi dan atau bahan-bahan yang diberikan kepada PIHAK KEDUA atau penggunaan dari informasi atau bahan-bahan tersebut.</div>
      </div>
      <center>Pasal 10</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">PARA PIHAK sepakat bahwa hal-hal yang belum diatur dalam Perjanjian ini akan diatur kemudian dalam suatu addendum atau amandemen yang disetujui bersama secara tertulis oleh PARA PIHAK dan merupakan bagian integral yang tidak terpisahkan dari Perjanjian ini serta mempunyai kekuatan hukum yang sama.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Perjanjian ini bersama-sama dengan lampiran, berikut seluruh perubahan, penambahan, pengurangan, addendum dan atau amandemennya yang dibuat dari waktu ke waktu adalah merupakan satu kesatuan yang tidak dapat dipisahkan serta merupakan satu-satunya perjanjian antara PARA PIHAK mengenai hal-hal yang diatur dalam Perjanjian ini dan menghapuskan semua pembicaraan, perjanjian dan kesepakatan lainnya baik lisan maupun tertulis yang pernah dilakukan diantara PARA PIHAK sebelum tanggal ditandatanganinya Perjanjian ini.</div>
      </div>
      <br />
      <center>Pasal 11</center>
      <p align="justify">Pembatalan salah satu pasal dalam Perjanjian ini, yang disebabkan oleh ditemukan dan atau timbulnya peraturan baru yang menjadikan salah satu pasal tersebut batal demi hukum, maka pembatalan tersebut tidak menjadikan pasal-pasal lain dalam Perjanjian ini menjadi batal, dan pasal-pasal lainnya tetap berlaku selama tidak berkaitan langsung dan atau tidak langsung dengan salah satu pasal yang dibatalkan, dan dalam hal suatu ketentuan dalam Perjanjian ini menjadi batal atau tidak dapat dilaksanakan, maka PARA PIHAK sepakat untuk dengan itikad baik dan selama dimungkinkan oleh ketentuan peraturan perundang-undangan yang berlaku, mengganti ketentuan tersebut dengan ketentuan yang sah dan atau dapat dilaksanakan.</p>
      <br/><br/><br/><br/>
      <center>Pasal 12</center>
      <p align="justify">Masing-masing Pihak tidak dapat dimintai pertanggungjawaban atas kegagalan, keterlambatan dan atau ketidaksempurnaan pemenuhan kewajiban masing-masing Pihak dalam Perjanjian ini menurut syarat-syarat yang diatur dalam Perjanjian ini, apabila kegagalan, keterlambatan dan atau ketidaksempurnaan tersebut disebabkan oleh kejadian-kejadian diluar kendali PARA PIHAK, peristiwa mana termasuk tetapi tidak terbatas pada bencana alam, kebakaran, gempa bumi, banjir, epidemi, perang, huru-hara, atau pemberlakuan atau perubahan peraturan perundang-undangan, likuidasi, bangkrut, pembatasan oleh Pemerintah, yang kesemuanya langsung berhubungan dengan pelaksanaan Perjanjian ini (disebut “Keadaan Memaksa”).</p>
      <center>Pasal 13</center>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">Perjanjian ini tunduk pada hukum negara Republik Indonesia.</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Hal-hal yang tidak atau belum cukup diatur dalam Perjanjian ini tunduk pada peraturan perundang-undangan mengenai ketenagakerjaan yang berlaku.</div>
      </div>
      <p align="justify">Demikian Perjanjian ini dibuat dengan sebenarnya, dimengerti dan disetujui semua pihak tanpa ada paksaan dari pihak manapun serta dilakukan dalam keadaan sehat jasmani maupun rohani.</p>
      <center>
        <table width="100%" border="1">
          <tr>
            <td width="50%">
              <div align="center">
                <span>PIHAK KEDUA</span>
                <!-- <br/> <img src="{{ public_path('data_file_ttd/'.$param['user']->user_id.'.jpeg') }}" alt="$param['user']->user_id" style="width: 150px; height: 75px; object-fit: cover;"> <br/> -->
                <span>
                  <strong>{{ $param['user']->nama_lengkap }}</strong>
                </span>
              </div>
            </td>
            <td width="50%">
              <div align="center">
                <span>PIHAK PERTAMA</span>
                <br/> <img src="{{ public_path('backend/assets/img_hrd/cap_aks.jpeg') }}" alt="cap_aks" style="width: 150px; height: 75px; object-fit: cover;"> <br/>
                <strong>Herman Julianto</strong>
                <br />
                <strong>Direktur HRD</strong>
              </div>
            </td>
          </tr>
        </table>
      </center>
    </div>
  </div>
</body>

</html>