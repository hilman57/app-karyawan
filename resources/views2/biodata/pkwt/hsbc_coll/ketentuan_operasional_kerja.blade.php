<!DOCTYPE html>
<html>

<head>
  <title>KETENTUAN OPERASIONAL KERJA</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    hr {
      border: 3px solid black;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container">
    <center>
      <h5>PERATURAN OPERASIONAL PEKERJA (POP)</h5>
    </center>
    <div align="justify">
      <div class="row">
        <div class="col-3">Nama / Jabatan</div>
        <div class="col-9">: {{ $param['user']->nama_lengkap }} / {{ $param['user']->position_name }}</div>
      </div>
      <div class="row">
        <div class="col-3">Tanggal Aktivasi Dokumen</div>
        <div class="col-9">: {{ $param['date_pkwt'] }}</div>
      </div>
      <p>Peraturan Operasional Pekerja (selanjutnya disebut POP) disusun untuk memberi panduan dasar bagi para pekerja (selanjutnya disebut “Pekerja”) dalam melaksanakan tugas dalam posisi/jabatan tersebut di atas pada PT Aseanindo Kapita Solusi (selanjutnya disebut Perusahaan )</p>
      <p>POP ini diberikan kepada Pekerja sebagai lampiran dari Perjanjian Kerja Waktu Tertentu beserta seluruh perubahan, perpanjangan, dan pembaharuannya dan berlaku bagi Pekerja sejak Pekerja menerima POP ini. Pekerja diwajibkan untuk membaca, mempelajari, memahami, melaksanakan, dan mematuhi segala hal yang diatur dalam POP ini.</p>
      <p>Setiap saat Perusahaan berhak dan memiliki kewenangan untuk meninjau dan merubah isi dari POP ini. Perusahaan berkewajiban untuk menginformasikan dan menyerahkan perubahan atas POP tersebut kepada Pekerja paling lambat 14 hari kerja setelah perubahan yang ditunjukan dalam Tanggal Aktivasi Dokumen.</p>
      <!-- Part 1 -->
      <div class="row">
        <div class="col-1"><strong>1. </strong></div>
        <div class="col-11"><strong><u>FRAUD</u></strong></div>
      </div>
      <!-- Part 1 Point 1.1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1.1. </div>
        <div class="col-10">
          Definisi Fraud <br />
          Yang dimaksud dengan Fraud dalam hal ini adalah segala bentuk penipuan, pemalsuan, pencurian, penggelapan dokumen atau adanya kelalaian/kesalahan dalam melaksanakan salah satu dari rangkaian proses penagihan hutang kepada nasabah. Termasuk di dalam definisi ini dan juga merupakan tindakan Fraud antara lain:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">Mengambil, memanfaatkan, memalsukan, menggandakan, data atau dokumen nasabah untuk kepentingan pribadi dan atau pihak lainnya.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">Melakukan perbuatan dalam rangka membantu penyelesaian hutang nasabah di luar kewenangan dan peraturan atau kebijakan yang berlaku pada Perusahaan yang meliputi namun tidak terbatas pada : memberikan potongan (discount), dan atau perpanjangan jangka waktu pembayaran hutang, dan atau melakukan tindakan yang menghentikan proses penagihan hutang kepada nasabah.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">Menerima, mengambil keuntungan dalam bentuk apa pun dari nasabah dalam proses penagihan hutang untuk kepentingan pribadi.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-9">Memberitahukan informasi mengenai nasabah kepada pihak lain seperti informasi data pribadi, informasi rekening, informasi pinjaman dan spesimen tanda tangan untuk kepentingan pribadi. </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-9">Memberikan keterangan atau membuat surat keterangan palsu atau yang dipalsukan, baik untuk Pekerja sendiri atau rekan sekerjanya; </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-9">Meminta uang kepada nasabah.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-9">Menerima uang atau hadiah dari nasabah.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">h. </div>
        <div class="col-9">Terikat perjanjian kontrak kerja dengan institusi lain (perbankan atau non-perbankan) atau menerima bayaran dari institusi lain. </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-9">Meminjam dan/atau meminjamkan uang kepada sesama pekerja. ataupun kepada pekerja Bank lain.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">j. </div>
        <div class="col-9">Tidak mengembalikan semua dokumen, yang pernah diterima oleh Karyawan dari Perusahaan atau atasan langsung, kepada Perusahaan atau atasan langsung pada hari terakhir Karyawan hadir bekerja sebelum perjanjian atau hubungan kerja antara Karyawan dan Perusahaan berakhir atau tidak dilanjutkan.</div>
      </div>
      <!-- Part 1 Point 1.2 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1.2. </div>
        <div class="col-10">Sanksi Fraud</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">Sesuai bobot kesalahan yang ditetapkan semata-mata berdasarkan penilaian Perusahaan. bahwa atas tindakan Fraud tersebut Pekerja dapat dikenakan Surat Peringatan sampai dengan Pemutusan Hubungan Kerja tanpa adanya ganti kerugian dan/atau kompensasi dalam bentuk apapun. Kerugian yang diderita Perusahaan maupun nasabah akibat tindakan fraud yang dilakukan Pekerja akan menjadi tanggung jawab penuh Pekerja dan wajib diselesaikan dalam waktu 1 X 24 jam dari temuan kasus fraud tersebut atau pengakuan Pekerja atas kasus fraud tersebut.</div>
      </div>
      <br /><br /><br /><br /><br /><br/>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">Pekerja yang melakukan tindakan Fraud termasuk akan tetapi tidak terbatas pada penipuan, meminta atau mengambil uang nasabah, mengajukan aplikasi tanpa sepengetahuan calon nasabah, memalsukan tanda tangan atau dokumen-dokumen calon nasabah akan dikenakan sanksi Pemutusan Hubungan Kerja tanpa adanya ganti kerugian dan/atau kompensasi dalam bentuk apapun.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">Tindakan Fraud akan mempengaruhi penilaian (review) kinerja karyawan dan/atau penilaian kerja Pekerja.</div>
      </div>
      <!-- Part 1 Point 1.3 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1.3. </div>
        <div class="col-10">
          Pencegahan Fraud <br />
          Dalam rangka mencegah terjadinya Fraud, Perusahaan berhak melakukan pemeriksaan terhadap tas dan/atau barang bawaan dan/atau laci dari Pekerja termasuk melakukan pemeriksaan terhadap Pekerja.
        </div>
      </div>
      <!-- Part 2 -->
      <div class="row">
        <div class="col-1"><strong>2. </strong></div>
        <div class="col-11"><strong><u>WAKTU KERJA</u></strong><br />Pekerja wajib mematuhi waktu kerja yang telah ditentukan oleh Perusahaan sebagai berikut :</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Hari kerja normal adalah 5 hari kerja pada Hari Senin sampai dengan Jum’at jam 08.00 – 17.00.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Waktu kerja operasional adalah 8 jam dalam 1 hari, 40 jam dalam 1 minggu atau setara dengan 40 (empat puluh) jam kerja dalam 6 (enam) hari kerja / satu minggu.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Waktu istirahat 1 jam setiap harinya dan khusus pada Hari Jum’at, Pekerja yang beragama Islam dapat menggunakan waktu istirahat mulai pukul 11.30 hingga 13.30 untuk melaksanakan Sholat Jum’at dan makan siang tanpa mengurangi target produktivitas yang wajib dicapai.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Setiap saat Perusahaan dapat merubah jadwal jam kerja dan jam istirahat sesuai kebutuhan operasional Perusahaan, sepanjang tidak bertentangan dengan peraturan perundang-undangan;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Pekerja yang bekerja di luar jam kerja yang ditentukan tanpa perintah Pengusaha atau atasan langsung, maka tidak akan diperhitungkan sebagai lembur.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-10">
          Pekerja yang tidak bekerja tanpa persetujuan atasan langsung, tanpa alasan yang dapat dipertanggungjawabkan, atau sakit lebih dari 1 hari tanpa surat dokter yang sah, maka Pekerja dikualifikasikan mangkir.
        </div>
      </div>
      <!-- Part 3 -->
      <div class="row">
        <div class="col-1"><strong>3. </strong></div>
        <div class="col-11"><strong><u>TATA TERTIB PADA</u> Perusahaan.</strong> <br /> Pekerja wajib mentaati peraturan tata tertib pada <strong>Perusahaan</strong>. <br /> seperti (namun tidak terbatas):</div>
      </div>
      <!-- Part 3 Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Datang tepat waktu pada jam masuk kerja atau pada jam kembali bekerja sesudah waktu istirahat sesuai waktu kerja yang ditetapkan;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Memberitahukan kepada atasan langsung jika terlambat hadir;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Mengisi absensi secara benar dan jujur setiap masuk bekerja;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Pakaian kerja untuk hari Senin – Kamis adalah:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-9">
          pria mengenakan kemeja, celana kain, dan sepatu formal;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-9">
          perempuan mengenakan celana/rok kain, atasan formal, sepatu formal);
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Pakaian kerja untuk hari Jumat , Sabtu, Minggu atau hari libur yang diwajibkan masuk adalah: - celana jeans, kemeja/kaos berkerah, sepatu casual);
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-10">
          Access/ID card wajib selalu dikenakan selama berada dalam lingkungan tempat kerja;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-10">
          Menjaga lingkungan kerja tetap bersih (meja kerja, toilet, pantry, dsb);
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">h. </div>
        <div class="col-10">
          Menjaga lingkungan, hubungan dan komunikasi kerja yang harmonis dengan seluruh Pekerja di tempat kerja;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-10">
          Menjaga peralatan kerja tetap bersih dan berfungsi dengan baik (komputer, headset, telepon, dsb);
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">j. </div>
        <div class="col-10">
          Mengikuti tata cara menelpon dengan baik (misal : salam, pemilihan kata, intonasi, artikulasi);
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">k. </div>
        <div class="col-10">
          Tidak makan dan minum saat berbicara dengan nasabah;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">l. </div>
        <div class="col-10">
          Mengikuti dan mematuhi Tata Tertib Penggunaan Loker yang sudah ditentukan, termasuk namun tidak terbatas dengan menyimpan semua barang pribadi di dalam loker. Dalam hal ini, tidak diperkenankan membawa masuk tas, telpon genggam, dan alat fotografi ke dalam ruangan kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">m. </div>
        <div class="col-10">
          Tidak menghubungi/menerima telepon, SMS dengan menggunakan alat komunikasi seperti handphone dan lain-lain pada saat jam kerja;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">n. </div>
        <div class="col-10">
          Tidak mendengarkan radio/musik pada saat jam kerja;
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">o. </div>
        <div class="col-10">
          Tidak melakukan kecurangan dalam proses bekerja.
        </div>
      </div>
      <!-- Part 4 -->
      <div class="row">
        <div class="col-1"><strong>4. </strong></div>
        <div class="col-11"><strong><u>TUNJANGAN TRANSPORTASI</u></strong> <br /> Pekerja yang bekerja pada waktu kerja normal dan bekerja sampai dengan di atas jam 19.00 akan menerima tunjangan transportasi sebesar Rp. 25.000,-</div>
      </div>
      <br /><br /><br />
      <!-- Part 5 -->
      <div class="row">
        <div class="col-1"><strong>5. </strong></div>
        <div class="col-11">
          <strong><u>TARGET KERJA</u></strong> <br />
          Pekerja wajib memenuhi target kerja yang telah ditetapkan dan dikomunikasikan secara terbuka dan tertulis oleh Perusahaan.
        </div>
      </div>
      <!-- Part 5 Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          atau atasannya langsung dan ditandatangani oleh Pekerja.
        </div>
      </div>
      <!-- Part 5 Point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Pencapaian target atau prestasi kerja Pekerja akan dicatat dimana data tersebut menjadi acuan atas penilaian prestasi kerja Pekerja selama bertugas pada posisi/jabatan tersebut diatas dan tidak menjadi kadaluwarsa dengan berakhirnya suatu periode dari Perjanjian Kerja Waktu Tertentu.
        </div>
      </div>
      <!-- Part 5 Point c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Apabila Pekerja tidak memenuhi target kerja, maka Pekerja akan diberikan sanksi yang bentuk dan jenis sanksinya berupa pemberian Surat Peringatan sampai dengan Pemutusan Hubungan Kerja.
        </div>
      </div>
      <!-- Part 5 Point d -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Pengusaha akan melakukan pengukuran hasil kerja atau penilaian terhadap kinerja Pekerja dimana waktu, indikator, dan mekanisme pelaksanaan penilaian kinerja menjadi kewenangan Perusahaan sepenuhnya.
        </div>
      </div>
      <!-- Part 6 -->
      <div class="row">
        <div class="col-1"><strong>6. </strong></div>
        <div class="col-11"><strong><u>KEWAJIBAN DAN TANGGUNG JAWAB LAINNYA</u></strong> <br /> Pekerja yang bertugas pada posisi/jabatan tersebut di atas wajib:</div>
      </div>
      <!-- Part 6 Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Melaksanakan setiap dan segala aturan yang ditetapkan oleh Perusahaan termasuk peraturan perundangan, norma kesopanan dan kesusilaan yang berlaku,
        </div>
      </div>
      <!-- Part 6 Point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Mengikuti ketentuan yang ditetapkan oleh Perusahaan dalam aspek kedisiplinan, tata tertib, etika bisnis, dan kepatuhan pada peraturan kerja,
        </div>
      </div>
      <!-- Part 6 Point c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Menyimpan dengan benar untuk kepentingan Perusahaan setiap dan semua dokumen yang diterimanya dari Perusahaan dan segera menyerahkan dokumen-dokumen tersebut pada saat berakhirnya hubungan kerja.
        </div>
      </div>
      <!-- Part 7 -->
      <div class="row">
        <div class="col-1"><strong>7. </strong></div>
        <div class="col-11"><strong><u>SURAT PERINGATAN</u></strong> <br /> Surat Peringatan I, II dan III yang masing-masing masa berlakunya 3 (tiga) bulan akan diberikan kepada Pekerja sebagai berikut:</div>
      </div>
      <!-- Part 7 Point 7.1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">7.1. </div>
        <div class="col-10">
          Surat Peringatan I (pertama) <br /> Surat Peringatan I diberikan bila Pekerja:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a.</div>
        <div class="col-9">
          Melakukan kesalahan dalam melaksanakan pekerjaannya sehingga menimbulkan komplain (valid complaint) dari nasabah sesuai dengan ketentuan yang ditetapkan oleh Perusahaan,
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b.</div>
        <div class="col-9">
          Mangkir 1 hari kerja dalam kurun waktu 1 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c.</div>
        <div class="col-9">
          Melanggar tata tertib yang disebutkan pada butir - butir nomor 3 mengenai Tata Tertib pada Perusahaan
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d.</div>
        <div class="col-9">
          Tidak memenuhi target bulanan dan skor minimum yang telah ditetapkan dan dikomunikasikan oleh Perusahaan
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">e.</div>
        <div class="col-9">
          Tidak memenuhi kewajiban dan tanggung jawab lainya seperti yang disebutkan pada butir-butir nomor 7 a dan b.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">f.</div>
        <div class="col-9">
          Mengulangi kesalahan atau pelanggaran ringan sebanyak 3 kali dalam 1 bulan, dimana atas kesalahan tersebut telah diberikan peringatan lisan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">g.</div>
        <div class="col-9">
          Datang terlambat masuk kerja dengan alasan yang tidak dapat diterima oleh atasan sebanyak 3 kali dalam 1 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">h.</div>
        <div class="col-9">
          Meninggalkan tempat kerja dengan alasan yang tidak ada hubungannya dengan pekerjaan tanpa seijin atasan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">i.</div>
        <div class="col-9">
          Tidak mengisi daftar kehadiran atau absensi lebih dari 3 kali dalam 1 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">j.</div>
        <div class="col-9">
          Tidak melaksanakan tugas atau perintah dari atasannya yang berkaitan dengan jenis pekerjaan (job description) yang disandangnya.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">k.</div>
        <div class="col-9">
          Terlambat menyampaikan Surat Keterangan Dokter dalam waktu 3x24 jam dari hari pertama masuk kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">l.</div>
        <div class="col-9">
          Menolak untuk kerja lembur tanpa alasan yang dapat diterima.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">m.</div>
        <div class="col-9">
          Menolak perintah untuk memeriksa kesehatan dirinya.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">n.</div>
        <div class="col-9">
          Tidak lulus dalam sertifikasi Call Model training, dimana kesempatan telah diberikan sebanyak 3 (tiga) kali.
        </div>
      </div>
      <!-- Part 7 Point 7.2 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">7.2. </div>
        <div class="col-10">
          Surat Peringatan II (Kedua) <br /> Surat Peringatan II diberikan bila Pekerja:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a.</div>
        <div class="col-9">
          Terbukti melakukan kesalahan dalam melaksanakan pekerjaannya yang mengakibatkan nasabah melakukan pengaduan melalui media massa yang berpotensi merusak citra dan nama baik Perusahan.
        </div>
      </div>
      <br /><br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b.</div>
        <div class="col-9">
          Melakukan pelanggaran dan/atau kesalahan dalam masa berlakunya Surat Peringatan I (Pertama).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c.</div>
        <div class="col-9">
          Mangkir 2 hari kerja secara berturut-turut atau tidak dalam kurun waktu 1 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d.</div>
        <div class="col-9">
          Melakukan tindakan yang kasar/tidak sopan terhadap atasan, bawahan, rekan kerja, dan pihak lain.
        </div>
      </div>
      <!-- Part 7 Point 7.3 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">7.3. </div>
        <div class="col-10">
          Surat Peringatan III (Ketiga) <br /> Surat Peringatan III diberikan bila Pekerja:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a.</div>
        <div class="col-9">
          Lalai dalam melaksanakan pekerjaannya yang mengakibatkan kerugian pada Perusahaan dan setelah dilakukan pemeriksaan terdapat unsur kesengajaan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b.</div>
        <div class="col-9">
          Pekerja melakukan pelanggaran dan/atau kesalahan dalam masa berlakunya Surat Peringatan II (kedua) atau dalam masa berlakunya Surat Peringatan I (pertama) jika bobot kesalahannya setara dengan sebab diberikannya sanksi Surat Peringatan II (kedua).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c.</div>
        <div class="col-9">
          Mangkir 3 hari kerja secara berturut-turut atau tidak dalam kurun waktu 1 bulan.
        </div>
      </div>
      <!-- Part 8 -->
      <div class="row">
        <div class="col-1"><strong>8. </strong></div>
        <div class="col-11"><strong><u>PEMUTUSAN HUBUNGAN KERJA</u></strong> <br /> Perusahaan akan melakukan Pemutusan Hubungan Kerja tanpa ganti kerugian dan/atau kompensasi dalam bentuk apapun jika:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Pekerja lalai dalam melaksanakan pekerjaannya yang mengakibatkan kerugian pada Perusahaan dan setelah dilakukan pemeriksaan terdapat unsur kesengajaan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Dalam masa berlaku Surat Peringatan I atau II atau III, karyawan melakukan perbuatan yang bobot kesalahannya mengakibatkan diberikannya surat peringatan dalam tabel dibawah sebagai berikut.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-10">
          <table border="1" width="100%">
            <tr>
              <th>Dalam masa Berlaku dari Surat Peringatan : </th>
              <th>Melakukan Kesalahan yang mengakibatkan diberikannya Surat Peringatan : </th>
            </tr>
            <tr>
              <td>I (Pertama)</td>
              <td>III (Ketiga)</td>
            </tr>
            <tr>
              <td>II (Kedua)</td>
              <td>II (Kedua) atau III (Ketiga)</td>
            </tr>
            <tr>
              <td>III (Ketiga)</td>
              <td>I (Pertama) atau II (Kedua) atau III (Ketiga)</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- Part 9 -->
      <div class="row">
        <div class="col-1"><strong>9. </strong></div>
        <div class="col-11"><strong><u>PROGRAM INSENTIF</u></strong></div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Program insentif adalah program yang bersifat periodik untuk jangka waktu tertentu yang diberikan oleh Perusahaan bila Pekerja memenuhi kriteria untuk mendapatkan insentif sesuai yang diatur dalam kebijakan insentif.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Setiap saat Perusahaan berhak untuk melakukan perubahan terhadap kebijakan insentif
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Kebijakan insentif beserta perubahan-perubahannya yang telah ditetapkan oleh Perusahaan akan dikomunikasikan secara terbuka kepada Pekerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Pekerja yang telah mengajukan surat pengunduran diri dan atau sudah tidak lagi menjadi Pekerja tidak berhak mendapatkan insentif yang belum dibayarkan. Pekerja dengan ini melepaskan haknya untuk menuntut pembayaran-pembayaran insentif, hadiah maupun penghargaan yg belum terbayarkan tersebut dan/atau ganti rugi dan/atau melakukan gugatan baik secara pidana maupun perdata kepada Perusahaan atas tidak dibayarkannya insentif, hadiah, maupun pengharagaan yang belum terbayarkan tersebut.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Apabila pihak kedua mendapatkan Surat Peringatan akibat adanya keluhan dari nasabah maka pihak kedua dengan ini memberi kuasa kepada pihak pertama untuk memotong besarnya incentive yang diterima pihak kedua untuk bulan terjadinya keluhan nasabah sesuai tabel dibawah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-10">
          <table border="1" width="100%">
            <tr>
              <th>Tingkat surat peringatan akibat keluhan nasabah</th>
              <th>Besarnya pemotongan incentive</th>
            </tr>
            <tr>
              <td>Surat peringatan I ( Pertama )</td>
              <td>25%</td>
            </tr>
            <tr>
              <td>Surat peringatan II ( Kedua )</td>
              <td>50%</td>
            </tr>
            <tr>
              <td>Surat peringatan III ( Ketiga )</td>
              <td>100%</td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-10">
          Apabila pihak kedua tidak masuk kerja tanpa keterangan atau alasan yang tidak dapat diterima oleh atasannya maka pihak kedua dengan ini memberi kuasa kepada pihak pertama untuk memotong besarnya incentive yang diterima pihak kedua ( dibulan perhitunagan incentive ).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-10">
          <table border="1" width="100%">
            <tr>
              <th>Pemotongan incentive bulan berjalan</th>
              <th>Besarnya pemotongan incentive</th>
            </tr>
            <tr>
              <td>Tidak masuk kerja 1 hari tanpa keterangan</td>
              <td>5%</td>
            </tr>
            <tr>
              <td>Tidak masuk kerja 2 hari tanpa keterangan</td>
              <td>10%</td>
            </tr>
            <tr>
              <td>Tidak masuk kerja 3 hari atau lebih tanpa keterangan</td>
              <td>15%</td>
            </tr>
          </table>
        </div>
      </div>
      <br /><br /><br />
      <!-- Part 10 -->
      <div class="row">
        <div class="col-1"><strong>10. </strong></div>
        <div class="col-11"><strong><u>PENGUNDURAN DIRI DAN PEMUTUSAN HUBUNGAN KERJA KARENA DIKUALIFIKASIKAN MENGUNDURKAN DIRI</u></strong></div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Dalam hal pengunduran diri dan Pemutusan Hubungan Kerja karena Dikualifikasikan Mengundurkan Diri mengikuti ketentuan dalam Perjanjian Kerja Waktu Tertentu.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Dalam hal Pekerja mengundurkan diri tanpa memenuhi persyaratan pengunduran diri atau diputus hubungan kerjanya karena dikualifikasikan mengundurkan diri, maka Pekerja tidak akan mendapatkan kesempatan lagi untuk direkrut oleh perusahaan.
        </div>
      </div>
      <br />
      <div align="left">
        <p>Ditandatangani pada tanggal : {{ $param['date_pkwt'] }}</p>
        <br/> <img src="{{ public_path('data_file_ttd/'.$param['user']->user_id.'.jpeg') }}" alt="$param['user']->user_id" style="width: 150px; height: 75px; object-fit: cover;"> <br/>
        <p>({{ $param['user']->nama_lengkap }})</p>
      </div>
      <hr />
      <div align="center">
        <strong>Pernyataan Karyawan</strong>
      </div>
      <div>
        <p>Dengan ini saya menyatakan bahwa :</p>
        <div class="row">
          <div class="col-1">1. </div>
          <div class="col-11">Saya telah menerima, membaca, dan memahami Peraturan Operasional Pekerja untuk posisi/jabatan saya di atas.</div>
        </div>
        <div class="row">
          <div class="col-1">2. </div>
          <div class="col-11">Saya bersedia untuk mematuhi dan tunduk pada Peraturan Operasional Pekerja ini.</div>
        </div>
      </div>
      <br />
      <div>Ditandatangani pada tanggal : {{ $param['date_pkwt'] }} <br /> <strong>Pekerja</strong></div>
      <br/> <img src="{{ public_path('data_file_ttd/'.$param['user']->user_id.'.jpeg') }}" alt="$param['user']->user_id" style="width: 150px; height: 75px; object-fit: cover;"> <br/>
      <div>({{ $param['user']->nama_lengkap }})</div>
    </div>
  </div>
</body>

</html>