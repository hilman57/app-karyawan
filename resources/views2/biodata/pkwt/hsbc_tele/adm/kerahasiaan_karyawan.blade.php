<!DOCTYPE html>
<html>

<head>
  <title>PERJANJIAN KERAHASIAAN KARYAWAN</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <style type="text/css">
    /* table tr td,
    table tr th {
      font-size: 9pt;
    } */
    body {
      font-size: 11px;
    }
  </style>
  <div class="container">
    <center>
      <h5>PERJANJIAN KERAHASIAAN KARYAWAN</h5>
    </center>
    <br />
    <div align="justify">
      <p>Saya, {{ $param['user']->nama_lengkap }} pemegang Kartu Tanda Penduduk No. : {{ $param['user']->ktp }} memahami bahwa sebagai karyawan dari PT. ASEANINDO KAPITA SOLUSI dalam melaksanakan tugasnya untuk memberikan Jasa berdasarkan Perjanjian Pemberian Jasa Nomor : {{ $param['user']->contract }} berikut segala perubahan dan/atau penambahannya dari waktu ke waktu antara PT BANK HSBC INDONESIA Indonesia dan Perusahaan, (“Perjanjian”), Saya akan menerima akses untuk atau menerima informasi rahasia mengenai PT BANK HSBC INDONESIA Indonesia, nasabahnya dan pemohon Kartu Kredit dan Kredit Tanpa Agunan (“Nasabah”) dan Saya dengan ini menerima dan menyetujui hal-hal sebagai berikut: </p>
      <p>Saya tidak akan, baik selama atau setelah masa kerja Saya dengan Perusahaan, menyebarluaskan kepada siapapun, atau menggunakan setiap rahasia dagang atau setiap informasi rahasia mengenai kegiatan operasional, kesepakatan, transaksi, hubungan, bisnis atau keuangan PT BANK HSBC INDONESIA atau Nasabahnya.</p>
      <p>Saya tidak akan menyebarluaskan informasi mengenai Nasabah kepada pihak ketiga tanpa persetujuan dari Nasabah secara tertulis dimana kewajiban melaksanakan hal tersebut sesuai dengan ketentuan hukum yang berlaku.</p>
      <p>Saya tidak akan menggandakan setiap dokumen atau informasi yang berhubungan dengan PT BANK HSBC INDONESIA, bisnis PT BANK HSBC INDONESIA, Nasabah atau bisnis dari Nasabah untuk dipergunakan sendiri dan berkewajiban untuk setiap saat apabila diminta oleh PT BANK HSBC INDONESIA atau Perusahaan, menghancurkan (sebagaimana dibuktikan dengan suatu pernyataan tertulis) atau mengembalikan segera kepada PT BANK HSBC INDONESIA setiap dan seluruh informasi yang Saya miliki, atau Saya kuasai.</p>
      <p>Saya berkewajiban mengikuti instruksi, kebiasaan dan/atau kebijakan yang dikeluarkan oleh PT BANK HSBC INDONESIA dan/atau Perusahaan dari waktu ke waktu (termasuk namun tidak terbatas untuk menjaga meja kerja Saya, di manapun berada, bersih dari segala dokumen dan barang-barang lainnya yang berkaitan dengan rahasia dagang atau setiap informasi rahasia mengenai kegiatan operasional, kesepakatan, transaksi, hubungan, bisnis atau keuangan PT BANK HSBC INDONESIA atau Nasabahnya selama Saya tidak berada di meja kerja Saya tersebut) untuk tujuan menjalankan tugas-tugas Saya kepada Perusahaan dan setuju bahwa Saya akan menjaga segala tindakan Saya agar Perusahaan tidak melanggar tugas dan kewajiban berdasarkan ketentuan Perjanjian ini.</p>
      <p>Saya mengerti bahwa setiap pelanggaran atas Perjanjian Kerahasiaan ini akan berakibat pada diajukannya tindakan hukum terhadap Saya.</p>
      <p>Perjanjian Kerahasiaan Karyawan ini tunduk dan diatur pada ketentuan hukum negara Republik Indonesia.</p>
      <table border="0" width="100%">
        <tr>
          <td width="50%">
            <br/><br/><p>[materai Rp 10.000]</p><br/>
            <u>{{ $param['user']->nama_lengkap }}</u>
          </td>
          <td width="50%">
          <br/> <img src="{{ public_path('backend/assets/img_hrd/cap_aks.jpeg') }}" alt="cap_aks" style="margin-left: -20px;width: 150px; height: 75px; object-fit: cover;"> <br/>
            <u>Herman Julianto</u><br />Saksi
          </td>
        </tr>
        <tr>
          <td width="90%">
            Tanggal : {{ $param['date_pkwt'] }}
          </td>
          <td width="10%">
            Tanggal : {{ $param['date_pkwt'] }}
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>

</html>