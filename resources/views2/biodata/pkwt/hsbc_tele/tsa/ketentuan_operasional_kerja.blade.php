<!DOCTYPE html>
<html>

<head>
  <title>KETENTUAN OPERASIONAL KERJA</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    hr {
      border: 3px solid black;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container">
    <center>
      <h5>KETENTUAN OPERASIONAL KERJA <br /> <small>Outbound Contact Center - {{ $param['user']->position_name }}</small></h5>
    </center>
    <div align="justify">
      <p>Sebagai satu kesatuan dengan Pasal 5 Perjanjian Kerja Waktu Tertentu, berikut adalah Ketentuan Operasional Kerja (selanjutnya disebut “KOK”) untuk PIHAK KEDUA/{{ $param['user']->position_name }} (selanjutnya dalam KOK ini disebut “TSR”) yang berlaku efektif mulai tanggal {{ $param['date_pkwt'] }}</p>
      <!-- Part I -->
      <div class="row">
        <div class="col-1"><strong>I. </strong></div>
        <div class="col-11"><strong>Fraud</strong></div>
      </div>
      <!-- Part I Point 1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1. </div>
        <div class="col-10">
          Definisi Fraud <br />
          Yang dimaksud dengan Fraud adalah penipuan, pemalsuan, pencurian, dan atau penggelapan dokumen atau data-data calon atau nasabah HSBC. TSR dalam situasi apapun tidak dibenarkan atau dilarang untuk membantu siapapun untuk memperoleh data-data nasabah HSBC dengan cara yang tidak sesuai dengan peraturan yang telah disepakati maupun ketentuan perundangan yang berlaku. <br />
          Termasuk di dalam definisi ini dan juga merupakan Daftar Pembatasan TSR antara lain:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">Mengambil/meminta uang kepada calon nasabah.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">Mengajukan aplikasi tanpa sepengetahuan calon nasabah.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">Berbohong kepada calon nasabah dengan menjanjikan atau menawarkan fasilitas/manfaat palsu seperti: </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-9">Menjanjikan suku bunga dan biaya tertentu atas pinjaman yang disetujui.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">ii. </div>
        <div class="col-9">Menjanjikan/menawarkan hadiah yang tidak sejalan dengan program yang sedang berlaku.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">iii. </div>
        <div class="col-9">Menjanjikan limit tertentu/jaminan persetujuan palsu ke calon nasabah. </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-9">Memanfaatkan atau mengambil dokumen/salinan dokumen calon nasabah atau aplikasi bukan miliknya. </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-9">Memanfaatkan atau mengambil dokumen/salinan dokumen calon nasabah untuk kepentingan pribadi. </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-9">Mengambil dokumen/salinan dokumen tanpa melalui kurir yang telah ditunjuk oleh perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-9">Memalsukan tanda tangan atau dokumen/salinan dokumen calon nasabah.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">h. </div>
        <div class="col-9">Mengganti, merusakkan, atau menghilangkan isi aplikasi milik orang lain. </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-9">Membantu calon nasabah untuk mendapatkan Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC, dengan cara yang tidak sesuai dengan ketentuan perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">j. </div>
        <div class="col-9">Memiliki sub-agen untuk membantu dalam melakukan penjualan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">k. </div>
        <div class="col-9">Memasarkan atau menjual produk selain dari produk HSBC.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">l. </div>
        <div class="col-9">l. Memiliki pekerjaan ganda tanpa seizin pihak perusahaan atau terikat kontrak kerja dengan institusi lain ( perbankan/non perbankan ).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">m. </div>
        <div class="col-9">Menyalin/menggandakan aplikasi dan atau dokumen pelengkap aplikasi/permohonan untuk dimanfaatkan bagi keperluan pribadi (baik untuk proses pembuatan Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC,) atau keperluan lain di luar proses pembuatan Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC, kecuali telah memperoleh izin tertulis dari pihak perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">n. </div>
        <div class="col-9">Dalam keadaan atau alasan apapun menerima uang pembayaran nasabah, pinjam-meminjam uang atau meminta imbalan berupa uang atau dalam bentuk apapun dari calon nasabah atau nasabah HSBC untuk membantu calon nasabah HSBC memperoleh Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC, atau dengan cara melanggar peraturan HSBC atau peraturan yang telah disepakati oleh TSR serta ketentuan perundang-undangan yang berlaku untuk mencapai target TSR yang telah ditentukan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">o. </div>
        <div class="col-9">Mengabaikan ketentuan yang menyangkut pekerjaan sehari-hari, antara lain seperti contoh dibawah ini (namun tidak terbatas hal dibawah ini):</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Menjaga kerahasiaan password yang berhubungan dengan system dan tidak menginformasikannya kepada pihak lain (sharing password).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Merahasiakan data nasabah dan tidak membicarakannya di tempat umum (selain area Outbound Contact Center).</div>
      </div>
      <br /><br /><br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">p. </div>
        <div class="col-9">Mengabaikan salah satu dari rangkaian proses pengajuan Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC, melalui proses telesales sebagai berikut:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-8">Melakukan proses Know Your Customer (KYC) dengan baik dan benar sesuai dengan prosedur yang berlaku.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">ii. </div>
        <div class="col-8">Menanyakan minimal 18 informasi dari calon nasabah sesuai peraturan yang berlaku dan wajib mengisinya di dalam aplikasi sebelum aplikasi tersebut dikirimkan melalui faks (khusus aplikasi New To Bank)</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">iii. </div>
        <div class="col-8">Melakukan pengiriman aplikasi kepada calon nasabah dengan menggunakan faks sistematis (tidak manual) dari PC TSR (khusus aplikasi New To Bank)</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">iv. </div>
        <div class="col-8">Menghubungi calon nasabah kembali sesaat setelah melakukan pengiriman aplikasi melalui faks sistematis untuk memastikan bahwa faks telah diterima dengan jelas oleh calon nasabah (khusus aplikasi New To Bank)</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">v. </div>
        <div class="col-8">Melakukan pembuatan surat perintah pengambilan aplikasi dan salinan dokumen ke calon nasabah berdasarkan kesepakatan yang telah disetujui antara TSR dan calon nasabah (khusus aplikasi New To Bank)</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">q. </div>
        <div class="col-9">Menerima uang atau hadiah dari nasabah dan tidak melaporkannya kepada atasan atau pihak perusahaan tempat TSR bekerja.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">r. </div>
        <div class="col-9">Meminjamkan atau meminjam uang dengan sesama karyawan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">s. </div>
        <div class="col-9">Mengisi absensi dengan data yang tidak benar.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">t. </div>
        <div class="col-9">Melakukan tindakan yang membahayakan hubungan baik antara nasabah dan perusahaan.</div>
      </div>
      <!-- Part I Point 2 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2. </div>
        <div class="col-10">Sanksi Fraud</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">Sesuai bobot kesalahan yang cukup diputuskan semata-mata berdasarkan putusan PIHAK PERTAMA dan/atau Tim Fraud dari Perusahaan, TSR setuju dapat dikenakan Peringatan Lisan (Verbal Warning), Surat Peringatan (Warning Letter) I/II/III atau Pemutusan Hubungan Kerja tanpa adanya ganti kerugian dalam bentuk apapun.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">Tindakan Fraud akan mempengaruhi penilaian kinerja (Performance Review) dan perpanjangan kontrak TSR.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">TSR, melalui perjanjian kerja ini, memberikan kuasa kepada PIHAK PERTAMA untuk tidak membayarkan komisi, tunjangan, bonus, dan atau hadiah yang masih belum diterima PIHAK KEDUA sebagai uang pengganti kerugian administrasi dan verifikasi tindakan kecurangan sebesar Rp 150.000 per kasus. Jumlah ini belum termasuk penggantian kerugian yang diakibatkan oleh TSR.</div>
      </div>
      <!-- Part I Point 3 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">3. </div>
        <div class="col-10">
          Pencegahan Fraud <br />
          TSR menyetujui bahwa dalam rangka mencegah terjadinya Fraud, perusahaan akan dari waktu kewaktu mengadakan pemeriksaan terhadap barang bawaan TSR dan atau laci/loker dari TSR termasuk melakukan investigasi kepada TSR sebagai bagian dari proses investigasi indikasi fraud.
        </div>
      </div>
      <!-- Part II -->
      <div class="row">
        <div class="col-1"><strong>II. </strong></div>
        <div class="col-11"><strong>TATA TERTIB KERJA</strong></div>
      </div>
      <!-- Part II Point 1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1. </div>
        <div class="col-10">
          Waktu kerja adalah 5 (lima) atau 6 hari dan 1 atau 2 (dua) hari libur dalam seminggu dengan ketentuan 8 (delapan) jam kerja dan 1 (satu) jam atau 30 menit istirahat dalam 1 (satu) hari. Tergantung dari penetapan shifting dari pihak PERTAMA.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2. </div>
        <div class="col-10">
          TSR wajib mengikuti jam kerja yang berlaku, termasuk penetapan shifting dari PIHAK PERTAMA, yang akan dikomunikasikan kepada TSR.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">3. </div>
        <div class="col-10">
          Datang tepat waktu pada jam masuk kerja, dan pulang kerja sesuai dengan waktu kerja yang ditetapkan perusahaan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4. </div>
        <div class="col-10">
          Tidak diperkenankan datang kerja terlambat (maksimal 5 menit) > 2 x atau total keterlambatan >30 menit dalam 1 (satu) bulan tanpa seizin Manager.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">5. </div>
        <div class="col-10">
          Tidak diperkenankan istirahat lebih awal dari jadwal yang ditentukan tanpa seizin Manager.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6. </div>
        <div class="col-10">
          Tidak diperkenankan terlambat kembali bekerja setelah istirahat dengan total keterlambatan lebih dari atau sama dengan 15 menit dari jadwal yang ditetapkan dalam 1 bulan tanpa seizin Manager.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">7. </div>
        <div class="col-10">
          Bekerja lembur harus berdasarkan perintah dari Manager sesuai kebutuhan perusahaan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">8. </div>
        <div class="col-10">
          Konfirmasi ketidakhadiran harus disampaikan langsung oleh TSR selambat-lambatnya 1 (satu) jam sebelum jadwal masuk kerja dan harus mendapatkan persetujuan dari Manager melalui telepon atau SMS.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">9. </div>
        <div class="col-10">
          Jika TSR tidak bekerja tanpa ijin/persetujuan Manager atau sakit tanpa surat keterangan dokter yang sah dan benar maka TSR dikategorikan sebagai mangkir.
        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">10. </div>
        <div class="col-10">
          Setiap ketidakhadiran karena sakit harus disertai surat keterangan dokter yang sah dan benar yang diberikan pada hari pertama masuk kerja kepada Manager.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">11. </div>
        <div class="col-10">
          Surat sakit yang berlaku hanyalah yang asli, tercantum alamat, nomor telepon tempat praktek dokter, cap dan tanda tangan asli dokter yang memeriksa.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">12. </div>
        <div class="col-10">
          Jika sakit yang diderita menyebabkan TSR harus beristirahat > 1 hari atau dirawat di rumah sakit, foto copy surat sakit dapat dikirimkan terlebih dahulu melalui Fax ditujukan kepada Supervisor dan/atau Manager.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">13. </div>
        <div class="col-10">
          Tidak diperkenankan berjualan apapun di area kerja untuk kepentingan pribadi.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">14. </div>
        <div class="col-10">
          Tidak diperkenankan membawa masuk tas, telepon genggam, dan alat tulis menulis ke dalam ruangan kerja. Semua barang pribadi TSR harus disimpan di dalam loker yang sudah disediakan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">15. </div>
        <div class="col-10">
          Tidak diperkenankan mendengarkan radio/musik pada saat bekerja dengan menggunakan media apapun.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">16. </div>
        <div class="col-10">
          Tidak diperkenankan menggunakan fasilitas kantor untuk keperluan pribadi (antara lain seperti: bermain game di komputer, berbicara melalui extension untuk keperluan pribadi, menggunakan telepon untuk keperluan pribadi, dll).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">17. </div>
        <div class="col-10">
          Mengenakan Access/ID card sendiri selama berada di lingkungan tempat kerja dan saat memasuki dan keluar tempat kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">18. </div>
        <div class="col-10">
          Tidak diperkenankan berbicara/tertawa secara berlebihan dan berkelompok di ruangan kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">19. </div>
        <div class="col-10">
          Tidak diperkenankan tidur, duduk dengan posisi merebahkan kepala pada sandaran kursi pada saat berkomunikasi dengan nasabah atau melepas sepatu selama jam kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">20. </div>
        <div class="col-10">
          Tidak diperkenankan keluar dari area kerja pada saat bekerja tanpa izin dari atasan yang berwenang /manager.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">21. </div>
        <div class="col-10">
          TSR dilarang berjalan-jalan pada waktu kerja yang telah ditentukan kecuali untuk mengambil minum, ke kamar kecil atau shalat dan ini harus dengan sepengetahuan/izin dari atasan yang berwenang/manager.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">22. </div>
        <div class="col-10">
          Tidak diperkenankan makan selama waktu kerja berlangsung dan atau sambil berbicara dengan nasabah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">23. </div>
        <div class="col-10">
          Pakaian kerja :
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">
          Senin - Kamis (Busana Casual)
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-9">Pria :</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Kemeja lengan pendek maupun panjang berkerah.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Celana panjang pantalon dengan warna solid/gelap.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Sepatu kantor dan kaos kaki (sepatu keds/olahraga dan sepatu sandal tidak termasuk ke dalam kategori ini).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-9">Wanita :</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Kemeja, blouse, atau sweater dengan lengan pendek maupun panjang.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Rok minimal di bawah lutut dan celana panjang berbahan kain.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Sepatu kantor (sepatu keds/olahraga, dan sepatu sandal tidak termasuk ke dalam kategori ini).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">
          Jumat (Office Casual)
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-9">Pria :</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Kemeja lengan pendek maupun panjang atau polo shirt.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Celana panjang (boleh dengan bahan jeans namun tidak robek).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Sepatu keds, sepatu olahraga, maupun sepatu casual lainnya (tidak diperkenankan menggunakan sepatu sandal).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-9">Wanita :</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Kemeja atau polo shirt atau pakaian lain berlengan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Celana panjang/rok (boleh dengan bahan jeans namun tidak robek dan minimal di bawah lutut).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Sepatu sandal bertali, sepatu keds/olahraga maupun sepatu casual lainnya.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">
          Sabtu – Minggu, Hari libur lainnya
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-9">Pria :</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">T-shirt berlengan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Celana panjang (boleh dengan bahan jeans).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Sepatu sandal/fantofel/sepatu keds maupun sepatu olahraga.</div>
      </div>
      <br /><br /><br /><br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-9">Wanita :</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">T-shirt atau pakaian lain berlengan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Celana panjang/Rok ( boleh dengan bahan jeans dan minimal di bawah lutut ).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-8">Sepatu sandal/sepatu keds/olahraga/sepatu casual lainnya (Tidak diperkenankan menggunakan sandal).</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">24. </div>
        <div class="col-10">
          Diwajibkan menjaga kebersihan dan kerapihan area kerja, antara lain seperti :
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">
          Menjaga kebersihan dan kerapihan workstation selama bekerja atau setelah jam kerja selesai.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">
          Tidak mencoret meja kerja, keyboard, komputer, loker, dinding, toilet dan infrastruktur lainnya dalam area kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">
          Tidak meletakan makanan di sekitar workstation.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-9">
          Tidak meninggalkan sepatu di kolong meja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-9">
          Me-lock off komputer saat meninggalkan meja kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-9">
          Selalu menghancurkan terlebih dahulu sebelum membuang dokumen yang berhubungan dengan data nasabah dan yang tidak dipergunakan lagi.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-9">
          Menjalankan “Clean Desk Policy”
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">25. </div>
        <div class="col-10">
          Menjaga lingkungan, hubungan yang baik dan komunikasi kerja yang harmonis dengan seluruh pekerja dan atasan di tempat kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">26. </div>
        <div class="col-10">
          Diwajibkan untuk mengembalikan semua dokumen (seperti antara lain: training material, log book, dll) dan peralatan kerja (seperti antara lain: headset, kunci loker/drawer, kalkulator, kartu akses masuk, dll) yang pernah diterima oleh TSR dari PIHAK PERTAMA, kepada PIHAK PERTAMA atau atasan langsung pada hari terakhir TSR hadir bekerja sebelum perjanjian atau hubungan kerja antara TSR dan PIHAK PERTAMA berakhir atau tidak dilanjutkan. Bila ada kerusakan/hilangnya peralatan kerja, maka TSR bertanggung jawab atas penggantiannya.
        </div>
      </div>
      <!-- Part III -->
      <div class="row">
        <div class="col-1"><strong>III. </strong></div>
        <div class="col-11"><strong>TARGET DAN PRODUKTIFITAS MINIMUM</strong></div>
      </div>
      <!-- Part III Point 1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1. </div>
        <div class="col-10">
          Target yang harus dicapai oleh TSR akan mengikuti ketentuan yang berlaku dan ditetapkan oleh Perusahaan dari waktu ke waktu yang akan dikomunikasikan secara terbuka kepada TSR melalui PIHAK PERTAMA.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2. </div>
        <div class="col-10">
          Pengukuran hasil kerja akan dilakukan setiap bulan secara teratur. Waktu kerja, cuti dan hari yang terpakai pada masa training / pelatihan adalah tidak diperhitungkan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">3. </div>
        <div class="col-10">
          Prestasi kerja TSR akan dicatat dimana data tersebut menjadi acuan atas penilaian prestasi kerja yang bersangkutan selama bertugas di Outbound Contact Center dimana TSR ditempatkan dan tidak menjadi kedaluawarsa dengan berakhirnya suatu periode dari perjanjian kerja yang bersangkutan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4. </div>
        <div class="col-10">
          Metode pengukuran atas target dan produktivitas tersebut dapat berubah sesuai dengan JD (Job Description) yang berlaku dan akan di komunikasikan secara terbuka dan tertulis melalui PIHAK PERTAMA .
        </div>
      </div>
      <!-- Part IV -->
      <div class="row">
        <div class="col-1"><strong>IV. </strong></div>
        <div class="col-11"><strong>KEWAJIBAN DAN TANGGUNG JAWAB LAINNYA</strong></div>
      </div>
      <!-- Part IV Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Wajib untuk selalu mengikuti ketentuan dan atau perubahan dan perkembangan ketentuan yang ditetapkan oleh Perusahaan dalam aspek kedisiplinan, tata tertib, etika bisnis, kepatuhan pada peraturan kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Bertanggung jawab untuk mencari kesempatan sebanyak-banyaknya untuk menawarkan penjualan Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC yang ditargetkan dengan tidak mengesampingkan pelayanan yang baik kepada nasabah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Wajib untuk menguasai setiap produk yang dijual, termasuk setiap program atau promosi yang yang sedang berjalan dan bisa menjelaskan dengan baik, akurat serta tidak mengandung unsur penipuan (breaching sales) kepada calon nasabah.
        </div>
      </div>
      <!-- Part V -->
      <div class="row">
        <div class="col-1"><strong>V. </strong></div>
        <div class="col-11">
          <strong>MEKANISME PEMBERIAN SURAT PERINGATAN DAN PEMUTUSAN HUBUNGAN KERJA</strong><br />
          TSR yang tidak mencapai target atau melanggar peraturan-peraturan yang telah ditetapkan maka akan mendapatkan Peringatan Lisan (Verbal Warning) atau Surat Peringatan I(Pertama), II (Kedua) dan III (Ketiga) yang masing-masing masa berlakunya 1 bulan atau Pemutusan Hubungan Kerja tanpa ganti kerugian dalam bentuk apapun sesuai prosedur berlaku, sebagai berikut :
        </div>
      </div>
      <!-- Part V Point 1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1. </div>
        <div class="col-10">
          Surat Peringatan Pertama (ke-1) diberikan apabila TSR :
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">
          Mangkir sebanyak 1 hari kerja dalam periode 1 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">
          Melakukan kesalahan dalam penawaran atau penjualan produk Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC, yang mengakibatkan kerugian non finansial.
        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">
          Merokok di area kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-9">
          Tidak dapat memenuhi standar prestasi kerja yang ditetapkan dalam waktu yang telah disepakati.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-9">
          Tidak mematuhi tata tertib kerja pada butir II.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-9">
          Tidak memenuhi kewajiban dan tanggung jawab lainnya pada butir IV.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-9">
          Terbukti secara sengaja / tidak sengaja melakukan penjualan produk / non produk perusahaan kepada nasabah dengan cara diluar ketentuan yang berlaku (breaching sales).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">h. </div>
        <div class="col-9">
          Tidak mematuhi instruksi kerja dari atasan yang berhubungan dengan pekerjaan
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-9">
          Tidak memberikan surat keterangan dokter yang sah dan benar, apabila sakit 2 (hari) hari berturut-turut atau lebih, dalam 3 x 24 jam dari hari pertama masuk kerja setelah tidak masuk kerja karena sakit.
        </div>
      </div>
      <!-- Part V Point 2 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2. </div>
        <div class="col-10">
          Surat Peringatan Kedua (ke-2) diberikan apabila TSR:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">
          Mangkir sebanyak 2 hari kerja dalam periode 1 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">
          Melakukan perbuatan ulang yang tercantum dalam Surat Peringatan I (Pertama) dan/atau perbuatan lainnya yang bobot kesalahannya mengakibatkan diberikannya Surat Peringatan I (Pertama) dalam masa berlakunya surat peringatan I (Pertama).
        </div>
      </div>
      <!-- Part V Point 3 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">3. </div>
        <div class="col-10">
          Surat Peringatan Ketiga (ke-3) diberikan apabila TSR :
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">
          Mangkir sebanyak 3 hari kerja dalam periode 1 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">
          Melakukan perbuatan yang bobot kesalahannya mengakibatkan diberikannya Surat Peringatan I (pertama) dalam masa berlakunya Surat Peringatan II (kedua) atau melakukan perbuatan yang bobot kesalahannya mengakibatkan diberikannya Surat Peringatan II (kedua) dalam masa berlakunya Surat Peringatan I (pertama).
        </div>
      </div>
      <!-- Part V Point 4 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4. </div>
        <div class="col-10">
          Pemutusan Hubungan Kerja (PHK) tanpa ganti kerugian dalam bentuk apapun, apabila TSR :
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">
          Melanggar integritas yang ditetapkan perusahaan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">
          Melakukan tindakan fraud, misconduct atau mis-selling dalam melakukan perannya sebagai TSR.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">
          Menggunakan kata-kata yang tidak sopan pada saat melayani nasabah yang mana hal tersebut berpotensi merusak citra dan nama baik Perusahaan dimana TSR ditempatkan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-9">
          Dalam masa berlaku Surat Peringatan I atau II atau III, melakukan perbuatan yang bobot kesalahannya mengakibatkan diberikannya surat peringatan dalam tabel dibawah sebagai berikut:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-9">
          <table border="1" width="100%">
            <tr>
              <td>Dalam Masa Berlaku dari Surat Peringatan :</td>
              <td>Melakukan Kesalahan yang mengakibatkan diberikannya Surat Peringatan :</td>
            </tr>
            <tr>
              <td>I (Pertama)</td>
              <td>III (Ketiga)</td>
            </tr>
            <tr>
              <td>II (Kedua)</td>
              <td>II (Kedua) atau III (Ketiga)</td>
            </tr>
            <tr>
              <td>III (Ketiga)</td>
              <td>I (Pertama) atau II (Kedua) atau III (Ketiga)</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- Part VI -->
      <div class="row">
        <div class="col-1"><strong>VI. </strong></div>
        <div class="col-11"><strong>MEKANISME PEMBERIAN SURAT PANGGILAN KERJA DAN PENGUNDURAN DIRI</strong></div>
      </div>
      <!-- Part VI Point 1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1. </div>
        <div class="col-10">
          Apabila TSR mangkir (tidak bekerja tanpa ijin dari atasan langsung atau sakit tanpa surat dokter yang sah dan benar) selama 5 hari kerja berturut-turut atau lebih, dan PIHAK PERTAMA telah mengirimkan Surat Panggilan sebanyak 2 kali, yang dialamatkan sesuai dengan catatan yang disampaikan TSR kepada PIHAK PERTAMA, maka TSR dapat diputus hubungan kerjanya karena dikualifikasikan mengundurkan diri dari pekerjaannya.
        </div>
      </div>
      <!-- Part VI Point 2 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2. </div>
        <div class="col-10">
          Apabila TSR mengundurkan diri dari pekerjaannya tanpa ada pemberitahuan secara tertulis minimal 1 (Satu) bulan sebelumnya kepada Perusahaan atau atasan langsung dan PIHAK PERTAMA, atau TSR mangkir sebelum berakhirnya masa kontrak/perjanjian maka TSR tidak berhak atas surat referensi kerja.
        </div>
      </div>
      <!-- Part VII -->
      <div class="row">
        <div class="col-1"><strong>VII. </strong></div>
        <div class="col-11"><strong>PEMBAYARAN KOMISI DAN BONUS</strong></div>
      </div>
      <!-- Part VII Point 1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1. </div>
        <div class="col-10">
          Komisi penjualan beserta bonus akan dihitung dan dibayarkan per bulan berdasarkan skema komisi dan bonus yang telah ditentukan. Perhitungan dan pembayaran tersebut tidak dapat diperhitungkan ataupun diakumulasikan ke bulan berikutnya.
        </div>
      </div>
      <!-- Part VII Point 2 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2. </div>
        <div class="col-10">
          Khusus bagi TSR yang mengundurkan diri pada bulan berjalan tidak berhak atas pembayaran insentif/tunjangan shift pada bulan berikutnya Contoh : TSR mengundurkan diri pada bulan Januari maka pembayaran terakhir akan jatuh pada bulan Pebruari. TSR tidak berhak mengklaim Insentif / Tunjangan Shift diluar dari ketentuan tersebut diatas.
        </div>
      </div>
      <br /><br /><br />
      <!-- Part VII Point 3 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">3. </div>
        <div class="col-10">
          Apabila TSR tidak dapat memenuhi target minimum yang ditetapkan dalam pengukuran kinerja bulanan, maka TSR yang bersangkutan tidak akan mendapatkan insentif penjualan di bulan tersebut.
        </div>
      </div>
      <!-- Part VII Point 4 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4. </div>
        <div class="col-10">
          Apabila TSR terbukti secara sengaja / tidak sengaja melakukan penjualan Kartu Kredit dan atau KTA dan atau produk HSBC lainnya dan atau produk pihak ketiga yang didistribusikan HSBC kepada nasabah dengan cara diluar ketentuan yang berlaku (breaching sales) maka TSR tersebut tidak akan mendapatkan seluruh insentif penjualan pada bulan tersebut.
        </div>
      </div>
      <!-- Part VIII -->
      <div class="row">
        <div class="col-1"><strong>VIII. </strong></div>
        <div class="col-11"><strong>PENGUNDURAN DIRI DAN PEMUTUSAN HUBUNGAN KERJA KARENA DIKUALIFIKASIKAN MENGUNDURKAN DIRI</strong></div>
      </div>
      <!-- Part VIII Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Dalam hal pengunduran diri dan Pemutusan Hubungan Kerja karena Dikualifikasikan Mengundurkan Diri mengikuti ketentuan dalam Perjanjian Kerja Waktu Tertentu.
        </div>
      </div>
      <!-- Part VIII Point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Dalam hal Pekerja mengundurkan diri tanpa memenuhi persyaratan pengunduran diri atau diputus hubungan kerjanya karena dikualifikasikan mengundurkan diri, maka Pekerja tidak akan mendapatkan kesempatan lagi untuk direkrut oleh Perusahaan serta tidak berhak atas surat referensi kerja.
        </div>
      </div>
      <br />
      <div align="left">
        <p>Ditandatangani pada tanggal : {{ $param['date_pkwt'] }}</p>
        <br /><br />
        <p>({{ $param['user']->nama_lengkap }})</p>
      </div>
      <hr />
      <div align="center">
        <strong>Pernyataan Karyawan</strong>
      </div>
      <div>
        <p>Dengan ini saya menyatakan bahwa :</p>
        <div class="row">
          <div class="col-1">1. </div>
          <div class="col-11">Saya telah menerima, membaca, dan memahami Peraturan Operasional Pekerja untuk posisi/jabatan saya di atas.</div>
        </div>
        <div class="row">
          <div class="col-1">2. </div>
          <div class="col-11">Saya bersedia untuk mematuhi dan tunduk pada Peraturan Operasional Pekerja ini.</div>
        </div>
      </div>
      <br />
      <div>Ditandatangani pada tanggal : {{ $param['date_pkwt'] }} <br /> <strong>Pekerja</strong></div>
      <br /><br /><br /><br />
      <div>({{ $param['user']->nama_lengkap }})</div>
    </div>
  </div>
</body>

</html>