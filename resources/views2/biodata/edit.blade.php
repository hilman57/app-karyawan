@extends('layouts.master')

@section('title')
<title>Edit Biodata</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
   <div class="page-content">
      <div class="container-fluid">

         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               @if ($message = Session::get('success'))
               <div class="alert alert-success">
                  <p>{{ $message }}</p>
               </div>
               @elseif ($message = Session::get('error'))
               <div class="alert alert-danger">
                  <p>{{ $message }}</p>
               </div>
               @endif
               <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                  <h4 class="mb-sm-0 font-size-18">Edit Biodata</h4>
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        {{-- <li class="breadcrumb-item"><a href="javascript: void(0);">Utilitas</a></li> --}}
                        <li class="breadcrumb-item"><a href="{{ route('biodata.index') }}">Biodata</a></li>
                        <li class="breadcrumb-item active">Edit Biodata</li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <!-- end page title -->
         <div class="row">
            <div class="col-lg-12">
               <form method="POST" action="{{ route('biodata.update')  }}">
                  @foreach ($data as $detail)
                  @csrf
                  <input type="hidden" id="userId" value="{{ $detail->user_id }}">
                  <input type="hidden" name="user_id" value="{{ $detail->user_id }}">
                  <div class="card">
                     <div class="card-body">
                        <div>
                           <div class="row">
                              <div class="col-lg-8">
                                 <div class="row">
                                    <h4>Data Pribadi</h4>
                                    <div class="col-lg">
                                       <label class="form-label">Nama Lengkap</label>
                                       <input class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" type="text" name="nama" value="{{ $detail->nama_lengkap }}" required>
                                    </div>
                                    <div class="col-lg">
                                       <label class="form-label">Jabatan</label>
                                       <div id="wrap-jabatan">
                                          <select class="form-control m-bot15" name="jabatan">
                                             <?php foreach ($jabatan as $value) : ?>
                                                <option <?php if ($detail->id_jabatan == $value->id) { ?> selected="selected" <?php } ?> value='<?php echo $value->id; ?>'>
                                                   <?php echo $value->position_name;  ?>
                                                </option>
                                             <?php endforeach; ?>
                                          </select>
                                          <p class="text-danger">{{ $errors->first('jabatan') }}</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col-lg-6">
                                       <label class="form-label">KTP</label>
                                       <input class="form-control {{ $errors->has('ktp') ? 'is-invalid':'' }}" type="number" name="ktp" value="{{ $detail->ktp }}" required>
                                       <p class="text-danger">{{ $errors->first('ktp') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col-lg">
                                       <label class="form-label">Site</label>
                                       <select class="form-control m-bot15" name="site" onchange="pickSite(this.value, '{{ $detail->id_jabatan }}')">
                                          <?php foreach ($site as $value) : ?>
                                             <option <?php if ($detail->id_site == $value->id) { ?> selected="selected" <?php } ?> value='<?php echo $value->id; ?>'>
                                                <?php echo $value->site_name;  ?>
                                             </option>
                                          <?php endforeach; ?>
                                       </select>
                                       <p class="text-danger">{{ $errors->first('site') }}</p>
                                    </div>
                                    <div class="col-lg">
                                       <label class="form-label">Status</label>
                                       <select class="form-control m-bot15" name="status_kry">
                                          <?php foreach ($status as $value) : ?>
                                             <option <?php if ($detail->id_status == $value->id) { ?> selected="selected" <?php } ?> value='<?php echo $value->id; ?>'>
                                                <?php echo $value->status_ky;  ?>
                                             </option>
                                          <?php endforeach; ?>
                                       </select>
                                       <p class="text-danger">{{ $errors->first('status_kry') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">No. NPWP</label>
                                       <input class="form-control {{ $errors->has('npwp') ? 'is-invalid':'' }}" type="number" name="npwp" value="{{ $detail->npwp }}">
                                       <p class="text-danger">{{ $errors->first('npwp') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Tanggal NPWP</label>
                                       <input class="form-control {{ $errors->has('tgl_npwp') ? 'is-invalid':'' }}" type="date" name="tgl_npwp" value="{{ $detail->tgl_npwp }}">
                                       <p class="text-danger">{{ $errors->first('tgl_npwp') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div>
                                    <label class="form-label">Email</label>
                                    <input class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" type="text" name="email" value="{{ $detail->email }}" required>
                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Telepon</label>
                                       <input class="form-control {{ $errors->has('telpon') ? 'is-invalid':'' }}" type="number" name="telpon" value="{{ $detail->telpon }}">
                                       <p class="text-danger">{{ $errors->first('telpon') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">HP</label>
                                       <input class="form-control {{ $errors->has('hp') ? 'is-invalid':'' }}" type="number" name="hp" value="{{ $detail->hp }}" required>
                                       <p class="text-danger">{{ $errors->first('hp') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Tempat Lahir</label>
                                       <input class="form-control {{ $errors->has('tempat') ? 'is-invalid':'' }}" type="text" name="tempat" value="{{ $detail->tempat }}" required>
                                       <p class="text-danger">{{ $errors->first('tempat') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Tanggal Lahir</label>
                                       <input class="form-control " name="tgl_lahir" type="date" id="" value="{{$detail->tgl_lahir}}">
                                       <p class="text-danger"></p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Jenis Kelamin</label>
                                       <select class="form-control m-bot15" name="jenis_kelamin" required>
                                          <option <?php if ('P' == $detail->jenis_kelamin) { ?> selected="selected" <?php }  ?> value='L'>
                                             Laki-laki
                                          </option>
                                          <option <?php if ('W' == $detail->jenis_kelamin) { ?> selected="selected" <?php } ?> value='P'>
                                             Perempuan
                                          </option>

                                       </select>

                                       <p class="text-danger">{{ $errors->first('jenis_kelamin') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Agama</label>
                                       <select class="form-control m-bot15" name="agama" required>
                                          <option value="Islam" <?php if ($detail->agama == 'Islam') { ?> selected="selected" <?php } ?>>Islam</option>
                                          <option value="Kristen" <?php if ($detail->agama == 'Kristen') { ?> selected="selected" <?php } ?>>Kristen</option>
                                          <option value="Hindu" <?php if ($detail->agama == 'Hindu') { ?> selected="selected" <?php } ?>>Hindu</option>
                                          <option value="Budha" <?php if ($detail->agama == 'Budha') { ?> selected="selected" <?php } ?>>Budha</option>
                                          <option value="Lainnya" <?php if ($detail->agama == 'Lainnya') { ?> selected="selected" <?php } ?>>Lainnya</option>
                                       </select>
                                       <p class="text-danger">{{ $errors->first('agama') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Berat Badan</label>
                                       <input class="form-control {{ $errors->has('berat_badan') ? 'is-invalid':'' }}" type="number" name="berat_badan" value="{{ $detail->berat_badan }}">
                                       <p class="text-danger">{{ $errors->first('berat_badan') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Tinggi Badan</label>
                                       <input class="form-control {{ $errors->has('tinggi_badan') ? 'is-invalid':'' }}" type="number" name="tinggi_badan" value="{{ $detail->tinggi_badan }}">
                                       <p class="text-danger">{{ $errors->first('tinggi_badan') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div>
                                    <label class="form-label">Alamat</label>
                                    <input class="form-control {{ $errors->has('alamat') ? 'is-invalid':'' }}" type="text" name="alamat" value="{{ $detail->alamat }}" required>
                                    <p class="text-danger">{{ $errors->first('alamat') }}</p>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Kota</label>
                                       <input class="form-control {{ $errors->has('kota') ? 'is-invalid':'' }}" type="text" name="kota" value="{{ $detail->kota }}">
                                       <p class="text-danger">{{ $errors->first('kota') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Kodepos</label>
                                       <input class="form-control {{ $errors->has('kodepos') ? 'is-invalid':'' }}" type="number" name="kodepos" value="{{ $detail->kodepos }}" required>
                                       <p class="text-danger">{{ $errors->first('kodepos') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Pendidikan Terakhir</label>
                                       <input class="form-control {{ $errors->has('pendidikan_terakhir') ? 'is-invalid':'' }}" type="text" name="pendidikan_terakhir" value="{{ $detail->pendidikan_terakhir }}" required>
                                       <p class="text-danger">{{ $errors->first('pendidikan_terakhir') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Nama Lembaga</label>
                                       <input class="form-control {{ $errors->has('nama_lembaga') ? 'is-invalid':'' }}" type="text" name="nama_lembaga" value="{{ $detail->nama_lembaga }}" required>
                                       <p class="text-danger">{{ $errors->first('nama_lembaga') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div>
                                    <label class="form-label">Nama Ibu Kandung</label>
                                    <input class="form-control {{ $errors->has('nama_ibu_kandung') ? 'is-invalid':'' }}" type="text" name="nama_ibu_kandung" value="{{ $detail->nama_ibu_kandung }}">
                                    <p class="text-danger">{{ $errors->first('nama_ibu_kandung') }}</p>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Instagram</label>
                                       <input class="form-control {{ $errors->has('instagram') ? 'is-invalid':'' }}" type="text" name="instagram" value="{{ $detail->instagram }}">
                                       <p class="text-danger">{{ $errors->first('instagram') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Facebook</label>
                                       <input class="form-control {{ $errors->has('facebook') ? 'is-invalid':'' }}" type="text" name="facebook" value="{{ $detail->facebook }}">
                                       <p class="text-danger">{{ $errors->first('facebook') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Linkedin</label>
                                       <input class="form-control {{ $errors->has('linkedin') ? 'is-invalid':'' }}" type="text" name="linkedin" value="{{ $detail->linkedin }}">
                                       <p class="text-danger">{{ $errors->first('linkedin') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <h4>Data Keluarga</h4>
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Status Pernikahan</label>
                                       <select class="form-control m-bot15" name="status" required>
                                          <option <?php if ('1' == $detail->status) { ?> selected="selected" <?php }  ?> value='1'>
                                             Belum Menikah
                                          </option>
                                          <option <?php if ('2' == $detail->status) { ?> selected="selected" <?php } ?> value='2'>
                                             Menikah
                                          </option>
                                          <option <?php if ('3' == $detail->status) { ?> selected="selected" <?php } ?> value='3'>
                                             Bercerai
                                          </option>

                                       </select>
                                       <p class="text-danger">{{ $errors->first('status') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Nama Pasangan</label>
                                       <input class="form-control {{ $errors->has('nama_pasangan') ? 'is-invalid':'' }}" type="text" name="nama_pasangan" value="{{ $detail->nama_pasangan }}">
                                       <p class="text-danger">{{ $errors->first('nama_pasangan') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Pekerjaan Pasangan</label>
                                       <input class="form-control {{ $errors->has('pekerjaan_istri_suami') ? 'is-invalid':'' }}" type="text" name="pekerjaan_istri_suami" value="{{ $detail->pekerjaan_istri_suami }}">
                                       <p class="text-danger">{{ $errors->first('pekerjaan_istri_suami') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Jumlah Anak</label>
                                       <input class="form-control {{ $errors->has('jumlah_anak') ? 'is-invalid':'' }}" type="number" name="jumlah_anak" value="{{ $detail->jumlah_anak }}">
                                       <p class="text-danger">{{ $errors->first('jumlah_anak') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">Foto</label><br>
                                       {{-- @php echo  asset('data_file/'.$detail->foto); @endphp  --}}
                                       <label class="form-label">Dowonload :</label>&nbsp;
                                       <a href="{{asset('data_file/'.$detail->foto)}}" class="text-primary">{{ $detail->foto }}</a>
                                       {{-- <input class="form-control {{ $errors->has('foto') ? 'is-invalid':'' }}" type="text" name="foto" value="{{ $detail->foto }}" disabled> --}}
                                       <p class="text-danger">{{ $errors->first('foto') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">SKCK</label><br>
                                       <label class="form-label">Dowonload :</label>&nbsp;
                                       <a href="{{asset('data_file_skck/'.$detail->skck)}}" class="text-primary">{{ $detail->skck }}</a>
                                       {{-- <input class="form-control {{ $errors->has('skck') ? 'is-invalid':'' }}" type="text" name="skck" value="{{ $detail->skck }}" disabled> --}}
                                       <p class="text-danger">{{ $errors->first('skck') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col">
                                       <label class="form-label">CV</label><br>
                                       <label class="form-label">Dowonload :</label>&nbsp;
                                       <a href="{{asset('data_file_cv/'.$detail->cv)}}" class="text-primary">{{ $detail->cv }}</a>
                                       {{-- <input class="form-control {{ $errors->has('cv') ? 'is-invalid':'' }}" type="text" name="cv" value="{{ $detail->cv }}" disabled> --}}
                                       <p class="text-danger">{{ $errors->first('cv') }}</p>
                                    </div>
                                    <div class="col">
                                       <label class="form-label">Ijazah</label><br>
                                       <label class="form-label">Dowonload :</label>&nbsp;
                                       <a href="{{asset('data_file_ijazah/'.$detail->ijazah)}}" class="text-primary">{{ $detail->ijazah }}</a>
                                       {{-- <input class="form-control {{ $errors->has('ijazah') ? 'is-invalid':'' }}" type="text" name="ijazah" value="{{ $detail->ijazah }}" disabled> --}}
                                       <p class="text-danger">{{ $errors->first('ijazah') }}</p>
                                    </div>
                                 </div>
                              </div>
                              <h4>Pengalaman Kerja</h4>
                              <input type="hidden" id="routeUpdatePengalamanKerja" value="{{ route('biodata.updt_pgk') }}">
                              <div class="row" style="padding-top: 10px;" id="notif">
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="table-responsive">
                                       <table id="table-pengalaman" class="table table-striped mb-0">
                                          <thead>
                                             <tr>
                                                <th>Nama Perusahaan</th>
                                                <th>Jabatan</th>
                                                <th>Masa Kerja</th>
                                                <th>Alasan Keluar</th>
                                                <th>Alamat Perusahaan</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                          </tbody>
                                          <div class="modal hide" id="modalPengalaman" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                             <div class="modal-dialog modal-dialog-scrollable">
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <h5 class="modal-title">Edit Pengalaman Kerja</h5>
                                                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                   </div>
                                                   <div class="modal-body">
                                                      <h4>isi data</h4>
                                                      <form id="formShowDetail">
                                                         <div class="col-lg-8">
                                                            <input class="form-control " type="hidden" name="id" id="id">
                                                            <div class="row">
                                                               <div class="col">
                                                                  <label class="form-label">Nama Perusahaan</label>
                                                                  <input class="form-control " type="text" name="namaPerusahaan" id="namaPerusahaan">
                                                               </div>
                                                               <div class="col">
                                                                  <label class="form-label">Alamat Perusahaan</label>
                                                                  <input class="form-control " type="text" name="alamatPerusahaan" id="alamatPerusahaan">
                                                               </div>
                                                            </div>
                                                         </div>

                                                         <div class="col-lg-8">
                                                            <div class="row">
                                                               <div class="col">
                                                                  <label class="form-label">Jabatan</label>
                                                                  <input class="form-control " type="text" name="jabatanKerja" id="jabatanKerja">
                                                               </div>
                                                               <div class="col">
                                                                  <label class="form-label"> Masa Kerja</label>
                                                                  <input class="form-control " type="text" name="masaKerja" id="masaKerja">
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-lg-8">
                                                            <div>
                                                               <label class="form-label">Alasan Keluar</label>
                                                               <input class="form-control " type="text" name="alasanKeluar" id="alasanKeluar">
                                                            </div>
                                                         </div>
                                                   </div>
               </form>

               <div class="modal-footer">
                  <button type="button" class="btn btn-primary waves-effect waves-light" id="btnSubmitFinal">Submit</button>
                  <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">Close</button>
               </div>
            </div><!-- /.modal-content -->

         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      </table>
   </div>
</div>
</div>
<div class="col-lg-8"><br>
   <h4>Data Keluarga Dekat</h4>
   <div class="row">
      <div class="col">
         <label class="form-label">Nama Keluarga</label>
         <input class="form-control {{ $errors->has('nama_keluarga') ? 'is-invalid':'' }}" type="text" name="nama_keluarga" value="{{ $detail->nama_keluarga }}">
         <p class="text-danger">{{ $errors->first('nama_keluarga') }}</p>
      </div>
      <div class="col">
         <label class="form-label">Hubungan</label>
         <input class="form-control {{ $errors->has('hubungan_klg_dihub') ? 'is-invalid':'' }}" type="text" name="hubungan_klg_dihub" value="{{ $detail->hubungan_klg_dihub }}" required>
         <p class="text-danger">{{ $errors->first('hubungan_klg_dihub') }}</p>
      </div>
   </div>
</div>
<div class="col-lg-8">
   <div>
      <label class="form-label">Alamat Keluarga Dihubungi</label>
      <input class="form-control {{ $errors->has('alamat_keluarga_dihubungi') ? 'is-invalid':'' }}" type="text" name="alamat_keluarga_dihubungi" value="{{ $detail->alamat_keluarga_dihubungi }}" required>
      <p class="text-danger">{{ $errors->first('alamat_keluarga_dihubungi') }}</p>
   </div>
</div>
<div class="col-lg-8">
   <div>
      <label class="form-label">Kota Keluarga</label>
      <input class="form-control {{ $errors->has('kota_keluarga') ? 'is-invalid':'' }}" type="text" name="kota_keluarga" value="{{ $detail->kota_keluarga }}">
      <p class="text-danger">{{ $errors->first('kota_keluarga') }}</p>
   </div>
</div>
{{-- <div class="col-lg-8">
                                 <div>
                                    <label class="form-label">Hubungan Keluarga</label>
                                    <input class="form-control {{ $errors->has('hubungan_klg_dihub') ? 'is-invalid':'' }}" type="text" name="hubungan_klg_dihub" value="{{ $detail->hubungan_klg_dihub }}" >
<p class="text-danger">{{ $errors->first('hubungan_klg_dihub') }}</p>
</div>
</div> --}}
<div class="col-lg-8">
   <div class="row">
      <div class="col">
         <label class="form-label">Keluarga Telepon</label>
         <input class="form-control {{ $errors->has('keluarga_telpon') ? 'is-invalid':'' }}" type="number" name="keluarga_telpon" value="{{ $detail->keluarga_telpon }}">
         <p class="text-danger">{{ $errors->first('keluarga_telpon') }}</p>
      </div>
      <div class="col">
         <label class="form-label">Keluarga HP</label>
         <input class="form-control {{ $errors->has('keluarga_hp') ? 'is-invalid':'' }}" type="number" name="keluarga_hp" value="{{ $detail->keluarga_hp }}">
         <p class="text-danger">{{ $errors->first('keluarga_hp') }}</p>
      </div>
   </div>
</div>
<div class="col-lg-8">
   <h4>Data Bank</h4>
   <div class="row">
      <div class="col">
         <label class="form-label">No. Rekening</label>
         <input class="form-control {{ $errors->has('rekening') ? 'is-invalid':'' }}" type="text" name="rekening" value="{{ $detail->rekening }}">
         <p class="text-danger">{{ $errors->first('rekening') }}</p>
      </div>
      <div class="col">
         <label class="form-label">Nama Rekening</label>
         <input class="form-control {{ $errors->has('nama_rekening') ? 'is-invalid':'' }}" type="text" name="nama_rekening" value="{{ $detail->nama_rekening }}">
         <p class="text-danger">{{ $errors->first('nama_rekening') }}</p>
      </div>
   </div>
</div>
<div class="col-lg-8">
   <div>
      <label class="form-label">Cabang</label>
      <input class="form-control {{ $errors->has('cabang') ? 'is-invalid':'' }}" type="text" name="cabang" value="{{ $detail->cabang }}">
      <p class="text-danger">{{ $errors->first('cabang') }}</p>
   </div>
</div>
<div class="col-lg-8">
   <div class="row">
      <div class="col">
         <label class="form-label">AOC</label>
         <input class="form-control {{ $errors->has('aoc') ? 'is-invalid':'' }}" type="text" name="aoc" value="{{ $detail->aoc }}">
         <p class="text-danger">{{ $errors->first('aoc') }}</p>
      </div>
      <div class="col">
         <label class="form-label">Nama Leader</label>
         <input class="form-control {{ $errors->has('nama_leader') ? 'is-invalid':'' }}" type="text" name="nama_leader" value="{{ $detail->nama_leader }}">
         <p class="text-danger">{{ $errors->first('nama_leader') }}</p>
      </div>
   </div>
</div>
<div class="col-lg-8">
   <div class="row">
      <div class="col">
         <label class="form-label">Tunjangan</label>
         <input class="form-control {{ $errors->has('tunjangan') ? 'is-invalid':'' }}" type="number" name="tunjangan" value="{{ number_format($detail->tunjangan) }}">
         <p class="text-danger">{{ $errors->first('tunjangan') }}</p>
      </div>
      <div class="col">
         <label class="form-label">Gaji</label>
         <input class="form-control {{ $errors->has('gaji') ? 'is-invalid':'' }}" type="number" name="gaji" value="{{ number_format($detail->gaji) }}">
         <p class="text-danger">{{ $errors->first('gaji') }}</p>
      </div>
   </div>
</div>

<div class="col-lg-8">
   <h4>Lainnya</h4>
   <div class="row">
      <div class="col">
         <label class="form-label">Status Karyawan</label>
         <select class="form-control m-bot15" name="flag">
            <option value="1" <?php if ($detail->flag == '1') { ?> selected="selected" <?php } ?>>Aktif</option>
            <option value="2" <?php if ($detail->flag == '2') { ?> selected="selected" <?php } ?>>Tidak Aktif</option>
         </select>
         <p class="text-danger">{{ $errors->first('flag') }}</p>
      </div>
   </div>
</div>


</div>

<button type="submit" class="btn btn-primary">Submit</button>
<a href="{{ route('biodata.index') }}" class="btn btn-danger">Cancel</a>
</div>
@endforeach
</div>
</div>
</form>
</div>
</div>
<!-- end row -->
</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
</div>
<!-- end main content-->
<!-- /.control-sidebar -->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/biodata/edit.js') }}"></script>
@endsection