@extends('layouts.master')

@section('title')
    <title>Dashboard Admin</title>
@endsection

@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">
                <div class="page-content">
                    <div class="container-fluid">
                    @include('partials.messages')

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                    <h4 class="mb-sm-0 font-size-18">Dashboard</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                                            <li class="breadcrumb-item active">Dashboard</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">                            
                            <div class="col-xl-12">
                                <div class="row">
                                    @foreach ($Applicant as $val)                                   
                                    <div class="col-md-6">
                                        <div class="card mini-stats-wid">
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="flex-grow-1">
                                                        <p class="text-muted fw-medium">Applicant</p>
                                                        <h4 class="mb-0">{{$val->user_count}}</h4>
                                                    </div>
                                                    <div class="flex-shrink-0 align-self-center">
                                                        <div class="mini-stat-icon avatar-sm rounded-circle bg-primary">
                                                            <span class="avatar-title">
                                                                <i class="bx bx-copy-alt font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                    @foreach ($Interview as $val)
                                    <div class="col-md-6">
                                        <div class="card mini-stats-wid">
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="flex-grow-1">
                                                        <p class="text-muted fw-medium">Interview</p>
                                                        <h4 class="mb-0">{{$val->user_count}}</h4>
                                                    </div>
                                                    <div class="flex-shrink-0 align-self-center ">
                                                        <div class="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                                            <span class="avatar-title rounded-circle bg-primary">
                                                                <i class="bx bx-archive-in font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                    @foreach ($Considered as $val)
                                    <div class="col-md-4">
                                        <div class="card mini-stats-wid">
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="flex-grow-1">
                                                        <p class="text-muted fw-medium">Considered</p>
                                                        <h4 class="mb-0">{{$val->user_count}}</h4>
                                                    </div>

                                                    <div class="flex-shrink-0 align-self-center">
                                                        <div class="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                                            <span class="avatar-title rounded-circle bg-primary">
                                                                <i class="bx bx-timer font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                    @foreach ($Rejected as $val)
                                    <div class="col-md-4">
                                        <div class="card mini-stats-wid">
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="flex-grow-1">
                                                        <p class="text-muted fw-medium">Rejected</p>
                                                        <h4 class="mb-0">{{$val->user_count}}</h4>
                                                    </div>

                                                    <div class="flex-shrink-0 align-self-center">
                                                        <div class="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                                            <span class="avatar-title rounded-circle bg-primary">
                                                                <i class="bx bx-window-close font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                    @foreach ($Accepted as $val)
                                    <div class="col-md-4">
                                        <div class="card mini-stats-wid">
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="flex-grow-1">
                                                        <p class="text-muted fw-medium">Accepted</p>
                                                        <h4 class="mb-0">{{$val->user_count}}</h4>
                                                    </div>

                                                    <div class="flex-shrink-0 align-self-center">
                                                        <div class="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                                            <span class="avatar-title rounded-circle bg-primary">
                                                                <i class="bx bx-calendar-check font-size-24"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <!-- end row -->
                                <div class="row">                            
                                    <div class="col-xl-12">
                                        <div class="row" style="background-color:#FFF">
                                            <div class="col-md-2">
                                                {{-- <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-sm-flex flex-wrap">
                                                            <h4 class="card-title mb-4">Pie Chart</h4>
                                                            <div id="pie_chart" class="apex-charts" dir="ltr"></div>
                                                        </div>                                        
                                                    </div>
                                                </div> --}}
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card">
                                                    <div class="card">
                                                        <div class="card-body" >
                                                            <div class="chart-container">
                                                                <div id="getData"></div>
                                                                <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                {{-- <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-sm-flex flex-wrap">
                                                            <h4 class="card-title mb-4">Pie Chart</h4>
                                                            <div id="pie_chart" class="apex-charts" dir="ltr"></div>
                                                        </div>                                        
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>    
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- container-fluid -->
                </div>
                <!-- End Page-content -->
@endsection
@section('scripts')
<script src="{{ URL::asset('backend/assets/js/pages/home/index.js') }}"></script>
@endsection
