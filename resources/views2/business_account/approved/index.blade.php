@extends('layouts.master')

@section('title')
<title>Data Approved</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

   <div class="page-content">
      <div class="container-fluid">


         <!-- start page title -->
         <div class="row">
               <div class="col-12">
                  <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                     <h4 class="mb-sm-0 font-size-18">List Approved Business Account</h4>

                     <div class="page-title-right">
                           <ol class="breadcrumb m-0">
                              <li class="breadcrumb-item"><a href="javascript: void(0);">Business Account</a></li>
                              <li class="breadcrumb-item active">List Approved</li>
                           </ol>
                     </div>

                  </div>
               </div>
         </div>
         <!-- end page title -->

         <div class="row">
               <div class="col-lg-12">
                  <div class="card">
                     <div class="card-body">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active mb-2" data-bs-toggle="tab" href="#tab-active-package" id="showActivePackage" role="tab">
                                       Active Package
                                 </a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link mb-2" data-bs-toggle="tab" href="#tab-non-active-package" id="showNonActivePackage" role="tab">
                                       Non Active Package
                                 </a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link mb-2" data-bs-toggle="tab" href="#tab-account-suspended" id="showAccountSuspended" role="tab">
                                       Account Suspended
                                 </a>
                              </li>
                           </ul>

                           <div class="row mt-3">
                              <div class="col-sm-4">
                                 <div class="search-box me-2 mb-2 d-inline-block">
                                       <div class="position-relative">
                                          <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                          <i class="bx bx-search-alt search-icon"></i>
                                       </div>
                                 </div>
                                 <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                              </div>
                           </div>

                           <!-- Tab panes -->
                           <div class="tab-content">

                              <div class="tab-pane active" id="tab-active-package" role="tabpanel">
                                 <table id="table-active-package" class="table table-bordered dt-responsive nowrap w-100 tabcontent">
                                       <thead>
                                          <tr>
                                             <th>Date Approve</th>
                                             <th>Company Name</th>
                                             <th>Address</th>
                                             <th>PIC Name</th>
                                             <th>Note</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>

                                       <tbody>
                                       </tbody>
                                 </table>
                              </div>

                              <div class="tab-pane" id="tab-non-active-package" role="tabpanel">
                                 <table id="table-non-active-package" class="table table-bordered dt-responsive nowrap w-100 tabcontent">
                                       <thead>
                                          <tr>
                                             <th>Date Approve</th>
                                             <th>Company Name</th>
                                             <th>Address</th>
                                             <th>PIC Name</th>
                                             <th>Note</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>

                                       <tbody>
                                       </tbody>
                                 </table>
                              </div>

                              <div class="tab-pane" id="tab-account-suspended" role="tabpanel">
                                 <table id="table-account-suspended" class="table table-bordered dt-responsive nowrap w-100 tabcontent">
                                       <thead>
                                          <tr>
                                             <th>Date Approve</th>
                                             <th>Company Name</th>
                                             <th>Address</th>
                                             <th>PIC Name</th>
                                             <th>Note</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>

                                       <tbody>
                                       </tbody>
                                 </table>
                              </div>

                           </div>
                     </div>
                  </div>
               </div>
         </div>
         <!-- end row -->

      </div> <!-- container-fluid -->
   </div>
   <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/business_account/approved/index.js') }}"></script>
@endsection