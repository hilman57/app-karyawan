@extends('layouts.master')

@section('title')
<title>Data Site</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">List Site</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">List Site</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOne">
                                        <button class="accordion-button fw-medium collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                            Form
                                        </button>
                                    </h2>
                                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body text-muted">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <form id="formInput">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <label class="form-label">Site Name</label>
                                                                <input class="form-control {{ $errors->has('site_name') ? 'is-invalid':'' }}" type="text" name="site_name" id="site_name" required>
                                                                <p class="text-danger">{{ $errors->first('site_name') }}</p>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <label class="form-label">Contract</label>
                                                                <input class="form-control {{ $errors->has('contract') ? 'is-invalid':'' }}" type="text" name="contract" id="contract">
                                                                <p class="text-danger">{{ $errors->first('contract') }}</p>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <label class="form-label">Last Number PKWT</label>
                                                                <input class="form-control {{ $errors->has('last_number_pkwt') ? 'is-invalid':'' }}" type="number" name="last_number_pkwt" id="last_number_pkwt" required>
                                                                <p class="text-danger">{{ $errors->first('last_number_pkwt') }}</p>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <label class="form-label">Status</label>
                                                                <select name="flag" id="flag" class="form-control {{ $errors->has('flag') ? 'is-invalid':'' }}" required>
                                                                    <option value="1">Active</option>
                                                                    <option value="0">Non Active</option>
                                                                </select>
                                                                <p class="text-danger">{{ $errors->first('flag') }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <label class="form-label">Address</label>
                                                                <textarea name="site_address" id="site_address" class="form-control {{ $errors->has('site_address') ? 'is-invalid':'' }}" required></textarea>
                                                                <p class="text-danger">{{ $errors->first('site_address') }}</p>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <label class="form-label">Valid From</label>
                                                                <input class="form-control {{ $errors->has('valid_from') ? 'is-invalid':'' }}" type="date" name="valid_from" id="valid_from" required>
                                                                <p class="text-danger">{{ $errors->first('valid_from') }}</p>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <label class="form-label">Valid Until</label>
                                                                <input class="form-control {{ $errors->has('valid_until') ? 'is-invalid':'' }}" type="date" name="valid_until" id="valid_until" required>
                                                                <p class="text-danger">{{ $errors->first('valid_until') }}</p>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-primary" id="btnSubmit">Submit</button>
                                                        <button type="button" class="btn btn-light" id="btnClear">Cancel</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingTwo">
                                        <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="true" aria-controls="flush-collapseTwo">
                                            List Data
                                        </button>
                                    </h2>
                                    <div id="flush-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body text-muted">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="search-box me-2 mb-2 d-inline-block">
                                                        <div class="position-relative">
                                                            <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                                            <i class="bx bx-search-alt search-icon"></i>
                                                        </div>
                                                    </div>
                                                    <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="d-flex flex-row-reverse mb-2">
                                                        <button type="button" class="btn btn-primary" id="btnAdd">Add New</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="padding-top: 10px;" id="notif">
                                            </div>
                                            <table id="table-menu" class="table table-bordered dt-responsive nowrap w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Site Name</th>
                                                        <th>Contract</th>
                                                        <th>Periode</th>
                                                        <th>Address</th>
                                                        <th>Number PKWT</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/site/index.js') }}"></script>
@endsection