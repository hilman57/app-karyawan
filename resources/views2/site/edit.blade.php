@extends('layouts.master')

@section('title')
<title>Edit Biodata</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
   <div class="page-content">
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                  <h4 class="mb-sm-0 font-size-18">Edit site</h4>
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('site.index') }}">Site</a></li>
                        <li class="breadcrumb-item active">Edit Site</li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <!-- end page title -->
         <div class="row">
            <div class="col-lg-12">
               <form method="POST" action="/site/update/{{ $detail->id }}">
                  @csrf
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-lg-4">
                              <label class="form-label">Site Name</label>
                              <input class="form-control {{ $errors->has('site_name') ? 'is-invalid':'' }}" type="text" name="site_name" id="site_name" value="{{ $detail->site_name }}" required>
                              <p class="text-danger">{{ $errors->first('site_name') }}</p>
                           </div>
                           <div class="col-lg-4">
                              <label class="form-label">Contract</label>
                              <input class="form-control {{ $errors->has('contract') ? 'is-invalid':'' }}" type="text" name="contract" id="contract" value="{{ $detail->contract }}">
                              <p class="text-danger">{{ $errors->first('contract') }}</p>
                           </div>
                           <div class="col-lg-2">
                              <label class="form-label">Last Number PKWT</label>
                              <input class="form-control {{ $errors->has('last_number_pkwt') ? 'is-invalid':'' }}" type="number" name="last_number_pkwt" id="last_number_pkwt" value="{{ $detail->last_number_pkwt }}" required>
                              <p class="text-danger">{{ $errors->first('last_number_pkwt') }}</p>
                           </div>
                           <div class="col-lg-2">
                              <label class="form-label">Status</label>
                              <select name="flag" id="flag" class="form-control {{ $errors->has('flag') ? 'is-invalid':'' }}" required>
                                 <option value="1" @if($detail->flag == 1) selected @endif>Active</option>
                                 <option value="0" @if($detail->flag == 0) selected @endif>Non Active</option>
                              </select>
                              <p class="text-danger">{{ $errors->first('flag') }}</p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                              <label class="form-label">Address</label>
                              <textarea name="site_address" id="site_address" class="form-control {{ $errors->has('site_address') ? 'is-invalid':'' }}" required>{{ $detail->site_address }}</textarea>
                              <p class="text-danger">{{ $errors->first('site_address') }}</p>
                           </div>
                           <div class="col-lg-4">
                              <label class="form-label">Valid From</label>
                              <input class="form-control {{ $errors->has('valid_from') ? 'is-invalid':'' }}" type="date" name="valid_from" id="valid_from" value="{{ $detail->valid_from }}" required>
                              <p class="text-danger">{{ $errors->first('valid_from') }}</p>
                           </div>
                           <div class="col-lg-4">
                              <label class="form-label">Valid Until</label>
                              <input class="form-control {{ $errors->has('valid_until') ? 'is-invalid':'' }}" type="date" name="valid_until" id="valid_until" value="{{ $detail->valid_until }}" required>
                              <p class="text-danger">{{ $errors->first('valid_until') }}</p>
                           </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('site.index') }}" class="btn btn-danger">Cancel</a>
                     </div>
                  </div>
            </div>
            </form>
         </div>
      </div>
      <!-- end row -->
   </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
</div>
<!-- end main content-->
<!-- /.control-sidebar -->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/site/edit.js') }}"></script>
@endsection