@extends('layouts.master')

@section('title')
    <title>Edit Package</title>
@endsection

@section('content')
<!-- ============================================================== -->
          <!-- Start right Content here -->
          <!-- ============================================================== -->
          <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Edit Package</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Package</a></li>
                                        <li class="breadcrumb-item"><a href="{{ route('helpdesk.index') }}">Helpdesk</a></li>
                                        <li class="breadcrumb-item active">Edit</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                            <form id="formEdit">
                                <input type="hidden" id="routeSaveData" value="{{ route('helpdesk.update', $data->uuid) }}">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" id="idEdit" value="{{ $data->uuid }}">
                                <input type="hidden" id="isDiscount" value="{{ $data->discount }}">
                                <input type="hidden" id="valueDiscount" value="{{ $data->discount }}">
                                <input type="hidden" id="valueEndDiscount" value="{{ $data->end_discount }}">
                                <input type="hidden" id="countDetail" value="{{ $countDetail }}">
                                <input type="hidden" id="flagAddDetail" value="0">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div>
                                                    <label class="form-label">Name</label>
                                                    <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="name" value="{{ $data->name }}" required>
                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div>
                                                        <label class="form-label">Price</label>
                                                        <input class="form-control {{ $errors->has('price') ? 'is-invalid':'' }}" type="text" name="price" id="price" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ $data->price }}" required>
                                                        <p class="text-danger">{{ $errors->first('price') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <h5 class="font-size-13 mb-2">Discount</h5>
                                                    <div>
                                                        <input type="checkbox" id="switch5" switch="warning" value="false" {{ !empty($data->discount) ? 'checked' : '' }}/>
                                                        <label for="switch5" data-on-label="Yes" data-off-label="No"></label>
                                                        <p class="text-danger">{{ $errors->first('is_percent') }}</p> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="fieldDiskon">
                                                <div class="col-xl-2">
                                                    <div>
                                                        <label class="form-label">Discount</label>
                                                        <input class="form-control {{ $errors->has('discount') ? 'is-invalid':'' }}" type="text" name="discount" id="isiDiskon" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ $data->discount }}">
                                                        <p class="text-danger">{{ $errors->first('discount') }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3">
                                                    <div>
                                                        <label class="form-label">Start</label>
                                                        <input class="form-control {{ $errors->has('start_discount') ? 'is-invalid':'' }}" name="start_discount" type="date" id="startDiscount" value="{{ $data->start_discount }}">
                                                        <p class="text-danger">{{ $errors->first('start_discount') }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3">
                                                    <div>
                                                        <label class="form-label">End</label>
                                                        <input class="form-control {{ $errors->has('end_discount') ? 'is-invalid':'' }}" name="end_discount" type="date" id="endDiscount" value="{{ $data->end_discount }}">
                                                        <p class="text-danger">{{ $errors->first('end_discount') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="list_detail">
                                                @if(!empty($details))
                                                <label for="input_description" class="form-label">List Description</label>
                                                    @foreach ($details as $key => $detail)
                                                    <div class="inner-repeater mb-3" data-index-detail="{{ $key }}">
                                                            <div data-repeater-list="inner-group" class="inner mb-3">
                                                                <div data-repeater-item class="inner mb-3 row">
                                                                    <div class="col-md-8 col-8 d-flex h-100 align-items-center">
                                                                        <div style="border-radius: 100px; height: 25px; width: 25px; text-align: center; display: flex; justify-content: center; align-items: center; background-color: #F4AE00; color: #fff;" class="me-2">
                                                                            <i class="fas fa-check"></i>
                                                                        </div>
                                                                        <input type="text" name="details[{{ $key }}]" id="input_details_{{ $key }}" class="inner form-control" value="{{ $detail }}"/>
                                                                    </div>
                                                                    @if($countDetail > 1)
                                                                    <div class="col-md-1 col-4">
                                                                        <div class="d-grid">
                                                                            <button type="button" data-type="btn-remove" data-index-detail="{{ $key }}" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                    </div>
                                                    @endforeach
                                                @else
                                                <div class="inner-repeater mb-3" data-index-detail="0">
                                                        <div data-repeater-list="inner-group" class="inner mb-3">
                                                            <label for="input_description_0" class="form-label">List Description</label>
                                                            <div data-repeater-item class="inner mb-3 row">
                                                                <div class="col-md-8 col-8 d-flex h-100 align-items-center">
                                                                    <div style="border-radius: 100px; height: 25px; width: 25px; text-align: center; display: flex; justify-content: center; align-items: center; background-color: #F4AE00; color: #fff;" class="me-2">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <input type="text" name="details[0]" id="input_details_0" class="inner form-control" placeholder="Enter here..."/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                @endif
                                            </div>
                                            <button type="button" id="button_add_details" class="btn btn-primary mb-3"><i class="fa fa-plus"></i> Description</button>
                                            <div>
                                                <button type="button" class="btn btn-primary btnSubmit">Submit</button>
                                                <a href="{{ route('helpdesk.index') }}" class="btn btn-light">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- sample modal content -->
                                <div id="modalIsiEmail" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="myModalLabel">Verification Process</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <input type="hidden" id="routeCheckUserEmail" value="{{ route('user.cek-email') }}">
                                            <input type="hidden" id="routeCheckUserPassword" value="{{ route('user.cek-password') }}">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div>
                                                            <label class="form-label">Email Address</label>
                                                            <input class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" type="text" name="email" id="fieldEmail" required>
                                                            <div class="alert alert-danger" id="errorEmail" style="display: none;">Email not found!</div>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div>
                                                            <label class="form-label">Password</label>
                                                            <input class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" type="password" name="password" id="fieldPassword" disabled required>
                                                            <div class="alert alert-danger" id="errorPassword" style="display: none;">Password not match!</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary waves-effect waves-light" id="btnSubmitFinal" disabled>Submit</button>
                                                <button type="button" class="btn btn-light waves-effect" data-bs-dismiss="modal">Cancel</button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
          </div>
          <!-- end main content-->
<!-- /.control-sidebar -->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/package/helpdesk/edit.js') }}"></script>
@endsection
