@extends('layouts.master')

@section('title')
<title>Form Interview</title>
@endsection

@section('content')
<div class="main-content">
  <div class="page-content">
    @if($status == NULL || $status == 'Dipertimbangkan')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          @if ($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{ $message }}</p>
          </div>
          @elseif ($message = Session::get('error'))
          <div class="alert alert-danger">
            <p>{{ $message }}</p>
          </div>
          @endif
          <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">FORM INTERVIEW</h4>
          </div>
          <div class="mb-3">
            {{-- @php
            $classActive = "btn btn-primary";
            $classNonActive = "btn btn-secondary";
            @endphp
            <button class="@if(count($result_interview) == 0) {{$classActive}} @else {{$classNonActive}} @endif px-5">HRD</button> &nbsp;
            <button class="@if(count($result_interview) == 1) {{$classActive}} @else {{$classNonActive}} @endif px-5">SPV</button> &nbsp;
            <button class="@if(count($result_interview) == 2) {{$classActive}} @else {{$classNonActive}} @endif px-5">USER</button> &nbsp;
            <button class="@if(count($result_interview) == 3) {{$classActive}} @else {{$classNonActive}} @endif px-5">Result</button> &nbsp; --}}
          </div>
        </div>
      </div>
      {{-- @if(count($result_interview) == 3) --}}

      {{-- @else --}}
      <form method="POST" action="/interview/{{ $id }}">
        @csrf
        <input type="hidden" name="isReject" />
        <div class="card">
          <div class="card-body">
            <div class="form-group mb-3 row align-items-center">
                <div class="col-2">
                  <label class="form-label">Pilih Jabatan</label>
                </div>
                <div class="col-8">
                    <select class="form-control m-bot15" name="interview_by">
                        <option selected="selected" value="HRD">HRD</option>
                        <option value="SPV">SPV</option>
                        <option value="USER">USER</option>
                        {{-- <option value="Result">Result</option> --}}
                  </select>
                </div>
              </div>


            @foreach($question as $key => $value)
            <div class="form-group mb-3 row align-items-center">
              <div class="col-2">
                <label class="form-label">{{ $value->question }}</label>
              </div>
              <div class="col-8">
                <textarea name="answer_{{ $value->id }}" class="form-control answer" placeholder="Answer..." required></textarea>
              </div>
            </div>
            @endforeach
            <button type="button" class="btn btn-danger" onclick="onReject('{{ $id }}')">Tolak</button>
            <button type="submit" class="btn btn-primary">Lanjut</button>
          </div>
        </div>
      </form>

      <form method="POST" action="/interview-final/{{ $id }}">
        @csrf
        <div class="card">
          <div class="card-body">
            <div class="form-group mb-3 row align-items-center">
              <div class="col-8">
                <label class="form-label">Final Interview</label> <br />
                <input type="radio" name="status_interview" value="5" @if($status==5) checked @endif required> Diterima <br />
                <input type="radio" name="status_interview" value="4" @if($status==4) checked @endif required> Ditolak <br />
                <input type="radio" name="status_interview" value="3" @if($status==3) checked @endif required> Dipertimbangkan <br />
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>
      {{-- @endif --}}
    </div>
    @else
    <div class="card">
      <div class="card-body">
        @php
        $interview_by = null;
        @endphp
        @foreach($detail_result_interview as $key => $value)
        @if($value->interview_by != $interview_by)
        <h3 class="mt-4 p-3 rounded bg-primary text-white text-center" style="width: 200px;">{{ $value->interview_by }}</h3>
        @endif
        @if($value->interview_by == 'HRD')
        <div class="form-group mb-3 row align-items-center">
          <div class="col-2">
            <label class="form-label">{{ $value->question }}</label>
          </div>
          <div class="col-8">
            <textarea class="form-control" placeholder="Answer..." readonly>{{ $value->answer }}</textarea>
          </div>
        </div>
        @elseif($value->interview_by == 'SPV')
        <div class="form-group mb-3 row align-items-center">
          <div class="col-2">
            <label class="form-label">{{ $value->question }}</label>
          </div>
          <div class="col-8">
            <textarea class="form-control" placeholder="Answer..." readonly>{{ $value->answer }}</textarea>
          </div>
        </div>
        @elseif($value->interview_by == 'USER')
        <div class="form-group mb-3 row align-items-center">
          <div class="col-2">
            <label class="form-label">{{ $value->question }}</label>
          </div>
          <div class="col-8">
            <textarea class="form-control" placeholder="Answer..." readonly>{{ $value->answer }}</textarea>
          </div>
        </div>
        @endif
        @php
        $interview_by = $value->interview_by;
        @endphp
        @endforeach
      </div>
    </div>
    @endif
  </div>
</div>
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/interview/index.js') }}"></script>
@endsection
