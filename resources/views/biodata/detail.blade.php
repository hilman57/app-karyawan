@extends('layouts.master')

@section('title')
<title>Biodata Karyawan</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
   <div class="page-content">
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                  <h4 class="mb-sm-0 font-size-18">Biodata</h4>
                  <div class="page-title-right">
                     <button class="btn btn-primary" onclick="window.print()">
                        <i class="fa fa-print"></i>
                        Print
                     </button>
                  </div>
               </div>
            </div>
         </div>
         <!-- end page title -->
         <div class="row">
            <div class="col-lg-12">
               @foreach ($data as $detail)
               <div class="card">
                  <div class="card-body">
                     <div>
                        <div class="row">
                           <div class="col-lg-8">
                              <div class="row">
                                 <h4>Data Pribadi</h4>
                                 <div class="col-lg">
                                    <label class="form-label">Nama Lengkap</label>
                                    <input class="form-control {{ $errors->has('nama') ? 'is-invalid':'' }}" type="text" name="nama" value="{{ $detail->nama_lengkap }}" disabled>
                                 </div>
                                 <div class="col-lg">
                                    <label class="form-label">Jabatan</label>
                                    <input class="form-control {{ $errors->has('id_jabatan') ? 'is-invalid':'' }}" type="text" name="id_jabatan" value="{{ $detail->position_name }}" disabled>
                                    <p class="text-danger">{{ $errors->first('jabatan') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col-lg-6">
                                    <label class="form-label">KTP</label>
                                    <input class="form-control {{ $errors->has('ktp') ? 'is-invalid':'' }}" type="text" name="ktp" value="{{ $detail->ktp }}" disabled>
                                    <p class="text-danger">{{ $errors->first('ktp') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Site</label>
                                    <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" value="{{ $detail->site_name }}" disabled>
                                    <p class="text-danger">{{ $errors->first('site') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Status</label>
                                    <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" value="{{ $detail->status_ky }}" disabled>
                                    <p class="text-danger">{{ $errors->first('status_kry') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">No. NPWP</label>
                                    <input class="form-control {{ $errors->has('npwp') ? 'is-invalid':'' }}" type="text" name="npwp" value="{{ $detail->npwp }}" disabled>
                                    <p class="text-danger">{{ $errors->first('npwp') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Tanggal NPWP</label>
                                    <input class="form-control {{ $errors->has('tgl_npwp') ? 'is-invalid':'' }}" type="date" name="tgl_npwp" value="{{ $detail->tgl_npwp }}" disabled>
                                    <p class="text-danger">{{ $errors->first('tgl_npwp') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div>
                                 <label class="form-label">Email</label>
                                 <input class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" type="text" name="email" value="{{ $detail->email }}" disabled>
                                 <p class="text-danger">{{ $errors->first('email') }}</p>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Telpon</label>
                                    <input class="form-control {{ $errors->has('telpon') ? 'is-invalid':'' }}" type="number" name="telpon" value="{{ $detail->telpon }}" disabled>
                                    <p class="text-danger">{{ $errors->first('telpon') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">HP</label>
                                    <input class="form-control {{ $errors->has('hp') ? 'is-invalid':'' }}" type="number" name="hp" value="{{ $detail->hp }}" disabled>
                                    <p class="text-danger">{{ $errors->first('hp') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Tempat Lahir</label>
                                    <input class="form-control {{ $errors->has('tempat') ? 'is-invalid':'' }}" type="text" name="tempat" value="{{ $detail->tempat }}" disabled>
                                    <p class="text-danger">{{ $errors->first('tempat') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Tanggal Lahir</label>
                                    <input class="form-control " name="tgl_lahir" type="date" id="" value="{{$detail->tgl_lahir}}" disabled>
                                    <p class="text-danger"></p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Jenis Kelamin</label>
                                    <?php if ($detail->jenis_kelamin == 'P') {
                                       $kl = "Perempuan";
                                    } else {
                                       $kl = "Laki-laki";
                                    } ?>
                                    <input class="form-control {{ $errors->has('jenis_kelamin') ? 'is-invalid':'' }}" type="text" name="jenis_kelamin" value="{{ $kl }}" disabled>
                                    <p class="text-danger">{{ $errors->first('jenis_kelamin') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Agama</label>
                                    <input class="form-control {{ $errors->has('agama') ? 'is-invalid':'' }}" type="text" name="agama" value="{{ $detail->agama }}" disabled>
                                    <p class="text-danger">{{ $errors->first('agama') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Berat Badan</label>
                                    <input class="form-control {{ $errors->has('berat_badan') ? 'is-invalid':'' }}" type="text" name="berat_badan" value="{{ $detail->berat_badan }}" disabled>
                                    <p class="text-danger">{{ $errors->first('berat_badan') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Tinggi Badan</label>
                                    <input class="form-control {{ $errors->has('tinggi_badan') ? 'is-invalid':'' }}" type="text" name="tinggi_badan" value="{{ $detail->tinggi_badan }}" disabled>
                                    <p class="text-danger">{{ $errors->first('tinggi_badan') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div>
                                 <label class="form-label">Alamat</label>
                                 <input class="form-control {{ $errors->has('alamat') ? 'is-invalid':'' }}" type="text" name="alamat" value="{{ $detail->alamat }}" disabled>
                                 <p class="text-danger">{{ $errors->first('alamat') }}</p>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Kota</label>
                                    <input class="form-control {{ $errors->has('kota') ? 'is-invalid':'' }}" type="text" name="kota" value="{{ $detail->kota }}" disabled>
                                    <p class="text-danger">{{ $errors->first('kota') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Kodepos</label>
                                    <input class="form-control {{ $errors->has('kodepos') ? 'is-invalid':'' }}" type="text" name="kodepos" value="{{ $detail->kodepos }}" disabled>
                                    <p class="text-danger">{{ $errors->first('kodepos') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Pendidikan Terakhir</label>
                                    <input class="form-control {{ $errors->has('pendidikan_terakhir') ? 'is-invalid':'' }}" type="text" name="pendidikan_terakhir" value="{{ $detail->pendidikan_terakhir }}" disabled>
                                    <p class="text-danger">{{ $errors->first('pendidikan_terakhir') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Nama Lembaga</label>
                                    <input class="form-control {{ $errors->has('nama_lembaga') ? 'is-invalid':'' }}" type="text" name="nama_lembaga" value="{{ $detail->nama_lembaga }}" disabled>
                                    <p class="text-danger">{{ $errors->first('nama_lembaga') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div>
                                 <label class="form-label">Nama Ibu Kandung</label>
                                 <input class="form-control {{ $errors->has('nama_ibu_kandung') ? 'is-invalid':'' }}" type="text" name="nama_ibu_kandung" value="{{ $detail->nama_ibu_kandung }}" disabled>
                                 <p class="text-danger">{{ $errors->first('nama_ibu_kandung') }}</p>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <h4>Data Keluarga & Berkas</h4>
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Status Pernikahan</label>
                                    <?php if ($detail->status == '1') {
                                       $status = "Belum Menikah";
                                    } else if ($detail->status == '2') {
                                       $status = "Menikah";
                                    } else {
                                       $status = "Bercerai";
                                    } ?>

                                    <input class="form-control {{ $errors->has('status') ? 'is-invalid':'' }}" type="text" name="status" value="{{ $status }}" disabled>
                                    <p class="text-danger">{{ $errors->first('status') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Nama Pasangan</label>
                                    <input class="form-control {{ $errors->has('nama_pasangan') ? 'is-invalid':'' }}" type="text" name="nama_pasangan" value="{{ $detail->nama_pasangan }}" disabled>
                                    <p class="text-danger">{{ $errors->first('nama_pasangan') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Pekerjaan Pasangan</label>
                                    <input class="form-control {{ $errors->has('pekerjaan_istri_suami') ? 'is-invalid':'' }}" type="text" name="pekerjaan_istri_suami" value="{{ $detail->pekerjaan_istri_suami }}" disabled>
                                    <p class="text-danger">{{ $errors->first('pekerjaan_istri_suami') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Jumlah Anak</label>
                                    <input class="form-control {{ $errors->has('jumlah_anak') ? 'is-invalid':'' }}" type="text" name="jumlah_anak" value="{{ $detail->jumlah_anak }}" disabled>
                                    <p class="text-danger">{{ $errors->first('jumlah_anak') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Foto</label><br>
                                    {{-- @php echo  asset('data_file/'.$detail->foto); @endphp  --}}
                                    <label class="form-label">Dowonload :</label>&nbsp;
                                    <a href="{{asset('data_file/'.$detail->foto)}}" class="text-primary">{{ $detail->foto }}</a>
                                    {{-- <input class="form-control {{ $errors->has('foto') ? 'is-invalid':'' }}" type="text" name="foto" value="{{ $detail->foto }}" disabled> --}}
                                    <p class="text-danger">{{ $errors->first('foto') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">SKCK</label><br>
                                    <label class="form-label">Dowonload :</label>&nbsp;
                                    <a href="{{asset('data_file_skck/'.$detail->skck)}}" class="text-primary">{{ $detail->skck }}</a>
                                    {{-- <input class="form-control {{ $errors->has('skck') ? 'is-invalid':'' }}" type="text" name="skck" value="{{ $detail->skck }}" disabled> --}}
                                    <p class="text-danger">{{ $errors->first('skck') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">CV</label><br>
                                    <label class="form-label">Dowonload :</label>&nbsp;
                                    <a href="{{asset('data_file_cv/'.$detail->cv)}}" class="text-primary">{{ $detail->cv }}</a>
                                    {{-- <input class="form-control {{ $errors->has('cv') ? 'is-invalid':'' }}" type="text" name="cv" value="{{ $detail->cv }}" disabled> --}}
                                    <p class="text-danger">{{ $errors->first('cv') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Ijazah</label><br>
                                    <label class="form-label">Dowonload :</label>&nbsp;
                                    <a href="{{asset('data_file_ijazah/'.$detail->ijazah)}}" class="text-primary">{{ $detail->ijazah }}</a>
                                    {{-- <input class="form-control {{ $errors->has('ijazah') ? 'is-invalid':'' }}" type="text" name="ijazah" value="{{ $detail->ijazah }}" disabled> --}}
                                    <p class="text-danger">{{ $errors->first('ijazah') }}</p>
                                 </div>
                              </div>
                           </div>
                           <h4>Pengalaman Kerja</h4>
                           {{-- @php echo  $no++."."; @endphp --}}
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                       <thead>
                                          <tr>
                                             <th>No</th>
                                             <th>Nama Perusahaan</th>
                                             <th>Jabatan</th>
                                             <th>Masa Kerja</th>
                                             <th>Alasan Keluar</th>
                                             <th>Alamat Perusahaan</th>
                                          </tr>
                                       </thead>
                                       <tbody>

                                          @php $no = 1; @endphp
                                          @foreach ($pgk as $pgk_detail)
                                          <tr>
                                             <th scope="row"> @php echo $no++."."; @endphp</th>
                                             <th scope="row">{{ $pgk_detail->namaPerusahaan }}</th>
                                             <th scope="row">{{ $pgk_detail->jabatanKerja }}</th>
                                             <th scope="row">{{ $pgk_detail->masaKerja }}</th>
                                             <th scope="row">{{ $pgk_detail->alasanKeluar }}</th>
                                             <th scope="row">{{ $pgk_detail->alamatPerusahaan }}</th>
                                          </tr>
                                          @endforeach
                                       </tbody>
                                       <?php
                                       // echo $no;
                                       $pgk = $no > 1;
                                       if ($pgk == '' || $pgk == 0) {
                                          echo '<tr>
                                          <th></th>
                                          <th></th>
                                          <th></th>
                                                      <th>No data available in table</th>
                                           <th></th>
                                           <th></th>
                                          ';
                                       }
                                       ?>
                                    </table>
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-8"><br>
                              <h4>Data Keluarga Dekat</h4>
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Nama Keluarga</label>
                                    <input class="form-control " type="text" name="" value="{{ $detail->nama_keluarga }}" disabled>

                                 </div>
                                 <div class="col">
                                    <label class="form-label">Hubungan</label>
                                    <input class="form-control " type="text" name="" value="" disabled>

                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div>
                                 <label class="form-label">Alamat Keluarga Dihubungi</label>
                                 <input class="form-control " type="text" name="" value="{{ $detail->alamat_keluarga_dihubungi }}" disabled>

                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div>
                                 <label class="form-label">Kota Keluarga</label>
                                 <input class="form-control " type="text" name="" value="{{ $detail->kota_keluarga }}" disabled>

                              </div>
                           </div>

                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Keluarga Telpon</label>
                                    <input class="form-control " type="number" name="" value="{{ $detail->keluarga_telpon }}" disabled>

                                 </div>
                                 <div class="col">
                                    <label class="form-label">Keluarga HP</label>
                                    <input class="form-control" type="number" name="" value="{{ $detail->keluarga_hp }}" disabled>

                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-8">
                              <h4>Data Bank</h4>
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">No. Rekening</label>
                                    <input class="form-control {{ $errors->has('rekening') ? 'is-invalid':'' }}" type="text" name="rekening" value="{{ $detail->rekening }}" disabled>
                                    <p class="text-danger">{{ $errors->first('rekening') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Nama Rekening</label>
                                    <input class="form-control {{ $errors->has('nama_rekening') ? 'is-invalid':'' }}" type="text" name="nama_rekening" value="{{ $detail->nama_rekening }}" disabled>
                                    <p class="text-danger">{{ $errors->first('nama_rekening') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div>
                                 <label class="form-label">Cabang</label>
                                 <input class="form-control {{ $errors->has('cabang') ? 'is-invalid':'' }}" type="text" name="cabang" value="{{ $detail->cabang }}" disabled>
                                 <p class="text-danger">{{ $errors->first('cabang') }}</p>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">AOC</label>
                                    <input class="form-control {{ $errors->has('aoc') ? 'is-invalid':'' }}" type="text" name="aoc" value="{{ $detail->aoc }}" disabled>
                                    <p class="text-danger">{{ $errors->first('aoc') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Nama Leader</label>
                                    <input class="form-control {{ $errors->has('nama_leader') ? 'is-invalid':'' }}" type="text" name="nama_leader" value="{{ $detail->nama_leader }}" disabled>
                                    <p class="text-danger">{{ $errors->first('nama_leader') }}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Tunjangan</label>
                                    <input class="form-control {{ $errors->has('tunjangan') ? 'is-invalid':'' }}" type="text" name="tunjangan" value="{{ number_format($detail->tunjangan) }}" disabled>
                                    <p class="text-danger">{{ $errors->first('tunjangan') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Gaji</label>
                                    <input class="form-control {{ $errors->has('gaji') ? 'is-invalid':'' }}" type="text" name="gaji" value="{{ number_format($detail->gaji) }}" disabled required>
                                    <p class="text-danger">{{ $errors->first('gaji') }}</p>
                                 </div>

                              </div>
                           </div>

                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Instagram</label>
                                    <input class="form-control {{ $errors->has('instagram') ? 'is-invalid':'' }}" type="text" name="instagram" value="{{ $detail->instagram }}" disabled>
                                    <p class="text-danger">{{ $errors->first('instagram') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Facebook</label>
                                    <input class="form-control {{ $errors->has('facebook') ? 'is-invalid':'' }}" type="text" name="facebook" value="{{ $detail->facebook }}" disabled>
                                    <p class="text-danger">{{ $errors->first('facebook') }}</p>
                                 </div>
                                 <div class="col">
                                    <label class="form-label">Linkedin</label>
                                    <input class="form-control {{ $errors->has('linkedin') ? 'is-invalid':'' }}" type="text" name="linkedin" value="{{ $detail->linkedin }}" disabled>
                                    <p class="text-danger">{{ $errors->first('linkedin') }}</p>
                                 </div>
                              </div>
                           </div>

                           <div class="col-lg-8">
                              <div class="row">
                                 <div class="col">
                                    <label class="form-label">Status Karyawan</label>
                                    <input class="form-control {{ $errors->has('flag') ? 'is-invalid':'' }}" type="text" name="flag" value="{{ $detail->status_ky }}" disabled>
                                    <p class="text-danger">{{ $errors->first('flag') }}</p>
                                 </div>
                                 <div class="col">
                                 </div>
                              </div>
                           </div>

                           @endforeach
                        </div>
                     </div>
                     </form>
                  </div>
               </div>
            </div> <!-- container-fluid -->
         </div>
         <!-- End Page-conte -->
      </div>
      <!-- end main content1-->
      @endsection
      @section('scripts')
      @parent
      <script src="{{ URL::asset('backend/assets/js/pages/biodata/index.js') }}"></script>
      @endsection
