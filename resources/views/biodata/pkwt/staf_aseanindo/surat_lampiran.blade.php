<!DOCTYPE html>
<html lang="en">

<head>
  <title>SURAT LAMPIRAN</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container" align="justify">
    <center>
      <h5>
        <u><strong>SURAT PERNYATAAN</strong></u>
      </h5>
    </center>
    <br />
    <div class="container">
      <p>Yang bertanda tangan dibawah ini :</p>
      <div class="row">
        <div class="col-2">Nama Lengkap</div>
        <div class="col-10">: {{ $param['user']->nama_lengkap }}</div>
      </div>
      <div class="row">
        <div class="col-2">No. KTP</div>
        <div class="col-10">: {{ $param['user']->ktp }}</div>
      </div>
      <div class="row">
        <div class="col-2">Alamat</div>
        <div class="col-10">: {{ $param['user']->alamat }}</div>
      </div>
      <p>Menyatakan bahwa :</p>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">Tidak memiliki hubungan darah (seperti orangtua, anak, saudara laki-laki atau perempuan) dengan karyawan PT Aseanindo Networks Solutions, dan/atau tidak menikah dengan sesama karyawan PT Aseanindo Networks Solutions, dan/atau bukan pasangan suami atau istri dengan sesama karyawan PT Aseanindo Networks Solutions, termasuk kepada hubungan pernikahan yang terjadi (seperti bapak atau ibu mertua, menantu laki-laki atau perempuan, saudara ipar laki-laki-laki atau perempuan) yang bekerja sebagai karyawan PT Aseanindo Networks Solutions;</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Tidak melakukan rangkap pekerjaan seperti part time atau sampingan di perusahaan lain.</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">Tidak sedang menjabat atau bekerja di perusahaan lain.</div>
      </div>
      <br />
      <p>Apabila dikemudian hari ditemukan dan dapat dibuktikan, maka saya bersedia diproses sesuai dengan ketentuan dan/atau peraturan yang berlaku di Management PT Aseanindo Networks Solutions Demikian surat pernyataan ini dibuat sesuai dengan keadaan yang sebenarnya dan dapat dipergunakan sebagaimana mestinya.</p>
      <p>Jakarta, {{ date('d-m-Y') }}</p>
      <br /><br /><br />
      <p><small>Materai Rp. 10.000,-</small></p>
      <p>({{ $param['user']->nama_lengkap }})</p>
    </div>
  </div>
</body>

</html>