<!DOCTYPE html>
<html lang="en">

<head>
  <title>ADDENDIUM PERTAMA</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container" align="justify">
    <center>
      <h5>ADDENDUM</h5>
    </center>
    <div class="container">
      <br />
      <p>Addendum ini untuk melengkapi Surat Perjanjian Kerja Waktu Tertentu No. {{ $param['last_number_pkwt'] }}/PKWT-Aseanindo/{{ date('Y') }} atas nama {{ $param['user']->nama_lengkap }} yang berisi hal-hal sebagai berikut:</p>
      <!-- Point 1 -->
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-2">Penggolongan <br /> Jabatan <br /> Unit Kerja</div>
        <div class="col-9"><br /> :............ <br /> :............</div>
      </div>
      <!-- Point 2 -->
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Jam Kerja</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-2">Hari/Jam Kerja</div>
        <div class="col-2">: Senin s/d Jumat</div>
        <div class="col-7">: Jam 08.00 – 17.00 (istirahat 1 jam)</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-2"></div>
        <div class="col-2">&nbsp;&nbsp;Hari Sabtu</div>
        <div class="col-7">: Jam 08.00 – 13.00 WIB</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-11">Atau mengikuti peraturan yang disesuaikan dengan jadwal kerja yang berlaku ditempat Pihak Kedua diperbantukan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-11">Jam kerja diluar ketentuan diatas akan diberlakukan dengan adanya pemberitahuan
          sebelumnya oleh Unit Kerja terkait.</div>
      </div>
      <!-- Point 3 -->
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">Remunerasi:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a.</div>
        <div class="col-3">Gaji Pokok</div>
        <div class="col-8">: Rp {{ number_format($param['user']->gaji) }}</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-3">Tunjangan Jabatan</div>
        <div class="col-8">: Rp {{ $param['user']->tunjangan }}</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b.</div>
        <div class="col-3">Insentif & Bonus</div>
        <div class="col-8">: Tidak ada</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c.</div>
        <div class="col-3">Overtime</div>
        <div class="col-8">: Tidak ada</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d.</div>
        <div class="col-3">Tunjangan Hari Raya</div>
        <div class="col-8">: 1 (satu) bulan upah bruto setelah bekerja selama 12 bulan</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e.</div>
        <div class="col-3">Tunjangan Transportasi</div>
        <div class="col-8">: Tidak ada</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f.</div>
        <div class="col-3">BPJS Kesehatan</div>
        <div class="col-8">: 5% (Subsidi Perusahaan)</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">g.</div>
        <div class="col-3">BPJS Ketenagakerjaan</div>
        <div class="col-8">: 4,24% ditanggung perusahaan,2% ditanggung Pihak kedua (Karyawan)</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">h.</div>
        <div class="col-3">PPh 21</div>
        <div class="col-8">: ditanggung oleh Pihak Pertama (Perusahaan)</div>
      </div>
      <!-- Point 4 -->
      <div class="row">
      <div class="col-1">4.</div>
        <div class="col-11">
        Cuti : <br/> 12 (dua belas) hari kerja setelah bekerja selama 1 tahun secara terus menerus.
        </div>
      </div>
      <!-- Point 5 -->
      <div class="row">
        <div class="col-1">5.</div>
        <div class="col-11">Izin Meninggalkan Pekerjaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a.</div>
        <div class="col-10">Untuk kondisi yang bersifat darurat seperti kematian keluarga, atau untuk menghadiri upacara kekeluargaan seperti perkawinan dan kelahiran, Pihak Pertama dapat memberikan izin kepada Pihak Kedua untuk meninggalkan pekerjaan di luar cuti tahunan dengan pengaturan ditetapkan oleh Pihak Pertama dan disetujui oleh Unit Kerja terkait di perusahaan. <br /> Perincian meninggalkan pekerjaan adalah sebagai berikut:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-2">Perkawinan</div>
        <div class="col-5">: Pihak Kedua sendiri <br />&nbsp;Anak <br />&nbsp;Saudara Kandung</div>
        <div class="col-2">3 hari <br /> 2 hari <br /> 2 hari</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-2">Kelahiran Anak</div>
        <div class="col-5">: (istri Pihak Kedua)</div>
        <div class="col-2">2 hari</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">-</div>
        <div class="col-2">Kematian</div>
        <div class="col-5">: Suami / istri, anak / orang tua Pihak Kedua, &nbsp;Saudara kandung, kakek / nenek, mertua. <br />&nbsp;Orang tinggal serumah dengan Pihak Kedua</div>
        <div class="col-2"><br /> 2 hari <br /> 2 hari</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b.</div>
        <div class="col-10">Izin karena sakit harus dilampirkan dengan surat keterangan dokter.</div>
      </div>
      <!-- Point 6 -->
      <div class="row">
        <div class="col-1">6.</div>
        <div class="col-11">Pengunduran diri: <br /> Pihak Kedua harus mengajukan 1 bulan sebelumnya, dan pada tanggal pengunduran diri memberikan surat kepada Pihak Pertama dan disetujui oleh unit kerja terkait dari Perusahaan dengan melampirkan surat administrasi pengambilan barang yang sudah bukan hak dari Pihak Kedua.</div>
      </div> <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
      <!-- Point 7 -->
      <div class="row">
        <div class="col-1">7.</div>
        <div class="col-11">Tindakan disiplin dan Pemutusan Hubungan Kerja (PHK). <br /> Pihak Pertama akan mengenakan tindakan disiplin kepada Pihak Kedua yang berkaitan dengan performa kerja selama ditugaskan di Perusahaan atau apabila Pihak Kedua melakukan pelanggaran terhadap aturan yang telah ditetapkan oleh Pihak Pertama dan telah disetujui oleh Perusahaan. Tindakan-tindakan disiplin yang berlaku: Warning Letter (Surat Peringatan) ataupun Pemutusan Hubungan Kerja, dimana pengaturannya ditentukan oleh Pihak Pertama dan telah disetujui oleh Unit Kerja terkait. Pelanggaran yang dapat dikenai Tindakan Disiplin maupun Pemutusan Hubungan Kerja sesuai dengan yang tercantum dalam pasal 4 Surat Perjanjian ini.</div>
      </div>
      <p>Addendum (Lembar Tambahan) ini dibuat di Jakarta dan mulai berlaku pada tanggal ........... serta dapat diubah sewaktu-waktu oleh Pihak Pertama apabila dianggap perlu.</p>
      <div class="row">
        <div class="col-7">
          <strong>Jakarta, </strong> {{ date('d-m-Y') }} <br />
          <strong>Pihak Pertama,</strong>
          <br /> <img src="{{ public_path('backend/assets/img_hrd/cap_ans.png') }}" alt="cap_ans" style="width: 150px; height: 75px; object-fit: cover; margin-left: -10px;"> <br />
          <strong><u>Herman Julianto</u></strong><br />
          <span>Direktur HRD</span>
        </div>
        <div class="col-5">
          <br />
          <strong>Pihak Kedua</strong>
          <br /><br /><br /><br /><br /><br />
          <span> {{ $param['user']->nama_lengkap }}</span>
        </div>
      </div>
    </div>
  </div>
</body>

</html>