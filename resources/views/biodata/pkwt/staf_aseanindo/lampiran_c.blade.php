<!DOCTYPE html>
<html lang="en">

<head>
  <title>LAMPIRAN C</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container" align="justify">
    <center>
      <h5>
        <u><strong>LAMPIRAN C</strong></u>
        <p><strong>SURAT PERNYATAAN <br /> PERILAKU DAN ETIKA</strong></p>
      </h5>
    </center>
    <div class="container">
      <p>Sehubungan dengan tugas yang diberikan oleh <strong>PT. Aseanindo Network Solutions</strong> kepada saya, Saya yang bertanda tangan dibawah ini:</p>
      <div class="row">
        <div class="col-1"><strong>Nama</strong></div>
        <div class="col-11">: {{ $param['user']->nama_lengkap }}</div>
      </div>
      <div class="row">
        <div class="col-1"><strong>Alamat</strong></div>
        <div class="col-11">: {{ $param['user']->alamat }}</div>
      </div>
      <br />
      <p>Menyatakan hal-hal sebagai berikut:</p>
      <div class="row">
        <div class="col-1">1.</div>
        <div class="col-11">Tidak akan melanggar ketentuan pihak pengguna jasa dalam menerima setoran tunai atau menerima cek / bilyet giro dari nasabah yang dilakukan baik diterima langsung dari nasabah maupun melakukan penyerahan melalui Teller;</div>
      </div>
      <div class="row">
        <div class="col-1">2.</div>
        <div class="col-11">Tidak akan melakukan penipuan, pencurian dan penggelapan barang / uang milik Klien dan / atau pihak ketiga dan / atau pengguna jasa pelayanan berdasarkan Perjanjian ini dan / atau pemberi jasa lainnya;</div>
      </div>
      <div class="row">
        <div class="col-1">3.</div>
        <div class="col-11">Tidak akan memberi keterangan palsu atau yang dipalsukan sehingga merugikan Klien dan / atau pihak pengguna jasa pelayanan serta pihak ketiga lainnya berdasarkan Perjanjian ini;</div>
      </div>
      <div class="row">
        <div class="col-1">4.</div>
        <div class="col-11">Tidak akan bermabuk-mabukan, meminum minuman keras yang memabukkan, madat, memakai obat bius atau menyalahgunakan obat terlarang lainnya atau narkotika di tempat pelayanan atau pada saat sedang melakukan tugas dan kewajibannya;</div>
      </div>
      <div class="row">
        <div class="col-1">5.</div>
        <div class="col-11">Tidak akan melakukan perbuatan asusila atau melakukan perjudian di tempat pelayanan;</div>
      </div>
      <div class="row">
        <div class="col-1">6.</div>
        <div class="col-11">Tidak akan melakukan tindak kejahatan seperti menyerang, mengintimidasi atau menipu Klien dan / atau pihak ketiga dan / atau pengguna jasa pelayanan berdasarkan Perjanjian ini dan /atau pemberi jasa lainnya;</div>
      </div>
      <div class="row">
        <div class="col-1">7.</div>
        <div class="col-11">Tidak akan memperdagangkan barang terlarang baik di dalam maupun di luar tempat pelayanan;</div>
      </div>
      <div class="row">
        <div class="col-1">8.</div>
        <div class="col-11">Tidak akan menganiaya, mengancam secara fisik atau mental, menghina secara kasar Klien , dan / atau pihak ketiga dan / atau pengguna jasa pelayanan berdasarkan Perjanjian ini dan / atau pemberi jasa lainnya;</div>
      </div>
      <div class="row">
        <div class="col-1">9.</div>
        <div class="col-11">Tidak akan membujuk Klien atau pemberi jasa lainnya untuk melakukan suatu perbuatan yang bertentangan dengan hukum atau kesusilaan serta peraturan perundang-undangan yang berlaku;</div>
      </div>
      <div class="row">
        <div class="col-1">10.</div>
        <div class="col-11">Tidak akan ceroboh atau sengaja merusak, merugikan atau membiarkan dalam keadaan bahaya barang milik Klien dan / atau pihak ketiga dan / atau pengguna jasa pelayanan berdasarkan Perjanjian ini;</div>
      </div>
      <div class="row">
        <div class="col-1">11.</div>
        <div class="col-11">Tidak akan membongkar atau membocorkan rahasia atau data-data Klien dan / atau pihak ketiga dan / atau pengguna jasa pelayanan berdasarkan Perjanjian ini;</div>
      </div>
    </div>
  </div>
</body>

</html>