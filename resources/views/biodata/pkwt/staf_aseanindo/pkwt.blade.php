<!DOCTYPE html>
<html lang="en">

<head>
  <title>PERJANJIAN KERAHASIAAN KARYAWAN</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }
    body {
      font-size: 8.5px;
    }
  </style>
</head>

<body>
  <img src="{{ public_path('backend/assets/img_hrd/header.png') }}" alt="header" width="100%">
  <br /><br />
  <div class="container" align="jusitfy">
    <center>
      <h5>
        <u><strong>SURAT PERJANJIAN KERJA WAKTU TERTENTU</strong></u> <br>
        <small>No. {{ $param['last_number_pkwt'] }} /PKWT-{{ $param['singkatan_site'] }}/{{ $param['romans'][date('m')] }}/{{ date('Y') }}</small>
      </h5>
    </center>
    <div>
      <p style="margin-bottom: -1px;">Kami yang bertanda tangan di bawah ini:</p>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">Nama</div>
        <div class="col-1">:</div>
        <div class="col-8"><strong>Herman Julianto</strong></div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">Jabatan</div>
        <div class="col-1">:</div>
        <div class="col-8">Direktur HRD</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">Alamat</div>
        <div class="col-1">:</div>
        <div class="col-8">Setiabudi Building 2, Lt.6 Suite 601 Jl. H.R. Rasuna Said Kav.62, Kuningan Jakarta 12920</div>
      </div>
      <p style="margin-bottom: -1px;">Dalam hal ini bertindak untuk dan atas nama <strong>PT. {{ $param['user']->site_name }}</strong>, perseroan terbatas yang didirikan menurut hukum Indonesia, selanjutnya disebut sebagai Pihak Pertama.</p>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">Nama</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->nama_lengkap }}</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">Jenis Kelamin</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->jenis_kelamin == 'P' ? 'Perempuan' : 'Laki-laki' }}</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">Tempat & tanggal lahir</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->tempat }}, {{ $param['tgl_lahir'] }}</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">No. KTP</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->ktp }}</div>
      </div>
      <div class="row" style="margin-bottom: -7px;">
        <div class="col-3">Alamat</div>
        <div class="col-1">:</div>
        <div class="col-8">{{ $param['user']->alamat }}</div>
      </div>
      <p style="margin-bottom: -1px;">Dalam hal ini bertindak untuk dan atas nama sendiri, selanjutnya dalam perjanjian ini disebut sebagai Pihak Kedua.</p>
      <p style="margin-bottom: -1px;">Pihak Pertama dan Pihak Kedua (selanjutnya secara bersama-sama disebut “PARA PIHAK”), SETUJU untuk mengikatkan diri pada perjanjian ini.</p>
      <p style="margin-bottom: -1px;">PARA PIHAK telah mengetahui dan sepakat:</p>
      <div class="row">
        <div class="col-1">a.</div>
        <div class="col-11">Bahwa untuk melaksanakan perjanjian tersebut, PIHAK PERTAMA membuat Perjanjian Kerja Waktu Tertentu dengan PIHAK KEDUA yang mana PIHAK KEDUA setuju dan sepakat akan bekerja untuk PIHAK PERTAMA dan atau perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1">b.</div>
        <div class="col-11">Bahwa walaupun Perjanjian memiliki jangka waktu berlaku sebagaimana ditetapkan di bawah ini, Perjanjian ini akan berakhir dengan sendirinya jika Perjanjian Penyedia Jasa Pekerja antara PIHAK PERTAMA dan Perusahaan/klien berakhir, tanpa ada kewajiban dari PARA PIHAK untuk memberikan ganti kerugian dalam bentuk apapun kepada pihak lainnya.</div>
      </div>
      <p>PARA PIHAK sepakat untuk melaksanakan Perjanjian ini dengan ketentuan-ketentuan sebagai berikut:</p>
      <center><strong>PASAL 1</strong></center>
      <div class="row">
        <div class="col-1">1.1</div>
        <div class="col-11">Pihak Pertama akan memberikan pekerjaan kepada Pihak Kedua sebagai Programmer Junior dengan lokasi kerja di cabang Gedung Setiabudi/cabang lainnya yang ditunjuk (untuk selanjutnya disebut “Perusahaan”)</div>
      </div>
      <div class="row">
        <div class="col-1">1.2</div>
        <div class="col-11">Bila diperlukan, perusahaan dapat menempatkan PIHAK KEDUA pada tugas lain sesuai dengan kemampuannya</div>
      </div>
      <div class="row">
        <div class="col-1">1.3</div>
        <div class="col-11">Pihak Kedua bersedia mematuhi ketentuan-ketentuan yang diatur dalam peraturan maupun ketentuan – ketentuan khusus yang berlaku.</div>
      </div>
      <div class="row">
        <div class="col-1">1.4</div>
        <div class="col-11">Hubungan Kerja antara Pihak Pertama dan Pihak Kedua akan berakhir pada tanggal <strong>17 Januari 2023</strong> tanpa suatu syarat apapun dan dengan masa percobaan selama 3 (tiga) bulan.</div>
      </div>
      <center><strong>PASAL 2</strong></center>
      <div class="row">
        <div class="col-1">2.1</div>
        <div class="col-11">Pihak Pertama memberi gaji kotor perbulan sesuai dengan pangkat dan jabatan yang diberikan kepada Pihak Kedua, sebagaimana yang tercantum didalam Addendum yang merupakan bagian tidak terpisahkan dari perjanjian ini.</div>
      </div>
      <div class="row">
        <div class="col-1">2.2</div>
        <div class="col-11">Penerimaan gaji akan jatuh pada setiap tanggal 25 dan apabila tanggal tersebut jatuh tempo pada hari libur maka akan dibayarkan sebelum tanggal tersebut.</div>
      </div>
      <div class="row">
        <div class="col-1">2.3</div>
        <div class="col-11">Pihak Pertama mengikutsertakan Pihak Kedua dalam program BPJS dan untuk program JHT akan dipotong gajinya untuk disetorkan kepada PT. BPJS sesuai dengan peraturan Menteri Tenaga Kerja No.03/Men/1994</div>
      </div>
      <div class="row">
        <div class="col-1">2.4</div>
        <div class="col-11">Pihak Kedua tidak berhak atas pembayaran apapun selain dari yang diatur oleh perjanjian ini.</div>
      </div>
      <div class="row">
        <div class="col-1">2.5</div>
        <div class="col-11">Untuk jangka waktu kerja kurang dari sebulan, PIHAK KEDUA akan menerima upah prorate atau proposional dari jumlah gaji kotor.</div>
      </div>
      <center><strong>PASAL 3</strong></center>
      <div class="row">
        <div class="col-1">3.1</div>
        <div class="col-11">Pihak Pertama menetapkan lebih lanjut tugas-tugas Pihak Kedua dalam pekerjaan sebagaimana disebutkan dalam Addendum.</div>
      </div>
      <div class="row">
        <div class="col-1">3.2</div>
        <div class="col-11">Bila diperlukan, PIHAK KEDUA bersedia dan sanggup untuk melakukan pekerjaan diluar jam kerja yang telah ditetapkan atau pada hari libur atas perintah dari pihak Perusahaan</div>
      </div>
      <div class="row">
        <div class="col-1">3.3</div>
        <div class="col-11">Waktu kerja adalah 5 (lima) hari seminggu menurut jadwal yang ditentukan oleh perusahaan yang tidak bertentangan dengan ketentuan undang-undang.</div>
      </div>
      <div class="row">
        <div class="col-1">3.4</div>
        <div class="col-11">Apabila Pihak Kedua tidak hadir bekerja dalam waktu kerjanya tanpa alasan yang dapat dipertanggung jawabkan atau tanpa persetujuan Pihak Pertama atau perusahaan maka gaji Pihak Kedua akan mengalami pemotongan sebesar Rp.50.000,- (lima puluh ribu rupiah) per hari</div>
      </div>
      <center><strong>PASAL 4</strong></center>
      <div class="row">
        <div class="col-1">4.1</div>
        <div class="col-11">Pihak Pertama setiap saat dapat melakukan pemutusan hubungan kerja kepada Pihak Kedua jika Pihak Kedua melakukan kesalahan baik dengan sengaja atau tidak sengaja yang berupa perbuatan sebagai berikut:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1a</div>
        <div class="col-11">Penipuan, pencurian dan penggelapan barang/uang milik perusahaan atau rekan sekerja.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1b</div>
        <div class="col-11">Memberikan keterangan palsu atau yang dipalsukan sehingga merugikan perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1c</div>
        <div class="col-11">Mabuk, minum-minuman keras yang memabukkan, madat, memakai obat bius atau menyalahgunakan obat terlarang lainnya di tempat kerja.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1d</div>
        <div class="col-11">Melakukan perbuatan asusila atau melakukan perjudian di tempat kerja.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1e</div>
        <div class="col-11">Melakukan tindak kejahatan seperti menyerang, mengintimidasi atau menipu Pihak Pertama atau teman sekerja atau karyawan perusahaan dan memperdagangkan barang terlarang baik didalam maupun diluar lingkungan perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1f</div>
        <div class="col-11">Menganiaya, mengancam secara fisik atau mental, menghina secara kasar Pihak Pertama atau teman sekerja atau karyawan dari perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1g</div>
        <div class="col-11">Membujuk Pihak Pertama atau rekan sekerja atau karyawan dari perusahaan untuk melakukan suatu perbuatan yang bertentangan dengan hukum atau kesusilaan serta peraturan perundangan yang berlaku.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1h</div>
        <div class="col-11">Dengan ceroboh atau sengaja merusak, merugikan atau membiarkan dalam keadaan bahaya barang milik perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1i</div>
        <div class="col-11">Dengan ceroboh atau sengaja merusak atau membiarkan diri atau teman sekerjanya atau karyawan dari perusahaan dalam keadaan bahaya.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1j</div>
        <div class="col-11">Membongkar atau membocorkan rahasia perusahaan atau mencemarkan nama baik perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1k</div>
        <div class="col-11">Melakukan kesalahan yang bobotnya sama setelah mendapat peringatan terakhir yang masih berlaku.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1l</div>
        <div class="col-11">Melanggar ketentuan-ketentuan lain yang diatur dalam Perjanjian Kerja atau Peraturan Perusahaan.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1m</div>
        <div class="col-11">Absen dalam waktu sedikit-dikitnya 5 (lima) hari kerja berturut-turut tanpa keterangan secara tertulis dengan bukti yang sah dan dimana Perusahaan telah memberitahukan hal tersebut kepada Pihak Pertama.</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">4.1n</div>
        <div class="col-11">Absen tanpa keterangan-keterangan yang sah dan tidak berturut-turut sejumlah 8 (delapan) hari kerja dalam sebulan.</div>
      </div>
      <div class="row">
        <div class="col-1">4.2</div>
        <div class="col-11">Pihak Kedua dapat diputuskan hubungan kerjanya bila Pihak Kedua tidak memenuhi persyaratan kerja yang ditetapkan oleh Pihak Pertama dan Perusahaan.</div>
      </div>
      <center><strong>PASAL 5</strong></center>
      <p>Semua informasi dan atau bahan-bahan yang didapat dari pekerjaan yang dilakukan berdasarkan perjanjian ini baik yang sudah selesai atau masih dalam proses penyelesaian adalah merupakan milik dari perusahaan. Untuk segala keperluan termasuk dan tidak terbatas penggandaan, modifikasi, atau pengungkapan dari informasi dan atau bahan-bahan tersebut harus mendapat persetujuan tertulis dari Perusahaan.</p>
      <center><strong>PASAL 6</strong></center>
      <p>Pihak Kedua berjanji untuk melindungi Pihak Pertama dan Perusahaan dari dan terhadap kerugian atau kewajiban yang timbul dari tuntutan apapun akibat dari penyalahgunaan trade secret, patent, copy right atau pelanggaran hak-hak kepemilikan dari informasi dan atau bahan-bahan yang diberikan oleh Pihak Kedua atau penggunaan dari informasi atau bahan-bahan tersebut.</p>
      <center><strong>PASAL 7</strong></center>
      <p>Pihak Pertama tidak terikat pada janji lisan yang diberikan oleh siapapun, kecuali yang terscantum dalam perjanjian ini.</p>
      <center><strong>PASAL 8</strong></center>
      <p>Dalam hal timbul perbedaan pengertian mengenai pasal-pasal Perjanjian ini oleh kedua belah pihak, akan diselesaikan secara musyawarah, maka persoalannya akan diajukan kepada Kantor Departemen Tenaga Kerja ditempat kedudukan Pihak Pertama untuk mendapatkan penyelesaian sebagaimana mestinya.</p>
      <center><strong>PASAL 9</strong></center>
      <p>Pembatalan oleh salah satu pihak Perjanjian ini atas satu atau lebih haknya berdasarkan Perjanjian ini tidak dianggap sebagai pembatalan yang berkelanjutan atas hak-hak yang lainnya berdasarkan Perjanjian ini oleh pihak tersebut untuk menuntut pihak lain tersebut melakukan kewajibannya sepenuhnya melaksanakan semua hak-hak pihak tersebut sesuai dengan ketentuan-ketentuan didalam Perjanjian ini.</p>
      <center><strong>PASAL 10</strong></center>
      <p>Pihak Pertama dan Pihak Kedua tidak dibenarkan mengalihkan hak kewajiban berdasarkan Perjanjian ini, mengontrakkan kepada orang lain seluruh atau sebagian pekerjaan yang dilakukan berdasarkan Perjanjian ini.</p>
      <center><strong>PASAL 11</strong></center>
      <p>Dalam terjadi pertentangan antara syarat dan ketentuan dalam batang tubuh perjanjian ini dengan yang terkandung dalam lampiran-lampiran terlampir, maka syarat dan ketentuan yang termuat dalam batang tubuh ini yang berlaku.</p>
      <center><strong>PASAL 12</strong></center>
      <div class="row">
        <div class="col-1">12.1</div>
        <div class="col-11">Perjanjian ini dibuat dan ditandatangani oleh Pihak Pertama dan Pihak Kedua dalam keadaan sadar dan tanpa paksaan dari siapapun, pada tanggal {{ $param['date_pkwt'] }} di Jakarta dibuat dalam rangkap 2 (dua) yang mempunyai kekuatan hukum yang sama, masing-masing untuk Pihak Pertama dan Pihak Kedua.</div>
      </div>
      <div class="row">
        <div class="col-1">12.2</div>
        <div class="col-11">Dalam hal pengakhiran berdasarkan ketentuan-ketentuan dalam perjanjian ini, Pihak Pertama dan Pihak Kedua dengan tegas dan setuju untuk membatalkan dan mengabaikan hak dan kewajiban masing-masing berdasarkan Pasal 1266 dan 1267 Kitab Undang-undang Hukum Perdata.</div>
      </div>
      <center><strong>PASAL 13</strong></center>
      <p>Perjanjian ini mengikat Pihak Pertama dan Pihak Kedua serta berlaku mulai tanggal {{ $param['valid_from'] }} sampai dengan tanggal {{ $param['valid_until'] }}
      <p>
      <div class="row">
        <div class="col-7">
          <strong>Jakarta, {{ $param['date_pkwt'] }}</strong><br />
          <strong>Pihak Pertama</strong>
          <br /> <img src="{{ public_path('backend/assets/img_hrd/cap_ans.png') }}" alt="cap_ans" style="width: 150px; height: 75px; object-fit: cover;margin-left: -20px;"> <br />
          <strong>
            <u>Herman Julianto</u> <br /> Direktur HRD
          </strong>
        </div>
        <div class="col-5">
        <br /><br/> <img src="{{ public_path('data_file_ttd/'.$param['user']->user_id.'.jpeg') }}" alt="$param['user']->user_id" style="width: 150px; height: 75px; object-fit: cover;margin-left: -20px;"> <br />
          <strong>Pihak Kedua,</strong>
        </div>
      </div>
    </div>
  </div>
</body>

</html>