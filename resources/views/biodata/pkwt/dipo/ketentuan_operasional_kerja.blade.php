<!DOCTYPE html>
<html>

<head>
  <title>KETENTUAN OPERASIONAL KERJA</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    hr {
      border: 3px solid black;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container">
    <center>
      <h5>Ketentuan Operasional Kerja (KOK) {{ $param['user']->position_name }} bagian Penagihan</h5>
    </center>
    <div align="justify">
      <p>
        Sebagai satu kesatuan dari Perjanjian Kerja Waktu Tertentu (PKWT) No {{ $param['last_number_pkwt'] }}/ANS/DC/DSF/{{ date('Y') }} tertanggal {{ $param['date_pkwt'] }}, maka berikut adalah Ketentuan Operasional Kerja (yang selanjutnya disebut “KOK”) untuk {{ $param['user']->position_name }} (yang selanjutnya dalam KOK ini disebut “PIHAK KEDUA”) yang mulai berlaku efektif mulai {{ $param['valid_until'] }}

      </p>
      <!-- Part 1 -->
      <div class="row">
        <div class="col-1"><strong>1. </strong></div>
        <div class="col-11"><strong>FRAUD</strong></div>
      </div>
      <!-- Part 1 Point 1.1 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1.1. </div>
        <div class="col-10">
          Definisi Fraud <br />
          Yang dimaksud dengan <i>Fraud</i> dalam hal ini adalah segala bentuk penipuan, pemalsuan, pencurian, penggelapan dokumen atau salah satu dari rangkaian proses penagihan hutang kepada nasabah. Termasuk di dalam definisi ini dan juga merupakan Daftar Pembatasan PIHAK KEDUA antara lain:
        </div>
      </div>
      <!-- Part 1 Point 1.1 = a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">Mengambil, memanfaatkan, memalsukan, menggandakan, data/dokumen nasabah untuk kepentingan pribadi.</div>
      </div>
      <!-- Part 1 Point 1.1 = b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">Membantu nasabah untuk mendapatkan potongan (discount) dan atau perpanjangan jangka waktu pembayaran hutang yang tidak sesuai dengan peraturan, prosedur dan kebijakan yang berlaku pada perusahaan.</div>
      </div>
      <!-- Part 1 Point 1.1 = c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">Menerima, mengambil keuntungan dalam bentuk apa pun dari nasabah dalam proses penagihan hutang untuk kepentingan pribadi.</div>
      </div>
      <!-- Part 1 Point 1.1 = d -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-9">Meminta uang kepada nasabah.</div>
      </div>
      <!-- Part 1 Point 1.1 = e -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-9">Meminjam dan/atau meminjamkan uang kepada sesama pekerja yang bekerja di Perusahaan, ataupun kepada pekerja Perusahaan.</div>
      </div>
      <!-- Part 1 Point 1.1 = f -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-9">Melakukan tindakan yang dapat mengancam hubungan baik antara nasabah dengan Perusahaan.</div>
      </div>
      <!-- Part 1 Point 1.1 = g -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-9">Memberitahukan informasi mengenai nasabah kepada pihak lain seperti informasi data pribadi, informasi rekening, informasi pinjaman dan spesimen tanda tangan.</div>
      </div>
      <!-- Part 1 Point 1.1 = h -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">h. </div>
        <div class="col-9">Terikat perjanjian kontrak kerja dengan institusi lain (perbankan atau non-perbankan) atau menerima bayaran dari institusi lain. </div>
      </div>
      <!-- Part 1 Point 1.1 = i -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-9">Dalam keadaan atau dengan alasan apapun, menerima uang setoran nasabah, pinjam-meminjam uang atau meminta imbalan berupa uang atau dalam bentuk apapun dari nasabah atau calon nasabah Perusahaan untuk membantu calon nasabah Perusahaan memperoleh pijaman dari Perusahaan, atau dengan cara melanggar peraturan Perusahaan atau peraturan yang telah disepakati oleh PIHAK KEDUA serta ketentuan perundang-undangan yang berlaku untuk mencapai target kerja PIHAK KEDUA.</div>
      </div>
      <!-- Part 1 Point 1.1 = j -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">j. </div>
        <div class="col-9">Mengabaikan kewajiban untuk mengembalikan semua dokumen, yang pernah diterima oleh PIHAK KEDUA dari Perusahaan atau atasan langsung, kepada Perusahaan atau atasan langsung pada hari terakhir PIHAK KEDUA hadir bekerja sebelum perjanjian atau hubungan kerja antara PIHAK KEDUA dan PIHAK PERTAMA berakhir atau tidak dilanjutkan.</div>
      </div>
      <!-- Part 1 Point 1.1 = k -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">k. </div>
        <div class="col-9">Memberikan keterangan atau membuat surat keterangan palsu atau yang dipalsukan, baik untuk PIHAK KEDUA sendiri atau rekan sekerjanya.</div>
      </div>
      <!-- Part 1 Point 1.2 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1.2. </div>
        <div class="col-10">Sanksi Fraud</div>
      </div>
      <!-- Part 1 Point 1.2 = a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-9">Sesuai bobot kesalahan yang cukup diputuskan semata-mata berdasarkan putusan PIHAK PERTAMA dan/atau Tim Fraud dari Perusahaan, PIHAK KEDUA setuju dapat dikenakan Surat Peringatan (Warning Letter) atau Pemutusan Hubungan Kerja tanpa adanya ganti kerugian dalam bentuk apapun.</div>
      </div>
      <!-- Part 1 Point 1.2 = b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-9">PIHAK KEDUA yang melakukan tindakan fraud seperti penipuan, meminta uang nasabah atau calon nasabah, pemalsuan, pencurian, dan /atau penggelapan dokumen-dokumen nasabah atau calon nasabah akan dikenakan sanksi Pemutusan Hubungan Kerja tanpa adanya ganti kerugian dalam bentuk apapun.</div>
      </div>
      <!-- Part 1 Point 1.2 = c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-9">Tindakan Fraud akan mempengaruhi penilaian (review) perpanjangan kontrak kerja PIHAK KEDUA.</div>
      </div>
      <!-- Part 1 Point 1.3 -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">1.3. </div>
        <div class="col-10">
          Pencegahan Fraud <br />
          PIHAK KEDUA menyepakati dan bersedia, dalam rangka mencegah terjadinya Fraud, Perusahaan dari waktu ke waktu akan melakukan pemeriksaan terhadap tas dan/atau barang bawaan dan/atau laci dari PIHAK KEDUA termasuk melakukan investigasi kepada PIHAK KEDUA sebagai bagian dari proses investigasi indikasi fraud.
        </div>
      </div>
      <!-- Part 2 -->
      <div class="row">
        <div class="col-1"><strong>2. </strong></div>
        <div class="col-11"><strong>Jam dan waktu kerja pada perusahaan.</strong></div>
      </div>
      <!-- Part 2 point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Jam kerja normal adalah jam 08.00 – 17.00 (Senin-Jum’at, istirahat 1 jam)
        </div>
      </div>
      <!-- Part 2 point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Khusus pada hari Jum’at, bagi pekerja pria yang beragama Islam dapat menggunakan waktu istirahat mulai pukul 11:30 hingga 13:30 untuk melaksanakan ibadah agamanya dan istirahat untuk makan siang tanpa mengurangi target produktivitas yang harus dicapai.
        </div>
      </div>
      <!-- Part 2 point c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          PIHAK PERTAMA dan atau Perusahaan dapat merubah dan menentukan jadwal jam kerja, jam istirahat dan jam kerja bergilir (shift) setiap saat sesuai kebutuhan operasional Perusahaan, sepanjang tidak bertentangan dengan peraturan perundang-undangan yang berlaku.
        </div>
      </div>
      <!-- Part 2 point d -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Jika PIHAK KEDUA bekerja lebih dari jam kerja yang telah ditentukan atau bekerja pada hari libur, dan tidak atas perintah PIHAK PERTAMA atau Perusahaan atau atasan langsung, maka tidak akan diperhitungkan sebagai Lembur.
        </div>
      </div>
      <!-- Part 2 point e -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Apabila PIHAK KEDUA tidak bekerja tanpa ijin dengan persetujuan atasan langsung atau sakit lebih dari 1 (satu) hari tanpa surat dokter yang sah dan benar, maka PIHAK KEDUA dikualifikasikan sebagai mangkir.
        </div>
      </div>
      <!-- Part 3 -->
      <div class="row">
        <div class="col-1"><strong>3. </strong></div>
        <div class="col-11"><strong>Tata Tertib Pada Perusahaan.</strong> <br /> PIHAK KEDUA harus mentaati peraturan tata tertib pada perusahaan.</div>
      </div>
      <!-- Part 3 Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Datang tepat waktu pada jam masuk kerja atau pada jam kembali bekerja sesudah waktu istirahat sesuai waktu kerja yang ditetapkan
        </div>
      </div>
      <!-- Part 3 Point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Setiap keterlambatan hadir harus diberitahukan kepada atasan langsung.
        </div>
      </div>
      <!-- Part 3 Point c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Mengisi absensi secara benar dan jujur setiap masuk bekerja.
        </div>
      </div>
      <!-- Part 3 Point d -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Pakaian kerja untuk hari Senin – Kamis adalah Office Casual (mis. pria mengenakan kemeja, celana kain, dan sepatu formal; perempuan mengenakan celana/rok kain, atasan formal, sepatu formal)
        </div>
      </div>
      <!-- Part 3 Point e -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Pakaian kerja untuk hari Jumat adalah Business Casual (mis. celana jeans, kemeja/kaos berkerah, sepatu casual)
        </div>
      </div>
      <!-- Part 3 Point f -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">f. </div>
        <div class="col-10">
          <i>Access/ID</i> card harus selalu dikenakan selama berada dalam lingkungan tempat kerja
        </div>
      </div>
      <!-- Part 3 Point g -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">g. </div>
        <div class="col-10">
          Menjaga lingkungan kerja tetap bersih (meja kerja, toilet, pantry, dsb)
        </div>
      </div>
      <!-- Part 3 Point h -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">h. </div>
        <div class="col-10">
          Menjaga lingkungan, hubungan dan komunikasi kerja yang harmonis dengan seluruh karyawan di tempat kerja
        </div>
      </div>
      <!-- Part 3 Point i -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">i. </div>
        <div class="col-10">
          Menjaga peralatan kerja tetap bersih dan berfungsi dengan baik (komputer, headset, telepon, dsb)
        </div>
      </div>
      <!-- Part 3 Point j -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">j. </div>
        <div class="col-10">
          Mengikuti tata cara menelpon dengan baik (mis. salam, pemilihan kata, intonasi, artikulasi).
        </div>
      </div>
      <!-- Part 3 Point k -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">k. </div>
        <div class="col-10">
          Tidak makan dan minum saat berbicara dengan nasabah
        </div>
      </div>
      <!-- Part 3 Point l -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">l. </div>
        <div class="col-10">
          Tidak menghubungi/menerima telepon, SMS dengan menggunakan alat komunikasi seperti handphone dan lain-lain pada saat jam kerja
        </div>
      </div>
      <!-- Part 3 Point m -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">m. </div>
        <div class="col-10">
          Tidak mendengarkan radio/musik pada saat jam kerja.
        </div>
      </div>
      <!-- Part 4 -->
      <div class="row">
        <div class="col-1"><strong>4. </strong></div>
        <div class="col-11"><strong>Standar Prestasi Kerja Minimum</strong></div>
      </div>
      <!-- Part 4 part a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Perusahaan akan mengkomunikasikan secara terbuka target bulanan yang harus dicapai dalam bulan tersebut kepada PIHAK KEDUA.
        </div>
      </div>
      <!-- Part 4 part b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Prestasi kerja minimum bagi PIHAK KEDUA yang ditempatkan sebagai {{ $param['user']->position_name }} melekat pada target yang ditetapkan setiap bulannya.
        </div>
      </div>
      <!-- Part 4 part c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Prestasi kerja PIHAK KEDUA akan dicatat dimana data tersebut menjadi acuan atas penilaian prestasi kerja yang bersangkutan selama bertugas sebagai {{ $param['user']->position_name }} dimana PIHAK KEDUA ditempatkan dan tidak menjadi kedaluwarsa dengan berakhirnya suatu periode dari perjanjian kerja yang bersangkutan.
        </div>
      </div>
      <!-- Part 4 part d -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Pengukuran hasil kerja PIHAK KEDUA akan dilakukan setiap bulan secara teratur oleh perusahaan.
        </div>
      </div>
      <!-- Part 5 -->
      <div class="row">
        <div class="col-1"><strong>5. </strong></div>
        <div class="col-11">
          <strong>Kewajiban dan tanggung jawab lainnya</strong> <br />
          PIHAK KEDUA bertugas sebagai berikut:
        </div>
      </div>
      <!-- Part 5 Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Wajib untuk selalu melaksanakan setiap dan segala aturan yang ditetapkan oleh PIHAK PERTAMA dan atau perusahaan yang saat ini berlaku dan mungkin diberlakukan oleh PIHAK PERTAMA dan atau perusahaan dari waktu ke waktu, termasuk peraturan perundangan, norma kesopanan dan kesusilaan yang berlaku,
        </div>
      </div>
      <!-- Part 5 Point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Wajib untuk selalu mengikuti ketentuan dan atau perubahan dan perkembangan ketentuan yang ditetapkan oleh perusahaan dalam aspek kedisiplinan, tata tertib, etika bisnis, kepatuhan pada peraturan kerja,
        </div>
      </div>
      <!-- Part 5 Point c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Wajib menyimpan dengan baik untuk kepentingan PIHAK PERTAMA dan perusahaan setiap dan semua dokumen yang diterimanya dari PIHAK PERTAMA dan perusahaan, dan segera menyerahkan dokumen-dokumen tersebut pada saat perjanjian berakhir,
        </div>
      </div>
      <br/>
      <!-- Part 5 Point d -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">d. </div>
        <div class="col-10">
          Berhak untuk mengakhiri perjanjian ini dengan pemberitahuan tertulis kepada perusahaan dan PIHAK PERTAMA selambatnya 1 (satu) bulan sebelumnya.
        </div>
      </div>
      <!-- Part 5 Point e -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">e. </div>
        <div class="col-10">
          Selama PIHAK KEDUA terikat kontrak kerja, PIHAK KEDUA tidak diperkenankan hamil, dan apabila hal itu terjadi maka PIHAK KEDUA dianggap mengundurkan diri dari Perusahaan tanpa ada kewajiban apapun dari Perusahaan
        </div>
      </div>
      <!-- Part 6 -->
      <div class="row">
        <div class="col-1"><strong>6. </strong></div>
        <div class="col-11"><strong>Surat Peringatan (SP) berkenaan dengan Standar Prestasi Kerja Minimum</strong> <br /> Surat peringatan I, II dan III akan diberikan kepada PIHAK KEDUA melalui mekanisme sebagai berikut:</div>
      </div>
      <!-- Part 6 Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Surat Peringatan I (pertama) yang masa berlakunya 1 (satu) bulan. Surat peringatan I diberikan bila PIHAK KEDUA:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Tidak memenuhi target bulanan dan skor minimum yang telah ditetapkan dan dikomunikasikan oleh perusahaan secara TERTULIS disetujui kedua belah pihak.
        </div>
      </div>
      <!-- Part 6 Point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Surat Peringatan II (Kedua) yang masa berlakunya 2 (dua) bulan. Surat peringatan II diberikan bila PIHAK KEDUA:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Tidak memenuhi target bulanan dan skor minimum yang telah ditetapkan dan dikomunikasikan oleh perusahaan secara TERTULIS disetujui kedua belah pihak sebanyak 2 kali dalam kurun waktu 3 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Apabila pencapaian kurang dari 50 % (lima puluh persen) dari target yang telah ditetapkan dan dikomunikasikan oleh perusahaan secara TERTULIS disetujui kedua belah pihak.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan perbuatan ulang yang tercantum dalam Surat Peringatan I (Pertama) dan/atau perbuatan lainnya yang bobot kesalahannya setara dengan kesalahan yang mengakibatkan diberikannya Surat Peringatan I (Pertama) dalam masa berlakunya Surat Peringatan I (Pertama).
        </div>
      </div>
      <!-- Part 6 Point c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Surat Peringatan III (Ketiga) yang masa berlakunya 3 (tiga) bulan. Surat peringatan III diberikan bila PIHAK KEDUA:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Tidak memenuhi target bulanan dan skor minimum yang telah ditetapkan dan dikomunikasikan oleh perusahaan secara TERTULIS dan disetujui kedua belah pihak sebanyak 3 kali dalam kurun waktu 6 bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan perbuatan ulang yang tercantum dalam Surat Peringatan I (Pertama) dan/atau perbuatan lainnya yang bobot kesalahannya setara dengan kesalahan yang mengakibatkan diberikannya Surat Peringatan I (Pertama) dalam masa berlakunya Surat Peringatan II (Kedua).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan perbuatan yang bobot kesalahannya setara dengan kesalahan yang mengakibatkan diberikannya Surat Peringatan II (Kedua) dalam masa berlakunya Surat Peringatan I (Pertama).
        </div>
      </div>
      <!-- Part 7 -->
      <div class="row">
        <div class="col-1"><strong>7. </strong></div>
        <div class="col-11"><strong>Surat Peringatan (SP) berkenaan dengan Tata Tertib</strong> <br /> Surat peringatan I, II dan III yang masing-masing masa berlakunya 3 (tiga) bulan akan diberikan kepada PIHAK KEDUA melalui mekanisme sebagai berikut:</div>
      </div>
      <!-- Part 7 Point a -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-10">
          Surat Peringatan I (pertama). Surat peringatan I diberikan bila PIHAK KEDUA:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan kesalahan dalam melaksanakan pekerjaannya yang menimbulkan pengaduan keras (hard complaint) dari nasabah walaupun tidak menimbulkan kerugian pada perusahaan,
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Mangkir 3 hari kerja dalam kurun waktu 1 (satu) bulan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melanggar tata tertib perusahaan (butir 3) lebih dari 3 kali dalam 1 (satu) bulan,
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Tidak memenuhi kewajiban dan tanggung jawab lainya seperti yang disebutkan pada butir 5.a atau butir 5.b.
        </div>
      </div>
      <!-- Part 7 Point b -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-10">
          Surat Peringatan II (Kedua). Surat peringatan II diberikan bila PIHAK KEDUA:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan kesalahan dalam melaksanakan pekerjaannya yang mengakibatkan nasabah melakukan pengaduan melalui media massa yang berpotensi merusak citra dan nama baik perusahaan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan perbuatan ulang yang tercantum dalam Surat Peringatan I (Pertama) dan/atau perbuatan lainnya yang bobot kesalahannya setara dengan kesalahan yang mengakibatkan diberikannya Surat Peringatan I (Pertama) dalam masa berlakunya Surat Peringatan I (Pertama).
        </div>
      </div>
      <!-- Part 7 Point c -->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-10">
          Surat Peringatan III (Ketiga). Surat peringatan III diberikan bila PIHAK KEDUA:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Lalai dalam melaksanakan pekerjaannya yang mengakibatkan kerugian pada perusahaan dan setelah dilakukan pemeriksaan tidak terdapat unsur kesengajaan.
        </div>
      </div>
      <br/><br/><br/><br/>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan perbuatan ulang yang tercantum dalam Surat Peringatan I (Pertama) dan/atau perbuatan lainnya yang bobot kesalahannya setara dengan kesalahan yang mengakibatkan diberikannya Surat Peringatan I (Pertama) dalam masa berlakunya Surat Peringatan II (Kedua).
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">- </div>
        <div class="col-9">
          Melakukan perbuatan yang bobot kesalahannya setara dengan kesalahan yang mengakibatkan diberikannya Surat Peringatan II (Kedua) dalam masa berlakunya Surat Peringatan I (Pertama).
        </div>
      </div>
      <!-- Part 8 -->
      <div class="row">
        <div class="col-1"><strong>8. </strong></div>
        <div class="col-11"><strong>Pemutusan Hubungan Kerja tanpa ganti kerugian dalam bentuk apapun terjadi jika:</strong> <br /> PIHAK KEDUA melakukan perbuatan yang bobot kesalahannya tersebut setara dengan ketentuan dalam tabel dibawah, dalam masa berlakunya surat peringatan sebelumnya. Tabel tersebut adalah sebagai berikut:</div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-11">
          <table border="1" width="100%">
            <tr>
              <th>Masa Berlaku dari Surat Peringatan : </th>
              <th>Melakukan Kesalahan setara dengan Surat Peringatan : </th>
            </tr>
            <tr>
              <td>I (Pertama)</td>
              <td>III (Ketiga)</td>
            </tr>
            <tr>
              <td>II (Kedua)</td>
              <td>II (Kedua) atau III (Ketiga)</td>
            </tr>
            <tr>
              <td>III (Ketiga)</td>
              <td>I (Pertama) atau II (Kedua) atau III (Ketiga)</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- Part 9 -->
      <div class="row">
        <div class="col-1"><strong>9. </strong></div>
        <div class="col-11">
          <strong>Program insentif</strong> <br />
          Program insentif adalah program yang bersifat periodik untuk jangka waktu tertentu yang diberikan oleh perusahaan bila PIHAK KEDUA mencapai target yang telah ditetapkan. Adapun periode, target dan besaran insentif tersebut akan dikomunikasikan secara terbuka oleh perusahaan kepada PIHAK KEDUA dan tidak terbatas pada perubahan-perubahan yang dilakukan oleh perusahaan.
        </div>
      </div>
      <!-- Part 10 -->
      <div class="row">
        <div class="col-1"><strong>10. </strong></div>
        <div class="col-11">
          <strong>Surat panggilan kerja</strong> <br />
          Apabila PIHAK KEDUA mangkir (tidak bekerja tanpa ijin atau sakit tanpa surat dokter yang sah dan benar) selama 5 (lima) hari kerja atau lebih berturut-turut, dan PIHAK PERTAMA telah mengirimkan surat panggilan sebanyak 2 (dua) kali yang dialamatkan sesuai catatan yang disampaikan PIHAK KEDUA kepada PIHAK PERTAMA, maka PIHAK KEDUA dapat diputus hubungan kerjanya karena dikualifikasikan mengundurkan diri dari pekerjaannya.
        </div>
      </div>
      <div class="row">
        <div class="col-4" align="center">
          <strong>Mengetahui dan menyetujui</strong><br />
          <strong>Pihak Kedua</strong>
          <br /><br /><br /><br /><br />
          ( {{ $param['user']->nama_lengkap }} )
        </div>
        <div class="col-4"></div>
        <div class="col-4" align="center">
          <strong>Pihak Pertama</strong>
          <br /> <img src="{{ public_path('backend/assets/img_hrd/cap_ans.png') }}" alt="cap_ans" style="width: 150px; height: 75px; object-fit: cover;"> <br />
          <u>( Herman Juliano )</u> <br /> Direktur HRD
        </div>
      </div>
    </div>
  </div>
</body>

</html>