<!DOCTYPE html>
<html>

<head>
  <title>KETENTUAN OPERASIONAL KERJA</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }
    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container">
    <center>
      <h5>KETENTUAN OPERASIONAL KERJA <br /> <small>Outbound Contact Centre Multi Product</small></h5>
    </center>
    <div align="justify">
      <p>Sebagai satu kesatuan dengan Perjanjian Kerja Waktu Tertentu No. {{ $param['last_number_pkwt'] }}/{{ $param['singkatan'] }}-BNI/PKWT/{{ $param['romans'][date('m')] }}/{{ date('Y') }} <strong>tertanggal {{ $param['date_pkwt'] }}</strong> maka berikut adalah Ketentuan Operasional {{ $param['user']->position_name }} BNI yang berlaku efektif mulai <strong>tanggal {{ $param['date_pkwt'] }}</strong>.</p>
      <!-- point 1 -->
      <div class="row">
        <div class="col-1">1. </div>
        <div class="col-11">
          Komisi dan Bonus <br />
          Komisi penjualan beserta bonus akan dihitung dan dibayarkan per bulan berdasarkan skema komisi dan bonus yang telah ditentukan. Perhitungan dan pembayaran tersebut tidak dapat diperhitungkan ataupun diakumulasikan ke bulan berikutnya.
        </div>
      </div>
      <!-- point 2 -->
      <div class="row">
        <div class="col-1">2. </div>
        <div class="col-11">
          Tindakan Kecurangan (Fraud Case)<br />
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2.1. </div>
        <div class="col-10">
          Definisi Tindakan Kecurangan<br />
          Adalah penipuan, pemalsuan, pencurian, dan atau penggelapan dokumen atau salah satu dari rangkaian proses pengajuan produk LOP (Loan On Phone) calon nasabah. {{ $param['user']->position_name }} dalam situasi apapun tidak dibenarkan atau dilarang untuk membantu calon nasabah untuk memperoleh LOP dari BNI dengan cara yang tidak sesuai dengan peraturan yang telah disepakati maupun ketentuan perundangan yang berlaku. Termasuk di dalam definisi ini dan juga merupakan daftar pembatasan {{ $param['user']->position_name }} antara lain: <br />
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.1. </div>
        <div class="col-9">
          Mengambil/meminta uang kepada calon nasabah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.2. </div>
        <div class="col-9">
          Mengajukan aplikasi tanpa sepengetahuan calon nasabah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.3. </div>
        <div class="col-9">
          Berbohong ke calon nasabah dengan menjanjikan atau menawarkan fasilitas/manfaat palsu seperti:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-8">
          Menjanjikan suku bunga dan biaya tertentu atas LOP yang disetujui.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">b. </div>
        <div class="col-8">
          Menjanjikan/menawarkan hadiah yang tidak sejalan dengan program yang sedang berlaku.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">c. </div>
        <div class="col-8">
          Menjanjikan limit tertentu/jaminan persetujuan palsu ke calon nasabah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.4. </div>
        <div class="col-9">
          Memanfaatkan atau mengambil dokumen/salinan dokumen calon nasabah atau aplikasi bukan miliknya.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.5. </div>
        <div class="col-9">
          Memanfaatkan atau mengambil dokumen/salinan dokumen calon nasabah untuk kepentingan pribadi.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.6. </div>
        <div class="col-9">
          Memalsukan tanda tangan atau dokumen/salinan dokumen calon nasabah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.7. </div>
        <div class="col-9">
          Mengganti, merusakkan, atau menghilangkan isi aplikasi milik orang lain.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.8. </div>
        <div class="col-9">
          Membantu calon nasabah untuk mendapatkan LOP BNI dengan cara yang tidak benar.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.9. </div>
        <div class="col-9">
          {{ $param['user']->position_name }} tidak diperkenankan memiliki sub-agen untuk membantu {{ $param['user']->position_name }} dalam melakukan penjualan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.10. </div>
        <div class="col-9">
          {{ $param['user']->position_name }} setuju bahwa dalam rangka mencegah terjadinya kecurangan, BNI akan dari waktu ke waktu mengadakan pemeriksaan terhadap barang bawaan pribadi dari {{ $param['user']->position_name }}.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.11. </div>
        <div class="col-9">
          {{ $param['user']->position_name }} dilarang memasarkan atau menjual produk selain dari produk BNI.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.12. </div>
        <div class="col-9">
          Menyalin/menggandakan aplikasi dan atau dokumen pelengkap aplikasi/permohonan untuk dimanfaatkan bagi keperluan pribadi atau keperluan lain di luar proses pembuatan LOP BNI kecuali telah memperoleh izin tertulis dari pihak perusahaan dan BNI.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.13. </div>
        <div class="col-9">
          {{ $param['user']->position_name }} dalam keadaan atau alasan apapun DILARANG untuk menerima uang pembayaran nasabah, pinjam-meminjam uang atau meminta imbalan berupa uang atau dalam bentuk apapun dari calon nasabah atau nasabah BNI untuk membantu calon nasabah BNI memperoleh LOP BNI, atau dengan cara melanggar peraturan BNI atau peraturan yang telah disepakati oleh {{ $param['user']->position_name }} serta ketentuan perundang-undangan yang berlaku untuk mencapai target {{ $param['user']->position_name }} yang telah ditentukan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.1.14. </div>
        <div class="col-9">
          Mengabaikan salah satu dari rangkaian proses pengajuan LOP melalui proses telesales sebagai berikut:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">a. </div>
        <div class="col-8">
          Melakukan proses Know Your Nasabah (KYN) dengan baik dan benar sesuai dengan prosedur yang berlaku.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">2.2. </div>
        <div class="col-10">
          Sanksi Tindakan kecurangan:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.2.1. </div>
        <div class="col-9">
          Surat peringatan (warning letter) sesuai dengan bobot kesalahan berdasarkan penilaian dari BNI atau pemutusan hubungan kerja tanpa adanya ganti kerugian dalam bentuk apapun.
        </div>
      </div>
      <br /><br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.2.2. </div>
        <div class="col-9">
          Tindakan kecurangan akan dikenakan sanksi terberat yaitu pemutusan hubungan kerja tanpa adanya ganti kerugian dalam bentuk apapun.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1"></div>
        <div class="col-1">2.2.3. </div>
        <div class="col-9">
          {{ $param['user']->position_name }}, melalui perjanjian kerja ini, memberikan kuasa kepada pihak pertama untuk tidak membayarkan komisi, tunjangan, bonus, dan hadiah yang masih belum diterima pihak kedua sebagai uang pengganti kerugian administrasi.
        </div>
      </div>
      <!-- point 3 -->
      <div class="row">
        <div class="col-1">3. </div>
        <div class="col-11">
          Catatan Standar Prestasi Kerja<br />
          Data prestasi kerja {{ $param['user']->position_name }} akan tercatat dan menjadi acuan untuk menilai prestasi kerja yang bersangkutan selama bertugas sebagai {{ $param['user']->position_name }} di mana {{ $param['user']->position_name }} ditempatkan dan tidak kadaluwarsa dengan berakhirnya suatu periode dari perjanjian kerja yang bersangkutan, target penjualan tersebut adalah:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">3.1. </div>
        <div class="col-11">
          memiliki target yang ditentukan oleh klien.
        </div>
      </div>
      <!-- Point 4 -->
      <div class="row">
        <div class="col-1">4. </div>
        <div class="col-11">
          Pengukuran Hasil Kerja<br />
          Pengukuran hasil kerja akan dilakukan setiap bulan secara teratur. Cuti dan hari yang terpakai pada masa evaluasi kinerja tertulis tidak diperhitungkan.
        </div>
      </div>
      <!-- Point 5 -->
      <div class="row">
        <div class="col-1">5. </div>
        <div class="col-11">
          Standar Prestasi Minimum<br />
          Prestasi kerja minimum bagi {{ $param['user']->position_name }} sebagai berikut:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">5.1. </div>
        <div class="col-10">
          Bertugas mendengarkan recording sesuai dengan ketentuan yang berlaku dan perubahannya kepada nasabah.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">5.2. </div>
        <div class="col-10">
          Memberikan dan mengutamakan layanan terbaik dan efisien kepada nasabah sesuai dengan program dan penawaran produk BNI.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">5.3. </div>
        <div class="col-10">
          Dalam menyampaikan informasi produk LOP BNI, {{ $param['user']->position_name }} akan memastikan informasi yang disampaikan adalah akurat, benar, dan tidak mengandung unsur penipuan mengenai layanan atau produk BNI yang tersebut sesuai dengan standar ketentuan operasional yang berlaku.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">5.4. </div>
        <div class="col-10">
          Melayani dan membantu nasabah untuk pengisian form aplikasi LOP BNI.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">5.5. </div>
        <div class="col-10">
          Setiap {{ $param['user']->position_name }} wajib menguasai setiap produk dan promosi yang sedang berjalan dan bisa menjelaskannya dengan baik dan benar kepada calon nasabah.
        </div>
      </div>
      <!-- Point 6 -->
      <div class="row">
        <div class="col-1">6. </div>
        <div class="col-11">
          Hal-hal yang tidak boleh dilakukan saat bekerja adalah sebagai berikut:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.1. </div>
        <div class="col-10">
          Membawa masuk tas, telepon gengam, dan alat tulis menulis ke dalam ruangan kerja dan semua barang pribadi {{ $param['user']->position_name }} harus disimpan di dalam loker yang sudah disediakan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.2. </div>
        <div class="col-10">
          Berbicara / tertawa secara berlebihan dan bergerombol di ruangan kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.3. </div>
        <div class="col-10">
          Tidur, bersandar, dan atau melepas sepatu selama jam kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.4. </div>
        <div class="col-10">
          Keluar dari area kerja pada saat bertugas tanpa izin dari atasan yang berwenang.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.5. </div>
        <div class="col-10">
          {{ $param['user']->position_name }} dilarang berjalan-jalan pada waktu kerja yang telah ditentukan kecuali untuk mengambil minum, ke kamar kecil dan shalat dan hal ini harus sepengetahuan oleh atasan nya.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.6. </div>
        <div class="col-10">
          Dilarang makan selama waktu kerja berlangsung.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.7. </div>
        <div class="col-10">
          Berpakaian tidak sopan, menggunakan kaos oblong kecuali hari Sabtu dan dilarang menggunakan sandal jepit pada waktu kerja kecuali pada waktu shalat.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">6.8. </div>
        <div class="col-10">
          Selama {{ $param['user']->position_name }} terikat kontrak kerja, {{ $param['user']->position_name }} tidak diperkenankan hamil, dan apabila hal itu terjadi maka {{ $param['user']->position_name }} dianggap mengundurkan diri dari Perusahaan tanpa ada kewajiban apapun dari Perusahaan.
        </div>
      </div>
      <!-- Point 7 -->
      <div class="row">
        <div class="col-1">7. </div>
        <div class="col-11">
          Disiplin Kehadiran Kerja
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">7.1. </div>
        <div class="col-10">
          Apabila {{ $param['user']->position_name }} tidak hadir karena sakit maka {{ $param['user']->position_name }} atau wakilnya wajib menyerahkan surat keterangan dokter yang sah kepada atasan langsung paling lambat 3 (tiga) hari sejak {{ $param['user']->position_name }} tidak hadir bekerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">7.2. </div>
        <div class="col-10">
          Surat sakit yang berlaku hanyalah yang asli, tercantum alamat, nomor telepon tempat praktek dokter, cap dan tanda tangan asli dokter yang memeriksa.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">7.3. </div>
        <div class="col-10">
          Terlambat 3X dalam sebulan akan mendapatkan SP 1, Absen tanpa keterangan 2X dalam sebulan mendapatkan SP 1, SP bersifat akumulatif selam 3 (tiga) bulan.
        </div>
      </div>
      <!-- Point 8 -->
      <div class="row">
        <div class="col-1">8. </div>
        <div class="col-11">
          Surat Panggilan Kerja <br />
          Bagi {{ $param['user']->position_name }} yang tidak bekerja tanpa izin atau sakit tanpa surat dokter yang sah dan benar maka yang bersangkutan sejalan dengan ketentuan yang berlaku akan memperoleh Surat Panggilan Kerja sampai 3 kali dalam 3 hari, jika setelah dipanggil sampai 3 kali tidak memberikan keterangan tertulis maka yang bersangkutan dianggap mengundurkan diri dari pekerjaannya.
        </div>
      </div>
      <!-- Point 9 -->
      <div class="row">
        <div class="col-1">9. </div>
        <div class="col-11">
          Sanksi/Surat Peringatan <br />
          Surat peringatan dikeluarkan jika melanggar ketentuan tersebut di atas dengan ketentuan sebagai berikut:
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">9.1. </div>
        <div class="col-10">
          {{ $param['user']->position_name }} yang tidak hadir tanpa keterangan dalam 1 (satu) periode dan atau berturut – turut dalam perhitungan cut off absensi akan diberikan Surat Peringatan ke-1.
        </div>
      </div>
      <br /><br />
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">9.2. </div>
        <div class="col-10">
          Peringatan verbal diberikan apabila {{ $param['user']->position_name }} melakukan pelanggaran pertama sesuai dengan ketentuan yang disebutkan diatas, kecuali pelanggaran terhadap kehadiran kerja.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">9.3. </div>
        <div class="col-10">
          Surat peringatan ke-1 diberikan apabila {{ $param['user']->position_name }} masih melakukan kesalahan yang sama setelah peringatan verbal diberikan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">9.4. </div>
        <div class="col-10">
          Surat peringatan ke-1 diberikan apabila {{ $param['user']->position_name }} melanggar Ketentuan Operasional Kerja yang berlaku setelah peringatan verbal dikeluarkan.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">9.5. </div>
        <div class="col-10">
          Surat peringatan ke-2 diberikan apabila {{ $param['user']->position_name }} melakukan kesalahan yang sama setelah surat peringatan ke-1 diberikan dan atau melakukan pelanggaran tata tertib/merusak citra dan nama baik BNI.
        </div>
      </div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-1">9.6. </div>
        <div class="col-10">
          Surat peringatan ke-3 dan terakhir yang berakibat pemutusan hubungan kerja tanpa ada ganti kerugian dalam bentuk apapun apabila {{ $param['user']->position_name }} masih melakukan pelanggaran apapun setelah surat peringatan ke-2 dikeluarkan.
        </div>
      </div>
      <br>
      <div>
        <p>Mengetahui dan Menyetujui: </p>
        <br><br>
        <p>({{$param['user']->nama_lengkap}}) <br /> Nama Karyawan</p>
      </div>
    </div>
  </div>
</body>

</html>