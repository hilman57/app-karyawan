<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>KODE ETIK PENAGIHAN</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <center>
    <h3><u>KODE ETIK PENAGIHAN</u></h3>
    <div class="mt-3">PERIHAL YANG TIDAK BOLEH DILAKUKAN DALAM MELAKUKAN PENAGIHAN</div>
  </center>
  <div class="container" align="justify">
    <strong>Hal Komunikasi:</strong>
    <div class="row mt-3">
      <div class="col-1">1.</div>
      <div class="col-11">
      	Melakukan hubungan komunikasi menggunakan telepon dan/atau sarana komunikasi lainnya,  selain yang disediakan perusahaan.
      </div>
    </div>
    <div class="row">
      <div class="col-1">2.</div>
      <div class="col-11">
      	Melakukan hubungan komunikasi melalui telepon dan/atau sarana komunikasi lainnya di luar jam kerja yang ditentukan perusahaan.
      </div>
    </div>
    <div class="row">
      <div class="col-1">3.</div>
      <div class="col-11">
      	Dalam melakukan hubungan komunikasi dengan nasabah memperkenalkan diri sebagai staff Bank.
      </div>
    </div>
    <div class="row">
      <div class="col-1">4.</div>
      <div class="col-11">
      	Melakukan hubungan komunikasi melalui telepon dan/atau sarana komunikasi lainnya dalam keadaan mabuk atau tidak sadar.
      </div>
    </div>
    <div class="row">
      <div class="col-1">5.</div>
      <div class="col-11">
      	Melakukan hubungan komunikasi melalui telepon dan/atau sarana komunikasi lainnya dengan ancaman, penekanan yang berlebihan, menggunakan kata-kata kotor, kata-kata binatang, rasis dan kata-kata lain yang dapat menyebabkan harga diri nasabah maupun orang lain (seperti: emergency contact, karyawan, keluarga, dan lain-lain) terganggu.
      </div>
    </div>
    <div class="row">
      <div class="col-1">6.</div>
      <div class="col-11">
      	Melakukan hubungan komunikasi melalui telepon dan/atau sarana komunikasi lainnya dengan emergency contact, tetangga atau orang lain yang berada di lingkungan nasabah secara berulang-ulang untuk mendapatkan informasi mengenai nasabah.
      </div>
    </div>
    <div class="row">
      <div class="col-1">7.</div>
      <div class="col-11">
      	Memberikan informasi mengenai hutang nasabah kepada orang lain selain kepada nasabah tanpa persetujuan tertulis dari nasabah.
      </div>
    </div>
    <div class="row">
      <div class="col-1">8.</div>
      <div class="col-11">
      	Memberikan informasi yang tidak benar mengenai jumlah hutang nasabah.
      </div>
    </div>
    <div class="row">
      <div class="col-1">9.</div>
      <div class="col-11">
      	Melakukan teror dengan cara apapun, misalnya (namun tidak terbatas) melakukan telepon gelap, surat kaleng, mengirimkan pesan singkat (SMS) gelap dan lain sebagainya.
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-1">10.</div>
      <div class="col-11">
      	Melakukan penagihan atau menghubungi nasabah setelah berakhirnya masa penanganan.
      </div>
    </div>
    <strong>Hal perundingan:</strong>
    <div class="row mt-3">
      <div class="col-1">1.</div>
      <div class="col-11">
      	Membuat perjanjian sendiri berkompromi atau berkolusi dengan nasabah tanpa sepengetahuan pihak Bank.
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-1">2.</div>
      <div class="col-11">
      	Memberikan janji tertentu kepada nasabah (seperti, mengubah status pelaporan di Bank Indonesia maupun lembaga lainnya, pengeluaran surat pelunasan, pengeluaran surat perjanjian, pemberian keringanan di luar ketentuan yang diberlakukan oleh Bank, dan lain-lain) tanpa sepengetahuan pihak Bank.
      </div>
    </div>
    <strong>Hal Pembayaran:</strong>
    <div class="row mt-3">
      <div class="col-1">1.</div>
      <div class="col-11">
      	Menerima pembayaran dari nasabah 
      </div>
    </div>
    <div class="row">
      <div class="col-1">2.</div>
      <div class="col-11">
      	Menerima barang dari nasabah sebagai jaminan pembayaran.
      </div>
    </div>
    <div class="row">
      <div class="col-1">3.</div>
      <div class="col-11">
      	Menerima uang atau barang dari nasabah untuk menunda/menghindari penagihan.
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-1">4.</div>
      <div class="col-11">
      	Menerima hadiah dalam bentuk apapun dari nasabah.
      </div>
    </div>
    <strong>Hal lain-lain:</strong>
    <div class="row mt-3">
      <div class="col-1">1.</div>
      <div class="col-11">
      	Mengadakan hubungan dengan media cetak, elektronik dan media apapun, tanpa persetujuan tertulis dari Bank.
      </div>
    </div>
    <div class="row">
      <div class="col-1">2.</div>
      <div class="col-11">
      	Memberikan data nasabah kepada pihak lain.
      </div>
    </div>
    <div class="row">
      <div class="col-1">3.</div>
      <div class="col-11">
      	Tidak memberikan laporan hasil pembicaraan dengan nasabah.
      </div>
    </div>
    <div class="row">
      <div class="col-1">4.</div>
      <div class="col-11">
      	Menyembunyikan secara sengaja maupun tidak, data nasabah yang terbaru.
      </div>
    </div>
    <div class="row">
      <div class="col-1">5.</div>
      <div class="col-11">
      	Mengalihkan.penagihan kepada pihak lain.
      </div>
    </div>
    <div class="row">
      <div class="col-1">6.</div>
      <div class="col-11">
      	Mengajukan keberatan/keluhan terhadap hasil kesepakatan langsung antara nasabah dan HSBC mengenai penyelesaian hutang nasabah yang terjadi karena:
      </div>
    </div>
    <div class="row">
      <div class="col-1"></div>
      <div class="col-1">a.</div>
      <div class="col-10">
      	Pihak Agency meminta bantuan HSBC untuk melakukan perundingan langsung dengan nasabah; <strong>atau</strong>
      </div>
    </div>
    <div class="row">
      <div class="col-1"></div>
      <div class="col-1">b.</div>
      <div class="col-10">
      	Nasabah secara langsung menghubungi HSBC, sementara saat itu tidak dimungkinkan perundingan tiga pihak (nasabah - Agency - HSBC)
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-1">7.</div>
      <div class="col-11">
      	Melakukan hal-hal lain yang bertentangan dengan norma-norma dan/atau hukum yang berlaku.
      </div>
    </div>
    <div class="mb-5">
    Pelanggaran terhadap Kode Etik ini merupakan pelanggaran (”Perjanjian”), terhadap mana HSBC memiliki hak untuk mengambil tindakan-tindakan yang diatur dalam Perjanjian maupun peraturan perundang-undangan yang berlaku.
    </div>
    <div>
      Jakarta, {{ $param['date_pkwt'] }} <br/>
      Telah mengetahui & menyetujui,
    </div>
    <br/><br/><br/><br/>
    <div>{{ $param['user']->nama_lengkap }}</div>
  </div>
</body>

</html>