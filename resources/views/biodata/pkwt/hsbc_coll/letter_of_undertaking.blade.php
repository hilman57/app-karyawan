<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>LETTER OF UNDERTAKING</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
    .row::after {
      content: "";
      clear: both;
      display: table;
    }

    [class*="col-"] {
      float: left;
      /* padding: 5px; */
      margin-bottom: 5px;
    }

    .col-1 {
      width: 3.33%;
    }

    .col-2 {
      width: 16.66%;
    }

    .col-3 {
      width: 25%;
    }

    .col-4 {
      width: 33.33%;
    }

    .col-5 {
      width: 41.66%;
    }

    .col-6 {
      width: 50%;
    }

    .col-7 {
      width: 58.33%;
    }

    .col-8 {
      width: 66.66%;
    }

    .col-9 {
      width: 75%;
    }

    .col-10 {
      width: 83.33%;
    }

    .col-11 {
      width: 91.66%;
    }

    .col-12 {
      width: 100%;
    }

    .box {
      border: 1px black solid;
      padding: 5px;
    }

    body {
      font-size: 11px;
    }
  </style>
</head>

<body>
  <div class="container" align="justify">
    <strong><u>Letter of Undertaking of Affiliated Person Concerning Access to HSBC IT Systems (“Undertaking”)</u></strong>
    <div class="row">
      <div class="col-3">Name</div>
      <div class="col-9">: <strong>{{ $param['user']->nama_lengkap }}</strong></div>
    </div>
    <div class="row">
      <div class="col-3">Personal ID Number (KTP)</div>
      <div class="col-9">: <strong>{{ $param['user']->ktp }}</strong></div>
    </div>
    <p>Access to HSBC IT Systems is assigned by HSBC Group (“HSBC”) to the Affiliated Person on the following terms and conditions :</p>
    <b>Definitions</b>
    <div class="row">
      <div class="col-2">HSBC IT Systems</div>
      <div class="col-1">:</div>
      <div class="col-9">
        Various information technology systems along with associated computers and communication systems for which HSBC has management responsibility. These include, but not limited to, HUB, HFE, HCC, TREATS, CARM, Lotus Notes, GHSS, and any other systems HSBC may have management responsibility of in the future, to which access may be granted by HSBC to the Affiliated Person as and when HSBC deems required.
      </div>
    </div>
    <div class="row">
      <div class="col-2">Transaction</div>
      <div class="col-1">:</div>
      <div class="col-9">
        Any request, instruction, input, maintenance or query to HSBC IT Systems and any associated information that is presented (including but not limited to presentation in soft copy and/or hard copy formats) from each of such request, instruction, input, maintenance or query to effect a financial or non financial addition, change, deletion or enquiry on any record in HSBC IT Systems.
      </div>
    </div>
    <div class="row">
      <div class="col-2">Password</div>
      <div class="col-1">:</div>
      <div class="col-9">
        Any confidential password, code or number issued to the Affiliated Person by HSBC and any password, code or number adopted by the Affiliated Person which may be used for accessing to HSBC IT Systems.
      </div>
    </div>
    <div class="row">
      <div class="col-2">Standing Instruction</div>
      <div class="col-1">:</div>
      <div class="col-9">
        Any documents governing the Code of Conduct, Declaration of Secrecy and/or terms and conditions of service engagement between HSBC and the employer of the Affiliated Person.
      </div>
    </div>
    <b>Terms and Conditions</b>
    <div class="row">
      <div class="col-1">1.</div>
      <div class="col-11">
        The Affiliated Person shall be responsible for all Transactions carried out using his/her Password.
      </div>
    </div>
    <div class="row">
      <div class="col-1">2.</div>
      <div class="col-11">
        The Affiliated Person undertakes to keep all Password(s) strictly private and confidential at all times. The Affiliated
        Person agrees to be responsible for all Transactions effected through HSBC IT Systems by use of such
        Password(s) provided that the Affiliated Person shall not be responsible for any Transaction effected by use of
        such Password(s) after HSBC has received written notice from the Affiliated Person informing HSBC of the
        Affiliated Person’s unauthorised or inadvertent disclosure or compromise of the Password(s).
      </div>
    </div>
    <div class="row">
      <div class="col-1">3.</div>
      <div class="col-11">
        This Undertaking is in addition to, and not in substitution for, any other approved IT security instructions and
        standards including but not limited to Group Standard Manual (GSM), HSBC IT Functional Instruction Manual (IT
        FIM), HSBC Group IT Security Standard, Second Level IT Security Standard as may be revised, amended, and
        updated from time to time by HSBC and any other agreement(s) between the Affiliated Person and HSBC.
      </div>
    </div>
    <div class="row">
      <div class="col-1">4.</div>
      <div class="col-11">
        This Undertaking shall be effective on the date of its signing by the Affiliated Person.
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-1">5.</div>
      <div class="col-11">
        This Undertaking shall be governed by and construed in accordance with HSBC’s current Standing Instructions to
        the Affiliated Person.
      </div>
    </div>
    <p>I hereby state that I have read and understood the content of this Undertaking (as amended from time to time by HSBC) and therefore agree to be bound by the provisions therein.</p>
    <br/><br/><br/><br/>
    <div class="row">
      <div class="col-8">
        Affiliated Person	: {{ $param['user']->nama_lengkap }} <br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        (Signature)
      </div>
      <div class="col-4">
        {{ $param['date_pkwt'] }} <br/>
        (Date)
      </div>
    </div>
  </div>
</body>

</html>