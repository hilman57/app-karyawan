@extends('layouts.master')

@section('title')
<title>Biodata Karyawan</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Biodata</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Biodata</a></li>
                                {{-- <li class="breadcrumb-item active">List</li> --}}
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active mb-2" data-bs-toggle="tab" href="#tab-active-user" id="showActive" role="tab">
                                        Biodata
                                    </a>
                                </li>
                            </ul>
                            <div class="row mt-3 mb-2">
                                <div class="col-sm-6 d-flex">
                                    <!-- <div class="search-box me-2 mb-2 d-inline-block">
                                        <div class="position-relative">
                                            <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                            <i class="bx bx-search-alt search-icon"></i>
                                        </div>
                                    </div>
                                    <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button> -->
                                    <select name="site" id="site" class="form-control" required onchange="pickSite(this.value)">
                                        <option value="">--Choose Site --</option>
                                        @foreach($site as $row)
                                        <option value="{{ $row->id }}">{{ $row->site_name }}</option>
                                        @endforeach
                                    </select> &nbsp;
                                    <select name="jabatan" id="jabatan" class="form-control" required>
                                        <option value="">--Choose Jabatan --</option>
                                    </select> &nbsp;
                                    <select name="status_kry" id="status_kry" class="form-control" required>
                                        <option value="">--Choose Status --</option>
                                        @foreach($status as $row)
                                        <option value="{{ $row->id }}">{{ $row->status_ky }}</option>
                                        @endforeach
                                    </select> &nbsp;
                                    <button class="btn btn-primary" onclick="beginSearch()">Search</button>
                                </div>
                            </div>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-active-user" role="tabpanel">
                                    <table id="table-active" class="table table-bordered dt-responsive nowrap w-100 tabcontent">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Jabatan</th>
                                                <th>NIK</th>
                                                <th>Site</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="data-biodata"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal hide" id="modalCreateBerkas" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Create Berkas</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <form id="formShowDetail">
                                            <div class="modal-body">
                                                <input class="form-control " type="hidden" name="id" id="idPengalaman">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <label class="form-label">Type</label>
                                                        <select name="type_pkwt" id="type_pkwt" class="form-control" required onchange="setTypePkwt()">
                                                            <option value="">--Choose --</option>
                                                            <option value="3">Perjanjian Kerja Waktu Tertentu</option>
                                                            <option value="1">Kerahasiaan Karyawan</option>
                                                            <option value="2">Ketentuan Operasional Kerja</option>
                                                            <option value="4">Addendium Pertama (Khusus Aseanindo)</option>
                                                            <option value="5">Lampiran B (Khusus Aseanindo)</option>
                                                            <option value="6">Lampiran C (Khusus Aseanindo)</option>
                                                            <option value="7">Surat Lampiran (Khusus Aseanindo)</option>
                                                            <option value="8">Kode Etik Penagihan (Khusus HSBC COLL)</option>
                                                            <option value="9">Letter Of Undertaking (Khusus HSBC COLL)</option>
                                                        </select>
                                                        <p class="text-danger">{{ $errors->first('type_pkwt') }}</p>
                                                    </div>
                                                </div>
                                                <div class="row" id="wrap-tanggal" style="display: none;">
                                                    <div class="col-12">
                                                        <label class="form-label">Tanggal PKWT</label>
                                                        <input class="form-control {{ $errors->has('tgl_pkwt') ? 'is-invalid':'' }}" type="date" name="tgl_pkwt" id="tgl_pkwt" onchange="setTanggalPKWT()">
                                                        <p class="text-danger">{{ $errors->first('tgl_pkwt') }}</p>
                                                    </div>
                                                </div>
                                                <div class="row" id="wrap-last-contract" style="display: none;">
                                                    <div class="col-12">
                                                        <label class="form-label">Last Contract</label>
                                                        <input class="form-control {{ $errors->has('last_contract') ? 'is-invalid':'' }}" type="date" name="last_contract" id="last_contract" onchange="setLastContract()">
                                                        <p class="text-danger">{{ $errors->first('last_contract') }}</p>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" class="btn btn-primary waves-effect waves-light" id="btnSubmitFinalcetak" target="_blank">CETAK PDF</a>
                                                    <button type="button" class="btn btn-danger" onclick="closeModal()">Close</button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </form>
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
    <!-- end main content-->
    @endsection
    @section('scripts')
    @parent
    <script src="{{ URL::asset('backend/assets/js/pages/biodata/index.js') }}"></script>
    @endsection