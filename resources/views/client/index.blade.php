@extends('layouts.master')
@section('title')
<title>Data Client</title>
@endsection
@section('content')
<div class="main-content">
  <div class="page-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button fw-medium collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                      Form
                    </button>
                  </h2>
                  <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body text-muted">
                      <div class="row">
                        <div class="col-lg-12">
                          <form id="formInput">
                            <div class="row">
                              <div class="col-lg-6">
                                <label class="form-label">Client</label>
                                <input class="form-control {{ $errors->has('client') ? 'is-invalid':'' }}" type="text" name="client" id="client" required>
                                <p class="text-danger">{{ $errors->first('client') }}</p>
                              </div>
                              <div class="col-lg-6">
                                <label class="form-label">Client Name</label>
                                <input class="form-control {{ $errors->has('clientname') ? 'is-invalid':'' }}" type="text" name="clientname" id="clientname" required>
                                <p class="text-danger">{{ $errors->first('clientname') }}</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-6">
                                <label class="form-label">No Kontrak</label>
                                <input class="form-control {{ $errors->has('no_kontrak') ? 'is-invalid':'' }}" type="text" name="no_kontrak" id="no_kontrak" required>
                                <p class="text-danger">{{ $errors->first('no_kontrak') }}</p>
                              </div>
                              <div class="col-lg-6">
                                <label class="form-label">Last PKWT Number</label>
                                <input class="form-control {{ $errors->has('last_number_pkwt') ? 'is-invalid':'' }}" type="text" name="last_number_pkwt" id="last_number_pkwt" required>
                                <p class="text-danger">{{ $errors->first('last_number_pkwt') }}</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-6">
                                <label class="form-label">Valid From</label>
                                <input class="form-control {{ $errors->has('validfrom') ? 'is-invalid':'' }}" type="date" name="validfrom" id="validfrom" required>
                                <p class="text-danger">{{ $errors->first('validfrom') }}</p>
                              </div>
                              <div class="col-lg-6">
                                <label class="form-label">Valid Until</label>
                                <input class="form-control {{ $errors->has('validuntil') ? 'is-invalid':'' }}" type="date" name="validuntil" id="validuntil" required>
                                <p class="text-danger">{{ $errors->first('validuntil') }}</p>
                              </div>
                            </div>
                            <div>
                              <button type="button" class="btn btn-primary" id="btnSubmit">Submit</button>
                              <button type="reset" class="btn btn-light">Cancel</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingTwo">
                    <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="true" aria-controls="flush-collapseTwo">
                      Data Client
                    </button>
                  </h2>
                  <div id="flush-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body text-muted">
                      <div class="row">
                        <div class="col-sm-8">
                          <div class="search-box me-2 mb-2 d-inline-block">
                            <div class="position-relative">
                              <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                              <i class="bx bx-search-alt search-icon"></i>
                            </div>
                          </div>
                          <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                        </div>
                        <div class="col-sm-4">
                          <div class="d-flex flex-row-reverse mb-2">
                            <button type="button" class="btn btn-primary" id="btnAdd">Add New</button>
                          </div>
                        </div>
                      </div>
                      <div class="row" style="padding-top: 10px;" id="notif"></div>
                      <table id="table-active" class="table table-bordered dt-responsive nowrap w-100 tabcontent">
                        <thead>
                          <tr>
                            <th>Client</th>
                            <th>Client Name</th>
                            <th>Contract</th>
                            <th>Periode</th>
                            <th>PKWT</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- end col -->
      </div> <!-- end row -->
    </div> <!-- container-fluid -->
  </div>
  <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/client-master/index.js') }}"></script>
@endsection