@extends('layouts.master')

@section('title')
<title>Data Verification</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

   <div class="page-content">
      <div class="container-fluid">


         <!-- start page title -->
         <div class="row">
               <div class="col-12">
                  <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                     <h4 class="mb-sm-0 font-size-18">List Verification Business Account</h4>

                     <div class="page-title-right">
                           <ol class="breadcrumb m-0">
                              <li class="breadcrumb-item"><a href="javascript: void(0);">Business Account</a></li>
                              <li class="breadcrumb-item active">List Verification</li>
                           </ol>
                     </div>

                  </div>
               </div>
         </div>
         <!-- end page title -->

         <div class="row">
               <div class="col-lg-12">
                  <div class="card">
                     <div class="card-body">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active mb-2" data-bs-toggle="tab" href="#tab-new" id="showNew" role="tab">
                                       New
                                 </a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link mb-2" data-bs-toggle="tab" href="#tab-rejected" id="showRejected" role="tab">
                                       Rejected
                                 </a>
                              </li>
                           </ul>

                           <div class="row mt-3">
                              <div class="col-sm-4">
                                 <div class="search-box me-2 mb-2 d-inline-block">
                                       <div class="position-relative">
                                          <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                          <i class="bx bx-search-alt search-icon"></i>
                                       </div>
                                 </div>
                                 <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                              </div>
                           </div>

                           <!-- Tab panes -->
                           <div class="tab-content">

                              <div class="tab-pane active" id="tab-new" role="tabpanel">
                                 <table id="table-new" class="table table-bordered dt-responsive nowrap w-100 tabcontent">
                                       <thead>
                                          <tr>
                                             <th>Date</th>
                                             <th>Company Name</th>
                                             <th>Address</th>
                                             <th>PIC Name</th>
                                             <th>Note</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>

                                       <tbody>
                                       </tbody>
                                 </table>
                              </div>

                              <div class="tab-pane" id="tab-rejected" role="tabpanel">
                                 <table id="table-rejected" class="table table-bordered dt-responsive nowrap w-100 tabcontent">
                                       <thead>
                                          <tr>
                                             <th>Date</th>
                                             <th>Company Name</th>
                                             <th>Address</th>
                                             <th>PIC Name</th>
                                             <th>Note</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>

                                       <tbody>
                                       </tbody>
                                 </table>
                              </div>

                           </div>
                     </div>
                  </div>
               </div>
         </div>
         <!-- end row -->

      </div> <!-- container-fluid -->
   </div>
   <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/business_account/verification/index.js') }}"></script>
@endsection