@extends('layouts.master')

@section('title')
    <title>Edit Package</title>
@endsection

@section('content')
<!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Edit Package</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Package</a></li>
                                        <li class="breadcrumb-item"><a href="{{ route('additional-in-app-call.index') }}">Additional In-App Call</a></li>
                                        <li class="breadcrumb-item active">Edit</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                            <form id="formEdit">
                            <input type="hidden" id="routeSaveData" value="{{ route('additional-in-app-call.update', $data->uuid) }}">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" id="idEdit" value="{{ $data->uuid }}">
                            <input type="hidden" id="countDetail" value="{{ $countDetail }}">
                            <input type="hidden" id="flagAddDetail" value="0">
                            <div class="card">
                                <div class="card-body">
                                    <div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div>
                                                <label class="form-label">Name</label>
                                                <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="name" value="{{ $data->name }}" required>
                                                <p class="text-danger">{{ $errors->first('name') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div>
                                                    <label class="form-label">Price</label>
                                                    <input class="form-control {{ $errors->has('price') ? 'is-invalid':'' }}" type="text" name="price" id="price" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ $data->price }}" required>
                                                    <p class="text-danger">{{ $errors->first('price') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="list_detail">
                                            @if(!empty($details))
                                                @foreach ($details as $key => $detail)
                                                <div class="inner-repeater mb-3" data-index-detail="{{ $key }}">
                                                        <div data-repeater-list="inner-group" class="inner mb-3">
                                                            <label for="input_description_{{ $key }}" class="form-label">List Description</label>
                                                            <div data-repeater-item class="inner mb-3 row">
                                                                <div class="col-md-8 col-8 d-flex h-100 align-items-center">
                                                                        <div style="border-radius: 100px; height: 25px; width: 25px; text-align: center; display: flex; justify-content: center; align-items: center; background-color: #F4AE00; color: #fff;" class="me-2">
                                                                            <i class="fas fa-check"></i>
                                                                        </div>
                                                                    <input type="text" name="details[{{ $key }}]" id="input_details_{{ $key }}" class="inner form-control" value="{{ $detail }}"/>
                                                                </div>
                                                                @if($countDetail > 1)
                                                                <div class="col-md-1 col-4">
                                                                    <div class="d-grid">
                                                                        <button type="button" data-type="btn-remove" data-index-detail="{{ $key }}" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="inner-repeater mb-3" data-index-detail="0">
                                                        <div data-repeater-list="inner-group" class="inner mb-3">
                                                            <label for="input_description_0" class="form-label">List Description</label>
                                                            <div data-repeater-item class="inner mb-3 row">
                                                                <div class="col-md-8 col-8 d-flex h-100 align-items-center">
                                                                        <div style="border-radius: 100px; height: 25px; width: 25px; text-align: center; display: flex; justify-content: center; align-items: center; background-color: #F4AE00; color: #fff;" class="me-2">
                                                                            <i class="fas fa-check"></i>
                                                                        </div>
                                                                    <input type="text" name="details[0]" id="input_details_0" class="inner form-control" placeholder="Enter here..."/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                @endif
                                        </div>
                                        <button type="button" id="button_add_details" class="btn btn-primary mb-3"><i class="fa fa-plus"></i> Description</button>
                                        <div>
                                            <button type="button" class="btn btn-primary" id="btnSubmit">Submit</button>
                                            <a href="{{ route('additional-in-app-call.index') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container-fluid -->
            </div>
        <!-- End Page-content -->
        </div>
        <!-- end main content-->
<!-- /.control-sidebar -->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/package/additional-in-app-call/edit.js') }}"></script>
@endsection
