@extends('layouts.master')

@section('title')
<title>Data Transaction Fee</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">List Fee</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Master</a></li>
                                <li class="breadcrumb-item active">List Fee</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="d-flex">
                                <div class="col-lg-4 p-1 pe-3">
                                    <div style="background-color: #F8F8FB;" class="card h-100">
                                        <div class="card-body ">
                                            <h5 class="mb-sm-3 font-size-18 ">Create Fee</h5>
                                            <form id="formInput">
                                                <input type="hidden" id="routeGetDataFee" value="{{ route('fee.get-data-fee') }}">
                                                <input type="hidden" id="routeSaveData" value="{{ route('fee.save') }}">
                                                <div class="col">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div>
                                                                <label class="form-label">Name</label>
                                                                <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" id="namaFee" required>
                                                                <p class="text-danger">{{ $errors->first('name') }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <h5 class="font-size-13 mb-2">Is Percent</h5>
                                                            <div>
                                                                <input type="checkbox" class="isPercent" id="switch5" switch="warning" name="is_percent"/>
                                                                <label for="switch5" data-on-label="Yes" data-off-label="No"></label>
                                                                <p class="text-danger">{{ $errors->first('is_percent') }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div>
                                                                <label class="form-label">Value</label>
                                                                <input class="form-control {{ $errors->has('price') ? 'is-invalid':'' }}" type="text" name="price" id="nilaiFee" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                                                                <p class="text-danger">{{ $errors->first('price') }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-primary" id="btnSave">Submit</button>
                                                    <button type="button" class="btn btn-light" id="btnClear">Clear</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col">

                                    <div class="row">
                                        <div class="d-flex justify-content-between align-items-center mb-2">
                                            <div class="col-lg-4 d-flex  align-items-center">
                                                <h5 class="card-title mb-0">List Data Fee</h5>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <div class="search-box me-2  d-inline-block">
                                                    <div class="position-relative">
                                                        <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                                        <i class="bx bx-search-alt search-icon"></i>
                                                    </div>
                                                </div>
                                                <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 10px;" id="notif">
                                    </div>
                                    <table id="table-fee" class="table table-bordered dt-responsive nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Value</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/fee/index.js') }}"></script>
@endsection