@extends('layouts.master')

@section('title')
    <title>Edit FAQ</title>
@endsection

@section('content')
<!-- ============================================================== -->
          <!-- Start right Content here -->
          <!-- ============================================================== -->
          <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Edit FAQ</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Master</a></li>
                                        <li class="breadcrumb-item"><a href="{{ route('faq.index') }}">FAQ</a></li>
                                        <li class="breadcrumb-item active">Edit FAQ</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('faq.update', $data->uuid) }}" method="post">
                            @csrf
                                <input type="hidden" name="_method" value="PUT">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div>
                                                        <label class="form-label">Pertanyaan</label>
                                                        <textarea class="form-control {{ $errors->has('question') ? 'is-invalid' : '' }}" name="question" rows="3">{{ $data->question }}</textarea>
                                                        <p class="text-danger">{{ $errors->first('question') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div>
                                                        <label class="form-label">Jawaban</label>
                                                        <textarea class="form-control {{ $errors->has('answer') ? 'is-invalid' : '' }}" name="answer" rows="3">{{ $data->answer }}</textarea>
                                                        <p class="text-danger">{{ $errors->first('answer') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ route('faq.index') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end row -->

                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
          </div>
          <!-- end main content-->
<!-- /.control-sidebar -->
@endsection
