@extends('layouts.master')

@section('title')
    <title>Privacy Policy</title>
@endsection

@section('content')
<!-- ============================================================== -->
          <!-- Start right Content here -->
          <!-- ============================================================== -->
          <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Privacy Policy</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Master</a></li>
                                        <li class="breadcrumb-item"><a href="{{ route('privacy-policy.index') }}">Privacy Policy</a></li>
                                        <li class="breadcrumb-item active">Input</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                            @if(empty($data->uuid))
                            <form action="{{ route('privacy-policy.store') }}" method="post">
                                @csrf
                            @else
                            <form action="{{ route('privacy-policy.update', $data->uuid) }}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                                 <div class="card">
                                    <div class="card-body">
                                       <div>
                                          <div class="row">
                                             <div class="col-12">
                                                 @include ('partials.messages')
                                                   <div class="card">
                                                      <textarea id="elm1" name="text">{{ empty($data->text) ? '' : $data->text }}</textarea>
                                                   </div>
                                             </div>
                                          </div>
                                          <button type="submit" class="btn btn-primary">Submit</button>
                                          <a href="{{ route('home') }}" class="btn btn-light">Cancel</a>
                                       </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end row -->

                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
          </div>
          <!-- end main content-->
<!-- /.control-sidebar -->
@endsection