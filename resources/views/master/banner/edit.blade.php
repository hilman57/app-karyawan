@extends('layouts.master')

@section('title')
    <title>Edit Banner</title>
@endsection

@section('content')
<!-- ============================================================== -->
          <!-- Start right Content here -->
          <!-- ============================================================== -->
          <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Edit Banner</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Master</a></li>
                                        <li class="breadcrumb-item"><a href="{{ route('banner.index') }}">Banner</a></li>
                                        <li class="breadcrumb-item active">Edit Banner</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                          <form action="{{ route('banner.update', $data->uuid) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="card">
                                <div class="card-body">
                                    <div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div>
                                                    <label class="form-label">Name</label>
                                                    <input class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" type="text" name="name" value="{{ $data->name }}" required>
                                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div>
                                                    <label for="formFile" class="form-label">Image</label>
                                                    <input class="form-control {{ $errors->has('image') ? 'is-invalid':'' }}"" type="file" name="image" id="formFile">
                                                    <p class="text-danger">{{ $errors->first('image') }}</p>
                                                      @if (!empty($data->image))
                                                        <img src="{{ asset('storage/image/banner/' . $data->image) }}"
                                                            alt="{{ $data->name }}"
                                                            width="150px" height="150px"><p></p>
                                                      @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-lg-8">
                                              <div>
                                                  <label class="form-label">Order Number</label>
                                                  <input class="form-control {{ $errors->has('no_order') ? 'is-invalid':'' }}" type="number" name="no_order" value="{{ $data->no_order }}" required>
                                                  <p class="text-danger">{{ $errors->first('no_order') }}</p>
                                              </div>
                                          </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a href="{{ route('banner.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </div>
                            </div>
                          </form>
                        </div>
                    </div>
                    <!-- end row -->

                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
          </div>
          <!-- end main content-->
<!-- /.control-sidebar -->
@endsection
