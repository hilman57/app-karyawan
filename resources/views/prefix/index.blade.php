@extends('layouts.master')

@section('title')
<title>Data Menu</title>
@endsection

@section('content')
<!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18">List Menu</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item active">List Menu</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                <div class="accordion accordion-flush" id="accordionFlushExample">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingOne">
                                            <button class="accordion-button fw-medium collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                            Form
                                            </button>
                                        </h2>
                                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                            <div class="accordion-body text-muted">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <form id="formInput">
                                                        {{-- <input type="hidden" id="routeGetData" value="{{ route('site.edit') }}"> --}}
                                                        <input type="hidden" id="routeSaveData" value="{{ route('menu.save') }}">
                                                        <input type="hidden" id="idEdit">
                                                        <div class="row">
                                                            <div class="col-lg-8">
                                                                <div>
                                                                    <label class="form-label">Site</label>
                                                                    <select name="site_id" id="site_id" required class="form-select {{ $errors->has('site_id') ? 'is-invalid':'' }}">
                                                                        <option value="">Choose</option>
                                                                        @foreach ($site as $row)
                                                                                <option value="{{ $row->id }}">{{ ucfirst($row->site) }}</option>
                                                                        @endforeach
                                                                    </select>

                                                                    {{-- <input class="form-control {{ $errors->has('site_id') ? 'is-invalid':'' }}" type="text" name="site_id" id="site_id" required> --}}
                                                                    <p class="text-danger">{{ $errors->first('site_id') }}</p>
                                                                </div>
                                                                <div>
                                                                    <label class="form-label">Jabatan</label>
                                                                    <select name="jabatan_id" id="jabatan_id" required class="form-select {{ $errors->has('jabatan_id') ? 'is-invalid':'' }}">
                                                                        <option value="">Choose</option>
                                                                        @foreach ($jabatan as $row)
                                                                                <option value="{{ $row->id }}">{{ ucfirst($row->jabatan) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <p class="text-danger">{{ $errors->first('jabatan_id') }}</p>
                                                                </div>
                                                                <div>
                                                                    <label class="form-label">Start From</label>
                                                                    <input class="form-control {{ $errors->has('start_from') ? 'is-invalid':'' }}" type="text" name="start_from" id="start_from" required>
                                                                    <p class="text-danger">{{ $errors->first('start_from') }}</p>
                                                                </div>
                                                                <div>
                                                                    <label class="form-label">Last</label>
                                                                    <input class="form-control {{ $errors->has('last') ? 'is-invalid':'' }}" type="text" name="last" id="last" required>
                                                                    <p class="text-danger">{{ $errors->first('last') }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <button type="button" class="btn btn-primary" id="btnSubmit">Submit</button>
                                                        <button type="button" class="btn btn-light" id="btnClear">Cancel</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingTwo">
                                            <button class="accordion-button fw-medium" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="true" aria-controls="flush-collapseTwo">
                                            List Data
                                            </button>
                                        </h2>
                                        <div id="flush-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                            <div class="accordion-body text-muted">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="search-box me-2 mb-2 d-inline-block">
                                                            <div class="position-relative">
                                                                <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                                                <i class="bx bx-search-alt search-icon"></i>
                                                            </div>
                                                        </div>
                                                        <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="d-flex flex-row-reverse mb-2">
                                                        <button type="button" class="btn btn-primary" id="btnAdd">Add New</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="padding-top: 10px;" id="notif">
                                                </div>
                                                <table id="table-menu" class="table table-bordered dt-responsive nowrap w-100">
                                                    <thead>
                                                        <tr>
                                                        <th>Site</th>                                                       
                                                        <th>Jabatan</th>
                                                        <th>Start From</th>
                                                        <th>Last</th>
                                                        <th>Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
    <!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/datatables/prefix/index.js') }}"></script>
@endsection
