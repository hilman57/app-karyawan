@extends('layouts.master')

@section('title')
<title>Form Cleansing</title>
@endsection

@section('content')
<div class="main-content">
  <div class="page-content">
        <h4>Cleansing data</h4>
        <p>Tekan tombol untuk menghapus data</p>
        <button type="submit" class="btn btn-primary" onclick='UpdateStatus()'  >Submit</button>
        {{-- <input type='button' onclick='UpdateStatus()' value='Status Update'> --}}
  </div>
</div>
@endsection
@section('scripts')
<script src="{{ URL::asset('backend/assets/js/pages/cleansing/index.js') }}"></script>
@endsection
