@extends('layouts.master')

@section('title')
<title>Billing Yellow to Business Account</title>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Billing Yellow to Business Account</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Billing</a></li>
                                <li class="breadcrumb-item active">List Billing</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">
                            <div class="row">
                                <div class="d-flex justify-content-between mb-2">
                                    <div class="col">
                                        <div class="search-box me-2  d-inline-block">
                                            <div class="position-relative">
                                                <input style="background-color: #eff2f7; border: none;" type="text" id="myInputTextField" class="form-control" placeholder="Search...">
                                                <i class="bx bx-search-alt search-icon"></i>
                                            </div>
                                        </div>
                                        <button id="btnSearch" type="button" style="background-color: #B28BFF; border:none" class="btn btn-primary waves-effect waves-light">Find</button>
                                    </div>
                                    <div class="d-flex flex-row align-items-center">
                                        <div style="margin-right: 10px; width: 100%;" class="d-flex ">
                                            <a href="{{ route('billing.package.create') }}" class="btn btn-default " style="background-color: #eff2f7; border:none"><i class="fa fa-download"></i> Download</a>
                                        </div>
                                        <div class="input-daterange input-group" id="datepicker6" data-date-format="dd M, yyyy" data-date-autoclose="true" data-provide="datepicker" data-date-container='#datepicker6'>
                                            <div class="date-range-customes">
                                                <input type="text" class="" name="start" placeholder="Start Date" /><span>-</span>
                                                <input type="text" class="" name="end" placeholder="End Date" />
                                                <i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <!-- <div class="col-sm-3">
                                    <div class="d-flex flex-row-reverse mb-2">
                                        <a href="{{ route('billing.package.create') }}" class="btn btn-default" style="background-color: #eff2f7; border:none"><i class="fa fa-download"></i> Download</a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-daterange input-group" id="datepicker6" data-date-format="dd M, yyyy" data-date-autoclose="true" data-provide="datepicker" data-date-container='#datepicker6'>
                                        <div class="date-range-customes">
                                            <input type="text" class="" name="start" placeholder="Start Date" /><span>-</span>
                                            <input type="text" class="" name="end" placeholder="End Date" />
                                            <i class="fas fa-calendar-alt"></i>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            @include ('partials.messages')
                            <table id="table-billing" class="table table-bordered dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th style="max-width: 10px;"><span class="form-check form-check-warning"><input class="form-check-input" type="checkbox" id="formCheckcolor4"></span></th>
                                        <th>Date</th>
                                        <th>Invoice No</th>
                                        <th>Company Name</th>
                                        <th>PIC Name</th>
                                        <th>Payment Method</th>
                                        <th>Invoice</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
</div>
<!-- end main content-->
@endsection
@section('scripts')
@parent
<script src="{{ URL::asset('backend/assets/js/pages/billing/package/index.js') }}"></script>
@endsection