<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'category' => 1,
            'name' => 'owner',
            'created_at' => now(),
            'created_by' => 'system'
        ]);

        Role::create([
            'category' => 1,
            'name' => 'superadmin',
            'created_at' => now(),
            'created_by' => 'system'
        ]);
        
        Role::create([
            'category' => 2,
            'name' => 'ba',
            'created_at' => now(),
            'created_by' => 'system'
        ]);
        
        Role::create([
            'category' => 2,
            'name' => 'spv',
            'created_at' => now(),
            'created_by' => 'system'
        ]);
        
        Role::create([
            'category' => 2,
            'name' => 'agent',
            'created_at' => now(),
            'created_by' => 'system'
        ]);
        
        Role::create([
            'category' => 2,
            'name' => 'customer',
            'created_at' => now(),
            'created_by' => 'system'
        ]);
        
        Role::create([
            'category' => 1,
            'name' => 'marketing',
            'created_at' => now(),
            'created_by' => 'system'
        ]);
        
        Role::create([
            'category' => 1,
            'name' => 'finance',
            'created_at' => now(),
            'created_by' => 'system'
        ]);
    }
}
