<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\UserHasRole;
use DB;
use Ramsey\Uuid\Uuid as Generator;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', 'superadmin')->first();

        $user = User::create([
            'name' => 'administrator',
            'email' => 'admin@yelow.com',
            'password' => bcrypt('12345678'),
            'phone' => '+6282111662745',
            'is_superadmin' => 1,
            'status' => 1,
            'is_hide' => 0,
            'created_at' => now(),
            'created_by' => 'system'
        ]);

        UserHasRole::create([
            'user_uuid' => $user->uuid,
            'role_uuid' => $role->uuid
        ]);
    }
}
