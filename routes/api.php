<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\JWTController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function() {
    echo 'tes';
});

Route::post('/auth/login', '\App\Http\Controllers\Auth\LoginController@apilogin');
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('/auth/login', '\App\Http\Controllers\Auth\LoginController@apilogin');
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('/store', [BiodataController::class, 'store']);
    Route::post('/biodata/proses_upload', [BiodataController::class, 'proses_upload']);
    Route::get('/biodata/site', 'BiodataController@getSite');
    Route::get('/biodata/position/{site}', 'BiodataController@getPosition');
});
Route::post('/biodata/save', [BiodataController::class, 'save']);
Route::get('/biodata', [BiodataController::class, 'index']);
Route::get('/biodata/tampildata/{uuid}', 'BiodataController@tampildata');

