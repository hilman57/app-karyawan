<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/permission-error', 'HomeController@permissionError')->name('permission-error');

Route::group(['middleware' => ['cekRole:superadmin|finance|spv']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});



Route::group(['prefix' => 'utilitas', 'middleware' => ['cekRole:superadmin|admin|owner']], function () {
    // User

    Route::get('/user', 'Utilitas\UserController@index')->name('user.index');
    Route::post('/user/save', 'Utilitas\UserController@save')->name('user.save');
    Route::post('/user/update', 'Utilitas\UserController@update')->name('user.update');
    Route::get('/user/edit/{id}', 'Utilitas\UserController@edit')->name('user.edit');
    Route::get('/user/delete', 'Utilitas\UserController@delete')->name('user.delete');
    Route::post('/user/update-role', 'Utilitas\UserController@updateRole')->name('user.update-role');
    Route::post('/user/get-data', 'Utilitas\UserController@getData')->name('user.get-data');
    Route::get('/user/roles/{id}', 'Utilitas\UserController@roles')->name('user.roles');
    Route::put('/user/roles/{id}', 'Utilitas\UserController@setRole')->name('user.set_role');
    Route::post('/user/permission', 'Utilitas\UserController@addPermission')->name('user.add_permission');
    Route::get('/user/role-permission', 'Utilitas\UserController@rolePermission')->name('user.roles_permission');
    Route::put('/user/permission/{role}', 'Utilitas\UserController@setRolePermission')->name('user.setRolePermission');
    Route::post('/user/cek-email', 'Utilitas\UserController@cekEmail')->name('user.cek-email');
    Route::post('/user/cek-password', 'Utilitas\UserController@cekPassword')->name('user.cek-password');

    // Role
    Route::get('/role', 'Utilitas\RoleController@index')->name('role.index');
    Route::post('/role/save', 'Utilitas\RoleController@save')->name('role.save');
    Route::get('/role/delete', 'Utilitas\RoleController@delete')->name('role.delete');
    Route::post('/role/get-data', 'Utilitas\RoleController@getData')->name('role.get-data');

    //Menu
    Route::get('/menu', 'Utilitas\MenuController@index')->name('menu.index');
    Route::post('/menu/save', 'Utilitas\MenuController@save')->name('menu.save');
    Route::post('/menu/update', 'Utilitas\MenuController@update')->name('menu.update');
    Route::get('/menu/delete', 'Utilitas\MenuController@delete')->name('menu.delete');
    Route::post('/menu/get-data', 'Utilitas\MenuController@getDataMenu')->name('menu.get-data');

    //SubMenu
    Route::get('/submenu', 'Utilitas\SubMenuController@index')->name('submenu.index');
    Route::post('/submenu/save', 'Utilitas\SubMenuController@save')->name('submenu.save');
    Route::post('/submenu/update', 'Utilitas\SubMenuController@update')->name('submenu.update');
    Route::get('/submenu/delete', 'Utilitas\SubMenuController@delete')->name('submenu.delete');
    Route::post('/submenu/get-data', 'Utilitas\SubMenuController@getData')->name('submenu.get-data');
});



// test
Route::group(['middleware' => ['cekRole:superadmin|spv']], function () {
    Route::get('/aktifasi', 'Utilitas\UserAdminController@index')->name('user.index');
    Route::get('/aktifasi/active/{id}', 'Utilitas\UserAdminController@index');
    Route::post('/user/get-data2', 'Utilitas\UserAdminController@getData2')->name('user.get-data2');
    Route::POST('/aktifasi/update', 'Utilitas\UserAdminController@update');
    Route::POST('/aktifasi/save', 'Utilitas\UserAdminController@save');
    // Route::get('/user2/delete/{id}', 'Utilitas\UserAdminController@delete');
    Route::get('/aktifasi/delete/{id}', 'Utilitas\UserAdminController@delete');

    Route::POST('/biodata/save/', 'BiodataController@save')->name('biodata.save');
    Route::get('/biodata/create/', 'BiodataController@create')->name('biodata.simpan');
    Route::POST('/biodata/update/', 'BiodataController@update')->name('biodata.update');
    Route::POST('/biodata/updt_pgk/', 'BiodataController@updt_pgk')->name('biodata.updt_pgk');
    Route::POST('/biodata/search_biodata/', 'BiodataController@search_biodata')->name('biodata.search_biodata');

    Route::get('/biodata/active', 'BiodataController@dataActive');
    Route::get('biodata', 'BiodataController@index')->name('biodata.index');
    Route::get('/biodata/detail/{id}', 'BiodataController@detail')->name('biodata.detail');
    Route::get('/biodata/edit/{id}', 'BiodataController@edit')->name('biodata.edit');
    Route::get('/biodata/delete/{id}', 'BiodataController@destroy')->name('biodata.delete');
    Route::get('/biodata/delete_pgk/{id}', 'BiodataController@destroy_pgk')->name('biodata.delete_pgk');
    Route::get('/user/edit/{id}', 'Utilitas\UserController@edit')->name('user.edit');
    Route::get('/biodata/tampildata/{id}', 'BiodataController@listPengalamanKerja')->name('biodata.tampildata');
    Route::get('/biodata/cetak_pdf/{id}/{tanggal}/{last_contract}/{type}', 'BiodataController@cetak_pdf')->name('biodata.cetak_pdf');
    // UPdate
    Route::post('/biodata/get-data', 'BiodataController@getPengalamanKerja')->name('biodata.get-data');
    Route::GET('/home/data', 'HomeController@getchart')->name('home.data');
    // cleansing
    Route::get('cleansing', 'CleansingController@index')->name('cleansing.index');
    Route::POST('/cleansing/destroy/', 'CleansingController@destroy')->name('cleansing.destroy');

    Route::GET('/jabatan', 'JabatanController@index')->name('jabatan.index');
    Route::POST('/jabatan', 'JabatanController@editjs')->name('jabatan.edit');
    Route::GET('/jabatan/{site}', 'JabatanController@getDataBySite')->name('jabatan.bysite');
    Route::get('/jabatan/detail/{id}', 'JabatanController@edit')->name('jabatan.edit');
    Route::POST('/jabatan/update/{id}', 'JabatanController@update')->name('jabatan.update');
    Route::POST('/jabatan/save/', 'JabatanController@save')->name('jabatan.save');
    Route::get('/jabatan/delete/{id}', 'JabatanController@delete');

    Route::GET('/site', 'SiteController@index')->name('site.index');
    Route::POST('/site', 'SiteController@editjs')->name('site.edit');
    Route::get('/site/detail/{id}', 'SiteController@edit')->name('site.edit');
    Route::POST('/site/update/{id}', 'SiteController@update')->name('site.update');
    Route::POST('/site/save/', 'SiteController@save')->name('site.save');
    Route::get('/site/delete/{id}', 'SiteController@delete');

    Route::get('/interview/{id}', 'InterviewController@index')->name('interview.index');
    Route::post('/interview/{id}', 'InterviewController@store')->name('interview.store');
    Route::post('/interview-reject/{id}', 'InterviewController@storeReject')->name('interview.store-reject');
    Route::post('/interview-final/{id}', 'InterviewController@final_interview')->name('interview.final_interview');

    Route::GET('/client-master', 'ClientMasterController@index')->name('client.index');
    Route::GET('/client-master/get-data', 'ClientMasterController@getData')->name('client.getData');
    Route::GET('/client-master/edit/{id}', 'ClientMasterController@edit')->name('client.edit');
    Route::POST('/client-master/update/{id}', 'ClientMasterController@update')->name('client.update');
    Route::POST('/client-master/save/', 'ClientMasterController@save')->name('client.save');
    Route::get('/client-master/delete/{id}', 'ClientMasterController@delete');

    //Prefix
    Route::GET('/prefix', 'PrefixController@index')->name('prefix.index');
    Route::POST('/prefix', 'PrefixController@editjs')->name('prefix.edit');
    Route::get('/prefix/detail/{id}', 'PrefixController@edit')->name('prefix.edit');
    Route::POST('/prefix/update/', 'PrefixController@update')->name('prefix.update');
    Route::POST('/prefix/save/', 'PrefixController@save')->name('prefix.save');
    Route::get('/prefix/delete/{id}', 'PrefixController@delete');
});
//yg ini route aneh suka error
Route::group(['middleware' => ['cekRole:agent']], function () {
    Route::get('/biodata/tampildata/', 'BiodataController@tampildata')->name('biodata.tampildata');
});
