const token = $('meta[name="csrf-token"]').attr('content')
const routeUpdatePengalamanKerja = $('#routeUpdatePengalamanKerja').val()

showTable();
loadDoc();

function showTable() {
    let idUser = $('#userId').val()
    var dataTableActive = $("#table-pengalaman").DataTable({
        language: {
            paginate: {
                next: '<i class="fas fa-chevron-right"></i>',
                previous: '<i class="fas fa-chevron-left"></i>'
            }
        },
        processing: false,
        bLengthChange: false,
        bInfo: false,
        pageLength: 10,
        ajax: {
            url: "/biodata/tampildata/" + idUser,
            // url: "/user-account/active",
        },
        columns: [
            { data: "namaPerusahaan" },
            { data: "jabatanKerja" },
            { data: "masaKerja" },
            { data: "alasanKeluar" },
            { data: "alamatPerusahaan" },
            {
                data: "action",
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    var id = row.id;
                    return (
                        `   
                        <a class="badge bg-primary" href="javascript:void(0)" id="`+ id + `" onclick="edit(event)"><i class="fas fa-pen"></i></a>                       
                        <a href="/biodata/delete_pgk/`+ id + `" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                        `
                    );
                },
            },
        ],
        searchDelay: 750,
        buttons: [],
        columnDefs: [
            {
                defaultContent: "-",
                targets: "_all",
                className: "text-left",
            },
        ],
        bDestroy: true,
    });

    $(".dataTables_filter").hide();

    $("#btnSearch").click(function name() {
        let valInput = document.getElementById("myInputTextField").value;
        dataTableActive.search(valInput).draw();
    });
}

function edit(ev) {
    ev.preventDefault()
    let idPengalaman = ev.currentTarget.getAttribute('id')
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: '/biodata/get-data',
        data: { id: idPengalaman },
        success: function (response) {
            console.log(response)
            $('#modalPengalaman').modal('show')
            $('#id').val(response.id)
            $('#namaPerusahaan').val(response.namaPerusahaan)
            $('#alamatPerusahaan').val(response.alamatPerusahaan)
            $('#jabatanKerja').val(response.jabatanKerja)
            $('#masaKerja').val(response.masaKerja)
            $('#alasanKeluar').val(response.alasanKeluar)

        }
    })
}

$('#btnSubmitFinal').on('click', function (e) {
    e.preventDefault()

    var data = {
        id: $("#id").val(),
        namaPerusahaan: $("#namaPerusahaan").val(),
        jabatanKerja: $("#jabatanKerja").val(),
        alamatPerusahaan: $("#alamatPerusahaan").val(),
        masaKerja: $("#masaKerja").val(),
        alasanKeluar: $("#alasanKeluar").val(),

    }

    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: routeUpdatePengalamanKerja,
        data: data,
        success: function (response) {
            console.log(response.message)
            // alert(response.message)                
            $('#modalPengalaman').modal('hide')
            const notifAlert = `
            <div class="col-md-12 div`+ response.data.id + `">
                                    <div class="alert alert-`+ response.alert + `">` + response.message + `</div>
                                    </div>
                                    `
            $('#notif').append(notifAlert)
            setTimeout(function () {
                $('.div' + response.data.uuid).fadeOut('slow')
            }, 10000)
            showTable()
        },
        error: function (xhr) {
            var err = JSON.parse(xhr.responseText)
            var errorString = '<ul>'
            $.each(err.errors, function (key, value) {
                errorString += '<p>' + value + '</p>'
            })
            errorString += '</ul>'

            swal({
                type: "warning",
                title: errorString,
                showCancelButton: false,
                confirmButtonColor: "#e3342f",
                confirmButtonText: "Ok",
            })
        }
    })
});

function loadDoc(url, cFunction) {

    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            cFunction(this);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}
function myFunction(xhttp) {
    document.getElementById("demo").innerHTML =
        xhttp.responseText;
}