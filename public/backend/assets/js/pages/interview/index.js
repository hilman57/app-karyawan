function pickInterviewBy(val) {
  const content_status = `
    <div class="col-2">
      <label class="form-label">Status</label>
    </div>
    <div class="col-3">
      <select name="status_interview" class="form-control" required>
        <option value="">--Choose--</option>
        <option value="Terima">Terima</option>
        <option value="Tolak">Tolak</option>
        <option value="Pertimbangkan">Pertimbangkan</option>
      </select>
    </div>
  `
  if (val == 'HRD') $('#status_interview').append(content_status)
  else $('#status_interview').empty()
}

function onReject(id) {
  const token = $('meta[name="csrf-token"]').attr('content')
  var elements = document.getElementsByClassName("answer");
  var answer = [];
  for (var i = 0; i < elements.length; i++) {
    answer.push(elements[i].value)
  }
  $.ajax({
    headers: { 'X-CSRF-TOKEN': token },
    type: 'POST',
    url: `/interview-reject/${id}`,
    data: { answer: answer },
    success: function (response) {
      window.location.href = window.location.origin+'/biodata'
    }});
}
