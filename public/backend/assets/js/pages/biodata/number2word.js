const a = ['', 'Satu ', 'Dua ', 'Tiga ', 'Empat ', 'Lima ', 'Enam ', 'Tujuh ', 'Delapan ', 'Sembilan ', 'Sepuluh ', 'Sebelas ', 'Dua Belas ', 'Tiga Belas ', 'Empat Belas ', 'Lima Belas ', 'Enam Belas ', 'Tujuh Belas ', 'Delapan Belas ', 'Sembilan Belas ']
const b = ['', '', 'Dua Puluh ', 'tiga Puluh ', 'Empat Puluh ', 'Lima Puluh ', 'Enam Puluh ', 'Tujuh Puluh ', 'Delapan Puluh ', 'Sembilan Puluh ']
const c = ['', 'Seratus', 'Dua Ratus', 'tiga Ratus', 'Empat Ratus', 'Lima Ratus', 'Enam Ratus', 'Tujuh Ratus', 'Delapan Ratus', 'Sembilan Ratus']

function inWord(num) {
  if (num.toString().length == 7)
    satuan(num)
  else if (num.toString().length == 8)
    puluhan(num)
  else if (num.toString().length == 9)
    ratusan(num)
}

function satuan(num) {
  let n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{1})(\d{3})(\d{3})$/)
  if (!n) return
  let ribu = a[n[3][2]] == 'satu ' ? 'seribu ' : `${a[n[3][2]]} Ribu `
  let ratus = a[n[4][2]] == 'satu ' ? 'seratus ' : `${a[n[4][2]]} Ratus `
  let str = ''
  str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Juta ' : ''
  str += (n[3] != 0) ? (c[Number(n[3][0])] + ' ' + b[n[3][1]] + ' ' + ribu) : ''
  str += (n[4] != 0) ? (c[Number(n[4][0])] + ' ' + b[n[4][1]] + ' ' + ratus) : ''
  document.getElementById('toWord').innerHTML = `[${str} Rupiah]`
}

function puluhan(num) {
  let n = ('000000000' + num).substr(-10).match(/^(\d{2})(\d{2})(\d{3})(\d{3})$/)
  if (!n) return
  let ribu = a[n[3][2]] == 'satu ' ? 'seribu ' : `${a[n[3][2]]} Ribu `
  let ratus = a[n[4][2]] == 'satu ' ? 'seratus ' : `${a[n[4][2]]} Ratus `
  let str = ''
  str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Juta ' : ''
  str += (n[3] != 0) ? (c[Number(n[3][0])] + ' ' + b[n[3][1]] + ' ' + ribu) : ''
  str += (n[4] != 0) ? (c[Number(n[4][0])] + ' ' + b[n[4][1]] + ' ' + ratus) : ''
  document.getElementById('toWord').innerHTML = `[${str} Rupiah]`
}

function ratusan(num) {
  let n = ('000000000' + num).substr(-11).match(/^(\d{2})(\d{1})(\d{2})(\d{3})(\d{3})$/)
  if (!n) return
  let ribu = a[n[4][2]] == 'satu ' ? 'seribu ' : `${a[n[4][2]]} Ribu `
  let ratus = a[n[5][2]] == 'satu ' ? 'seratus ' : (a[n[5][2]] == '') ? '' : `${a[n[5][2]]} Ratus `
  let str = ''
  str += (n[2] != 0) ? (c[Number(n[2])] + ' ' + a[Number(n[3])]) + 'Juta ' : ''
  str += (n[3] != 0) ? (c[Number(n[4][0])] + ' ' + b[n[4][1]] + ' ' + ribu) : ''
  str += (n[4] != 0) ? (c[Number(n[5][0])] + ' ' + b[n[5][1]] + ' ' + ratus) : ''
  document.getElementById('toWord').innerHTML = `[${str} Rupiah]`
}

inWord(4150000)
    // [Empat Juta Seratus Lima Puluh Ribu Rupiah]