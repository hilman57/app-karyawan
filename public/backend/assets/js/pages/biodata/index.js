$(function () {

    tableActive();
    tableActiveDetail();
    // const token = $('meta[name="csrf-token"]').attr('content')
    const routeUpdatePengalamanKerja = $('#routeUpdatePengalamanKerja').val()

    function tableActive() {
        var dataTableActive = $("#table-active").DataTable({
            language: {
                paginate: {
                    next: '<i class="fas fa-chevron-right"></i>',
                    previous: '<i class="fas fa-chevron-left"></i>'
                }
            },
            processing: false,
            bLengthChange: false,
            bInfo: false,
            pageLength: 10,
            ajax: {
                url: "/biodata/active",
                // url: "/user-account/active",
            },
            columns: [
                { data: "nama_lengkap" },
                { data: "position_name" },
                { data: "ktp" },
                { data: "site_name" },
                { data: "status_ky" },
                {
                    data: "user_created_date",
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        const date = row.user_created_date.split(' ')
                        return (date[0])
                    }
                },
                {
                    data: "action",
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        var id = row.id;
                        if (row.id_status == 5) {
                            //    <a href="/biodata/detail/`+ id + `" class="badge bg-primary" title="detail"><i class="fa fa-eye"></i></a>
                            return (
                                `
                                    <a href="/biodata/edit/${id}" class="badge bg-primary" title="edit"><i class="fa fa-edit"></i></a>
                                    <a href="/biodata/delete/${id}" class="badge bg-danger" title="hapus" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                                    <a class="badge bg-primary" href="javascript:void(0)" title="cetak pdf" onclick="IDPengalaman(${id})"><i class="fa fa-id-card"></i></a>  
                                `
                            );
                        } else if (row.id_status == 2 || row.id_status == 4) {
                            return (
                                `
                                    <a href="/biodata/edit/${id}" class="badge bg-primary" title="edit"><i class="fa fa-edit"></i></a>
                                    <a href="/biodata/delete/${id}" class="badge bg-danger" title="hapus" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                                    <a href="/interview/${id}" class="badge bg-primary" href="javascript:void(0)" title="Form Interview"><i class="fa fa-list"></i></a>  
                                `
                            );
                        } else {
                            // <a href="/biodata/detail/${id}" class="badge bg-primary" title="detail"><i class="fa fa-eye"></i></a>
                            return (
                                `
                                        <a href="/biodata/edit/${id}" class="badge bg-primary" title="edit"><i class="fa fa-edit"></i></a>
                                        <a href="/biodata/delete/${id}" class="badge bg-danger" title="hapus" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                                    `
                            );
                        }
                    },
                },
            ],
            searchDelay: 750,
            buttons: [],
            columnDefs: [
                {
                    defaultContent: "-",
                    targets: "_all",
                    className: "text-left",
                },
            ],
            bDestroy: true,
        });

        $("#btnSearch").click(function name() {
            let valInput = document.getElementById("myInputTextField").value;
            dataTableActive.search(valInput).draw();
        });

        $("#filter-applicant").click(function name() {
            dataTableActive.search('Applicant').draw();
        });

        $("#filter-candidate").click(function name() {
            dataTableActive.search('Candidate').draw();
        });

        $("#filter-interview").click(function name() {
            dataTableActive.search('Interview').draw();
        });

        $("#filter-cPKWT").click(function name() {
            dataTableActive.search('Create PKWT').draw();
        });
    }



    function tableActiveDetail() {
        var dataTableActive = $("#table-active-detail").DataTable({
            language: {
                paginate: {
                    next: '<i class="fas fa-chevron-right"></i>',
                    previous: '<i class="fas fa-chevron-left"></i>'
                }
            },
            processing: false,
            bLengthChange: false,
            bInfo: false,
            pageLength: 10,
            ajax: {
                url: "/biodata/activedetail",
                // url: "/user-account/active",
            },
            columns: [

                { data: "nama_lengkap" },
                { data: "jabatan" },
                { data: "ktp" },
                { data: "site" },

                { data: "aoc" },
                { data: "nama_pasangan" },
                { data: "alamat_keluarga_dihubungi" },
                { data: "created_job" },
                { data: "nama_leader" },
                { data: "npwp" },
                { data: "tgl_npwp" },
                { data: "tempat" },
                { data: "tgl_lahir" },
                { data: "alamat" },
                { data: "jenis_kelamin" },
                { data: "kodepos" },

                { data: "telpon" },
                { data: "hp" },
                { data: "agama" },
                { data: "email" },
                { data: "pendidikan_terakhir" },
                { data: "nama_lembaga" },
                { data: "status" },
                { data: "istri_suami" },
                { data: "email_ibu" },
                { data: "pengalaman_kerja_nama_perusahaan" },
                { data: "pengalaman_kerja_nama_alamat_perusahaan" },
                { data: "pengalaman_kerja_jabatan" },
                { data: "pengalaman_kerja_masa_kerja" },
                { data: "pengalaman_kerja_alasan_keluar" },
                { data: "pekerjaan_istri_suami" },
                { data: "jumlah_anak" },
                { data: "berat_badan" },
                { data: "nama_ibu_kandung" },
                { data: "tinggi_badan" },
                { data: "rekening" },
                { data: "nama_rekening" },
                { data: "cabang" },
                { data: "nama_keluarga" },
                { data: "hubungan_klg_dihub" },
                { data: "alamat_lengkap" },
                { data: "keluarga_telpon" },
                { data: "keluarga_hp" },
                { data: "kota" },
                { data: "kota_keluarga" },
                { data: "mulai_kerja" },

                {
                    data: "action",
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        var id = row.id;
                        // return (
                        //     `
                        //         <a href="/biodata/detail" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                        //         <a href="/biodata/edit/`+ id + `" class="badge bg-primary"><i class="fa fa-check"></i></a>
                        //         <a href="/biodata/delete/`+ id + `" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                        //     `
                        // );
                    },
                },
            ],
            searchDelay: 750,
            buttons: [],
            columnDefs: [
                {
                    defaultContent: "-",
                    targets: "_all",
                    className: "text-left",
                },
            ],
            bDestroy: true,
        });
        // console.log('dataTableActive', dataTableActive);
        $(".dataTables_filter").hide();

        $("#btnSearch").click(function name() {
            let valInput = document.getElementById("myInputTextField").value;
            dataTableActive.search(valInput).draw();
        });

        $("#filter-applicant").click(function name() {
            dataTableActive.search('Applicant').draw();
        });

        $("#filter-candidate").click(function name() {
            dataTableActive.search('Candidate').draw();
        });

        $("#filter-interview").click(function name() {
            dataTableActive.search('Interview').draw();
        });

        $("#filter-cPKWT").click(function name() {
            dataTableActive.search('Create PKWT').draw();
        });
    }



    $("#showActive").on("click", function (e) {
        $("#tab-active-user").show();

        tableActive();
    });

    $('#btnSubmitFinal').on('click', function (e) {
        const token = $('meta[name="csrf-token"]').attr('content')
        e.preventDefault()

        var data = {
            id: $("#id").val(),
            namaPerusahaan: $("#namaPerusahaan").val(),
            jabatanKerja: $("#jabatanKerja").val(),
            alamatPerusahaan: $("#alamatPerusahaan").val(),
            masaKerja: $("#masaKerja").val(),
            alasanKeluar: $("#alasanKeluar").val(),

        }
        var url = "/biodata/updt_pgk/"

        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            url: url,
            data: data,
            success: function (response) {
                console.log(response.message)
            },
            error: function (xhr) {
                var err = JSON.parse(xhr.responseText)
                var errorString = '<ul>'
                $.each(err.errors, function (key, value) {
                    errorString += '<p>' + value + '</p>'
                })
                errorString += '</ul>'

                swal({
                    type: "warning",
                    title: errorString,
                    showCancelButton: false,
                    confirmButtonColor: "#e3342f",
                    confirmButtonText: "Ok",
                })
            }
        })
    });
});

function IDPengalaman(params) {
    // console.log(params)
    $('#idPengalaman').val(params)
    showModal('modalCreateBerkas')
}

function setTypePkwt() {
    let d = new Date()
    let id = $("#idPengalaman").val()
    let month = d.getMonth() + 1
    let tanggal = `${d.getFullYear()}-${(month < 10) ? '0' + month : month}-${d.getDate()}`
    let type = $('#type_pkwt').val()
    if (type == 3) {
        $('#wrap-tanggal').show()
        $('#wrap-last-contract').show()
        return false
    } else {
        $('#wrap-tanggal').hide()
        $('#wrap-last-contract').hide()
        $('#tgl_pkwt').val('')
        $('#last_contract').val('')
        $('#btnSubmitFinalcetak').attr('href', `/biodata/cetak_pdf/${id}/${tanggal}/${tanggal}/${type}`)
    }
}

function setTanggalPKWT() {
    let id = $("#idPengalaman").val()
    let tanggal = $('#tgl_pkwt').val()
    let last_contract = 'bni'
    let type = $('#type_pkwt').val()
    $('#btnSubmitFinalcetak').attr('href', `/biodata/cetak_pdf/${id}/${tanggal}/${last_contract}/${type}`)
}

function setLastContract() {
    let id = $("#idPengalaman").val()
    let tanggal = $('#tgl_pkwt').val()
    let last_contract = $('#last_contract').val()
    let type = $('#type_pkwt').val()
    $('#btnSubmitFinalcetak').attr('href', `/biodata/cetak_pdf/${id}/${tanggal}/${last_contract}/${type}`)
}

function closeModal() {
    $('#type_pkwt').val('')
    $('#tgl_pkwt').val('')
    $('#last_contract').val('')
    $('#modalCreateBerkas').modal('toggle')
}

function pickSite(val) {
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'GET',
        url: `/jabatan/${val}`,
        success: function (res) {
            let content = `<option value="">--Choose --</option>`
            res.data.map((item, i) => {
                content += `<option value="${item.user_id}">${item.position_name}</option>`
            })
            $('#jabatan').html(content)
        }
    })
}

function beginSearch() {
    const site = $('#site').val()
    const jabatan = $('#jabatan').val()
    const status = $('#status_kry').val()
    if (site == null && jabatan == null && status == null) {
        alert('Salah satu harus diisi!')
        return false
    }
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        url: '/biodata/search_biodata/',
        data: {
            site: site,
            jabatan: jabatan,
            status: status
        },
        success: function (res) {
            let content = ``
            if (res.data.length > 0) {
                res.data.map((item, i) => {
                    content += `<tr>`
                    content += `<td class="p-2">${item.nama_lengkap}</td>`
                    content += `<td class="p-2">${item.position_name}</td>`
                    content += `<td class="p-2">${item.ktp}</td>`
                    content += `<td class="p-2">${item.site_name}</td>`
                    content += `<td class="p-2">${item.status_ky}</td>`
                    content += `<td class="p-2">${item.user_created_date}</td>`
                    if (item.id_status == 5) {
                        content += `<td class="p-2">
                                    <a href="/biodata/edit/${item.user_id}" class="badge bg-primary" title="edit"><i class="fa fa-edit"></i></a>
                                    <a href="/biodata/delete/${item.user_id}" class="badge bg-danger" title="hapus" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                                    <a class="badge bg-primary" href="javascript:void(0)" title="cetak pdf" onclick="IDPengalaman(${item.user_id})"><i class="fa fa-id-card"></i></a>  </td>
                                `

                    } else if (item.id_status == 2 || item.id_status == 4) {
                        content += `<td class="p-2">
                        <a href="/biodata/edit/${item.user_id}" class="badge bg-primary" title="edit"><i class="fa fa-edit"></i></a>
                        <a href="/biodata/delete/${item.user_id}" class="badge bg-danger" title="hapus" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                        <a href="/interview/${item.user_id}" class="badge bg-primary" href="javascript:void(0)" title="Form Interview"><i class="fa fa-list"></i></a></td>
                            `
                    } else {
                        content += `<td class="p-2">
                        <a href="/biodata/edit/${item.user_id}" class="badge bg-primary" title="edit"><i class="fa fa-edit"></i></a>
                        <a href="/biodata/delete/${item.user_id}" class="badge bg-danger" title="hapus" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a></td>
                                `
                    }
                    content += `</tr>`
                })
                $('#data-biodata').html(content)
                // $("#table-active").DataTable({
                //     language: {
                //         paginate: {
                //             next: '<i class="fas fa-chevron-right"></i>',
                //             previous: '<i class="fas fa-chevron-left"></i>'
                //         }
                //     },
                //     processing: false,
                //     bLengthChange: false,
                //     bInfo: false,
                //     pageLength: 10,
                //     searchDelay: 750,
                //     buttons: [],
                //     columnDefs: [
                //         {
                //             defaultContent: "-",
                //             targets: "_all",
                //             className: "text-left",
                //         },
                //     ],
                //     bDestroy: true,
                // })
                // $(".dataTables_filter").hide();
                return false
            } else {
                $('#data-biodata').empty()
            }
        },
    })
}
