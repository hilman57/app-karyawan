$(function () {
   tableNew();

   function tableNew() {
      var datatableNew = $("#table-new").DataTable({
         language: {
               paginate: {
                  next: '<i class="fas fa-chevron-right"></i>',
                  previous: '<i class="fas fa-chevron-left"></i>'
               }
         },
         processing: false,
         bLengthChange: false,
         bInfo: false,
         pageLength: 10,
         ajax: {
               url: "/business-account/verification/new",
         },
         columns: [
               { data: "created_date" },
               { data: "name" },
               { data: "name" },
               { data: "name" },
               { data: "email" },
               {
                  data: "action",
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                     var id = row.id;
                     return (
                           `
                              <a href="/user-account/detail" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                              <a href="/user-account/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                           `
                     );
                  },
               },
         ],
         searchDelay: 750,
         buttons: [],
         columnDefs: [
               {
                  defaultContent: "-",
                  targets: "_all",
                  className: "text-left",
               },
         ],
         bDestroy: true,
      });

      $(".dataTables_filter").hide();

      $("#btnSearch").click(function name() {
         let valInput = document.getElementById("myInputTextField").value;
         datatableNew.search(valInput).draw();
      });
   }

   function tableRejected() {
      var datatableRejected = $("#table-rejected").DataTable({
         language: {
               paginate: {
                  next: '<i class="fas fa-chevron-right"></i>',
                  previous: '<i class="fas fa-chevron-left"></i>'
               }
         },
         processing: false,
         bLengthChange: false,
         bInfo: false,
         pageLength: 10,
         ajax: {
               url: "/business-account/verification/rejected",
         },
         columns: [
               { data: "created_date" },
               { data: "name" },
               { data: "name" },
               { data: "name" },
               { data: "email" },
               {
                  data: "action",
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                     var id = row.id;
                     return (
                           `
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                              <a href="/user-account/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                           `
                     );
                  },
               },
         ],
         searchDelay: 750,
         buttons: [],
         columnDefs: [
               {
                  defaultContent: "-",
                  targets: "_all",
                  className: "text-left",
               },
         ],
         bDestroy: true,
      });

      $(".dataTables_filter").hide();

      $("#btnSearch").click(function name() {
         let valInput = document.getElementById("myInputTextField").value;
         datatableRejected.search(valInput).draw();
      });
   }

   $("#showNew").on("click", function (e) {
      $("#tab-new").show();
      $("#tab-rejected").hide();
      tableNew();
   });
   $("#showRejected").on("click", function (e) {
      $("#tab-new").hide();
      $("#tab-rejected").show();
      tableRejected();
   });
});
