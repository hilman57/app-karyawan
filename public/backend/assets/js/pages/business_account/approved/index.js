$(function () {
   tableActivePackage();

   function tableActivePackage() {
      var datatableActivePackage = $("#table-active-package").DataTable({
         language: {
               paginate: {
                  next: '<i class="fas fa-chevron-right"></i>',
                  previous: '<i class="fas fa-chevron-left"></i>'
               }
         },
         processing: false,
         bLengthChange: false,
         bInfo: false,
         pageLength: 10,
         ajax: {
               url: "/business-account/approved/active-package",
         },
         columns: [
               { data: "created_date" },
               { data: "name" },
               { data: "name" },
               { data: "name" },
               { data: "email" },
               {
                  data: "action",
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                     var id = row.id;
                     return (
                           `
                              <a href="/user-account/detail" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                              <a href="/user-account/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                           `
                     );
                  },
               },
         ],
         searchDelay: 750,
         buttons: [],
         columnDefs: [
               {
                  defaultContent: "-",
                  targets: "_all",
                  className: "text-left",
               },
         ],
         bDestroy: true,
      });

      $(".dataTables_filter").hide();

      $("#btnSearch").click(function name() {
         let valInput = document.getElementById("myInputTextField").value;
         datatableActivePackage.search(valInput).draw();
      });
   }

   function tableNonActivePackage() {
      var datatableNonActivePackage = $("#table-non-active-package").DataTable({
         language: {
               paginate: {
                  next: '<i class="fas fa-chevron-right"></i>',
                  previous: '<i class="fas fa-chevron-left"></i>'
               }
         },
         processing: false,
         bLengthChange: false,
         bInfo: false,
         pageLength: 10,
         ajax: {
               url: "/business-account/approved/non-active-package",
         },
         columns: [
               { data: "created_date" },
               { data: "name" },
               { data: "name" },
               { data: "name" },
               { data: "email" },
               {
                  data: "action",
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                     var id = row.id;
                     return (
                           `
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                              <a href="/user-account/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                           `
                     );
                  },
               },
         ],
         searchDelay: 750,
         buttons: [],
         columnDefs: [
               {
                  defaultContent: "-",
                  targets: "_all",
                  className: "text-left",
               },
         ],
         bDestroy: true,
      });

      $(".dataTables_filter").hide();

      $("#btnSearch").click(function name() {
         let valInput = document.getElementById("myInputTextField").value;
         datatableNonActivePackage.search(valInput).draw();
      });
   }

   function tableAccountSuspended() {
      var datatableAccountSuspended = $("#table-account-suspended").DataTable({
         language: {
               paginate: {
                  next: '<i class="fas fa-chevron-right"></i>',
                  previous: '<i class="fas fa-chevron-left"></i>'
               }
         },
         processing: false,
         bLengthChange: false,
         bInfo: false,
         pageLength: 10,
         ajax: {
               url: "/business-account/approved/account-suspended",
         },
         columns: [
               { data: "created_date" },
               { data: "name" },
               { data: "name" },
               { data: "name" },
               { data: "email" },
               {
                  data: "action",
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                     var id = row.id;
                     return (
                           `
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-eye"></i></a>
                              <a href="/user-account/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                              <a href="/user-account/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                           `
                     );
                  },
               },
         ],
         searchDelay: 750,
         buttons: [],
         columnDefs: [
               {
                  defaultContent: "-",
                  targets: "_all",
                  className: "text-left",
               },
         ],
         bDestroy: true,
      });

      $(".dataTables_filter").hide();

      $("#btnSearch").click(function name() {
         let valInput = document.getElementById("myInputTextField").value;
         datatableAccountSuspended.search(valInput).draw();
      });
   }

   $("#showActivePackage").on("click", function (e) {
      $("#tab-active-package").show();
      $("#tab-non-active-package").hide();
      $("#tab-account-suspended").hide();
      tableActivePackage();
   });
   
   $("#showNonActivePackage").on("click", function (e) {
      $("#tab-active-package").hide();
      $("#tab-non-active-package").show();
      $("#tab-account-suspended").hide();
      tableNonActivePackage();
   });

   $("#showAccountSuspended").on("click", function (e) {
      $("#tab-active-package").hide();
      $("#tab-non-active-package").hide();
      $("#tab-account-suspended").show();
      tableAccountSuspended();
   });
});
