$(function () {
  tableActive();
  const token = $('meta[name="csrf-token"]').attr('content')
  let accordionOne = document.getElementById("flush-collapseOne")
  let accordionTwo = document.getElementById("flush-collapseTwo")

  function tableActive() {
    var dataTableActive = $("#table-active").DataTable({
      language: {
        paginate: {
          next: '<i class="fas fa-chevron-right"></i>',
          previous: '<i class="fas fa-chevron-left"></i>'
        }
      },
      processing: false,
      bLengthChange: false,
      bInfo: false,
      pageLength: 10,
      ajax: {
        url: "/client-master/get-data",
      },
      columns: [
        { data: "client" },
        { data: "clientname" },
        { data: "no_kontrak" },
        {
          data: "periode",
          orderable: false,
          searchable: false,
          render: function (data, type, row) {
            return (
              `${row.validfrom} - ${row.validuntil}`
            )
          }
        },
        { data: "last_number_pkwt" },
        {
          data: "action",
          orderable: false,
          searchable: false,
          render: function (data, type, row) {
            return (
              `
                <a href="/client-master/edit/${row.id}" class="badge bg-primary"><i class="fa fa-edit"></i></a>
                <a href="/client-master/delete/${row.id}" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
              `
            );
          },
        },
      ],
      searchDelay: 750,
      buttons: [],
      columnDefs: [
        {
          defaultContent: "-",
          targets: "_all",
          className: "text-left",
        },
      ],
      bDestroy: true,
    });

    $(".dataTables_filter").hide();

    $("#btnSearch").click(function name() {
      let valInput = document.getElementById("myInputTextField").value;
      dataTableActive.search(valInput).draw();
    });
  }

  $("#showActive").on("click", function (e) {
    $("#tab-active-user").show();
    tableActive();
  });

  $('#btnAdd').on('click', function (e) {
    accordionOne.classList.add("show")
    accordionTwo.classList.remove("show")
 });

 $("#btnSearch").click(function name() {
    let valInput = document.getElementById("myInputTextField").value;
    dataTableActive.search(valInput).draw();
  });

  $('#btnSubmit').on('click', function (e) {
    accordionOne.classList.remove("show")
    accordionTwo.classList.add("show")
    e.preventDefault()
    $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: `/client-master/save`,
      data: {
        client: $('#client').val(),
        clientname: $('#clientname').val(),
        no_kontrak: $('#no_kontrak').val(),
        validfrom: $('#validfrom').val(),
        validuntil: $('#validuntil').val(),
        last_number_pkwt: $('#last_number_pkwt').val(),
      },
      success: function (response) {
        document.getElementById("formInput").reset()
        const notifAlert = `
                               <div class="col-md-12 div`+ response.data.id + `">
                                  <div class="alert alert-`+ response.alert + `">` + response.message + `</div>
                               </div>
                            `
        $('#notif').append(notifAlert)
        setTimeout(function () {
          $('.div' + response.data.id).fadeOut('slow')
        }, 1500)
        tableActive()
      },
      error: function (xhr) {
        var err = JSON.parse(xhr.responseText)
        var errorString = '<ul>'
        $.each(err.errors, function (key, value) {
          errorString += '<p>' + value + '</p>'
        })
        errorString += '</ul>'

        swal({
          type: "warning",
          title: errorString,
          showCancelButton: false,
          confirmButtonColor: "#e3342f",
          confirmButtonText: "Ok",
        })
      }
    })
  });
});

