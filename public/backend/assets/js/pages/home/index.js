const token = $('meta[name="csrf-token"]').attr('content')
// document.getElementById("getData").innerHTML = "Hello World";

// $.ajax({
//     url: "/home/data",
//     type: 'GET',
//     dataType: 'json', // added data type
//     success: function (res) {
//         console.log(res);
//         alert(res);
//     }
// });


$.ajax({

    headers: { 'X-CSRF-TOKEN': token },
    url: '/home/data',
    type: 'GET', //this is your method
    // data: { match_id:id, start_time:newTime },
    dataType: 'json',
    success: function (response) {
        var xValues = [Object.keys(response.data)[0], Object.keys(response.data)[1], Object.keys(response.data)[2], Object.keys(response.data)[3], Object.keys(response.data)[4]];

        var yValues = [response.data.Applicant, response.data.Interview, response.data.Considered, response.data.Rejected, response.data.Accepted];
        var barColors = [
            "#b91d47",
            "#00aba9",
            "#2b5797",
            "#e8c3b9",
            "#1e7145"
        ];

        new Chart("myChart", {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: "Ini sample Pie charts"
                }
            }
        });
    }
});
// test

    // test
