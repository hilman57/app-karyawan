var dataTable = $("#table-faq").DataTable({
    language: {
        paginate: {
            next: '<i class="fas fa-chevron-right"></i>',
            previous: '<i class="fas fa-chevron-left"></i>'
        }
    },
    processing: false,
    bLengthChange: false,
    bInfo: false,
    pageLength: 10,
    ajax: {
        url: "/master/faq",
    },
    columns: [
        { data: "question" },
        { data: "answer" },
        {
            data: "action",
            orderable: false,
            searchable: false,
            render: function (data, type, row) {
                var id = row.id;
                return (
                    `
                        <a href="/master/faq/`+id+`/edit" class="badge bg-primary"><i class="fa fa-check"></i></a>
                        <a href="/master/faq/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                    `
                );
            },
        },
    ],
    searchDelay: 750,
    buttons: [],
    columnDefs: [
        {
            defaultContent: "-",
            targets: "_all",
            className: "text-left",
        },
    ],
});
$(".dataTables_filter").hide();

$("#btnSearch").click(function name() {
    let valInput = document.getElementById("myInputTextField").value;
    dataTable.search(valInput).draw();
});