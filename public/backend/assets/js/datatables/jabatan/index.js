// const token = $('meta[name="csrf-token"]').attr('content')
const routeGetData = $("#routeGetData").val()
// const routeSaveData = $("#routeSaveData").val()
let accordionOne = document.getElementById("flush-collapseOne")
let accordionTwo = document.getElementById("flush-collapseTwo")

displayData()

function displayData() {
   var dataTable = $("#table-menu").DataTable({
      ajax: {
         url: "/jabatan",
      },
      columns: [
         { data: "site_name" },
         { data: "position_name" },
         { data: "contract" },
         // {
         //    data: "salary",
         //    orderable: false,
         //    searchable: false,
         //    render: function(data, type, row) {
         //       return(row.salary.toLocaleString())
         //    }
         // },
         // {
         //    data: "allowance",
         //    orderable: false,
         //    searchable: false,
         //    render: function(data, type, row) {
         //       return(row.allowance.toLocaleString())
         //    }
         // },
         {
            data: "status",
            orderable: false,
            searchable: false,
            render: function(data, type, row) {
               let status = row.flag == 1 ? 'Active' : 'Non Active'
               return(status)
            }
         },
         {
            data: "action",
            orderable: false,
            searchable: false,
            render: function (data, type, row) {
               var id = row.id;
               return (`
                        <div class="d-flex">
                        <a href="/jabatan/detail/`+ id + `" class="btn-circle text-primary me-1"><i class="fa fa-pen"></i></a>
                        <a class="btn-circle text-danger" href="javascript:void(0)" id="`+ id + `" onclick="hapus(event)"><i class="fa fa-trash"></i></a>
                        </div>
                     `
               );
            },
         },
      ],
      searchDelay: 750,
      buttons: [],
      columnDefs: [
         {
            defaultContent: "-",
            targets: "_all",
            className: "text-left",
         },
      ],
      bDestroy: true,
   });
   $(".dataTables_filter").hide();

   $("#btnSearch").click(function name() {
      let valInput = document.getElementById("myInputTextField").value;
      dataTable.search(valInput).draw();
   });

}

function edit(ev) {
   accordionOne.classList.add("show")
   accordionTwo.classList.remove("show")

   ev.preventDefault();
   let id = ev.currentTarget.getAttribute('id')
   let idMenu = ev.currentTarget.getAttribute('id');
   // alert(routeGetData)
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: routeGetData,
      data: { id: idMenu },
      success: function (response) {
         $('#idEdit').val(idMenu)
         $('#jabatant').val(response.jabatan)

      }
   })
}

$('#btnSubmit').on('click', function (e) {
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
   e.preventDefault()
   if($('#site').val() == null && $('#position_name').val() == null && $('#contract').val() == null && $('#flag').val() == null) {
      alert('Form harus diisi semua!')
      return false
   }
   $.ajax({
      headers: { 'X-CSRF-TOKEN': token },
      type: 'POST',
      url: `/jabatan/save`,
      data: {
         id_site: $('#site').val(),
         position_name: $('#position_name').val(),
         contract: $('#contract').val(),
         // salary: $('#salary').val(),
         // allowance: $('#allowance').val(),
         flag: $('#flag').val(),
      },
      success: function (response) {
         document.getElementById("formInput").reset()
         const notifAlert = `
                              <div class="col-md-12 div`+ response.data.id + `">
                                 <div class="alert alert-`+ response.alert + `">` + response.message + `</div>
                              </div>
                           `
         $('#notif').append(notifAlert)
         setTimeout(function () {
            $('.div' + response.data.id).fadeOut('slow')
         }, 1500)
         displayData()
      },
      error: function (xhr) {
         var err = JSON.parse(xhr.responseText)
         var errorString = '<ul>'
         $.each(err.errors, function (key, value) {
            errorString += '<p>' + value + '</p>'
         })
         errorString += '</ul>'

         swal({
            type: "warning",
            title: errorString,
            showCancelButton: false,
            confirmButtonColor: "#e3342f",
            confirmButtonText: "Ok",
         })
      }
   })
});

$('#btnClear').on('click', function (e) {
   $('#idEdit').val('')
   document.getElementById("formInput").reset()
   accordionOne.classList.remove("show")
   accordionTwo.classList.add("show")
   // $('#icon').val('')
   // $('#url').val('')
   // $('#is_parent').val('')
   // $('#no_order').val('')
});

$('#btnAdd').on('click', function (e) {
   accordionOne.classList.add("show")
   accordionTwo.classList.remove("show")
});

function hapus(ev) {
   ev.preventDefault();
   let idMenu = ev.currentTarget.getAttribute('id');
   // let idMenu = getElementById('id');

   console.log(idMenu);
   swal({
      title: "Are you sure want to delete this data?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
   })

      // url: url,
      // data: data,
      // success: function (response) {
      //    document.getElementById("formInput").reset()
      //    const notifAlert = `

      .then((willDelete) => {
         if (willDelete) {
            $.ajax({
               headers: { 'X-CSRF-TOKEN': token },
               type: 'GET',
               url: "/jabatan/delete/" + idMenu,
               // data: idMenu,
               // data: { id: idMenu },
               success: function (response) {
                  // alert('halooo')
                  const notifAlert = `
                           <div class="col-md-12 div`+ response.data.id + `">
                              <div class="alert alert-`+ response.alert + `">` + response.message + `</div>
                           </div>
                     `
                  $('#notif').append(notifAlert)
                  setTimeout(function () {
                     $('.div' + response.data.id).fadeOut('slow')
                  }, 2500)
                  displayData()
               }
            })
         } else {
            swal("Hapus gagal.");
         }
      });
}
