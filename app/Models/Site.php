<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;


class Site extends Model
{

	public $timestamps = false;
	public $incrementing = false;
	
	
	protected $table = 'site';
   protected $primaryKey = 'id';
	protected $keyType = 'string';
	protected $casts = [
		'id' => 'string'
	];
	protected $fillable = [
		'id',
		'site_name',
		'contract',
		'valid_from',
		'valid_until',
		'site_address',
		'last_number_pkwt',
		'flag',
		'created_at'
	];

}
