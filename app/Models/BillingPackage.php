<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuid;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property Carbon|null $created_at
 * @property string|null $created_by
 * @property Carbon|null $updated_at
 * @property string|null $updated_by
 * @property Carbon|null $deleted_at
 * @property string|null $deleted_by
 * 
 * @property Collection|Administrator[] $administrators
 * @property Collection|Customer[] $customers
 *
 * @package App\Models
 */
class BillingPackage extends Model
{

	public $timestamps = false;
	public $incrementing = false; 
	
	use SoftDeletes, Uuid;
	

	protected $table = 'package_invoice';
	protected $primaryKey = 'uuid';
	protected $keyType = 'string';
	protected $casts = [
		'uuid' => 'string'
	];
	protected $fillable = [
		'uuid',
		'package_order_id',
		'invoice_number',
		'tax',
		'grand_total',
		'is_paid',
		'start_discount',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'deleted_at',
		'deleted_by',
	];

}
