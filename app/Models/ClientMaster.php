<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;


class ClientMaster extends Model
{

	public $timestamps = false;
	public $incrementing = false;
	
	
	protected $table = 'klien_master';
   protected $primaryKey = 'id';
	protected $keyType = 'string';
	protected $casts = [
		'id' => 'int'
	];
	protected $fillable = [
		'id',
		'client',
		'clientname',
		'no_kontrak',
    'validfrom',
    'validuntil',
    'flag',
    'last_number_pkwt'
	];

}
