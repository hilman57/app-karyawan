<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Prefix extends Model
{
    public $timestamps = false;
	public $incrementing = false;
	
	
	protected $table = 'prefix';
    protected $primaryKey = 'id';
	protected $keyType = 'string';
	protected $casts = [
		'id' => 'string'
	];
	protected $fillable = [
		'id',
		'site_id',
		'jabatan_id',
		'start_from',
		'last',
		
	];
}
