<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\PackageCategory;

class HelpdeskController extends Controller
{

	public function index()
	{
		if(request()->ajax())
		{
			$data = Package::join('package_category', 'package.package_category_id', '=', 'package_category.uuid')
							->select('package.*')
							->where('package_category.no_order', 1)
							->whereNull('package.deleted_at')
							->orderBy('package.name', 'ASC')->get();

			return datatables()->of($data)
				->addColumn('price', static function ($row) {
					return 'Rp. '.number_format($row->price, 2);
				})
				->addColumn('details', static function ($row) {
					$listDetail = '';
					foreach (json_decode($row->details) as $value) {
						$list = '<i class="bx bxs-circle"></i> '.$value.'<br>';
						$listDetail .= $list;
					}
					$list = $listDetail;
					return $list;
				})
				->addColumn('discount', static function ($row) {
					return !empty($row->discount) ? $row->discount.' %' : '-';
				})
				->addColumn('id', static function ($row) {
					return $row->uuid;
				})
				->rawColumns(['price', 'details', 'discount', 'id'])
				->make(true);
		}

		return view('package.helpdesk.index');
	}

	public function create()
	{
		return view('package.helpdesk.create');
	}

	public function store(Request $request)
	{
		$packageCategory = PackageCategory::where('no_order', 1)->first();
		
		//validasi data
		$this->validate($request, [
			'package_category_id' => 'string',
			'name' => 'string|max:100',
			'details' => 'array',
			'price' => 'integer',
			'discount' => 'nullable|integer',
			'start_discount' => 'nullable|date',
			'end_discount' => 'nullable|date',
			'final_price' => 'nullable|integer'
		]);

		$data = Package::create([
			'package_category_id' => $packageCategory->uuid,
			'name' => $request->name,
			'details' => json_encode($request->details),
			'price' => $request->price,
			'discount' => $request->discount,
			'start_discount' => $request->start_discount,
			'end_discount' => $request->end_discount,
			'final_price' => !empty($request->discount) ? $request->price - ($request->price * ($request->discount/100)) : $request->price,
			'created_at' => date("Y-m-d H:i:s"),
			'created_by' => session('sess_user')->name
		]);

		self::success('Package has been added.');

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function destroy($id)
	{
		$data = Package::where('uuid', $id)->first();
		$data->update([
			'deleted_by' => session('sess_user')->name
		]);
		$data->delete();

		self::danger('Package has been deleted.');
		return redirect()->back();
	}

   public function edit($id)
	{
		$data = Package::where('uuid', $id)->first();
		$details = json_decode($data->details);
		$countDetail = count($details); 
		return view('package.helpdesk.edit', compact('data', 'details', 'countDetail'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'package_category_id' => 'string',
			'name' => 'string|max:100',
			'details' => 'array',
			'price' => 'integer',
			'discount' => 'nullable|integer',
			'start_discount' => 'nullable|date',
			'end_discount' => 'nullable|date',
			'final_price' => 'nullable|integer'
		]);

		$data = Package::findOrFail($id);
	
		$data->update([
			'package_category_id' => $data->package_category_id,
			'name' => $request->name,
			'details' => json_encode($request->details),
			'price' => $request->price,
			'discount' => $request->discount,
			'start_discount' => $request->start_discount,
			'end_discount' => $request->end_discount,
			'final_price' => !empty($request->discount) ? $request->price - ($request->price * ($request->discount/100)) : $request->price,
			'updated_at' => date("Y-m-d H:i:s"),
			'updated_by' => session('sess_user')->name
		]);

		self::success('Package has been updated.');
		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
	}
}
