<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = Banner::whereNull('deleted_at')->orderBy('no_order', 'ASC')->get();

            return datatables()->of($data)
               ->addColumn('image', static function($row) {
                  $path = asset('storage/image/banner/'.$row->image);
                  if(!empty($row->image)){
                        return '<img src="'.$path.'" alt="'.$row->image.'" width="100%" style="max-width: 80px;"/>';
                	}else{
                        return '<img src="http://via.placeholder.com/50x50" alt="'.$row->title.'">';
                	}
               })
               ->addColumn('id', static function ($row) {
                  return $row->uuid;
               })
               ->rawColumns(['image', 'id'])
               ->make(true);
        }

    	return view('master.banner.index');
	}

   public function saveImage($name, $image)
   {

      $imageName = Str::slug($name) . time().'.' . $image->getClientOriginalExtension();
      $uploadPath = public_path('uploads/images/banner');
      $image->move($uploadPath,$imageName);
      return $imageName;
   }

	public function create()
	{
	    return view('master.banner.create');
	}

	public function store(Request $request)
    {

      //validasi data
      $this->validate($request, [
         'name' => 'string|max:100|unique:banner',
         'image' => 'required|mimetypes:text/plain,image/png,image/jpg,image/jpeg,image/svg',
         'no_order' => 'integer|unique:banner'
      ]);
         
         try {

            if ($request->hasFile('image')) {
               $image      = $request->file('image');
               $fileName   = time() . '.' . $image->getClientOriginalExtension();

               $img = Image::make($image->getRealPath());
               $img->resize(120, 120, function ($constraint) {
                  $constraint->aspectRatio();                 
               });

               $img->stream();

               Storage::disk('banner')->put($fileName, $img, 'public');
            }
            
            Banner::create([
               'name' => $request->name,
               'image' => $fileName,
               'no_order' => $request->no_order,
               'created_at' => date("Y-m-d H:i:s"),
               'created_by' => session('sess_user')->name
	        ]);

	        self::success('Banner has been added.');
	        return redirect(route('banner.index'));
	    } catch (\Exception $e) {
	        self::danger('Adding banner has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    {
		 $data = Banner::where('uuid', $id)->first();
       $data->update([
         'deleted_by' => session('sess_user')->name
       ]);
	    $data->delete();

	    self::danger('Banner has been deleted.');
	    return redirect()->back();
    }

   public function edit($id)
	{
	   $data = Banner::where('uuid', $id)->first();
	   return view('master.banner.edit', compact('data'));
	}

	public function update(Request $request, $id)
   {
      $this->validate($request, [
         'name' => 'string|max:100',
         'image' => 'mimetypes:text/plain,image/png,image/jpg,image/jpeg,image/svg',
         'no_order' => 'integer'
      ]);
         
         try {
            $data = Banner::where('uuid', $id)->first();

            if ($request->hasFile('image')) {

               // delete first then reupload
               Storage::disk('banner')->delete($data->image);

               $image      = $request->file('image');
               $fileName   = time() . '.' . $image->getClientOriginalExtension();

               $img = Image::make($image->getRealPath());
               $img->resize(120, 120, function ($constraint) {
                  $constraint->aspectRatio();                 
               });

               $img->stream();

               Storage::disk('banner')->put($fileName, $img, 'public');
            }

	         $data->update([
               'name' => $request->name,
               'image' => (!empty($request->hasFile('image'))) ? $fileName : $data->image,
               'no_order' => $request->no_order,
	            'updated_at' => date("Y-m-d H:i:s"),
	            'updated_by' => session('sess_user')->name
	         ]);
            
		   self::success('Banner has been updated.');
			return redirect(route('banner.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data banner failed to update.');
         return redirect()->back();
	    }
   }
}
