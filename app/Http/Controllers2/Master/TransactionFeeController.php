<?php

namespace App\Http\Controllers\Master;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TransactionFee;

class TransactionFeeController extends Controller
{

   public function index()
   {
      if(request()->ajax())
         {

         $data = TransactionFee::whereNull('deleted_at')->orderBy('name', 'ASC')->get();

            return datatables()->of($data)
               ->addColumn('value', static function ($row) {
                     if($row->is_percent == 1){
                        return $row->price.' %';
                     }else{
                        return $row->price;
                     }
               })
               ->addColumn('idFee', static function ($row) {
                  return $row->uuid;
               })
               ->rawColumns(['value', 'idFee'])
               ->make(true);
         }

      return view('master.fee.index');
   }

   public function save(Request $request)
   {
      //validasi data
      $this->validate($request, [
         'name' => 'string|max:100|unique:other_fee',
         'price' => 'integer',
         'is_percent' => 'integer'
      ]);
      
      $data = TransactionFee::create([
         'name' => $request->name,
         'price' => $request->price,
         'is_percent' => $request->is_percent,
         'created_at' => date("Y-m-d H:i:s"),
         'created_by' => session('sess_user')->name
      ]);

      return response()->json([
         'data' => $data,
         'success' => true,
         'alert' => 'success',
         'message' => 'Successfully add data'
      ]);
   }

   public function update(Request $request)
   {
      $this->validate($request, [
         'name' => 'string|max:100',
         'price' => 'integer',
         'is_percent' => 'integer'
      ]);
         
      $data = TransactionFee::where('uuid', $request->id)->first();
      $data->update([
         'name' => $request->name,
         'price' => $request->price,
         'is_percent' => $request->is_percent,
         'updated_at' => date("Y-m-d H:i:s"),
         'updated_by' => session('sess_user')->name
      ]);
      
      return response()->json([
         'data' => $data,
         'success' => true,
         'alert' => 'success',
         'message' => 'Successfully update data'
      ]);
   }

   public function delete(Request $request)
   {
      $data = TransactionFee::where('uuid', $request->id)->first();
      $data->update([
      'deleted_by' => session('sess_user')->name
      ]);
      $data->delete();

      return response()->json([
         'data' => $data,
         'success' => true,
         'alert' => 'danger',
         'message' => 'Successfully delete data'
      ]);
   }

   public function getDataFee(Request $request)
   {
      $data = TransactionFee::where('uuid', $request->id)->first();
      return response()->json($data);
   }
}
