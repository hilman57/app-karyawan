<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CompanyCategory;
use App\Models\HelpdeskCategory;

class CategoryController extends Controller
{

	public function index()
	{
		return view('master.category.index');
	}

	public function dataCompany()
	{
		if(request()->ajax())
		{

		$data = CompanyCategory::whereNull('deleted_at')->orderBy('name', 'ASC')->get();

			return datatables()->of($data)
				->addColumn('id', static function ($row) {
					return $row->uuid;
				})
				->rawColumns(['id'])
				->make(true);
		}

	}

   public function dataHelpdesk()
	{
		if(request()->ajax())
		{

		$data = HelpdeskCategory::whereNull('deleted_at')->orderBy('name', 'ASC')->get();

			return datatables()->of($data)
				->addColumn('id', static function ($row) {
					return $row->uuid;
				})
				->rawColumns(['id'])
				->make(true);
		}

	}

	public function saveCompanyCategory(Request $request)
	{
		//validasi data
		$this->validate($request, [
			'name' => 'string|max:100|unique:company_category',
			'description' => 'nullable|string',
		]);

		$data = CompanyCategory::create([
			'name' => $request->name,
			'description' => $request->description,
			'created_at' => date("Y-m-d H:i:s"),
			'created_by' => session('sess_user')->name
		]);

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function saveHelpdeskCategory(Request $request)
	{
		//validasi data
		$this->validate($request, [
			'name' => 'string|max:100|unique:helpdesk_category',
		]);

		$data = HelpdeskCategory::create([
			'name' => $request->name,
			'created_at' => date("Y-m-d H:i:s"),
			'created_by' => session('sess_user')->name
		]);

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function updateCompanyCategory(Request $request)
	{
		$this->validate($request, [
			'name' => 'string|max:100',
			'description' => 'nullable|string',
		]);
		
		$data = CompanyCategory::findOrFail($request->id);
	
		$data->update([
			'name' => $request->name,
			'description' => $request->description,
			'updated_at' => date("Y-m-d H:i:s"),
			'updated_by' => session('sess_user')->name
		]);

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
		
	}

	public function updateHelpdeskCategory(Request $request)
	{
		$this->validate($request, [
			'name' => 'string|max:100',
		]);

		$data = HelpdeskCategory::findOrFail($request->id);
	
		$data->update([
			'name' => $request->name,
			'updated_at' => date("Y-m-d H:i:s"),
			'updated_by' => session('sess_user')->name
		]);

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
	}

	public function deleteCompanyCategory(Request $request)
	{
		$data = CompanyCategory::where('uuid', $request->id)->first();
		$data->update([
			'deleted_by' => session('sess_user')->name
		]);
		$data->delete();

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'danger',
			'message' => 'Successfully delete data'
		]);
	}

	public function deleteHelpdeskCategory(Request $request)
	{
		$data = HelpdeskCategory::where('uuid', $request->id)->first();
		$data->update([
			'deleted_by' => session('sess_user')->name
		]);
		$data->delete();

		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'danger',
			'message' => 'Successfully remove data'
		]);
	}

	public function getDataCompanyCategory(Request $request)
   {
      $data = CompanyCategory::where('uuid', $request->id)->first();
      return response()->json($data);
   }

	public function getDataHelpdeskCategory(Request $request)
   {
      $data = HelpdeskCategory::where('uuid', $request->id)->first();
      return response()->json($data);
   }
}
