<?php

namespace App\Http\Controllers\Master;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bank;

class BankController extends Controller
{

   public function index()
   {
      if(request()->ajax())
         {

         $data = Bank::whereNull('deleted_at')->orderBy('name', 'ASC')->get();

            return datatables()->of($data)
                  ->addColumn('idBank', static function ($row) {
                     return $row->uuid;
                  })
                  ->rawColumns(['idBank'])
                  ->make(true);
         }

      return view('master.bank.index');
   }

   public function save(Request $request)
   {
      //validasi data
      $this->validate($request, [
         'name' => 'string|max:100|unique:bank',
      ]);
      
      $data = Bank::create([
         'name' => $request->name,
         'created_at' => date("Y-m-d H:i:s"),
         'created_by' => session('sess_user')->name
      ]);

      return response()->json([
         'data' => $data,
         'success' => true,
         'alert' => 'success',
         'message' => 'Successfully add data'
      ]);
      
   }

   public function update(Request $request)
   {
      $this->validate($request, [
         'name' => 'string|max:100|unique:bank'
      ]);
      
      $data = Bank::where('uuid', $request->id)->first();
      $data->update([
         'name' => $request->name,
         'updated_at' => date("Y-m-d H:i:s"),
         'updated_by' => session('sess_user')->name
      ]);
               
      return response()->json([
         'data' => $data,
         'success' => true,
         'alert' => 'success',
         'message' => 'Successfully update data'
      ]);
      
   }
   
   public function delete(Request $request)
   {
      $data = Bank::where('uuid', $request->id)->first();
      $data->update([
         'deleted_by' => session('sess_user')->name
      ]);
      $data->delete();

      return response()->json([
         'data' => $data,
         'success' => true,
         'alert' => 'danger',
         'message' => 'Successfully delete data'
      ]);
   }

   public function getDataBank(Request $request)
   {
      $data = Bank::where('uuid', $request->id)->first();
      return response()->json($data);
   }
}
