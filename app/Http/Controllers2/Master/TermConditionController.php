<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TermCondition;

class TermConditionController extends Controller
{

   public function index()
	{
      $data = TermCondition::whereNull('deleted_at')->first();

    	return view('master.term-condition.index', compact('data'));
	}

	public function create()
	{
	    return view('master.term-condition.create');
	}

	public function store(Request $request)
   {
      //validasi data
      $this->validate($request, [
         'text' => 'string',
      ]);
      
      try {
         
         TermCondition::create([
            'text' => $request->text,
            'created_at' => date("Y-m-d H:i:s"),
            'created_by' => session('sess_user')->name
         ]);

         self::success('Term & Condition has been added.');
         return redirect(route('term-condition.index'));
      } catch (\Exception $e) {
         self::danger('Adding Term & Condition has been failed.');
         return redirect()->back();
      }
   }

   public function edit($id)
	{
	   $data = TermCondition::where('uuid', $id)->first();
	   return view('master.term-condition.edit', compact('data'));
	}

	public function update(Request $request, $id)
   {
      $this->validate($request, [
         'text' => 'string',
      ]);
         
         try {
            $data = TermCondition::where('uuid', $id)->first();
	         $data->update([
               'text' => $request->text,
	            'updated_at' => date("Y-m-d H:i:s"),
	            'updated_by' => session('sess_user')->name
	         ]);
            
		   self::success('Term & Condition has been updated.');
			return redirect(route('term-condition.index'));
	    } catch (\Exception $e) {
	    	self::danger('Term & Condition failed to update.');
         return redirect()->back();
	    }
   }
}
