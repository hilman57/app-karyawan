<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('cekRole:superadmin, finance');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
      
       
        $Applicant = DB::table('mst_user')
                    ->select(DB::raw('count(mst_user.user_id) as user_count'))
                    ->where('id_status', '=', 1)
                    ->get();
        $Interview = DB::table('mst_user')
                    ->select(DB::raw('count(mst_user.user_id) as user_count'))
                    ->where('id_status', '=', 2)
                    ->get();
        $Considered = DB::table('mst_user')
                    ->select(DB::raw('count(mst_user.user_id) as user_count'))
                    ->where('id_status', '=', 3)
                    ->get();
        $Rejected = DB::table('mst_user')
                    ->select(DB::raw('count(mst_user.user_id) as user_count'))
                    ->where('id_status', '=', 4)
                    ->get();
        $Accepted = DB::table('mst_user')
                    ->select(DB::raw('count(mst_user.user_id) as user_count'))
                    ->where('id_status', '=', 5)
                    ->get();
       

       
       

        return view('home',compact('Applicant','Interview','Considered','Rejected','Accepted'));
    }
    public function getchart()
    {   
             
        
        $Applicant = DB::table('mst_user')
                    ->select()
                    ->where('id_status', '=', 1)
                    ->get();
        $Interview = DB::table('mst_user')
                    ->select()
                    ->where('id_status', '=', 2)
                    ->get();
        $Considered = DB::table('mst_user')
                    ->select()
                    ->where('id_status', '=', 3)
                    ->get();
        $Rejected = DB::table('mst_user')
                    ->select()
                    ->where('id_status', '=', 4)
                    ->get();
        $Accepted = DB::table('mst_user')
                    ->select()
                    ->where('id_status', '=', 5)
                    ->get();
        $data['Applicant'] = count($Applicant);
        $data['Interview'] = count($Interview);
        $data['Considered'] = count($Considered);
        $data['Rejected'] = count($Rejected);
        $data['Accepted'] = count($Accepted);
    //    dd($Applicant_c,$Candidate_c,$CreatePKWT_c,$Employee_c);
    return response()->json([
        'data' => $data,
        'success' => true,
        'alert' => 'success',
        'message' => 'Successfully update data'
    ]);

       
    }

    public function permissionError()
    {   
        return view('no-access');
    }
}
