<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\M_biodata;
use App\Models\User;
use App\Models\Role;
use App\Models\UserHasRole;
use App\Models\Package;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
// use Barryvdh\DomPDF\Facade\Pdf as PDF;
use Barryvdh\DomPDF\Facade as PDF;
use App\Helpers\AES;
use App\Models\Site;

class BiodataController extends Controller
{
    use AES;

    public function __construct()
    {
    }

    public function index()
    {
        $site = Site::all();
        $status = DB::table('status_kry')->get();
        return view('biodata.index', compact('site', 'status'));
    }

    public function dataActive()
    {
        $data = DB::table('mst_user')
            ->join('site', 'mst_user.id_site', '=', 'site.id')
            ->Join('position', 'mst_user.id_jabatan', '=', 'position.id')
            ->join('status_kry', 'mst_user.id_status', '=', 'status_kry.id')
            ->select('mst_user.*', 'site.site_name', 'position.position_name', 'status_kry.status_ky')
            ->get();
        return datatables()->of($data)
            ->addColumn('id', static function ($row) {
                return $row->user_id;
            })
            ->rawColumns(['id'])
            ->make(true);
    }
    public function detail($id)
    {
        $data = DB::table('mst_user')
            ->Join('site', 'mst_user.id_site', '=', 'site.id')
            ->Join('position', 'mst_user.id_jabatan', '=', 'position.id')
            ->leftJoin('tlk_gaji', 'mst_user.user_id', '=', 'tlk_gaji.uuid')
            ->leftJoin('status_kry', 'mst_user.id_status', '=', 'status_kry.id')
            ->where('user_id', '=', $id)
            ->select('mst_user.*', 'tlk_gaji.*', 'site.site_name', 'site.contract', 'position.position_name', 'status_kry.status_ky')
            ->get();
        $pgk = DB::table('mst_user')
            ->Join('pengalaman_kerja', 'mst_user.user_id', '=', 'pengalaman_kerja.id_png_kerja')
            ->select('pengalaman_kerja.*')
            ->where('mst_user.user_id', '=', $id)
            ->get();
        return view('biodata.detail', compact('data', 'pgk'));
    }

    public function tampildata($uuid)
    {
        $user = DB::table('mst_user')
            ->join('site', 'mst_user.id_site', '=', 'site.id')
            ->join('position', 'mst_user.id_jabatan', '=', 'position.id')
            ->where('mst_user.uuid', '=', $uuid)
            ->get();
        $pgk = DB::table('mst_user')
            ->join('pengalaman_kerja', 'mst_user.user_id', '=', 'pengalaman_kerja.id_png_kerja')
            ->select('pengalaman_kerja.*')
            ->where('mst_user.uuid', '=', $uuid)
            ->get();
        $data = array('user' => $user, 'pengalaman_kerja' => $pgk);
        return response()->json([
            'data' =>  $data,
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }

    public function activedetail($id)
    {
        $data = DB::table('mst_user')
            ->leftJoin('site', 'mst_user.id_site', '=', 'site.id')
            ->leftJoin('jabatan', 'mst_user.id_jabatan', '=', 'jabatan.id')
            ->get();
    }

    public function search_biodata(Request $request)
    {
        $data = DB::table('mst_user')
            ->select('mst_user.*', 'site.site_name', 'position.position_name', 'status_kry.status_ky')
            ->join('site', 'mst_user.id_site', '=', 'site.id')
            ->join('position', 'mst_user.id_jabatan', '=', 'position.id')
            ->join('status_kry', 'mst_user.id_status', '=', 'status_kry.id');
        if ($request->site) $data->where('mst_user.id_site', '=', $request->site);
        if ($request->jabatan) $data->where('mst_user.id_jabatan', $request->jabatan);
        if ($request->status) $data->where('mst_user.id_status', $request->status);
        $data = $data->get();
        return response()->json([
            'data' =>  $data,
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('biodata.create', compact('role'));
    }

    public function save(Request $request)
    {
        $check = User::where('email', $request->email)->first();
        if($check) {
            return response()->json([
                'data' => null,
                'success' => 0,
                'message' => 'Email is already exist!',
            ], 400);
        }
        $role = Role::where('name', $request->role)->first();
        $validate = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'email' => 'required|string|unique:user',
            'password' => 'required|min:6'
        ]);
        if ($validate->fails()) {
            $msg_error = null;
            $errors = json_decode($validate->errors());
            if (property_exists($errors, 'password')) {
                $msg_error .= implode(',', $errors->password);
            }
            if (property_exists($errors, 'email')) {
                $msg_error .= ($msg_error != null) ? ' || ' . implode(',', $errors->email) : '';
            }
            if (property_exists($errors, 'name')) {
                $msg_error .= ($msg_error != null) ? ' || ' . implode(',', $errors->name) : '';
            }
            return response()->json([
                'data' => null,
                'success' => 0,
                'message' => $msg_error,
            ], 400);
        }
        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'status' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'is_superadmin' => 0,
            'is_hide' => 0
        ]);
        $uuid = $user->uuid;
        $randomString = Str::random(30);
        $role = 'dc9c9e79-5ed1-4d0e-9717-73363bf45659';
        UserHasRole::create([
            'uuid'      => $randomString,
            'user_uuid' => $uuid,
            'role_uuid' => $role
        ]);
        return response()->json([
            'data' => $uuid,
            'success' => 1,
            'message' => 'Data success',
        ], 201);
    }

    public function destroy($id)
    {
        DB::table('mst_user')->where('user_id', '=', $id)->delete();
        self::danger('Menu has been deleted.');
        return redirect()->back();
    }

    public function destroy_pgk($id)
    {
        DB::table('pengalaman_kerja')->where('id', '=', $id)->delete();
        self::danger('Menu has been deleted.');
        return redirect()->back();
    }

    public function edit($id)
    {
        $data = DB::table('mst_user')
            ->leftJoin('tlk_gaji', 'mst_user.user_id', '=', 'tlk_gaji.uuid')
            ->Join('site', 'mst_user.id_site', '=', 'site.id')
            ->Join('position', 'mst_user.id_jabatan', '=', 'position.id')
            ->where('user_id', '=', $id)
            ->get();
        $jabatan = DB::table('position')
            ->join('mst_user', 'mst_user.id_jabatan', '=', 'position.id')
            ->join('site', 'site.id', '=', 'mst_user.id_site')
            ->where('mst_user.user_id', $id)
            ->get();
        $site = DB::table('site')
            ->get();
        $status = DB::table('status_kry')
            ->orderBy('status_kry.id', 'ASC')->get();

        if (request()->ajax()) {

            $data = DB::table('mst_user')
                ->Join('pengalaman_kerja', 'mst_user.user_id', '=', 'pengalaman_kerja.id_png_kerja')
                ->Join('tlk_gaji', 'mst_user.user_id', '=', 'tlk_gaji.uuid')
                ->Join('site', 'mst_user.id_site', '=', 'site.id')
                ->Join('position', 'mst_user.id_jabatan', '=', 'position.id')
                ->select('pengalaman_kerja.*')
                ->where('mst_user.user_id', '=', $id)
                ->get();

            return datatables()->of($data)
                ->addColumn('id', static function ($row) {
                    return $row->id;
                })
                ->rawColumns(['id'])
                ->make(true);
        }
        return view('biodata.edit', compact('data', 'jabatan', 'site', 'status'));
    }

    public function listPengalamanKerja($id)
    {
        if (request()->ajax()) {

            $data = DB::table('mst_user')
                ->Join('pengalaman_kerja', 'mst_user.user_id', '=', 'pengalaman_kerja.id_png_kerja')
                ->select('pengalaman_kerja.*')
                ->where('mst_user.user_id', '=', $id)
                ->get();

            return datatables()->of($data)
                ->addColumn('id', static function ($row) {
                    return $row->id;
                })
                ->rawColumns(['id'])
                ->make(true);
        }
    }

    public function update(Request $request)
    {
        $tunjangan = str_replace(",", "", $request->tunjangan);
        $gaji = str_replace(",", "", $request->gaji);
        $id = $request->user_id;
        $data = DB::table('tlk_gaji')
            ->where('tlk_gaji.uuid', '=', $id)
            ->get()->count();
        if ($data == 0 || $data == '') {
            DB::table('tlk_gaji')->insert([
                'uuid'                       => $id,
                'tunjangan'                  => $tunjangan,
                'gaji'                      => $gaji,
            ]);
        }
        DB::table('tlk_gaji')
            ->where('uuid', $id)
            ->update(
                [
                    'tunjangan'                 => $tunjangan,
                    'gaji'                      => $gaji,
                ]
            );
        DB::table('mst_user')
            ->where('user_id', $id)
            ->update(
                [
                    'nama_lengkap' => $request->nama,
                    'id_jabatan' => $request->jabatan,
                    'ktp' => $request->ktp,
                    'id_site' => $request->site,
                    'id_status' => $request->status_kry,
                    'aoc' => $request->aoc,
                    'nama_pasangan' => $request->nama_pasangan,
                    'alamat_keluarga_dihubungi' => $request->alamat_keluarga_dihubungi,
                    'nama_leader' => $request->nama_leader,
                    'npwp' => $request->npwp,
                    'tgl_npwp' => $request->tgl_npwp,
                    'tempat' => $request->tempat,
                    'alamat' => $request->alamat,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'kodepos' => $request->kodepos,
                    'telpon' => $request->telpon,
                    'hp' => $request->hp,
                    'agama' => $request->agama,
                    'email' => $request->email,
                    'pendidikan_terakhir' => $request->pendidikan_terakhir,
                    'nama_lembaga' => $request->nama_lembaga,
                    'status' => $request->status,
                    'email_ibu' => $request->email_ibu,
                    'pengalaman_kerja_nama_perusahaan' => $request->pengalaman_kerja_nama_perusahaan,
                    'pengalaman_kerja_nama_alamat_perusahaan' => $request->pengalaman_kerja_nama_alamat_perusahaan,
                    'pengalaman_kerja_jabatan' => $request->pengalaman_kerja_jabatan,
                    'pengalaman_kerja_masa_kerja' => $request->pengalaman_kerja_masa_kerja,
                    'pengalaman_kerja_alasan_keluar' => $request->pengalaman_kerja_alasan_keluar,
                    'pekerjaan_istri_suami' => $request->pekerjaan_istri_suami,
                    'jumlah_anak' => $request->jumlah_anak,
                    'berat_badan' => $request->berat_badan,
                    'nama_ibu_kandung' => $request->nama_ibu_kandung,
                    'tinggi_badan' => $request->tinggi_badan,
                    'rekening' => $request->rekening,
                    'nama_rekening' => $request->nama_rekening,
                    'cabang' => $request->cabang,
                    'nama_keluarga' => $request->nama_keluarga,
                    'hubungan_klg_dihub' => $request->hubungan_klg_dihub,
                    'alamat_lengkap' => $request->alamat_lengkap,
                    'keluarga_telpon' => $request->keluarga_telpon,
                    'keluarga_hp' => $request->keluarga_hp,
                    'kota' => $request->kota,
                    'kota_keluarga' => $request->kota_keluarga,
                    'instagram'                         => $request->instagram,
                    'facebook'                          => $request->facebook,
                    'linkedin'                          => $request->linkedin,
                ],
            );
        self::success('Biodata has been updated.');
        return redirect()->back()->withSuccess('Biodata has been updated.');
    }

    public function updt_pgk(Request $request)
    {
        $id = $request->id;
        $update = DB::table('pengalaman_kerja')
            ->where('id', $id)
            ->update(
                [
                    'namaPerusahaan' => $request->namaPerusahaan,
                    'jabatanKerja' => $request->jabatanKerja,
                    'alamatPerusahaan' => $request->alamatPerusahaan,
                    'masaKerja' => $request->masaKerja,
                    'alasanKeluar' => $request->alasanKeluar,
                ],
            );
        self::success('Biodata has been updated.');
        return response()->json([
            'data' => $update,
            'success' => true,
            'alert' => 'success',
            'message' => 'Successfully update data'
        ]);
    }



    public function store(Request $request)
    {
        $data = DB::table('mst_user')
            ->where('mst_user.user_id', '=', $request->noKTP)
            ->get()->count();
        if ($data == 1) {
            return response()->json([
                'message' => 'KTP Duplicate',
                'success' => 2
            ], 400);
        }
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            return response()->json(array(
                'message'    => $validator->errors()->toJson(),
                'data'       => [],
                'success'    => 0
            ), 400);
        }
        $tujuan_upload              = 'data_file';
        $tujuan_upload_cv           = 'data_file_cv';
        $tujuan_upload_ijazah       = 'data_file_ijazah';
        $tujuan_upload_skck         = 'data_file_skck';
        $tujuan_upload_dokumenLain  = 'data_file_dokumenLain';

        if ($request->hasFile('foto') === true) {
            $file       = $request->file('foto');
            $ext        = $file->getClientOriginalExtension();
            $isi_data   = $request->noKTP . '_foto.' . $ext;
            $file->move($tujuan_upload, $isi_data);
        } else {
            $isi_data = "";
        }
        if ($request->hasFile('skck') === true) {
            $skck            = $request->file('skck');
            $ext_skck        = $skck->getClientOriginalExtension();
            $isi_data_skck   = $request->noKTP . '_skck.' . $ext_skck;
            $skck->move($tujuan_upload_skck, $isi_data_skck);
        } else {
            $isi_data_skck = "";
        }
        if ($request->hasFile('cv') === true) {
            $cv             = $request->file('cv');
            $ext_cv          = $cv->getClientOriginalExtension();
            $isi_data_cv    = $request->noKTP . '_cv.' . $ext_cv;
            $cv->move($tujuan_upload_cv, $isi_data_cv);
        } else {
            $isi_data_cv = "";
        }
        if ($request->hasFile('ijazah') === true) {
            $ijazah           = $request->file('ijazah');
            $ext            = $ijazah->getClientOriginalExtension();
            $isi_data_ijazah  = $request->noKTP . '_ijazah.' . $ext;
            $ijazah->move($tujuan_upload_ijazah, $isi_data_ijazah);
        } else {
            $isi_data_ijazah = "";
        }
        if ($request->hasFile('dokumenLain') === true) {
            $dokumenLain           = $request->file('dokumenLain');
            $ext            = $dokumenLain->getClientOriginalExtension();
            $isi_data_dokumenLain  = $request->noKTP . '_doc_lain.' . $ext;
            $dokumenLain->move($tujuan_upload_dokumenLain, $isi_data_dokumenLain);
        } else {
            $isi_data_dokumenLain = "";
        }


        $file = base64_decode($request->tandaTangan);
        if ($file != null) {
            $imageData = $request->tandaTangan;
            $fileName = $request->noKTP . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            \Image::make($request->tandaTangan)->save(public_path('/data_file_ttd/') . $fileName);
        } else {
            $file = "";
        }


        $pgk = json_decode($request->pengalamanKerja);
        if ($pgk != null) {
            foreach ($pgk as  $data) {
                DB::table('pengalaman_kerja')->insert([
                    'id_png_kerja'                  => $request->noKTP,
                    'namaPerusahaan'                => $data->namaPerusahaan,
                    'alamatPerusahaan'              => $data->alamatPerusahaan,
                    'jabatanKerja'                  => $data->jabatanKerja,
                    'masaKerja'                     => $data->masaKerja,
                    'alasanKeluar'                  => $data->alasanKeluar,

                ]);
            }
        }

        $ins = DB::table('mst_user')->insert([
            'user_id'                           => $request->noKTP,
            'aoc'                               => $request->AOC,
            'agama'                             => $request->agama,
            'alamat'                            => $request->alamat,
            'alamat_lengkap'                    => $request->alamat,
            'alamat_keluarga_dihubungi'         => $request->alamatKeluargaDiHubungi,
            'berat_badan'                       => $request->bb,
            'cabang'                            => $request->cabangBank,
            'email'                             => $request->email,
            'email_ibu'                         => $request->emailIbu,
            'hubungan_klg_dihub'                => $request->hubunganKeluargaDiHubungi,
            'id_jabatan'                        => $request->jabatan,
            'jenis_kelamin'                     => $request->jk,
            'jumlah_anak'                       => $request->jumlahAnak,
            'kodepos'                           => $request->kodePos,
            'kota'                              => $request->kota,
            'kota_keluarga'                     => $request->kotaKeluargaDiHubungi,
            'nama_ibu_kandung'                  => $request->namaIbu,
            'nama_keluarga'                     => $request->namaKeluargaDiHubungi,
            'nama_leader'                       => $request->namaLeader,
            'nama_lembaga'                      => $request->namaLembaga,
            'nama_lengkap'                      => $request->namaLengkap,
            'nama_pasangan'                     => $request->namaPasangan,
            'nama_rekening'                     => $request->namaRekening,
            'hp'                                => $request->noHP,
            'keluarga_hp'                       => $request->noHpKeluargaDiHubungi,
            'ktp'                               => $request->noKTP,
            'npwp'                              => $request->noNPWP,
            'rekening'                          => $request->noRekening,
            'pekerjaan_istri_suami'             => $request->pekerjaanPasangan,
            'pendidikan_terakhir'               => $request->pendidikanTerakhir,
            'uuid'                              => $request->uuid,
            'status'                            => $request->status,
            'tgl_lahir'                         => $request->tanggalLahir,
            'tgl_npwp'                          => $request->tanggalNPWP,
            'tinggi_badan'                      => $request->tb,
            'telpon'                            => $request->telpRumah,
            'tempat'                            => $request->tempatLahir,
            'instagram'                         => $request->instagram,
            'facebook'                          => $request->facebook,
            'linkedin'                          => $request->linkedin,
            'created_job'                       => date('Y-m-d h:i:s'),
            'user_last_login'                   => date('Y-m-d h:i:s'),
            'user_forgot_passtime'              => date('Y-m-d h:i:s'),
            'user_created_date'                 => date('Y-m-d h:i:s'),
            'user_updated_date'                 => date('Y-m-d h:i:s'),
            'foto'                              => $isi_data,
            'skck'                              => $isi_data_skck,
            'cv'                                => $isi_data_cv,
            'ijazah'                            => $isi_data_ijazah,
            'dokumenLain'                       => $isi_data_dokumenLain,
            'id_site'                           => $request->site,
            'id_jabatan'                        => $request->posisiLamar,
            'tandaTangan'                       => $fileName,
        ]);
        return response()->json([
            'message' => 'Data success',
            'data' => $ins,
            'success' => 1
        ], 201);
    }


    public function getPengalamanKerja(Request $request)
    {
        $data = DB::table('pengalaman_kerja')
            ->where('id', $request->id)
            ->first();
        return response()->json($data);
    }

    public function getSite()
    {
        return response()->json([
            'message' => 'Data success',
            'data' => DB::table('site')->where('flag', 1)->get(),
            'success' => 1
        ], 200);
    }

    public function getPosition($site)
    {
        return response()->json([
            'message' => 'Data success',
            'data' => DB::table('position')->where('id_site', $site)->get(),
            'success' => 1
        ], 200);
    }

    public function cetak_pdf($id, $tanggal, $last_contract, $type)
    {
        /** get data user */
        $user = $this->getUser($id);
        /**
         * jika $last_contract value bni, maka _____________________, jika tidak $last_contract
         * jika $user->contract value kosong, maka _____________________, jika tidak $user->contract
         */
        $user->last_contract = ($last_contract == 'bni') ? '_____________________' : $last_contract;
        $user->contract = ($user->contract == '') ? '_____________________' : $user->contract;
        /**
         * data untuk dikirim ke compact
         */
        $param = array(
            'user'          => $user,
            'salary'        => self::Number2Word($user->gaji),
            'romans'        => self::RomansNumber(),
            'tgl_lahir'     => self::FormatTanggal($user->tgl_lahir),
            'valid_from'    => self::FormatTanggal($user->valid_from),
            'date_pkwt'     => self::FormatTanggal(($type != 3) ? $user->date_pkwt : $tanggal),
            'date_now'      => self::FormatTanggal(date('Y-m-d')),
            'day'           => self::FormatDay(date('l')),
            'break_month'   => explode(' ', self::FormatTanggal(date('Y-m-d'))),
            // 'efektif_kerja' => self::FormatTanggal(($type != 3) ? $user->date_pkwt : $tanggal),
            'singkatan'     => self::GenerateSingkatan($user->position_name)
        );
        /**
         * jika $user->date_pkwt value null, maka last_number_pkwt+1, jika tidak ambil last_number_pkwt dari db
         * jika $last_contract value bni, maka _______________, jika tidak FormatTanggal($last_contract)
         * jika $last_contract value bni, maka _______________, jika tidak DiffMonth(date('Y-m-d'), $last_contract)
         */
        $param['last_number_pkwt'] = $user->date_pkwt == null ? ((int)$user->last_number_pkwt + 1) : $user->last_number_pkwt;
        $param['valid_until'] = ($last_contract == 'bni') ? '_______________' : self::FormatTanggal($last_contract);
        $param['diff_month'] = ($last_contract == 'bni') ? '_______________' : self::DiffMonth(date('Y-m-d'), $last_contract);
        
        $folderSite = $this->pickSite($user->site_name);
        $folderJabatan = $this->pickJabatan($user->position_name);
        /** jika site bukan aseanindo/hsbc coll/dipo */
        if ($folderSite != 'staf_aseanindo' && $folderSite != 'hsbc_coll' && $folderSite != 'dipo') {
            if ($type == 1 && $folderSite != '' && $folderJabatan != '') {
                $file = 'biodata.pkwt.' . $folderSite . '.' . $folderJabatan . '.kerahasiaan_karyawan';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_kerahasiaan_karyawan.pdf');
            } else if ($type == 2 && $folderSite != '' && $folderJabatan != '') {
                $file = 'biodata.pkwt.' . $folderSite . '.' . $folderJabatan . '.ketentuan_operasional_kerja';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_ketentuan_operasional_kerja.pdf');
            } else if ($type == 3 && $folderSite != '' && $folderJabatan != '') {
                $file = 'biodata.pkwt.' . $folderSite . '.' . $folderJabatan . '.pkwt';
                $pdf = PDF::loadView($file, compact('param'));
                $this->incLastPKWT($user);
                $this->insertDatePKWTAndLastContract($user, $tanggal, $last_contract);
                return $pdf->download($user->ktp . '_' . $folderSite . '_pkwt.pdf');
            } else {
                echo "Selain site aseanindo & hsbc collection hanya boleh memilih type Kerahasiaan Karyawan, Ketentuan Operasional Kerja & PKWT!";
            }
        /** jika site hsbc coll/dipo */
        } else if ($folderSite == 'hsbc_coll' || $folderSite == 'dipo') {
            if ($type == 1 && $folderSite != '') {
                if ($folderJabatan == 'data_entry') $file = 'biodata.pkwt.' . $folderSite . '.' . $folderJabatan . '.kerahasiaan_karyawan';
                else $file = 'biodata.pkwt.' . $folderSite . '.kerahasiaan_karyawan';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_kerahasiaan_karyawan.pdf');
            } else if ($type == 2 && $folderSite != '') {
                if ($folderJabatan == 'data_entry') $file = 'biodata.pkwt.' . $folderSite . '.' . $folderJabatan . '.ketentuan_operasional_kerja';
                else $file = 'biodata.pkwt.' . $folderSite . '.ketentuan_operasional_kerja';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_ketentuan_operasional_kerja.pdf');
            } else if ($type == 3 && $folderSite != '') {
                if ($folderJabatan == 'data_entry') $file = 'biodata.pkwt.' . $folderSite . '.' . $folderJabatan . '.pkwt';
                else $file = 'biodata.pkwt.' . $folderSite . '.pkwt';
                $pdf = PDF::loadView($file, compact('param'));
                $this->incLastPKWT($user);
                $this->insertDatePKWTAndLastContract($user, $tanggal, $last_contract);
                return $pdf->download($user->ktp . '_' . $folderSite . '_pkwt.pdf');
            } else if ($type == 8 && $folderSite != '') {
                $file = 'biodata.pkwt.' . $folderSite . '.kode_etik_penagihan';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_kode_etik_penagihan.pdf');
            } else if ($type == 9 && $folderSite != '') {
                $file = 'biodata.pkwt.' . $folderSite . '.letter_of_undertaking';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_letter_of_undertaking.pdf');
            } else {
                echo "Site HSBC Collection hanya boleh memilih type Kerahasiaan Karyawan, Ketentuan Operasional Kerja, PKWT, Kode Etik Penagihan & Letter Of Undertaking!";
            }
        /** jika site aseanindo */
        } else {
            $param['singkatan_site'] =  self::GenerateSingkatan($user->site_name);
            if ($type == 1) {
                $file = 'biodata.pkwt.staf_aseanindo.perjanjian_kerahasiaan_karyawan';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_perjanjian_kerahasiaan_karyawan.pdf');
            } else if ($type == 3) {
                $file = 'biodata.pkwt.staf_aseanindo.pkwt';
                $pdf = PDF::loadView($file, compact('param'));
                $this->incLastPKWT($user);
                $this->insertDatePKWTAndLastContract($user, $tanggal, $last_contract);
                return $pdf->download($user->ktp . '_' . $folderSite . '_pkwt.pdf');
            } else if ($type == 4) {
                $file = 'biodata.pkwt.staf_aseanindo.addendium_pertama';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_addendium_pertama.pdf');
            } else if ($type == 5) {
                $file = 'biodata.pkwt.staf_aseanindo.lampiran_b';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_lampiran_b.pdf');
            } else if ($type == 6) {
                $file = 'biodata.pkwt.staf_aseanindo.lampiran_c';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_lampiran_c.pdf');
            } else if ($type == 7) {
                $file = 'biodata.pkwt.staf_aseanindo.surat_lampiran';
                $pdf = PDF::loadView($file, compact('param'));
                return $pdf->download($user->ktp . '_' . $folderSite . '_surat_lampiran.pdf');
            } else {
                echo 'Aseanindo tidak ada cetak KOK.';
            }
        }
    }

    protected function getUser($id)
    {
        $result = DB::table('mst_user')
            ->Join('tlk_gaji', 'mst_user.user_id', '=', 'tlk_gaji.uuid')
            ->Join('site', 'mst_user.id_site', '=', 'site.id')
            ->Join('position', 'mst_user.id_jabatan', '=', 'position.id')
            ->where('user_id', '=', $id)
            ->select('*', 'mst_user.id_site')
            ->first();
        return $result;
    }

    protected function incLastPKWT($user)
    {
        if($user->date_pkwt == null) {
            // $site = DB::table('site')->where('id', $user->id_site)->first();
            DB::table('site')->where('id', $user->id_site)->where('site_name', $user->site_name)->update(['last_number_pkwt' => $user->last_number_pkwt + 1]);
        }
    }

    protected function insertDatePKWTAndLastContract($user, $tanggal, $last_contract)
    {
        $last_contract = ($last_contract == 'bni') ? NULL : $last_contract;
        DB::table('mst_user')->where('user_id', $user->user_id)->update(['date_pkwt' => $tanggal, 'last_contract' => $last_contract]);
    }

    protected function pickSite($site)
    {
        $folder = '';
        // hsbc tele
        if (is_numeric(strpos(strtolower($site), 'coll'))) $folder = 'hsbc_coll';
        // hsbc coll
        if (is_numeric(strpos(strtolower($site), 'tele'))) $folder = 'hsbc_tele';
        // dipostar
        if (is_numeric(strpos(strtolower($site), 'dipo'))) $folder = 'dipo';
        // bni tele
        if (is_numeric(strpos(strtolower($site), 'bni'))) $folder = 'bni';
        // axa
        if (is_numeric(strpos(strtolower($site), 'axa'))) $folder = 'axa';
        // aseaindo
        if (is_numeric(strpos(strtolower($site), 'aseanindo'))) $folder = 'staf_aseanindo';
        // abinaya
        if (is_numeric(strpos(strtolower($site), 'abinaya'))) $folder = 'abinaya';
        return $folder;
    }

    protected function pickJabatan($jabatan)
    {
        $folder = '';
        if (is_numeric(strpos(strtolower($jabatan), 'account'))) $folder = 'am';
        if (is_numeric(strpos(strtolower($jabatan), 'manager'))) $folder = 'mgr';
        if (is_numeric(strpos(strtolower($jabatan), 'supervisor')) || is_numeric(strpos(strtolower($jabatan), 'spv'))) $folder = 'spv';
        if (is_numeric(strpos(strtolower($jabatan), 'tele'))) $folder = 'tsa';
        if (is_numeric(strpos(strtolower($jabatan), 'desk'))) $folder = 'tsa';
        if (is_numeric(strpos(strtolower($jabatan), 'admin'))) $folder = 'adm';
        if (is_numeric(strpos(strtolower($jabatan), 'staff'))) $folder = '';
        if (is_numeric(strpos(strtolower($jabatan), 'qual'))) $folder = 'qc';
        if (is_numeric(strpos(strtolower($jabatan), 'data'))) $folder = 'data_entry';
        return $folder;
    }
}
