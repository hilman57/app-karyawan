<?php

namespace App\Http\Controllers;

use App\Models\Prefix;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrefixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
	{   $site = DB::table('site')->get();
        $jabatan = DB::table('jabatan')->get();

		if(request()->ajax())
		{
            $data = DB::table('prefix')
            ->Join('site', 'prefix.site_id', '=', 'site.id')
            ->Join('jabatan', 'prefix.jabatan_id', '=', 'jabatan.id')
            ->get();
        // $data = Prefix::orderBy('id', 'ASC')->get();
			return datatables()->of($data)
				->make(true);
		}

		return view('prefix.index',compact('site','jabatan'));
	}

	// public function create()
	// {
    //     return view('prefix.create');
	// }

	public function save(Request $request)
	{
        // dd($request->site_id);
		$menu = Prefix::create([
			'site_id' => $request->site_id,			
			'jabatan_id' => $request->jabatan_id,			
			'start_from' => $request->start_from,			
			'last' => $request->last,			
		]);		

		return response()->json([
			'data' => $menu,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function delete(Request $request)
	{
        // dd($request);
      	$menu = Prefix::where('id', $request->id)->delete();
	
		return response()->json([
         'data' => $menu,
         'success' => true,
         'alert' => 'danger',
         'message' => 'Successfully delete data'
      ]);
	}

   public function edit($id)
	{
        // dd($id); 
       $menu2 = Prefix::where('id', $id)->first();
       $menu = DB::table('prefix')          
                ->where('prefix.id','=',$id)
                ->get();

	   return view('Prefix.edit', compact('menu'));
	}

	public function update(Request $request)
	{
        

		$menu = Prefix::findOrFail($request->id);
	
		$menu->update([
			'site_id' => $request->site_id,
			'jabatan_id' => $request->jabatan_id,
			'start_from' => $request->start_from,
			'last' => $request->last,
			
		]);

		
        self::success('Update has been saved.');
        return view('Prefix.index');
	}

	
    
    public function editjs(Request $request)
    {
        // dd($request->id);
        $menu = Prefix::where('id', $request->id)->first();
        // dd($menu);
        return view('Prefix.index', compact('menu'));
     }
}
