<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Jabatan;
use App\Models\Menu;
use App\Models\Site;
use Illuminate\Support\Facades\DB;

class JabatanController extends Controller
{

	public function index()
	{
		if (request()->ajax()) {

			// $data = Jabatan::orderBy('id', 'ASC')->get();
			$data = DB::table('position')
							->join('site', 'site.id', '=', 'position.id_site')
							->select('position.*', 'site.site_name')
							->get();
			return datatables()->of($data)->make(true);
		}
		$site = Site::where('flag', 1)->get();
		return view('jabatan.index', compact('site'));
	}

	public function getDataBySite($site) {
		return response()->json([
			'data' => DB::table('position')->where('id_site', $site)->get(),
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function create()
	{
		return view('utilitas.menu.create');
	}

	public function save(Request $request)
	{
		$position = Jabatan::create([
			'id_site' 			=> $request->id_site,
			'position_name' => $request->position_name,
            'contract' => ($request->contract) ? $request->contract : NULL,
			// 'salary' 				=> $request->salary,
			// 'allowance' 		=> $request->allowance,
			'flag' 					=> $request->flag,
		]);
		return response()->json([
			'data' => $position,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function delete(Request $request)
	{
		$menu = Jabatan::where('id', $request->id)->delete();
		return response()->json([
			'data' => $menu,
			'success' => true,
			'alert' => 'danger',
			'message' => 'Successfully delete data'
		]);
	}

	public function edit($id)
	{
		$detail = DB::table('position')->where('position.id', '=', $id)->first();
		$site = Site::where('flag', 1)->get();
		return view('jabatan.edit', compact('detail', 'site'));
	}

	public function update(Request $request)
	{
        // dd($request);
		$position = Jabatan::findOrFail($request->id);
		$position->update([
			'id_site' 			=> $request->site,
			'position_name' => $request->position_name,
            'contract' => ($request->contract) ? $request->contract : NULL,
		//	'salary' 				=> $request->salary,
		//	'allowance' 		=> $request->allowance,
			'flag' 					=> $request->flag,
		]);
		self::success('Update has been saved.');
		return redirect('/jabatan');
	}

	public function getDataMenu(Request $request)
	{
		$data = Menu::where('uuid', $request->id)->first();
		return response()->json($data);
	}

	public function editjs(Request $request)
	{
		$menu = Jabatan::where('id', $request->id)->first();
		return view('jabatan.index', compact('menu'));
	}
}
