<?php

namespace App\Http\Controllers\Auth;

use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\Menu;
use App\Models\SubMenu;
use Auth;
use Illuminate\Support\Facades\Route;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $user = User::select('user.*', 'role.name as nama_role')
            ->join('user_has_role', 'user.uuid', '=', 'user_has_role.user_uuid')
            ->join('role', 'role.uuid', '=', 'user_has_role.role_uuid')
            ->whereNull('user.deleted_at')
            ->where('user.email', $request->email)
            ->first();
        if (!$user) {
            self::danger('Email is not existed.');
            return redirect()->back();
        }

        $menu = Menu::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenu::join('menu', 'submenu.menu_id', '=', 'menu.uuid')
            ->select('submenu.*')
            ->where('status', 1)
            ->orderBy('no_order', 'ASC')
            ->get();

        $query = "
                SELECT b.menu_name FROM 
                role_has_permission a
                JOIN permission b ON a.permission_id = b.uuid
                LEFT JOIN role c ON a.role_id = c.uuid
                LEFT JOIN user_has_role d ON c.uuid = d.role_uuid
                LEFT JOIN user e ON d.user_uuid = e.uuid
                WHERE e.email = '" . $user->email . "'
                GROUP BY b.menu_name
                ";

        $userMenu = DB::select(DB::raw($query));

        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);


        if ($user) {
            if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
                if ($user->nama_role != 'agent') {
                    Session::put('sess_menu', $menu);
                    Session::put('sess_submenu', $submenu);
                    Session::put('sess_usermenu', $userMenu);
                    Session::put('sess_user', $user);
                    self::success('Successfully login. Welcome back, ' . $user->name . '!');
                    return redirect()->intended('/home');
                } else {
                    Session::flush();
                    Session::regenerate();
                    Auth::logout();
                    self::danger('Agent cannot login');
                    return redirect('/login');
                }
            } else {
                self::danger('Wrong email or password.');
                return redirect()->back();
            }
        } else {
            self::danger('User is not existed.');
            return redirect()->back();
        }
    }

    public function apilogin(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // dd($credentials);   
        $user = User::select('user.*', 'role.name as nama_role')
            ->join('user_has_role', 'user.uuid', '=', 'user_has_role.user_uuid')
            ->join('role', 'role.uuid', '=', 'user_has_role.role_uuid')
            ->whereNull('user.deleted_at')
            ->where('user.email', $request->email)
            ->first();

        if (!$user) {
            return response()->json([
                'success' => 2,
                'message' => 'Email is not existed.',
            ], 400);
        }

        $menu = Menu::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenu::join('menu', 'submenu.menu_id', '=', 'menu.uuid')
            ->select('submenu.*')
            ->where('status', 1)
            ->orderBy('no_order', 'ASC')
            ->get();

        $query = "
                SELECT b.menu_name FROM 
                role_has_permission a
                JOIN permission b ON a.permission_id = b.uuid
                LEFT JOIN role c ON a.role_id = c.uuid
                LEFT JOIN user_has_role d ON c.uuid = d.role_uuid
                LEFT JOIN user e ON d.user_uuid = e.uuid
                WHERE e.email = '" . $user->email . "'
                GROUP BY b.menu_name
                ";

        $userMenu = DB::select(DB::raw($query));

        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);



        if ($user) {
            if (auth()->attempt($credentials, ['email' => $request->email, 'password' => $request->password])) {
                $date = date('Y-m-d h:i:s');
                $id = $user->uuid;
                DB::table('user')
                    ->where('uuid', $id)
                    ->update(
                        ['updated_at' => $date],
                    );

                // dd($user->nama_role == 'agent');
                if ($user->nama_role != 'agent') {
                    Session::put('sess_menu', $menu);
                    Session::put('sess_submenu', $submenu);
                    Session::put('sess_usermenu', $userMenu);
                    Session::put('sess_user', $user);
                    self::success('Successfully login. Welcome back, ' . $user->name . '!');
                    return redirect()->intended('/home');
                } else {
                    // dd('haloo');
                    $date = date('Y-m-d h:i:s');
                    $id = $user->uuid;
                    DB::table('user')
                        ->where('uuid', $id)
                        ->update(
                            ['updated_at' => $date],
                        );
                    //    test
                    // dd($user);
                    try {
                        if (!$token = JWTAuth::attempt($credentials)) {
                            return response()->json([
                                'success' => false,
                                'message' => 'Login credentials are invalid.',
                            ], 400);
                        }
                    } catch (JWTException $e) {
                        return $credentials;
                        return response()->json([
                            'success' => false,
                            'message' => 'Could not create token.',
                        ], 500);
                    }




                    return response()->json([
                        'data' => array(
                            'id'    => $user->uuid,
                            'token' => $token,
                            'email' => $user->email
                        ),
                        'success' => 1,
                        'message' => 'Data success',
                    ], 201);
                }
            } else {
                return response()->json([
                    'success' => 2,
                    'message' => 'Wrong email or password',
                ], 400);
            }
        } else {
            return response()->json([
                'success' => 2,
                'message' => 'User is not existed.',
            ], 400);
        }
    }

    public function logout(Request $request)
    {
        Session::flush();
        Session::regenerate();
        Auth::logout();
        self::danger('Successfully logout from system');
        return redirect('/login');
    }
}
