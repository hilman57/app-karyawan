<?php

namespace App\Http\Controllers\Utilitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\SubMenu;
use App\Models\Role;
use App\Models\UserHasRole;
use App\Models\Permission;
use App\Models\RoleHasPermission;
use DB;
use Illuminate\Support\Facades\Hash;
class UserAdminController extends Controller
{
    public function index()
    {   
        $role = Role::orderBy('name', 'ASC')->get();
        $roles = Role::orderBy('name', 'ASC')->get();
        // dd('test')
        // dd(request()->ajax());
        if(request()->ajax())
        {

            $data = User::whereNull('deleted_at')
                        ->whereNotIn('uuid', ['b7320627-cea9-4a13-9c49-6ed53c7d5493','8ba3dff5-487c-450a-aa12-788e0c348b8a'])           
                       ->orderBy('created_at', 'DESC')->get();

            return datatables()->of($data)
                ->addColumn('role', static function ($row) {
                    // dd($row);
                    $roleName = UserHasRole::where('user_uuid', $row->uuid)->first();
                    // dd($row->uuid);
                    // dd($roleName->role_uuid);
                    $data = "dc9c9e79-5ed1-4d0e-9717-73363bf45659";
                    $role = Role::where('uuid', $data)->first();
                    // $role = Role::where('uuid', $roleName->role_uuid)->first();
                   
                    // dd($role->name);
                    return '<span class="badge bg-info">'.$role->name.'</span>';
                })
                
                ->addColumn('status', static function ($row) {
                    // dd($row);
                    if($row->status == 1){
                        return '<span class="badge bg-success">Active</span>';
                    }else{
                        return '<span class="badge bg-warning">Suspend</span>';
                    }
                })
                ->addColumn('idUser', static function ($row) {
                    // dd($row->uuid);
                    return $row->uuid;
                })
                ->rawColumns(['role', 'status', 'idUser'])
                ->make(true);
        }

        return view('utilitas.useradmin.index', compact('role', 'roles'));
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('utilitas.useradmin.create', compact('role'));
    }

    public function save(Request $request)
    {
        // dd($request->all());
        $role = Role::where('name', $request->role)->first();
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|string|unique:user',
            'password' => 'required|min:6',
            'role' => 'required|string|exists:role,name'
        ]);

        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'status' => 0,
            'is_superadmin' => 0,
            'is_hide' => 0
        ]);

        UserHasRole::create([
            'user_uuid' => $user->uuid,
            'role_uuid' => $role->uuid
        ]);

        return response()->json([
			'data' => $user,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
    }

    public function edit($id)
    {
        
        $user = User::where('uuid', $id)->first();
        return view('utilitas.useradmin.edit', compact('user'));
    }

    public function update(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|string|exists:user,email',
            'password' => 'nullable|min:6',
            'status' => 'required|integer',
        ]);

        $user = User::findOrFail($request->id);
        $password = !empty($request->password) ? bcrypt($request->password):$user->password;
        $user->update([
            'name' => $request->name,
            'password' => $password,
            'status' => $request->status
        ]);
        
        return response()->json([
			'data' => $user,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
    }

    public function delete(Request $request)
    {
        // dd($request->id);
        // $user = User::findOfFail($request->id);

        // $user->update([
            // 	'deleted_by' => session('sess_user')->name
            // ]);
            // $user->delete();
            
            // return response()->json([
            //     'data' => $user,
            //     'success' => true,
            //     'alert' => 'success',
            //     'message' => 'Successfully delete data'
            // ]);
        $user = DB::table('user')->where('uuid', '=', $request->id)->delete();
        self::danger('Menu has been deleted.');
		return redirect()->back();
    }

    public function roles(Request $request, $id)
    {
        $user = User::where('uuid', $id)->first();
        $roles = Role::all()->pluck('name');
        $userHasRole = UserHasRole::where('user_uuid', $user->uuid)->first();
        $roleName = Role::where('uuid', $userHasRole->role_uuid)->first()->name;
        return view('utilitas.useradmin.roles', compact('user', 'roles', 'roleName'));
    }

    public function setRole(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);
        
        $user = User::findOrFail($id);
        $role = Role::where('name', $request->role)->first(); 
        $updateRole = UserHasRole::where('user_uuid', $user->uuid)->first();;
        $updateRole->update([
            'user_uuid' => $user->uuid,
            'role_uuid' => $role->uuid
        ]);

        self::success('Data role berhasil diset.');
        return redirect()->back();
    }

    public function updateRole(Request $request)
    {
        $this->validate($request, [
            'roleUpdated' => 'required'
        ]);
        
        $user = User::findOrFail($request->id);
        $role = Role::where('uuid', $request->roleUpdated)->first(); 

        $updateRole = UserHasRole::where('user_uuid', $user->uuid)->first();;
        $updateRole->update([
            'user_uuid' => $user->uuid,
            'role_uuid' => $role->uuid
        ]);

        return response()->json([
			'data' => $updateRole,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update role'
		]);
    }

    public function rolePermission(Request $request)
    {
        $submenu= SubMenu::join('menu', 'submenu.menu_id', '=', 'menu.uuid')
                        ->select('submenu.*', 'menu.name as menu_name')
                        ->orderBy('menu.name', 'ASC')
                        ->orderBy('no_order', 'ASC')
                        ->get();
        $role = $request->get('role');

        $permissions = null;
        $hasPermission = null;
        
        $roles = Role::all()->pluck('name');
        
        if (!empty($role)) {
            $getRole = Role::where('name', $role)->first();
            
            $hasPermission = DB::table('role_has_permission')
            ->select('permission.name')
            ->join('permission', 'role_has_permission.permission_id', '=', 'permission.uuid')
            ->where('role_id', $getRole->uuid)->get()->pluck('name')->all();

            $permissions = Permission::orderBy('name', 'ASC')->pluck('name')->all();
        }
        return view('utilitas.useradmin.role_permission', compact('roles', 'permissions', 'hasPermission', 'submenu'));
    }

    public function addPermission(Request $request)
    { 
        $this->validate($request, [
            'name' => 'required|string|unique:permission'
        ]);

        $explodeMenu = explode('-', $request->name);
        
        $permission = Permission::firstOrCreate([
            'name' => $request->name,
            'menu_name' => $explodeMenu[0]
        ]);

        self::success('Data permission berhasil ditambahkan.');
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        $role = Role::where('name', $role)->first();
        $dataPermission = RoleHasPermission::where('role_id', $role->uuid)->get();

        if($request->has('permission')){
            if(count($dataPermission) == 0){
                foreach($request->permission as $data){
                    $permission = Permission::where('name', $data)->first();
                    RoleHasPermission::create([
                        'permission_id' => $permission->uuid,
                        'role_id' => $role->uuid
                    ]); 
                }
            }else{
                RoleHasPermission::where('role_id', $role->uuid)->delete();
                foreach($request->permission as $data){
                    $permission = Permission::where('name', $data)->first();
                    RoleHasPermission::create([
                        'permission_id' => $permission->uuid,
                        'role_id' => $role->uuid
                    ]); 
                }
            }
        }else{
            RoleHasPermission::where('role_id', $role->uuid)->delete();
        }


        self::success('Data permission berhasil disimpan.');
        return redirect()->back();
    }

    public function getData2(Request $request)
	{
        $data = User::select('user.*', 'role.name as roleName', 'role.uuid as idRole')
                ->join('user_has_role', 'user.uuid', '=', 'user_has_role.user_uuid')
                ->join('role', 'role.uuid', '=', 'user_has_role.role_uuid')
                ->whereNull('user.deleted_at')
                ->where('user.uuid', $request->id)
                ->first();
		return response()->json($data);
	}

    public function cekEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();
            if($user){
                return response()->json(true);
            }else{
                return response()->json(false);
            }
    }

    public function cekPassword(Request $request)
    {   
        $user = User::where('email', $request->email)->first();
        if(Hash::check($request->password , $user->password)){
            return response()->json(true);
        }else{
            return response()->json(false);
        }
    }

}
