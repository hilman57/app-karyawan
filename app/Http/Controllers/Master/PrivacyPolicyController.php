<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PrivacyPolicy;

class PrivacyPolicyController extends Controller
{

   public function index()
	{
      $data = PrivacyPolicy::whereNull('deleted_at')->first();

    	return view('master.privacy-policy.index', compact('data'));
	}

	public function create()
	{
	    return view('master.privacy-policy.create');
	}

	public function store(Request $request)
   {
      //validasi data
      $this->validate($request, [
         'text' => 'string',
      ]);
      
      try {
         
         PrivacyPolicy::create([
            'text' => $request->text,
            'created_at' => date("Y-m-d H:i:s"),
            'created_by' => session('sess_user')->name
         ]);

         self::success('Privacy Policy has been added.');
         return redirect(route('privacy-policy.index'));
      } catch (\Exception $e) {
         self::danger('Adding Privacy Policy has been failed.');
         return redirect()->back();
      }
   }

   public function edit($id)
	{
	   $data = PrivacyPolicy::where('uuid', $id)->first();
	   return view('master.privacy-policy.edit', compact('data'));
	}

	public function update(Request $request, $id)
   {
      $this->validate($request, [
         'text' => 'string',
      ]);
         
         try {
            $data = PrivacyPolicy::where('uuid', $id)->first();
	         $data->update([
               'text' => $request->text,
	            'updated_at' => date("Y-m-d H:i:s"),
	            'updated_by' => session('sess_user')->name
	         ]);
            
		   self::success('Privacy Policy has been updated.');
			return redirect(route('privacy-policy.index'));
	    } catch (\Exception $e) {
	    	self::danger('Privacy Policy failed to update.');
         return redirect()->back();
	    }
   }
}
