<?php

namespace App\Http\Controllers\Master;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;

class FaqController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = Faq::whereNull('deleted_at')->orderBy('question', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('id', static function ($row) {
                    return $row->uuid;
                })
                ->rawColumns(['id'])
                ->make(true);
        }

    	return view('master.faq.index');
	}

	public function create()
	{
	    return view('master.faq.create');
	}

	public function store(Request $request)
    {
        //validasi data
      $this->validate($request, [
         'question' => 'string',
         'answer' => 'string',
      ]);
         
         try {
            
            Faq::create([
               'question' => $request->question,
               'answer' => $request->answer,
               'created_at' => date("Y-m-d H:i:s"),
               'created_by' => session('sess_user')->name
	        ]);

	        self::success('Faq has been added.');
	        return redirect(route('faq.index'));
	    } catch (\Exception $e) {
	        self::danger('Adding faq has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    {
		 $data = Faq::where('uuid', $id)->first();
       $data->update([
         'deleted_by' => session('sess_user')->name
       ]);
	    $data->delete();

	    self::danger('Faq has been deleted.');
	    return redirect()->back();
    }

   public function edit($id)
	{
	   $data = Faq::where('uuid', $id)->first();
	   return view('master.faq.edit', compact('data'));
	}

	public function update(Request $request, $id)
   {
      $this->validate($request, [
         'question' => 'string',
         'answer' => 'string'
      ]);
         
         try {
            $data = Faq::where('uuid', $id)->first();
	         $data->update([
               'answer' => $request->answer,
               'question' => $request->question,
	            'updated_at' => date("Y-m-d H:i:s"),
	            'updated_by' => session('sess_user')->name
	         ]);
            
		   self::success('Faq has been updated.');
			return redirect(route('faq.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data faq failed to update.');
         return redirect()->back();
	    }
   }
}
