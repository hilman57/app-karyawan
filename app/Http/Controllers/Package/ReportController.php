<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\PackageCategory;

class ReportController extends Controller
{

	public function index()
	{
		if(request()->ajax())
		{
			$data = Package::join('package_category', 'package.package_category_id', '=', 'package_category.uuid')
							->select('package.*')
							->where('package_category.no_order', 6)
							->whereNull('package.deleted_at')
							->orderBy('package.name', 'ASC')->get();

			return datatables()->of($data)
				->addColumn('id', static function ($row) {
					return $row->uuid;
				})
				->rawColumns(['id'])
				->make(true);
		}

		return view('package.report.index');
	}

	public function create()
	{
		return view('package.report.create');
	}

	public function store(Request $request)
	{
		$packageCategory = PackageCategory::where('no_order', 6)->first();
		
		//validasi data
		$this->validate($request, [
			'package_category_id' => 'string',
			'name' => 'string|max:100',
			'details' => 'string',
			'price' => 'integer',
			'final_price' => 'nullable|integer'
		]);

		$data = Package::create([
			'package_category_id' => $packageCategory->uuid,
			'name' => $request->name,
			'details' => 'Package Report',
			'price' => $request->price,
			'final_price' => $request->price,
			'created_at' => date("Y-m-d H:i:s"),
			'created_by' => session('sess_user')->name
		]);

		self::success('Package has been added.');
		
		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function destroy($id)
	{
		$data = Package::where('uuid', $id)->first();
		$data->update([
			'deleted_by' => session('sess_user')->name
		]);
		$data->delete();

		self::danger('Package has been deleted.');
		return redirect()->back();
	}

   public function edit($id)
	{
		$data = Package::where('uuid', $id)->first();
		return view('package.report.edit', compact('data'));
	}

	public function update(Request $request, $id)
	{
		
		$this->validate($request, [
			'package_category_id' => 'string',
			'name' => 'string|max:100',
			'details' => 'string',
			'price' => 'integer',
			'final_price' => 'nullable|integer'
		]);

		$data = Package::findOrFail($id);
	
		$data->update([
			'package_category_id' => $data->package_category_id,
			'name' => $request->name,
			'details' => 'Package Report',
			'price' => $request->price,
			'final_price' => $request->price,
			'updated_at' => date("Y-m-d H:i:s"),
			'updated_by' => session('sess_user')->name
		]);

		self::success('Package has been updated.');
		
		return response()->json([
			'data' => $data,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully update data'
		]);
	}
}
