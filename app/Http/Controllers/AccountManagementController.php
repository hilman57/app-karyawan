<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;

class AccountManagementController extends Controller
{

	public function index()
	{
		return view('maintenance');
	}

	public function create()
	{
		return view('package.helpdesk.create');
	}

	public function store(Request $request)
	{
		//validasi data
		$this->validate($request, [
			'name' => 'string|max:100|unique:menu',
			'icon' => 'nullable|string|max:100',
			'url' => 'nullable|string|max:100',
			'is_parent' => 'integer',
			'no_order' => 'integer|unique:menu'
		]);

		try {

			$menu = Package::create([
				'name' => $request->name,
				'icon' => $request->icon,
				'url' => $request->url,
				'is_parent' => $request->is_parent,
				'no_order' => $request->no_order,
				'created_at' => date("Y-m-d H:i:s"),
				'created_by' => session('sess_user')->name
			]);

			if($request->is_parent == 2){
				SubPackage::create([
					'menu_id' => $menu->uuid,
					'status' => 0,
					'no_order' => 0,
					'created_at' => date("Y-m-d H:i:s"),
					'created_by' => session('sess_user')->name
				]);
			}

			self::success('Menu has been added.');
			return redirect(route('menu.index'));
		} catch (\Exception $e) {
			self::danger('Adding menu has been failed.');
			return redirect()->back();
		}
	}

	public function destroy($id)
	{
		$menu = Package::where('uuid', $id)->first();
		$submenu = SubPackage::where('menu_id', $menu->id)->delete();
		$permission = Permission::where('menu_name', str_replace(' ', '', strtolower($menu->name)))->delete();
		$menu->update([
			'deleted_by' => session('sess_user')->name
		]);
		$menu->delete();

		self::danger('Menu has been deleted.');
		return redirect()->back();
	}

   public function edit($id)
	{
		$menu = Package::where('uuid', $id)->first();
		return view('package.helpdesk.edit', compact('menu'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'string|max:100',
			'icon' => 'nullable|string|max:100',
			'url' => 'nullable|string|max:100',
			'is_parent' => 'integer',
			'no_order' => 'integer',
		]);

		try {
			$menu = Package::findOrFail($id);
		
			$menu->update([
				'name' => $request->name,
				'icon' => $request->icon,
				'url' => $request->url,
				'is_parent' => $request->is_parent,
				'no_order' => $request->no_order,
				'updated_at' => date("Y-m-d H:i:s"),
				'updated_by' => session('sess_user')->name
			]);

			self::success('Menu has been updated.');
			return redirect(route('menu.index'));
		} catch (\Exception $e) {
			self::danger('Data menu failed to update.');
			return redirect()->back();
		}
	}
}
