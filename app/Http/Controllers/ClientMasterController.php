<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ClientMaster;
use Illuminate\Support\Facades\DB;

class ClientMasterController extends Controller
{

  public function index()
  {
    return view('client.index');
  }

  public function getData()
  {
    $data = DB::table('klien_master')->get();
    return datatables()->of($data)
      ->addColumn('id', static function ($row) {
        return $row->id;
      })
      ->rawColumns(['id'])
      ->make(true);
  }

  public function save(Request $request)
  {
    $save = ClientMaster::create([
			'client'            => $request->client,			
			'clientname'        => $request->clientname,			
			'no_kontrak'        => $request->no_kontrak,			
			'validfrom'         => $request->validfrom,			
			'validuntil'        => $request->validuntil,			
			'flag'              => 0,			
			'last_number_pkwt'  => $request->last_number_pkwt,			
		]);		

		return response()->json([
			'data' => $save,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
  }

  public function edit($id)
  {
    $detail = DB::table('klien_master')->where('id', $id)->first();
    return view('client.edit', compact('detail'));
  }

  public function update(Request $request, $id)
  {
    $client = ClientMaster::findOrFail($id);
		$client->update([
			'client'            => $request->client,			
			'clientname'        => $request->clientname,			
			'no_kontrak'        => $request->no_kontrak,			
			'validfrom'         => $request->validfrom,			
			'validuntil'        => $request->validuntil,			
			'flag'              => 0,			
			'last_number_pkwt'  => $request->last_number_pkwt,	
		]);
    self::success('Update has been saved.');
    return redirect('/client-master');
  }

  public function delete($id)
  {
    $delete = ClientMaster::where('id', $id)->delete();
		return redirect()->back();
  }
}
