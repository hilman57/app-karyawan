<?php

namespace App\Http\Controllers;

use App\Models\cleansing;
use Illuminate\Http\Request;

class CleansingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cleansing.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cleansing  $cleansing
     * @return \Illuminate\Http\Response
     */
    public function destroy(cleansing $cleansing)
    {
        return response()->json([
            // 'data' =>  $data,
            'success' => 1,
            'message' => 'Test',
        ], 200);
    }
}
