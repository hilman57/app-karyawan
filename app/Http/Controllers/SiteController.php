<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller
{

	public function index()
	{
		if (request()->ajax()) {
			$data = Site::orderBy('id', 'ASC')->get();
			return datatables()->of($data)->make(true);
		}

		return view('site.index');
	}

	public function save(Request $request)
	{
		$menu = Site::create([
			'site_name' => $request->site_name,
			// 'contract' => ($request->contract) ? $request->contract : NULL,
			'valid_from' => $request->valid_from,
			'valid_until' => $request->valid_until,
			'site_address' => $request->site_address,
			'last_number_pkwt' => $request->last_number_pkwt,
		]);

		return response()->json([
			'data' => $menu,
			'success' => true,
			'alert' => 'success',
			'message' => 'Successfully add data'
		]);
	}

	public function delete(Request $request)
	{
		$menu = Site::where('id', $request->id)->delete();
		return response()->json([
			'data' => $menu,
			'success' => true,
			'alert' => 'danger',
			'message' => 'Successfully delete data'
		]);
	}

	public function edit($id)
	{
		$detail = DB::table('site')->where('id', $id)->first();
		return view('site.edit', compact('detail'));
	}

	public function update(Request $request, $id)
	{
		$site = Site::findOrFail($id);
		$site->update([
			'site_name' => $request->site_name,
			// 'contract' => ($request->contract) ? $request->contract : NULL,
			'valid_from' => $request->valid_from,
			'valid_until' => $request->valid_until,
			'site_address' => $request->site_address,
			'last_number_pkwt' => $request->last_number_pkwt,
			'flag' => $request->flag,
		]);
		self::success('Update has been saved.');
		return view('site.index');
	}



	public function editjs(Request $request)
	{
		$menu = Site::where('id', $request->id)->first();
		return view('site.index', compact('menu'));
	}
}
