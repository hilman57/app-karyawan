<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InterviewController extends Controller
{
  public function index($id)
  {
    $question = $this->getQuestion();
    $result_interview = $this->getResultInterview($id);
    $status = (count($result_interview) > 0) ? $result_interview[0]->status : NULL;
    $detail_result_interview = $this->getDetailResultInterview($id);
    return view('interview.index', compact('id', 'question', 'result_interview', 'status', 'detail_result_interview'));
  }

  public function store(Request $request, $id)
  {
    // dd($request);
    $result_interview = $this->getResultInterview($id);
    $question = $this->getQuestion();
    // tentukan interview by siapa
    if(count($result_interview) == 0) $interview_by = 'HRD';
    else if(count($result_interview) == 1) $interview_by = 'SPV';
    else $interview_by = 'USER';
    // array data result interview
    $data_result_interview['user_id'] = $id;
    $data_result_interview['interview_by'] = $request->interview_by;
    // insert data result interview
    DB::table('result_interview')->insert($data_result_interview);
    $id_result_interview = DB::getPdo()->lastInsertId();
    // id_result_interview
    foreach ($question as $value) {
      $data_detail_result_interview = array(
        'id_result_interview' => $request->interview_by,
        'id_question'         => $value->id,
        'answer'              => $request->{'answer_' . $value->id}
      );
      $this->storeDetail($data_detail_result_interview);
    }
    self::success('Answer has been inserted.');
    return redirect()->back();
  }

  public function storeReject(Request $request, $id)
  {
    $result_interview = $this->getResultInterview($id);
    $question = $this->getQuestion();
    // tentukan interview by siapa
    if(count($result_interview) == 0) $interview_by = 'HRD';
    else if(count($result_interview) == 1) $interview_by = 'SPV';
    else $interview_by = 'USER';
    // array data result interview
    $data_result_interview['user_id'] = $id;
    $data_result_interview['interview_by'] = $interview_by;
    // insert data result interview
    DB::table('result_interview')->insert($data_result_interview);
    $id_result_interview = DB::getPdo()->lastInsertId();
    foreach ($question as $key => $value) {
      $data_detail_result_interview = array(
        'id_result_interview' => $id_result_interview,
        'id_question'         => $value->id,
        'answer'              => $request->answer[$key]
      );
      $this->storeDetail($data_detail_result_interview);
    }
    DB::table('result_interview')->where('user_id', $id)->update(['status' => 4]);
    DB::table('mst_user')->where('user_id', $id)->update(['id_status' => 4]);
    return response()->json([
      'data' => [],
      'success' => true,
      'alert' => 'success',
      'message' => 'Successfully update data'
  ]);
  }

  public function final_interview(Request $request, $id)
  {
    DB::table('result_interview')->where('user_id', $id)->update(['status' => $request->status_interview]);
    DB::table('mst_user')->where('user_id', $id)->update(['id_status' => $request->status_interview]);
    return redirect('/biodata');
  }

  private function getQuestion()
  {
    return DB::table('question_interview')->get();
  }

  private function getResultInterview($user_id)
  {
    return DB::table('result_interview')->where('user_id', $user_id)->get();
  }

  private function getDetailResultInterview($user_id)
  {
    return DB::table('result_interview')
            ->join('detail_result_interview', 'result_interview.id', '=', 'detail_result_interview.id_result_interview')
            ->join('question_interview', 'detail_result_interview.id_question', '=', 'question_interview.id')
            ->where('result_interview.user_id', $user_id)
            ->select('result_interview.interview_by', 'detail_result_interview.answer', 'question_interview.question')
            ->orderByRaw('detail_result_interview.id')
            ->get();
  }

  private function storeDetail($data)
  {
    return DB::table('detail_result_interview')->insert($data);
  }
}
