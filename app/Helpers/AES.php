<?php
namespace App\Helpers;

trait AES {
  static $a = ['', 'Satu ', 'Dua ', 'Tiga ', 'Empat ', 'Lima ', 'Enam ', 'Tujuh ', 'Delapan ', 'Sembilan ', 'Sepuluh ', 'Sebelas ', 'Dua Belas ', 'Tiga Belas ', 'Empat Belas ', 'Lima Belas ', 'Enam Belas ', 'Tujuh Belas ', 'Delapan Belas ', 'Sembilan Belas '];
  static $b = ['', '', 'Dua Puluh', 'tiga Puluh', 'Empat Puluh', 'Lima Puluh', 'Enam Puluh', 'Tujuh Puluh', 'Delapan Puluh', 'Sembilan Puluh'];
  static $c = ['', 'Seratus', 'Dua Ratus', 'tiga Ratus', 'Empat Ratus', 'Lima Ratus', 'Enam Ratus', 'Tujuh Ratus', 'Delapan Ratus', 'Sembilan Ratus'];

  protected static function Number2Word($num) {
    if(strlen($num) == 7)
      return self::satuan($num);
    else if(strlen($num) == 8)
      return self::puluhan($num);
  }

  private static function satuan($num) {
    $concat = '000000000'.$num;
    $substring = substr($concat, -9);
    preg_match_all('/^(\d{2})(\d{1})(\d{3})(\d{3})$/', $substring, $n);
    if (!$n) return;
    $ribu = self::$a[(int)$n[3][0][2]] == 'satu ' ? 'seribu ' : self::$a[(int)$n[3][0][2]].'Ribu';
    $ratus = self::$a[(int)$n[4][0][2]] == 'satu ' ? 'seratus ' : (self::$a[(int)$n[4][0][2]] != '') ? self::$a[(int)$n[4][0][2]].'Ratus' : '';
    $str = '';
    $str .= ($n[2][0] != 0) ? self::$a[(int)$n[2][0]] . 'Juta ' : '';
    $str .= ($n[3][0] != 0) ? (self::$c[(int)$n[3][0][0]] . ' ' . self::$b[(int)$n[3][0][1]] . ' ' . $ribu) : '';
    $str .= ($n[4][0] != 0) ? (self::$c[(int)$n[4][0][0]] . ' ' . self::$b[(int)$n[4][0][1]] . ' ' . $ratus) : '';
    return $str.' Rupiah';
  }

  private static function puluhan($num) {
    $concat = '000000000'.$num;
    $substring = substr($concat, -10);
    preg_match_all('/^(\d{2})(\d{2})(\d{3})(\d{3})$/', $substring, $n);
    if (!$n) return;
    $ribu = self::$a[(int)$n[3][0][2]] == 'satu ' ? 'seribu ' : self::$a[(int)$n[3][0][2]].'Ribu';
    $ratus = self::$a[(int)$n[4][0][2]] == 'satu ' ? 'seratus ' : (self::$a[(int)$n[4][0][2]] != '') ? self::$a[(int)$n[4][0][2]].'Ratus' : '';
    $str = '';
    $str .= ($n[2][0] != 0) ? (self::$b[(int)$n[2][0][0]] . ' ' . self::$a[(int)$n[2][0]]) . 'Juta ' : '';
    $str .= ($n[3][0] != 0) ? (self::$c[(int)$n[3][0][0]] . ' ' . self::$b[(int)$n[3][0][1]] . ' ' . $ribu) : '';
    $str .= ($n[4][0] != 0) ? (self::$c[(int)$n[4][0][0]] . ' ' . self::$b[(int)$n[4][0][1]] . ' ' . $ratus) : '';
    return $str.' Rupiah';
  }

  protected static function RomansNumber() {
    $romans = array(
      '01'    => 'I',
      '02'    => 'II',
      '03'    => 'III',
      '04'    => 'IV',
      '05'    => 'V',
      '06'    => 'VI',
      '07'    => 'VII',
      '08'    => 'VIII',
      '09'    => 'IX',
      '10'    => 'X',
      '11'    => 'XI',
      '12'    => 'XII'
    );
    return $romans;
  }

  protected static function FormatTanggal($date) {
    $explode = explode('-', $date);
    $month = array(
      '01'    => 'Januari',
      '02'    => 'Februari',
      '03'    => 'Maret',
      '04'    => 'April',
      '05'    => 'Mei',
      '06'    => 'Juni',
      '07'    => 'Juli',
      '08'    => 'Agustus',
      '09'    => 'September',
      '10'    => 'Oktober',
      '11'    => 'November',
      '12'    => 'Desember'
    );
    return $explode[2].' '.$month[$explode[1]].' '.$explode[0];
  }

  protected static function FormatDay($day) {
    $days = array(
      'sunday'    => 'Minggu',
      'monday'    => 'Senin',
      'tuesday'   => 'Selasa',
      'wednesday' => 'Rabu',
      'thursday'  => 'Kamis',
      'friday'    => 'Jumat',
      'saturday'  => 'Sabut'
    );
    return $days[strtolower($day)];
  }

  protected static function DiffMonth($from, $until) {
    $date1 = $from;
    $date2 = $until;

    $ts1 = strtotime($date1);
    $ts2 = strtotime($date2);

    $year1 = date('Y', $ts1);
    $year2 = date('Y', $ts2);

    $month1 = date('m', $ts1);
    $month2 = date('m', $ts2);

    return (($year2 - $year1) * 12) + ($month2 - $month1);
  }

  protected static function GenerateSingkatan($string) {
    $explode = explode(' ', $string);
    $result = null;
    if(count($explode) > 1) {
      foreach($explode as $val) {
        $result .= mb_substr($val, 0, 1);
      }
      return $result;
    }
    if(strtolower($string) == 'telesales') $string = 'TSA';
    if(strtolower($string) == 'admin') $string = 'ADM';
    if(strtolower($string) == 'supervisor') $string = 'SPV';
    return $string;
  }
}